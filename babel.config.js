module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        extensions: ['.ios.js', '.android.js', '.js', '.jsx', '.ts', '.tsx', '.json', '.svg'],
        alias: {
          '*': ['./src'],
          '@components': ['./src/components'],
          '@atoms': ['./src/components/atoms'],
          '@molecules': ['./src/components/molecules'],
          '@organisms': ['./src/components/organisms'],
          '@assets': ['./src/assets'],
          '@config': ['./src/configs'],
          '@constantsApp': ['./src/configs/Constants.ts'],
          '@enum': ['./src/configs/Enum.ts'],
          '@models': ['./src/models'],
          '@navigations': ['./src/navigations'],
          '@services': ['./src/services'],
          '@scenes': ['./src/scenes'],
          '@stores': ['./src/stores'],
          '@styles': ['./src/styles'],
          '@utils': ['./src/utils'],
          '@helpers': ['./src/utils/helpers'],
          '@nativeModules': ['./nativeModules']
        }
      }
    ],
    [
      'babel-plugin-inline-import',
      {
        extensions: ['.svg']
      }
    ]
  ]
};
