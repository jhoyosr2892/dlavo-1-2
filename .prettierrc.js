module.exports = {
  noImplicitAny: false,
  semi: true,
  tabWidth: 2,
  printWidth: 140,
  singleQuote: true,
  trailingComma: 'none',
  jsxBracketSameLine: true,
  bracketSpacing: true,
  useTabs: false,
  arrowParens: 'avoid',
  jsxSingleQuote: true,
  endOfLine: "auto"
};
