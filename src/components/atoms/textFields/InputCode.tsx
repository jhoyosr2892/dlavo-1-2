import React, { useRef, useState } from 'react';
import { Keyboard, Pressable, StyleSheet, Text, TextInput, View } from 'react-native';
import SafeAreaView from '../safeAreaViews';

const CODE_LENGTH = 4;
// Props
interface InputCodeProps {
  codeLength: number;
  setValueCode: (text: string) => void;
  onSubmitEditing?: () => void;
}

const InputCode = ({ codeLength = CODE_LENGTH, setValueCode, onSubmitEditing }: InputCodeProps) => {
  const [code, setCode] = useState('');
  const [containerIsFocused, setContainerIsFocused] = useState(false);
  const ref = useRef<TextInput>(null);

  const handleOnPress = () => {
    if (ref && ref.current) {
      Keyboard.dismiss();
      ref.current.focus();
    }
  };

  const handleOnBlur = () => {
    setContainerIsFocused(false);
    if (onSubmitEditing) {
      onSubmitEditing();
    }
  };

  const codeDigitsArray = new Array(codeLength).fill(0);

  const toDigitInput = (_value: number, idx: number) => {
    const emptyInputChar = '-';
    const digit = code[idx] || emptyInputChar;

    const isCurrentDigit = idx === code.length;
    const isLastDigit = idx === CODE_LENGTH - 1;
    const isCodeFull = code.length === CODE_LENGTH;

    const isFocused = isCurrentDigit || (isLastDigit && isCodeFull);

    const containerStyle =
      containerIsFocused && isFocused ? { ...styles.inputContainer, ...styles.inputContainerFocused } : styles.inputContainer;

    return (
      <View key={idx} style={containerStyle}>
        <Text style={styles.inputText}>{digit}</Text>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <Pressable style={styles.inputsContainer} onPress={handleOnPress}>
        {codeDigitsArray.map(toDigitInput)}
      </Pressable>
      <TextInput
        ref={ref}
        value={code}
        onChangeText={text => {
          setCode(text);
          setValueCode(text);
        }}
        autoFocus={true}
        onSubmitEditing={handleOnBlur}
        keyboardType='number-pad'
        returnKeyType='send'
        textContentType='oneTimeCode'
        maxLength={CODE_LENGTH}
        style={styles.hiddenCodeInput}
      />
    </SafeAreaView>
  );
};

export default InputCode;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  inputsContainer: {
    width: '60%',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  hiddenCodeInput: {
    position: 'absolute',
    height: 0,
    width: 0,
    opacity: 0
  },
  inputContainer: {
    borderColor: '#cccccc',
    borderWidth: 2,
    borderRadius: 4,
    padding: 12
  },
  inputText: {
    fontSize: 24,
    width: 20,
    fontFamily: 'Menlo-Regular',
    textAlign: 'center'
  },
  inputContainerFocused: {
    borderColor: 'red',
    borderWidth: 2
  }
});
