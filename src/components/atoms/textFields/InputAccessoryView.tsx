// Libs
import React from 'react';
import { InputAccessoryView as InputAV, StyleProp, ViewStyle, ColorValue, StyleSheet } from 'react-native';

// Atoms
import View from '@atoms/views';
import Button from '@atoms/buttons';
import { dismissKeyboard } from '@atoms/keyboards';

// Constants
import { INPUT_ACCESSORY_ID, NAME_ICON, THEME_DEFAULT } from '@constantsApp';
import { EnumTypeButton, TypeLibraryIconsSystem } from 'configs/Enum';

// Styles
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  containerView: {
    paddingHorizontal: 5,
    flexDirection: 'row',
    flex: 1
  },
  optionDownIn: {
    flex: 1,
    flexDirection: 'row'
  },
  buttonsOptional: {
    flex: 1,
    padding: 0
  }
});

const InputAccessoryView = (props: {
  inputAccessoryId: string;
  style?: StyleProp<ViewStyle>;
  backgroundColor?: ColorValue;
  focusBottom?: () => void;
  focusTop?: () => void;
}) => {
  const { inputAccessoryId, style, backgroundColor, focusBottom, focusTop } = props;
  /**
   * TODO: falta implementar la solucion para el error cuando hay mas de 3
   * inputs
   */
  /**
   * NOTE: Cuando se agregue mas elementos a InputAccessoryView y el
   * Height cambie, por favor actualizar en el KeyboardAvoidingView el
   * keyboardVerticalOffset por el respectivo de este ya que si no queda
   * montado este elemento sobre el view del screen donde se use.
   *  */
  return (
    <InputAV nativeID={inputAccessoryId} style={[style, styles.container]} backgroundColor={backgroundColor || THEME_DEFAULT.transparent}>
      <View style={styles.containerView}>
        {INPUT_ACCESSORY_ID.in === inputAccessoryId && (
          <Button
            onPress={focusTop}
            type={EnumTypeButton.neutral}
            icon={NAME_ICON.keyboardArrowUp}
            libraryIcon={TypeLibraryIconsSystem.materialIcons}
            style={styles.buttonsOptional}
          />
        )}
        {INPUT_ACCESSORY_ID.down === inputAccessoryId && (
          <Button
            onPress={focusBottom}
            type={EnumTypeButton.neutral}
            icon={NAME_ICON.keyboardArrowDown}
            libraryIcon={TypeLibraryIconsSystem.materialIcons}
            style={styles.buttonsOptional}
          />
        )}
        {INPUT_ACCESSORY_ID.inDown === inputAccessoryId && (
          <View style={styles.optionDownIn}>
            <Button
              onPress={focusTop}
              type={EnumTypeButton.neutral}
              icon={NAME_ICON.keyboardArrowUp}
              libraryIcon={TypeLibraryIconsSystem.materialIcons}
              style={styles.buttonsOptional}
            />
            <Button
              onPress={focusBottom}
              type={EnumTypeButton.neutral}
              icon={NAME_ICON.keyboardArrowDown}
              style={styles.buttonsOptional}
              libraryIcon={TypeLibraryIconsSystem.materialIcons}
            />
          </View>
        )}
        <Button onPress={() => dismissKeyboard()} type={EnumTypeButton.neutral} text={'Cerrar teclado'} style={{ flex: 2 }} />
      </View>
    </InputAV>
  );
};

export default InputAccessoryView;
