import { GOOGLE_API_KEY, THEME_DEFAULT } from 'configs/Constants';
import { TypeLibraryIconsSystem } from 'configs/Enum';
import I18n from 'i18n-js';
import { translate } from 'locales';
import React, { useEffect, useRef, useState } from 'react';
import { StyleProp, StyleSheet, TextInputProps, ViewStyle } from 'react-native';
import {
  GooglePlaceData,
  GooglePlaceDetail,
  GooglePlacesAutocomplete,
  GooglePlacesAutocompleteRef
} from 'react-native-google-places-autocomplete';
import { stylesGeneral } from 'styles';
import TouchableO from '../buttons/TouchableOpacity';
import IconCustom from '../icons';
import ViewAtom from '../views';

// Props
interface rightButtonProps {
  show?: boolean;
  nameIcon?: string;
  libraryIcon?: TypeLibraryIconsSystem;
  colorIcon?: string;
}
interface GooglePlacesAutocompleteAtomProps {
  onPress: (data: GooglePlaceData, detail: GooglePlaceDetail | null) => void;
  containerStyle?: StyleProp<ViewStyle>;
  textInputStyle?: StyleProp<ViewStyle>;
  address?: string;
  rightButtonProps?: rightButtonProps;
  textInputProps?: TextInputProps;
}

const GooglePlacesAutocompleteAtom = ({
  onPress,
  containerStyle,
  textInputStyle,
  address,
  rightButtonProps,
  textInputProps
}: GooglePlacesAutocompleteAtomProps) => {
  /* States */
  const [isFocused, setIsFocused] = useState(false);
  const [rightButtonInfo, setRightButtonInfo] = useState<rightButtonProps>({
    show: rightButtonProps?.show !== undefined ? rightButtonProps?.show : true,
    colorIcon: rightButtonProps?.colorIcon,
    libraryIcon: rightButtonProps?.libraryIcon,
    nameIcon: rightButtonProps?.nameIcon
  });
  /* Refs */
  const googleAutocompleteRef = useRef<GooglePlacesAutocompleteRef>(null);

  useEffect(() => {
    if (address) {
      googleAutocompleteRef.current?.setAddressText(address);
    }
  }, []);

  const onChangeText = (data: string) => {
    const copy = { ...rightButtonInfo };
    if (copy && data.length > 0) {
      copy.show = true;
      copy.nameIcon = 'close-circle';
      copy.libraryIcon = TypeLibraryIconsSystem.ionicons;
    } else if (copy && data.length === 0) {
      copy.show = false;
    }
    setRightButtonInfo(copy);
  };

  return (
    <ViewAtom style={[styles.viewContainer, containerStyle]}>
      <GooglePlacesAutocomplete
        ref={googleAutocompleteRef}
        placeholder={translate('GOOGLE_PLACE_AUTOCOMPLETE_ATOM.PLACEHOLDER')}
        onPress={(data, details = null) => onPress(data, details)}
        query={{
          key: GOOGLE_API_KEY,
          language: I18n.currentLocale()
        }}
        styles={{
          textInput: [styles.searchInput, isFocused && styles.textInputFocused, textInputStyle],
          listView: [stylesGeneral.shadowAndElevation]
        }}
        fetchDetails={true}
        textInputProps={{
          onFocus: () => setIsFocused(true),
          onBlur: () => setIsFocused(false),
          autoFocus: true,
          onChangeText: onChangeText,
          ...textInputProps
        }}
        enableHighAccuracyLocation={true}
        renderRightButton={() =>
          rightButtonInfo?.show !== false ? (
            <ViewAtom style={styles.cleanContainer}>
              <TouchableO
                onPress={() => {
                  googleAutocompleteRef.current?.clear();
                  googleAutocompleteRef.current?.focus();
                }}>
                <IconCustom
                  name={rightButtonInfo?.nameIcon ?? 'close-circle'}
                  color={rightButtonInfo?.colorIcon ?? THEME_DEFAULT.primary}
                  library={rightButtonInfo?.libraryIcon ?? TypeLibraryIconsSystem.ionicons}
                />
              </TouchableO>
            </ViewAtom>
          ) : (
            <ViewAtom />
          )
        }
      />
    </ViewAtom>
  );
};

export default GooglePlacesAutocompleteAtom;

const styles = StyleSheet.create({
  viewContainer: {
    zIndex: 1,
    flex: 1
  },
  searchInput: {
    height: 50,
    borderWidth: 1,
    borderColor: THEME_DEFAULT.borderLine,
    marginTop: 25,
    flex: 1,
    paddingRight: 45,
    color: THEME_DEFAULT.black
  },
  textInputFocused: {
    borderColor: THEME_DEFAULT.borderLineFocused
  },
  cleanContainer: {
    alignSelf: 'flex-end',
    paddingRight: 7,
    position: 'absolute',
    right: 0,
    bottom: 0,
    top: 0,
    height: 100,
    justifyContent: 'center'
  }
});
