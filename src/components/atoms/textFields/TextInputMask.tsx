// Libs
import React, { useEffect, useState } from 'react';
import { KeyboardType, ReturnKeyTypeOptions, StyleProp, StyleSheet, TextInput, TextStyle, View } from 'react-native';
import MaskInput, { Mask } from 'react-native-mask-input';

// Atoms
import TouchableO from '@atoms/buttons/TouchableOpacity';
import Icon from '@atoms/icons';
import InputAccessoryView from '@atoms/textFields/InputAccessoryView';

// Helpers
import { validateTypeText } from '@helpers/Validate';
// import { CLog } from '@helpers/message';

// Constants
import { EnumFont, INPUT_ACCESSORY_ID, IS_ANDROID, NAME_ICON, SIZE_ICONS, THEME_DEFAULT } from '@constantsApp';
import { TypeLibraryIconsSystem, TypeTextContent } from 'configs/Enum';

const URL_PATH = 'src/components/atoms/textField/TextInputMask.tsx';

// Props
export interface TextFieldProps {
  placeholder?: string;
  style?: StyleProp<TextStyle>;
  containerStyle?: StyleProp<TextStyle>;
  textContentType?:
    | 'none'
    | 'URL'
    | 'addressCity'
    | 'addressCityAndState'
    | 'addressState'
    | 'countryName'
    | 'creditCardNumber'
    | 'emailAddress'
    | 'familyName'
    | 'fullStreetAddress'
    | 'givenName'
    | 'jobTitle'
    | 'location'
    | 'middleName'
    | 'name'
    | 'namePrefix'
    | 'nameSuffix'
    | 'nickname'
    | 'organizationName'
    | 'postalCode'
    | 'streetAddressLine1'
    | 'streetAddressLine2'
    | 'sublocality'
    | 'telephoneNumber'
    | 'username'
    | 'password'
    | 'newPassword'
    | 'oneTimeCode';
  editable?: boolean;
  onChange?: (formatted: string, extracted?: string | undefined) => void;
  setValue?: (value?: any) => void;
  secureTextEntry?: boolean;
  refInput?: React.RefObject<TextInput>;
  onSubmitEditing?: (e: any) => void;
  returnKeyType?: ReturnKeyTypeOptions;
  inputAccessoryId?: string;
  focusBottom?: () => void;
  focusTop?: () => void;
  autoFocusTF?: boolean;
  value?: string;
  keyboardType?: KeyboardType;
  onReady?: () => void;
  multiline?: boolean;
  numberOfLines?: number;
  maxLength?: number;
  autoCapitalize?: 'none' | 'sentences' | 'words' | 'characters';
  blurOnSubmit?: boolean;
  rightButtonProps?: {
    show?: boolean;
    nameIcon?: string;
    libraryIcon?: TypeLibraryIconsSystem;
    colorIcon?: string;
    type?: 'edit' | 'clean' | 'clean-only';
  };
  mask?: Mask;
}

const TextInputMaskAtom = ({
  placeholder,
  style,
  containerStyle,
  textContentType = TypeTextContent.none,
  editable,
  onChange,
  setValue,
  secureTextEntry = false,
  refInput,
  onSubmitEditing,
  returnKeyType,
  inputAccessoryId = INPUT_ACCESSORY_ID.close,
  focusBottom,
  focusTop,
  autoFocusTF = false,
  value,
  keyboardType,
  onReady,
  multiline,
  numberOfLines,
  maxLength,
  autoCapitalize,
  blurOnSubmit = true,
  rightButtonProps,
  mask
}: TextFieldProps) => {
  const [isFocused, setIsFocused] = useState(false);
  const [rightButtonInfo, setRightButtonInfo] = useState(rightButtonProps);
  const [show, setShow] = useState(true);

  useEffect(() => {
    if (onReady) {
      onReady();
    }
  }, []);

  const onChangeText = (formatted: string, extracted: string = '') => {
    if (rightButtonProps && extracted.length > 0) {
      rightButtonProps.nameIcon = 'close-circle';
      rightButtonProps.libraryIcon = TypeLibraryIconsSystem.ionicons;
      rightButtonProps.type = 'clean-only';
    }
    setRightButtonInfo(rightButtonProps);
    if (onChange) {
      onChange(formatted, extracted);
    }
  };

  const onSecureTextEntry = () => {
    // CLog(URL_PATH, 'onSecureTextEntry()', 'data, show', show);
    setShow(!show);
  };

  const showSecureTextEntry = (): boolean => {
    if (secureTextEntry && (textContentType === TypeTextContent.newPassword || textContentType === TypeTextContent.password)) {
      return show;
    }
    return false;
  };

  const onRightButtonPress = () => {
    switch (rightButtonInfo?.type) {
      case 'edit':
        refInput?.current?.focus();
        break;
      case 'clean':
        if (setValue) {
          setValue('');
        }
        // refInput?.current?.clear();
        refInput?.current?.focus();
        setRightButtonInfo(rightButtonProps);
        break;
      case 'clean-only':
        if (setValue) {
          setValue('');
        }
        // refInput?.current?.clear();
        setRightButtonInfo(rightButtonProps);
        break;
      default:
        refInput?.current?.focus();
        break;
    }
  };

  const onBlur = () => {
    setIsFocused(false);
    if (rightButtonProps) {
      setRightButtonInfo(rightButtonProps);
    }
  };
  const onFocus = () => {
    setIsFocused(true);
    const copy = { ...rightButtonInfo };
    if (value && value.length > 0) {
      copy.nameIcon = 'close-circle';
      copy.libraryIcon = TypeLibraryIconsSystem.ionicons;
      copy.type = 'clean-only';
    }
    setRightButtonInfo(copy);
  };

  return (
    <View style={[styles.container, isFocused && editable ? styles.textInputFocused : {}, containerStyle]}>
      {/* <TextInputMask
        ref={refInput}
        value={value}
        // eslint-disable-next-line jsx-a11y/no-autofocus
        autoFocus={autoFocusTF}
        style={[styles.textInput, validateTypeText(EnumFont.quinary), style]}
        placeholder={placeholder}
        onChangeText={(formatted: string, extracted?: string) => extracted && onChangeText(formatted, extracted)}
        secureTextEntry={showSecureTextEntry()}
        onFocus={onFocus}
        onBlur={onBlur}
        onSubmitEditing={onSubmitEditing}
        returnKeyType={returnKeyType}
        inputAccessoryViewID={inputAccessoryId}
        editable={editable}
        textContentType={textContentType}
        keyboardType={keyboardType}
        caretHidden={false}
        {...{ multiline, numberOfLines, maxLength, autoCapitalize, blurOnSubmit, mask }}
      /> */}
      <MaskInput
        ref={refInput}
        value={value}
        // eslint-disable-next-line jsx-a11y/no-autofocus
        autoFocus={autoFocusTF}
        style={[styles.textInput, validateTypeText(EnumFont.quinary), style]}
        placeholder={placeholder}
        onChangeText={(formatted: string, extracted?: string) => onChangeText(formatted, extracted)}
        secureTextEntry={showSecureTextEntry()}
        onFocus={onFocus}
        onBlur={onBlur}
        onSubmitEditing={onSubmitEditing}
        returnKeyType={returnKeyType}
        inputAccessoryViewID={inputAccessoryId}
        editable={editable}
        textContentType={textContentType}
        keyboardType={keyboardType}
        caretHidden={false}
        multiline={multiline}
        numberOfLines={numberOfLines}
        maxLength={maxLength}
        autoCapitalize={autoCapitalize}
        blurOnSubmit={blurOnSubmit}
        mask={mask}
      />
      {!multiline &&
        rightButtonInfo?.show &&
        textContentType !== TypeTextContent.newPassword &&
        textContentType !== TypeTextContent.password && (
          <TouchableO onPress={onRightButtonPress} style={styles.buttonOption}>
            <Icon
              name={rightButtonInfo?.nameIcon ?? 'edit'}
              color={rightButtonInfo?.colorIcon ?? THEME_DEFAULT.primary}
              library={rightButtonInfo?.libraryIcon ?? TypeLibraryIconsSystem.materialIcons}
            />
          </TouchableO>
        )}
      {(textContentType === TypeTextContent.newPassword || textContentType === TypeTextContent.password) && (
        <TouchableO onPress={onSecureTextEntry} style={styles.buttonOption}>
          <Icon
            name={NAME_ICON.eye}
            size={SIZE_ICONS.inputOption}
            color={showSecureTextEntry() ? THEME_DEFAULT.borderLine : THEME_DEFAULT.borderLineFocused}
          />
        </TouchableO>
      )}
      {!IS_ANDROID && <InputAccessoryView inputAccessoryId={inputAccessoryId} focusBottom={focusBottom} focusTop={focusTop} />}
    </View>
  );
};

export default TextInputMaskAtom;

// Styles
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: THEME_DEFAULT.borderLine,
    paddingHorizontal: 10,
    borderRadius: 5
  },
  textInput: {
    flex: 1,
    height: 50,
    color: THEME_DEFAULT.secondaryFont
  },
  textInputFocused: {
    borderWidth: 1,
    borderColor: THEME_DEFAULT.primary
  },
  buttonOption: {
    height: 30,
    width: 30,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center'
  },
  rightIconContent: {
    alignSelf: 'flex-end',
    paddingRight: 7,
    position: 'absolute',
    right: 0,
    bottom: 0,
    top: 0,
    // height: 100,
    justifyContent: 'center'
  }
});
