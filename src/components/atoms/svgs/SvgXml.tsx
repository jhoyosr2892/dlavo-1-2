// Libs
import React from 'react';
import { SvgXml as SX, Color } from 'react-native-svg';

// Constants
import { THEME_DEFAULT } from '@constantsApp';

// Props
type SvgProps = {
  height?: string | number;
  width?: string | number;
  xml: string | null;
  color?: Color;
};

const SvgXml = ({ height, width, xml, color = THEME_DEFAULT.secondary }: SvgProps) => {
  return <SX {...{ width, height, xml, color }} />;
};

export default SvgXml;
