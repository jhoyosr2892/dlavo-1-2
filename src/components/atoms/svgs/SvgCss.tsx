// Libs
import { THEME_DEFAULT } from 'configs/Constants';
import React from 'react';
import { GestureResponderEvent, StyleProp, ViewStyle } from 'react-native';
import { SvgCss as SC, Color } from 'react-native-svg';

// Props
type SvgCssProps = {
  xml: string;
  width?: string | number;
  height?: string | number;
  onPress?: (event: GestureResponderEvent) => void;
  style?: StyleProp<ViewStyle>;
  fill?: Color;
};

const SvgCss = ({ style, onPress, xml, width = '100%', height = '100%', fill = THEME_DEFAULT.white }: SvgCssProps) => {
  return <SC fill={fill} {...{ onPress, xml, width, height, style }} />;
};

export default SvgCss;
