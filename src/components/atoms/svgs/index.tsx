// Libs
import React, { ReactNode } from 'react';
import Svg, { Line as Li, LineProps, Circle as Ci, CircleProps } from 'react-native-svg';

// Props
type SvgProps = {
  height?: string;
  width?: string;
  children?: ReactNode;
  viewBox?: string;
};

const SvgCustom = ({ height, width, viewBox, children }: SvgProps) => {
  return <Svg {...{ width, height, viewBox }}>{children}</Svg>;
};

export const Line = ({ x1, x2, y1, y2, opacity, ...props }: LineProps) => {
  return <Li {...{ x1, x2, y1, y2, opacity, ...props }} />;
};

export const Circle = ({ cx, cy, opacity, r, ...props }: CircleProps) => {
  return <Ci {...{ cx, cy, opacity, r, ...props }} />;
};

export default SvgCustom;
