// Libs
import React, { useEffect, useState, useReducer } from 'react';
import { StyleSheet, ViewStyle, TextStyle } from 'react-native';
import { Animation, CustomAnimation } from 'react-native-animatable';

// Atoms
import Text from '@atoms/texts';
import ViewAnimated from '@atoms/views/ViewAnimated';
import TouchableO from '@atoms/buttons/TouchableOpacity';

// Helpers
//import { CLog } from '@helpers/Message';

// Constants
import { EnumFont, TIMER_ANIMATION, THEME_DEFAULT, EnumToast } from '@constantsApp';

//const URL_PATH = 'src/components/atoms/toasts/index.tsx';

// Styles
const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 20,
    left: 10,
    right: 10,
    borderRadius: 5,
    paddingHorizontal: 10,
    paddingVertical: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    // android
    elevation: 2,
    // ios
    shadowColor: THEME_DEFAULT.black,
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.8,
    shadowRadius: 2
  },
  textDescription: {
    flex: 1
  },
  button: {
    marginLeft: 5
  }
});

// Props Toast
interface ToastProps {
  text?: string | null;
  textButton?: string | null;
  animationIn?: Animation | string | CustomAnimation;
  animationOut?: Animation | string | CustomAnimation;
  dismountToast: () => void;
  onPressButton?: () => void;
  duration?: number;
  type?: EnumToast;
}

type StateStyleType = {
  containerStyle: ViewStyle;
  textStyle: TextStyle;
};

enum ActionStateStyle {
  container,
  text,
  all
}

const defaultTimeOut: ReturnType<typeof setTimeout> = setTimeout(() => {}, 0);
const defaultAnimated: Animation | string | CustomAnimation = 'slideInUp';
const defaultAnimatedOut: Animation | string | CustomAnimation = 'slideOutDown';
const stateStyleDefault: StateStyleType = { containerStyle: {}, textStyle: {} };

const Toast = ({
  text,
  textButton,
  animationIn = defaultAnimated,
  animationOut = defaultAnimatedOut,
  dismountToast,
  onPressButton,
  duration = TIMER_ANIMATION.toast,
  type
}: ToastProps) => {
  const [animate, setAnimate] = useState(defaultAnimated);
  const [timeoutIn, setTimeoutIn] = useState(defaultTimeOut);
  const [timeoutOut, setTimeoutOut] = useState(defaultTimeOut);

  const [stateStyle, dispachStyle] = useReducer(
    (prevState: StateStyleType, action: { type: ActionStateStyle; value: StateStyleType }): StateStyleType => {
      switch (action.type) {
        case ActionStateStyle.container:
          return {
            ...prevState,
            containerStyle: action.value.containerStyle
          };
        case ActionStateStyle.text:
          return {
            ...prevState,
            textStyle: action.value.textStyle
          };
        case ActionStateStyle.all:
          return {
            textStyle: action.value.textStyle,
            containerStyle: action.value.containerStyle
          };
      }
    },
    stateStyleDefault
  );

  const setTimeoutOutToast = () => {
    if (timeoutIn) {
      clearTimeout(timeoutIn);
    }
    if (timeoutOut) {
      clearTimeout(timeoutOut);
    }
    const timeoutOutSet: ReturnType<typeof setTimeout> = setTimeout(() => {
      dismountToast();
    }, TIMER_ANIMATION.animatesGeneral * 1.5);
    setTimeoutOut(timeoutOutSet);
  };

  const setTimeoutOutOkToast = () => {
    if (onPressButton) {
      onPressButton();
    }
    setAnimate(animationOut);
    setTimeoutOutToast();
  };

  useEffect(() => {
    // tipo de toast
    const styleToast = () => {
      let containerS: ViewStyle = {};
      let textS: TextStyle = {};
      switch (type) {
        case EnumToast.success:
          containerS = { backgroundColor: THEME_DEFAULT.primary };
          textS = { color: THEME_DEFAULT.primaryFont };
          break;
        case EnumToast.warn:
          containerS = { backgroundColor: THEME_DEFAULT.warn };
          textS = { color: THEME_DEFAULT.primaryFont };
          break;
        case EnumToast.error:
          containerS = { backgroundColor: THEME_DEFAULT.error };
          textS = { color: THEME_DEFAULT.primaryFont };
          break;
        default:
          containerS = { backgroundColor: THEME_DEFAULT.primary };
          textS = { color: THEME_DEFAULT.primaryFont };
          break;
      }
      dispachStyle({ type: ActionStateStyle.all, value: { containerStyle: containerS, textStyle: textS } });
    };
    styleToast();

    // animación de entrada
    const setTimeoutInToast = () => {
      setAnimate(animationIn);
      const timeoutInSet: NodeJS.Timeout = setTimeout(() => {
        setAnimate(animationOut);
        setTimeoutOutToast();
      }, duration);
      setTimeoutIn(timeoutInSet);
    };
    setTimeoutInToast();

    return () => {
      if (timeoutIn) {
        clearTimeout(timeoutIn);
      }
      if (timeoutOut) {
        clearTimeout(timeoutOut);
      }
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ViewAnimated style={[styles.container, stateStyle.containerStyle]} animation={animate} duration={TIMER_ANIMATION.animatesGeneral}>
      <Text style={[styles.textDescription, stateStyle.textStyle]} type={EnumFont.octonary}>
        {text || 'Text'}
      </Text>
      <TouchableO onPress={setTimeoutOutOkToast} style={styles.button}>
        <Text type={EnumFont.octonary} style={stateStyle.textStyle}>
          {textButton || 'OK'}
        </Text>
      </TouchableO>
    </ViewAnimated>
  );
};

export default Toast;
