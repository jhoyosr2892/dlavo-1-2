// libs
import React from 'react';
import { StyleProp, TextStyle } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FoundationIcons from 'react-native-vector-icons/Foundation';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';
import FeatherIcons from 'react-native-vector-icons/Feather';

// constants
import { SIZE_ICONS, THEME_DEFAULT } from '@constantsApp';
import { TypeLibraryIconsSystem } from '@enum';

// Props
type IconCustomProps = {
  name?: string | null;
  size?: number | null;
  color?: string | null;
  library?: TypeLibraryIconsSystem | null;
  style?: StyleProp<TextStyle>;
  onPress?: () => void;
};

const IconCustom = ({
  name = 'info',
  size = SIZE_ICONS.general,
  color = THEME_DEFAULT.white,
  library,
  style,
  onPress
}: IconCustomProps) => {
  switch (library) {
    case TypeLibraryIconsSystem.fontAwesome:
      return <FontAwesomeIcons {...{ onPress, name, size, color, style }} />;
    case TypeLibraryIconsSystem.materialCommunityIcons:
      return <MaterialCommunityIcons {...{ onPress, name, size, color, style }} />;
    case TypeLibraryIconsSystem.materialIcons:
      return <MaterialIcons {...{ onPress, name, size, color, style }} />;
    case TypeLibraryIconsSystem.simpleLineIcons:
      return <SimpleLineIcons {...{ onPress, name, size, color, style }} />;
    case TypeLibraryIconsSystem.foundation:
      return <FoundationIcons {...{ onPress, name, size, color, style }} />;
    case TypeLibraryIconsSystem.feather:
      return <FeatherIcons {...{ onPress, name, size, color, style }} />;
    default:
      return <Ionicons {...{ onPress, name, size, color, style }} />;
  }
};

IconCustom.displayName = 'IconCustom';
export default IconCustom;
