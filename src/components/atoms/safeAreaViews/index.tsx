// Libs
import React, { ReactNode } from 'react';
import { SafeAreaView as SAV, StyleProp, ViewStyle } from 'react-native';

// Props
type SafeAreaViewProps = {
  children?: ReactNode;
  style?: StyleProp<ViewStyle>;
};

const SafeAreaView = ({ children = null, style, ...props }: SafeAreaViewProps) => {
  return (
    <SAV style={style} {...props}>
      {children}
    </SAV>
  );
};

export default SafeAreaView;
