// Libs
import React from 'react';
import LottieView from 'lottie-react-native';
import { StyleProp, ViewStyle, Animated } from 'react-native';

// Helpers
import { validateSourceLottie } from '@helpers/Validate';

// Constants
import { AnimationObject } from '@constantsApp';

// Props
type LottieViewCustomProps = {
  source: string | AnimationObject | { uri: string };
  autoPlay?: boolean;
  loop?: boolean;
  style?: StyleProp<ViewStyle>;
  duration?: number;
  speed?: number;
  progress?: number | Animated.Value | Animated.AnimatedInterpolation;
};

const LottieViewCustom = ({ source, autoPlay = true, loop = true, style, duration, speed, progress }: LottieViewCustomProps) => {
  return (
    <LottieView
      source={validateSourceLottie(source)}
      autoPlay={autoPlay}
      loop={loop}
      style={style}
      duration={duration}
      speed={speed}
      progress={progress}
    />
  );
};

export default LottieViewCustom;
