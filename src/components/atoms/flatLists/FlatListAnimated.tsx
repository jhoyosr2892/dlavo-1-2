// Libs
import React from 'react';
import { Animated, FlatList, StyleProp, ViewStyle, ListRenderItem, NativeSyntheticEvent, NativeScrollEvent } from 'react-native';

// Constants
const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

// Props
type FlatListAnimatedProps = {
  renderItem: ListRenderItem<any> | null | undefined;
  style?: StyleProp<ViewStyle>;
  data: ReadonlyArray<any> | null | undefined;
  keyExtractor?: (item: any, index: number) => string;
  horizontal?: boolean;
  bounces?: boolean;
  scrollEventThrottle?: number;
  onScroll?: (event: NativeSyntheticEvent<NativeScrollEvent>) => void;
};

const FlatListAnimated = ({
  data,
  keyExtractor,
  renderItem,
  style,
  horizontal,
  bounces,
  scrollEventThrottle,
  onScroll
}: FlatListAnimatedProps) => {
  return (
    <AnimatedFlatList
      data={data}
      style={style}
      renderItem={renderItem}
      keyExtractor={keyExtractor}
      horizontal={horizontal}
      bounces={bounces}
      scrollEventThrottle={scrollEventThrottle}
      onScroll={onScroll}
    />
  );
};

export default FlatListAnimated;
