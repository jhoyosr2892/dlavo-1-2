// Libs
import React from 'react';
import {
  FlatList as FL,
  ListRenderItem,
  StyleSheet,
  StyleProp,
  ViewStyle,
  ViewToken,
  ViewabilityConfig,
  RefreshControl
} from 'react-native';

// Atoms
import NoResultView from '@atoms/views/NotResult';

// Constants
import { METRICS, THEME_DEFAULT } from '@constantsApp';

// Props
type FlatListProps = {
  data: ReadonlyArray<any> | null | undefined;
  renderItem: ListRenderItem<any> | null | undefined;
  keyExtractor?: (item: any, index: number) => string;
  extraData?: any;
  contentContainerStyle?: StyleProp<ViewStyle>;
  refreshing?: boolean;
  onRefresh?: () => void;
  showsVerticalScrollIndicator?: boolean;
  showsHorizontalScrollIndicator?: boolean;
  style?: StyleProp<ViewStyle>;
  viewabilityConfig?: ViewabilityConfig;
  /**
   * Called when the viewability of rows changes, as defined by the `viewablePercentThreshold` prop.
   */
  onViewableItemsChanged?: ((info: { viewableItems: Array<ViewToken>; changed: Array<ViewToken> }) => void) | null;
};

const styles = StyleSheet.create({
  content: {
    padding: METRICS.CONSTANTS_PADDING
  }
});

const FlatList = ({
  data,
  renderItem,
  keyExtractor,
  extraData,
  contentContainerStyle,
  refreshing,
  onRefresh,
  showsHorizontalScrollIndicator,
  showsVerticalScrollIndicator,
  style,
  viewabilityConfig,
  onViewableItemsChanged
}: FlatListProps) => {
  return (
    <FL
      refreshing={refreshing}
      showsVerticalScrollIndicator={showsVerticalScrollIndicator}
      showsHorizontalScrollIndicator={showsHorizontalScrollIndicator}
      refreshControl={
        onRefresh ? <RefreshControl colors={[THEME_DEFAULT.primary]} refreshing={refreshing || false} onRefresh={onRefresh} /> : undefined
      }
      ListEmptyComponent={NoResultView}
      contentContainerStyle={[styles.content, contentContainerStyle]}
      {...{ data, renderItem, keyExtractor, extraData, style, viewabilityConfig, onViewableItemsChanged }}
    />
  );
};

export default FlatList;
