// Libs
import React, { ReactNode } from 'react';
import { Modal, StyleSheet, StyleProp, ViewStyle } from 'react-native';

// Atoms
import Button from '@atoms/buttons';
import ViewAnimated from '@atoms/views/ViewAnimated';

// Helpers
//import { CLog } from '@helpers/Message';

// Constants
import { METRICS, THEME_DEFAULT, AnimatedString, NAME_ICON } from '@constantsApp';

//const URL_PATH = 'src/components/atoms/modal/ModalCustom.tsx';

// Props
type ModalCustomProps = {
  children: ReactNode;
  isVisible: boolean;
  onClose: (value: boolean) => void;
  isClose?: boolean;
  /** si se aplica el estilo por defecto del content **/
  isContentStyle?: boolean;
  stylesContent?: StyleProp<ViewStyle>;
  /** The statusBarTranslucent prop determines whether your modal should go
   * under the system statusbar. **/
  statusBarTranslucent?: boolean;
  /**
   * The `presentationStyle` determines the style of modal to show
   */
  presentationStyle?: 'fullScreen' | 'pageSheet' | 'formSheet' | 'overFullScreen';
  onShow?: () => void;
};

// Styles
const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: THEME_DEFAULT.backgroundTransparent
  },
  content: {
    width: METRICS.DEVICE_WIDTH - METRICS.CONSTANTS_MARGIN * 2,
    backgroundColor: THEME_DEFAULT.content,
    borderRadius: METRICS.BORDER_RADIUS,
    padding: METRICS.CONSTANTS_PADDING
  }
});

const ModalCustom = ({
  children = undefined,
  isVisible = false,
  onClose,
  isClose = false,
  isContentStyle = true,
  stylesContent,
  presentationStyle,
  statusBarTranslucent,
  onShow
}: ModalCustomProps) => {
  const closeModal = () => {
    //CLog(URL_PATH, 'closeModal', 'close', 'close');
    onClose(false);
  };

  return (
    <Modal
      animationType='slide'
      transparent={true}
      visible={isVisible}
      onRequestClose={closeModal}
      onDismiss={!isClose ? closeModal : undefined}
      {...{ statusBarTranslucent, presentationStyle, onShow }}>
      <ViewAnimated animation={isVisible ? AnimatedString.fadeIn : AnimatedString.fadeOut} style={styles.container}>
        {isClose && <Button icon={NAME_ICON.close} onPress={closeModal} />}
        <ViewAnimated
          animation={isVisible ? AnimatedString.zoomIn : AnimatedString.zoomOut}
          style={[isContentStyle ? styles.content : null, stylesContent]}>
          {children}
        </ViewAnimated>
      </ViewAnimated>
    </Modal>
  );
};

export default ModalCustom;
