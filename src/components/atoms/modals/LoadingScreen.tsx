// Libs
import React, { ReactNode } from 'react';
import { StyleSheet } from 'react-native';
import { Animation, CustomAnimation } from 'react-native-animatable';

// Atoms
import SpinnerLottie from '@atoms/spinners';
import ViewAnimated from '@atoms/views/ViewAnimated';

// Helpers
// import { CLog } from '@helpers/Message';

// Constants
import { THEME_DEFAULT, TIMER_ANIMATION } from '@constantsApp';

// const URL_PATH = 'src/components/atoms/modal/index.tsx';

// Styles
const styles = StyleSheet.create({
  container: {
    backgroundColor: THEME_DEFAULT.backgroundTransparent,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    top: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

// Props Modal
interface ModalProps {
  animateContainer?: Animation | string | CustomAnimation;
  animateContent?: Animation | string | CustomAnimation;
  onAnimationEnd?: Function;
  children?: ReactNode | null;
  duration?: number;
}

const LoadingScreen = ({
  animateContainer,
  onAnimationEnd,
  children = null,
  animateContent,
  duration = TIMER_ANIMATION.animatesGeneral
}: ModalProps) => {
  return (
    <ViewAnimated style={styles.container} animation={animateContainer} onAnimationEnd={onAnimationEnd} duration={duration}>
      <ViewAnimated animation={animateContent} duration={duration}>
        <SpinnerLottie />
        {children}
      </ViewAnimated>
    </ViewAnimated>
  );
};

export default LoadingScreen;
