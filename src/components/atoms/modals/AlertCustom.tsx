// Libs
import React, { useMemo, ReactNode } from 'react';
import { StyleSheet, StyleProp, TextStyle } from 'react-native';
import { Animation, CustomAnimation } from 'react-native-animatable';

// Atoms
import View from '@atoms/views';
import ViewAnimated from '@atoms/views/ViewAnimated';
import Text from '@atoms/texts';
import Button from '@atoms/buttons';

// Helpers
//import { CLog } from '@helpers/Message';

// Config
import { EnumTypeButton } from '@enum';

// Styles General
import { stylesGeneral } from '@styles/index';

// Constants
import { THEME_DEFAULT, TIMER_ANIMATION, StyleButtonAlert, METRICS, EnumFont } from '@constantsApp';

//const URL_PATH = 'src/components/atoms/modal/index.tsx';

// Styles
const styles = StyleSheet.create({
  container: {
    backgroundColor: THEME_DEFAULT.backgroundTransparent,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    top: 0,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: METRICS.CONSTANTS_PADDING
  },
  viewContent: {
    width: '100%',
    backgroundColor: THEME_DEFAULT.white,
    borderRadius: METRICS.BORDER_RADIUS,
    padding: METRICS.PADDING_MODAL_ALERT
  },
  titleView: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  messageView: {
    width: '100%',
    marginVertical: METRICS.CONSTANTS_MARGIN_INSIDE_MODAL_ALERT_LEFT
  },
  viewOptions: {
    width: '100%',
    flexDirection: 'row'
  },
  viewOptionsMore: {
    width: '100%',
    flexDirection: 'column'
  },
  buttonOptions: {
    flex: 1
  }
});

// Buttons of Options Alert
export interface AlertOptions {
  text?: string;
  onAction?: () => void;
  style?: StyleButtonAlert;
}

// Props Modal
interface ModalProps {
  animateContainer?: Animation | string | CustomAnimation;
  animateContent?: Animation | string | CustomAnimation;
  onAnimationEnd?: Function;
  duration?: number;
  // contenido
  title?: string;
  message?: string | ReactNode;
  styleMessage?: StyleProp<TextStyle>;
  options?: Array<AlertOptions>;
  // acción de pulsado un button
  onActionPress?: () => void;
}

const validateStringOrReactNode = (message: string | ReactNode, styleMessage?: StyleProp<TextStyle>): string | ReactNode => {
  if (typeof message === 'string') {
    return (
      <Text style={styleMessage} type={EnumFont.primary}>
        {message}
      </Text>
    );
  } else {
    return message;
  }
};

const AlertCustom = ({
  animateContainer,
  onAnimationEnd,
  animateContent,
  duration = TIMER_ANIMATION.animatesAlert,
  title,
  message,
  options,
  onActionPress,
  styleMessage
}: ModalProps) => {
  const validateMemo = useMemo(
    () => ({
      typeOption: (value?: StyleButtonAlert): EnumTypeButton => {
        switch (value) {
          case StyleButtonAlert.default:
            return EnumTypeButton.normal;
          case StyleButtonAlert.cancel:
            return EnumTypeButton.neutral;
          default:
            return EnumTypeButton.normal;
        }
      }
    }),
    []
  );

  return (
    <ViewAnimated style={styles.container} animation={animateContainer} onAnimationEnd={onAnimationEnd} duration={duration}>
      <ViewAnimated style={[styles.viewContent, stylesGeneral.shadowAndElevation]} animation={animateContent} duration={duration}>
        <View style={styles.titleView}>
          {title && (
            <Text type={EnumFont.septenary} isCapitalize>
              {title}
            </Text>
          )}
        </View>
        <View style={styles.messageView}>{message && validateStringOrReactNode(message, styleMessage)}</View>
        {options && (
          <View style={options.length > 2 ? styles.viewOptionsMore : styles.viewOptions}>
            {options.map(item => (
              <Button
                text={item.text}
                style={options.length > 2 ? null : styles.buttonOptions}
                onPress={() => {
                  if (onActionPress) {
                    onActionPress();
                  }
                  if (item.onAction) {
                    item.onAction();
                  }
                }}
                type={validateMemo.typeOption(item.style)}
                key={item.text}
              />
            ))}
          </View>
        )}
      </ViewAnimated>
    </ViewAnimated>
  );
};

export default AlertCustom;
