// Libs
import React from 'react';
import { StyleSheet } from 'react-native';
import { Animation, CustomAnimation } from 'react-native-animatable';

// Atoms
import Text from '@atoms/texts';
import TouchableO from '@atoms/buttons/TouchableOpacity';
import ViewAnimated from '@atoms/views/ViewAnimated';
import View from '@atoms/views';

// Styles general
import { stylesGeneral } from '@styles/index';

// Constants
import { THEME_DEFAULT, TIMER_ANIMATION, METRICS, EnumFont, IS_ANDROID, FAMILY_FONTS, SIZE_FONTS } from '@constantsApp';
import { translate } from 'locales';
import IconCustom from '../icons';
import { TypeLibraryIconsSystem } from 'configs/Enum';

export interface OptionsAlertSheet {
  text: string;
  onPress: () => void;
}

// Props
interface AlertSheetProps {
  infoNote?: string;
  errorNote?: string;
  successIcon?: boolean;
  title?: string;
  message?: string;
  options?: Array<OptionsAlertSheet>;
  cancel?: boolean;
  duration?: number;
  animateContainer: Animation | string | CustomAnimation;
  onAnimationEnd?: () => void;
  animateContent: Animation | string | CustomAnimation;
  // acción de pulsado un button
  onActionPress?: () => void;
}

// Styles
const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: THEME_DEFAULT.backgroundTransparent,
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  textCancel: {
    fontFamily: FAMILY_FONTS.primaryRegular,
    color: THEME_DEFAULT.white,
    fontWeight: 'bold'
  },
  viewContent: {
    paddingHorizontal: METRICS.PADDING_ALERT_SHEET,
    paddingTop: METRICS.PADDING_ALERT_SHEET,
    width: '100%',
    justifyContent: 'center',
    borderTopLeftRadius: METRICS.BORDER_RADIUS_ALERT_SHEET,
    borderTopRightRadius: METRICS.BORDER_RADIUS_ALERT_SHEET,
    backgroundColor: THEME_DEFAULT.content
  },
  viewHeader: {
    width: '100%',
    padding: METRICS.CONSTANTS_PADDING
  },
  viewOptions: {
    width: '100%'
  },
  viewCancel: {
    width: '100%',
    backgroundColor: THEME_DEFAULT.content
  },
  buttonOptionsCancel: {
    backgroundColor: THEME_DEFAULT.primary,
    marginTop: IS_ANDROID ? 2 : 0.5,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    borderRadius: 40 / 2,
    marginBottom: 16
  },
  buttonOptions: {
    backgroundColor: THEME_DEFAULT.primary,
    marginTop: IS_ANDROID ? 2 : 0.5,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    borderRadius: 40 / 2,
    marginBottom: 16
  },
  buttonOptionsLast: {
    backgroundColor: THEME_DEFAULT.content,
    marginTop: IS_ANDROID ? 2 : 0.5,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    borderRadius: 40 / 2,
    marginBottom: 16
  },
  errorText: {
    color: THEME_DEFAULT.error,
    fontSize: 14,
    letterSpacing: 2
  },
  infoNote: {
    color: THEME_DEFAULT.secondaryFont,
    fontSize: 14,
    letterSpacing: 2
  },
  title: {
    fontFamily: FAMILY_FONTS.primaryRegular,
    color: THEME_DEFAULT.primary,
    fontSize: 26,
    marginBottom: 6,
    fontWeight: '700'
  },
  message: {
    fontFamily: FAMILY_FONTS.primaryRegular,
    color: 'rgba(0,0,0,0.6)',
    fontSize: SIZE_FONTS.primary
  },
  buttonText: {
    fontFamily: FAMILY_FONTS.primaryRegular,
    color: THEME_DEFAULT.white,
    fontWeight: 'bold'
  },
  lastButtonText: {
    fontFamily: FAMILY_FONTS.primaryRegular,
    color: THEME_DEFAULT.black
  }
});

const AlertSheet = ({
  infoNote,
  errorNote,
  successIcon,
  title,
  message,
  options,
  cancel,
  duration = TIMER_ANIMATION.animatesAlert,
  animateContainer,
  onAnimationEnd,
  animateContent,
  onActionPress
}: AlertSheetProps) => {
  return (
    <ViewAnimated style={styles.container} animation={animateContainer} onAnimationEnd={onAnimationEnd} duration={duration}>
      <ViewAnimated style={{ width: '100%' }}>
        <TouchableO
          onPress={() => {
            if (onActionPress) {
              onActionPress();
            }
          }}
          style={{ width: '100%', height: '100%' }}
        />
      </ViewAnimated>
      <ViewAnimated style={[styles.viewContent, stylesGeneral.shadowAndElevation]} animation={animateContent} duration={duration}>
        <View style={styles.viewHeader}>
          {infoNote && <Text style={styles.infoNote}>{infoNote}</Text>}
          {errorNote && <Text style={styles.errorText}>{errorNote}</Text>}
          {title && (
            <View style={{ flexDirection: 'row' }}>
              {successIcon && (
                <IconCustom
                  name='check'
                  library={TypeLibraryIconsSystem.materialCommunityIcons}
                  color={THEME_DEFAULT.success}
                  style={{ marginRight: 5 }}
                />
              )}
              <Text style={styles.title}>{title}</Text>
            </View>
          )}
          <Text style={styles.message}>{message}</Text>
        </View>
        {options && (
          <View style={styles.viewOptions}>
            {options.map((element: OptionsAlertSheet, index: number) => (
              <TouchableO
                onPress={() => {
                  if (onActionPress) {
                    onActionPress();
                  }
                  if (element.onPress) {
                    element.onPress();
                  }
                }}
                key={index}
                style={options.length === index + 1 && options.length > 1 ? styles.buttonOptionsLast : styles.buttonOptions}>
                <Text style={options.length === index + 1 && options.length > 1 ? styles.lastButtonText : styles.buttonText}>
                  {element.text}
                </Text>
              </TouchableO>
            ))}
          </View>
        )}
        {cancel && (
          <View style={styles.viewCancel}>
            <TouchableO
              style={styles.buttonOptionsCancel}
              onPress={() => {
                if (onActionPress) {
                  onActionPress();
                }
              }}>
              <Text type={EnumFont.octonary} style={styles.textCancel}>
                {translate('GENERAL.MESSAGES.BUTTON_CANCEL')}
              </Text>
            </TouchableO>
          </View>
        )}
      </ViewAnimated>
    </ViewAnimated>
  );
};

export default AlertSheet;
