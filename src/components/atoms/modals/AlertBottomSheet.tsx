// Libs
import React from 'react';
import { StyleSheet } from 'react-native';
import { Animation, CustomAnimation } from 'react-native-animatable';

// Atoms
import Text from '@atoms/texts';
import TouchableO from '@atoms/buttons/TouchableOpacity';
import ViewAnimated from '@atoms/views/ViewAnimated';
import View from '@atoms/views';

// Styles general
import { stylesGeneral } from '@styles/index';

// Constants
import { THEME_DEFAULT, TIMER_ANIMATION, METRICS, EnumFont, IS_ANDROID } from '@constantsApp';
import { translate } from 'locales';

export interface OptionsAlertBottomSheet {
  text: string;
  onPress: () => void;
}

// Props
interface AlertSheetProps {
  title?: string;
  message?: string;
  options?: Array<OptionsAlertBottomSheet>;
  cancel?: boolean;
  duration?: number;
  animateContainer: Animation | string | CustomAnimation;
  onAnimationEnd?: () => void;
  animateContent: Animation | string | CustomAnimation;
  // acción de pulsado un button
  onActionPress?: () => void;
}

// Styles
const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: THEME_DEFAULT.backgroundTransparent,
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  textCancel: {
    color: THEME_DEFAULT.primary
  },
  viewContent: {
    paddingHorizontal: METRICS.PADDING_ALERT_SHEET,
    paddingTop: METRICS.PADDING_ALERT_SHEET,
    width: '100%',
    justifyContent: 'center'
  },
  viewHeader: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    padding: METRICS.CONSTANTS_PADDING,
    backgroundColor: THEME_DEFAULT.content,
    borderTopLeftRadius: METRICS.BORDER_RADIUS_ALERT_SHEET,
    borderTopRightRadius: METRICS.BORDER_RADIUS_ALERT_SHEET
  },
  viewOptions: {
    width: '100%',
    borderBottomLeftRadius: METRICS.BORDER_RADIUS_ALERT_SHEET,
    borderBottomRightRadius: METRICS.BORDER_RADIUS_ALERT_SHEET
  },
  viewCancel: {
    width: '100%',
    backgroundColor: THEME_DEFAULT.content
    // borderRadius: METRICS.BORDER_RADIUS_ALERT_SHEET,
    // marginTop: METRICS.MARGIN_ALERT_SHEET
  },
  buttonOptionsCancel: {
    padding: METRICS.CONSTANTS_PADDING,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonOptions: {
    backgroundColor: THEME_DEFAULT.content,
    marginTop: IS_ANDROID ? 2 : 0.5,
    padding: METRICS.CONSTANTS_PADDING,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonOptionsLast: {
    backgroundColor: THEME_DEFAULT.content,
    marginTop: IS_ANDROID ? 2 : 0.5,
    padding: METRICS.CONSTANTS_PADDING,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomLeftRadius: METRICS.BORDER_RADIUS_ALERT_SHEET,
    borderBottomRightRadius: METRICS.BORDER_RADIUS_ALERT_SHEET
  }
});

const AlertBottomSheet = ({
  title,
  message,
  options,
  cancel,
  duration = TIMER_ANIMATION.animatesAlert,
  animateContainer,
  onAnimationEnd,
  animateContent,
  onActionPress
}: AlertSheetProps) => {
  return (
    <ViewAnimated style={styles.container} animation={animateContainer} onAnimationEnd={onAnimationEnd} duration={duration}>
      <ViewAnimated style={{ width: '100%' }}>
        <TouchableO
          onPress={() => {
            if (onActionPress) {
              onActionPress();
            }
          }}
          style={{ width: '100%', height: '100%' }}
        />
      </ViewAnimated>
      <ViewAnimated style={[styles.viewContent, stylesGeneral.shadowAndElevation]} animation={animateContent} duration={duration}>
        <View style={styles.viewHeader}>
          <Text type={EnumFont.quinary}>{title}</Text>
          <Text type={EnumFont.quaternary}>{message}</Text>
        </View>
        {options && (
          <View style={styles.viewOptions}>
            {options.map((element: OptionsAlertBottomSheet, index: number) => (
              <TouchableO
                onPress={() => {
                  if (onActionPress) {
                    onActionPress();
                  }
                  if (element.onPress) {
                    element.onPress();
                  }
                }}
                key={index.toString()}
                style={options.length > index + 1 ? styles.buttonOptions : styles.buttonOptionsLast}>
                <Text type={EnumFont.septenary}>{element.text}</Text>
              </TouchableO>
            ))}
          </View>
        )}
        {cancel && (
          <View style={styles.viewCancel}>
            <TouchableO
              style={styles.buttonOptionsCancel}
              onPress={() => {
                if (onActionPress) {
                  onActionPress();
                }
              }}>
              <Text type={EnumFont.septenary} style={styles.textCancel}>
                {translate('GENERAL.MESSAGES.BUTTON_CANCEL')}
              </Text>
            </TouchableO>
          </View>
        )}
      </ViewAnimated>
    </ViewAnimated>
  );
};

export default AlertBottomSheet;
