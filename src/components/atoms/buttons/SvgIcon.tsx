// Libs
import React from 'react';
import { StyleProp, ViewStyle } from 'react-native';
// Atoms
import Icon from '@atoms/icons';
import SvgCss from '@atoms/svgs/SvgCss';
import TouchableO from '@atoms/buttons/TouchableOpacity';

// Constants
import { SIZE_ICONS } from '@constantsApp';
import { TypeLibraryIconsSystem } from '@enum';

const SIZE_GRAFIC = SIZE_ICONS.general;

// Props
type SvgIconButtonProps = {
  /**
   * SVG
   */
  svg?: string;
  widthSvg?: string | number;
  heightSvg?: string | number;
  /**
   * Icon
   */
  icon?: string | null;
  sizeIcon?: number | null;
  libraryIcon?: TypeLibraryIconsSystem;
  // solo funciona en los iconos
  color?: string;
  /**
   * Button
   */
  onPress?: () => void;
  style?: StyleProp<ViewStyle> | null;
  disabled?: boolean;
};

const SvgIconButton = ({
  svg,
  widthSvg = SIZE_GRAFIC,
  heightSvg = SIZE_GRAFIC,
  icon,
  sizeIcon = SIZE_GRAFIC,
  onPress,
  libraryIcon,
  style,
  color,
  disabled
}: SvgIconButtonProps) => {
  return (
    <TouchableO {...{ style, onPress, disabled }}>
      {icon && <Icon name={icon} size={sizeIcon} library={libraryIcon} {...{ color }} />}
      {svg && <SvgCss xml={svg} width={widthSvg} height={heightSvg} />}
    </TouchableO>
  );
};

export default SvgIconButton;
