// Libs
import React, { useState } from 'react';
import { GestureResponderEvent, LayoutChangeEvent, StyleSheet, StyleProp, ViewStyle, TextStyle } from 'react-native';

// Atoms
import Icon from '@atoms/icons';
import Text from '@atoms/texts';
import TouchableO from '@atoms/buttons/TouchableOpacity';

// Styles General
import { stylesGeneral } from '@styles/index';

// Constants
import { EnumFont, METRICS, THEME_DEFAULT, SIZE_ICONS } from '@constantsApp';

/* Config */
import { EnumTypeButton, TypeLibraryIconsSystem } from '@enum';

// const URL_PATH = 'src/components/atoms/button/custom/index.tsx';

// Props
interface ButtonProps {
  type?: EnumTypeButton;
  text?: string;
  typeText?: EnumFont;
  textStyle?: StyleProp<TextStyle> | null;
  onPress?: (event: GestureResponderEvent) => void;
  style?: StyleProp<ViewStyle> | null;
  icon?: string;
  colorIcon?: string | null;
  libraryIcon?: TypeLibraryIconsSystem | null;
  iconSize?: number;
  iconStyle?: StyleProp<ViewStyle> | null;
  disabled?: boolean | null;
}

// Styles
const styles = StyleSheet.create({
  // puede modificar el valor del gradiente revisar cuando se modifique
  allButton: {
    margin: 5
  },
  allButtonIcon: {
    padding: METRICS.CONSTANTS_PADDING_BUTTON_ICON
  },
  linealGradient: {
    padding: METRICS.CONSTANTS_PADDING_BUTTON,
    justifyContent: 'center',
    alignItems: 'center'
  },
  noGradient: {
    backgroundColor: THEME_DEFAULT.primary,
    padding: METRICS.CONSTANTS_PADDING_BUTTON,
    alignItems: 'center',
    justifyContent: 'center'
  },
  noGradientDisable: {
    backgroundColor: THEME_DEFAULT.buttonDisable,
    padding: METRICS.CONSTANTS_PADDING_BUTTON,
    alignItems: 'center',
    justifyContent: 'center'
  },
  neutral: {
    backgroundColor: THEME_DEFAULT.buttonNeutral,
    padding: METRICS.CONSTANTS_PADDING_BUTTON,
    alignItems: 'center',
    justifyContent: 'center'
  },
  neutralDisable: {
    backgroundColor: THEME_DEFAULT.buttonDisable,
    padding: METRICS.CONSTANTS_PADDING_BUTTON,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

const Button = ({
  type,
  text,
  typeText,
  textStyle,
  onPress,
  style,
  icon,
  colorIcon,
  libraryIcon,
  iconStyle,
  iconSize = SIZE_ICONS.generalSmall,
  disabled
}: ButtonProps) => {
  const [dimensionButton, setDimensionButton] = useState({ height: 0, width: 0 });

  const onLayout = (event: LayoutChangeEvent) => {
    // CLog(URL_PATH, 'onLayout()', 'event', event.nativeEvent.layout);
    setDimensionButton({
      height: event.nativeEvent.layout.height,
      width: event.nativeEvent.layout.width
    });
  };

  switch (type) {
    case EnumTypeButton.neutral:
      return (
        <TouchableO
          disabled={disabled}
          onPress={onPress}
          onLayout={onLayout}
          style={[
            { borderRadius: 25 },
            disabled ? styles.neutralDisable : styles.neutral,
            styles.allButton,
            icon ? styles.allButtonIcon : null,
            style,
            stylesGeneral.shadowAndElevation
          ]}>
          {text && <Text type={typeText || EnumFont.quinary}>{text || null}</Text>}
          {icon && <Icon name={icon} color={colorIcon || THEME_DEFAULT.secondaryFont} library={libraryIcon} size={iconSize} />}
        </TouchableO>
      );
    default:
      return (
        <TouchableO
          disabled={disabled}
          onPress={onPress}
          onLayout={onLayout}
          style={[
            { borderRadius: 25 },
            styles.noGradient,
            disabled ? styles.noGradientDisable : styles.noGradient,
            styles.allButton,
            icon ? styles.allButtonIcon : null,
            style,
            stylesGeneral.shadowAndElevation
          ]}>
          {text && (
            <Text type={typeText || EnumFont.sixth} style={textStyle}>
              {text || null}
            </Text>
          )}
          {icon && (
            <Icon name={icon} color={colorIcon || THEME_DEFAULT.primaryFont} library={libraryIcon} size={iconSize} style={iconStyle} />
          )}
        </TouchableO>
      );
  }
};

export default Button;
