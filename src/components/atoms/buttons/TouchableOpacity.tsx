// Libs
import React, { ReactNode } from 'react';
import { GestureResponderEvent, StyleProp, TouchableOpacity, ViewStyle, LayoutChangeEvent } from 'react-native';

// Props
interface TouchableOProps {
  children?: ReactNode | null;
  onPress?: (event: GestureResponderEvent) => void;
  style?: StyleProp<ViewStyle> | null;
  onLayout?: (event: LayoutChangeEvent) => void;
  disabled?: boolean | null;
}

const TouchableO = ({ children, onPress, style, onLayout, disabled, ...props }: TouchableOProps) => {
  return (
    <TouchableOpacity disabled={disabled} onPress={onPress} style={style} onLayout={onLayout} {...props}>
      {children || null}
    </TouchableOpacity>
  );
};

export default TouchableO;
