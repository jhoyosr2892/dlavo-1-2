import { THEME_DEFAULT } from 'configs/Constants';
import React, { useEffect, useState } from 'react';
import { Keyboard, StyleProp, StyleSheet, Text, View, ViewStyle } from 'react-native';
import { stylesGeneral } from 'styles';
import ViewAtom from '../views';
import ViewAnimated from '../views/ViewAnimated';
import TouchableO from './TouchableOpacity';

type ButtonButtonProps = {
  text: string;
  onPress?: () => void;
  disabled?: boolean;
  style?: StyleProp<ViewStyle>;
};

const BottomButtonAtom = ({ text, onPress, disabled = false, style }: ButtonButtonProps) => {
  const [show, setShow] = useState(true);
  useEffect(() => {
    const showSubscription = Keyboard.addListener('keyboardDidShow', () => {
      setShow(false);
    });
    const hideSubscription = Keyboard.addListener('keyboardDidHide', () => {
      setShow(true);
    });

    return () => {
      showSubscription.remove();
      hideSubscription.remove();
    };
  }, []);
  return (
    <ViewAnimated animation={show ? 'fadeIn' : 'fadeOut'} delay={100} style={styles.container}>
      <ViewAtom style={[styles.content, style]}>
        <TouchableO style={[styles.touchableO, disabled && styles.disableButton]} onPress={onPress} disabled={disabled}>
          <Text style={styles.text}>{text}</Text>
        </TouchableO>
      </ViewAtom>
    </ViewAnimated>
  );
};

export default BottomButtonAtom;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    position: 'absolute',
    bottom: 0
  },
  content: {
    backgroundColor: THEME_DEFAULT.white,
    padding: 10,
    paddingBottom: 20,
    borderTopStartRadius: 10,
    borderTopEndRadius: 10
  },
  touchableO: {
    height: 36,
    width: '100%',
    backgroundColor: THEME_DEFAULT.primary,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
    zIndex: 1
  },
  disableButton: {
    backgroundColor: THEME_DEFAULT.buttonDisable
  },
  text: {
    color: THEME_DEFAULT.white,
    fontSize: 18,
    fontWeight: 'bold'
  }
});
