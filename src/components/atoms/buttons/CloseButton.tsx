import { NAME_ROUTE } from 'configs/Constants';
import { TypeLibraryIconsSystem } from 'configs/Enum';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { navigationScreen } from 'utils/helpers/Navigation';
import IconCustom from '../icons';
import TouchableO from './TouchableOpacity';

export type CloseButtonAtomProps = { backRoute?: NAME_ROUTE };

const CloseButtonAtom = ({ backRoute }: CloseButtonAtomProps) => {
  const navigation = useNavigation();
  const onClose = () => {
    if (backRoute) {
      navigationScreen(backRoute);
    } else {
      navigation.goBack();
    }
  };
  return (
    <TouchableO style={styles.button} onPress={onClose}>
      <IconCustom name='close' library={TypeLibraryIconsSystem.ionicons} size={24} />
    </TouchableO>
  );
};

export default CloseButtonAtom;

const styles = StyleSheet.create({
  button: {
    marginVertical: 3,
    marginHorizontal: 11,
    alignItems: 'center',
    flex: 1,
    opacity: 1,
    flexDirection: 'row'
  }
});
