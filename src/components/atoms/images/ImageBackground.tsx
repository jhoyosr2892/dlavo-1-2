// Libs
import React, { ReactNode } from 'react';
import { ImageResizeMode, ImageURISource, StyleProp, ImageBackground as ImageB, ViewStyle } from 'react-native';

// Helpers
import { validateSourceImage } from '@helpers/Validate';

// Constants
import { ASSETS, METRICS } from '@constantsApp';

// Props
type ImageBackgroundProps = {
  style: StyleProp<ViewStyle>;
  children?: ReactNode;
  source?: string | null | ImageURISource | number;
  resizeMode?: ImageResizeMode;
  borderRadius?: number;
  defaultSource?: ImageURISource | number;
  loadingIndicatorSource?: ImageURISource | number;
  width?: number;
  height?: number;
};

const ImageBackground = ({
  resizeMode,
  style,
  children,
  source,
  borderRadius,
  width = METRICS.DEVICE_WIDTH,
  height = METRICS.DEVICE_HEIGHT / 6
}: ImageBackgroundProps) => {
  return (
    <ImageB source={validateSourceImage(source)} defaultSource={ASSETS.monogramPng} {...{ width, height, borderRadius, resizeMode, style }}>
      {children}
    </ImageB>
  );
};

export default ImageBackground;
