// Libs
import React from 'react';
import {
  Image as ImageRN,
  ImageSourcePropType,
  ImageURISource,
  ImageResolvedAssetSource,
  ImageResizeMode,
  StyleProp,
  ImageStyle
} from 'react-native';

// Helpers
import { validateSourceImage } from '@helpers/Validate';
//import { CLog } from '@helpers/Message';

// Constants
import { ASSETS } from '@constantsApp';

//const URL_PATH = 'src/components/atoms/images/index.tsx';

// Extrae el alto, ancho de una imagen
export const resolveAssetSource = (source: ImageSourcePropType): ImageResolvedAssetSource => ImageRN.resolveAssetSource(source);

// En base a una (dimension y una source) se devuelven las 2 dimensiones de una imagen sin distinción.
export const dimensionSourceImageWithBase = (source: ImageSourcePropType, dimension: number, isVertical = false) => {
  const { width, height } = resolveAssetSource(source);

  const widthImage = (): number => {
    if (height && width) {
      if (!isVertical) {
        if (height > width) {
          return dimension / (width / height);
        }
      } else {
        if (height < width) {
          return dimension / (width / height);
        }
      }
    }
    return dimension;
  };

  const heightImage = (): number => {
    if (height && width) {
      if (!isVertical) {
        if (height < width) {
          return dimension / (width / height);
        }
      } else {
        if (height > width) {
          return dimension / (width / height);
        }
      }
    }
    return dimension;
  };

  return { w: widthImage(), h: heightImage() };
};

// Props
interface ImageProps {
  source?: string | null | ImageURISource | number;
  resizeMode?: ImageResizeMode;
  style?: StyleProp<ImageStyle>;
}

// Componente Funcional de Imagen
const Image = ({ source = ASSETS.monogramPng, resizeMode, style, ...props }: ImageProps) => {
  return (
    <ImageRN source={validateSourceImage(source)} resizeMode={resizeMode} style={style} {...props} defaultSource={ASSETS.monogramPng} />
  );
};

export default Image;
