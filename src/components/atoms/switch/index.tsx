import React, { useState } from 'react';
import { Switch } from 'react-native';
/* Config */
import { THEME_DEFAULT } from 'configs/Constants';

export type SwitchAtomProps = {
  onSwitchPress: (value: boolean) => void;
  valueSelected?: boolean;
};

const SwitchAtom = ({ onSwitchPress, valueSelected }: SwitchAtomProps) => {
  const [value, setValue] = useState<boolean>(valueSelected ? valueSelected : false);

  const onValueChange = (value: boolean) => {
    setValue(value);
    onSwitchPress(value);
  };

  return (
    <Switch
      trackColor={{ true: THEME_DEFAULT.primary, false: 'gray' }}
      thumbColor={value ? THEME_DEFAULT.primaryLight : THEME_DEFAULT.buttonDisable}
      ios_backgroundColor='#3e3e3e'
      onValueChange={(value: boolean) => onValueChange(value)}
      value={value}
    />
  );
};

export default SwitchAtom;
