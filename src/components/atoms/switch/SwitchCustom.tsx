import { THEME_DEFAULT } from 'configs/Constants';
import { translate } from 'locales';
import React from 'react';
import { StyleSheet, Switch, Text, View } from 'react-native';
import TouchableO from '../buttons/TouchableOpacity';

export type SwitchCustomAtomProps = {
  value: boolean;
  leftText: string;
  rightText: string;
  onLeftPress: () => void;
  onSwitchPress: () => void;
  onRightPress: () => void;
};

const SwitchCustomAtom = ({
  value = false,
  leftText = translate('GENERAL.WORDS.WORD_NOT'),
  rightText = translate('GENERAL.WORDS.WORD_YES'),
  onLeftPress = () => {},
  onSwitchPress = () => {},
  onRightPress = () => {}
}: SwitchCustomAtomProps) => {
  return (
    <View style={styles.container}>
      <TouchableO style={styles.leftButton} onPress={() => onLeftPress()}>
        <Text style={styles.text}>{leftText}</Text>
      </TouchableO>
      <View style={styles.switchContent}>
        <Switch
          trackColor={{ true: THEME_DEFAULT.primaryDark }}
          thumbColor={value ? THEME_DEFAULT.primary : THEME_DEFAULT.buttonNeutral}
          ios_backgroundColor='#3e3e3e'
          onValueChange={() => onSwitchPress()}
          value={value}
        />
      </View>
      <TouchableO style={styles.rightButton} onPress={() => onRightPress()}>
        <Text style={styles.text}>{rightText}</Text>
      </TouchableO>
    </View>
  );
};

export default SwitchCustomAtom;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 10
  },
  leftButton: {
    flex: 0.4,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  switchContent: {
    flex: 0.2,
    justifyContent: 'center',
    flexDirection: 'row'
  },
  rightButton: {
    flex: 0.4,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  text: {
    fontSize: 20
  }
});
