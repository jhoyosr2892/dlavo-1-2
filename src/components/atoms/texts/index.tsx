// Libs
import React, { ReactNode, SyntheticEvent } from 'react';
import { Text as Tex, StyleProp, TextStyle, LayoutChangeEvent, GestureResponderEvent, StyleSheet } from 'react-native';

// Helpers
import { validateTypeText } from '@helpers/Validate';

// Styles
import { stylesGeneral } from '@styles/index';

// Constants
import { EnumFont, IS_ANDROID } from '@constantsApp';

export type TextProps = {
  children?: ReactNode | null;
  style?: StyleProp<TextStyle> | null;
  isCapitalize?: boolean | null;
  type?: EnumFont;
  key?: string | number;
  onLayout?: (event: LayoutChangeEvent) => void;
  numberOfLines?: number;
  onTextLayout?: (event: SyntheticEvent) => void;
  onPress?: (event: GestureResponderEvent) => void;
  ellipsizeMode?: 'head' | 'middle' | 'tail' | 'clip';
};

const styles = StyleSheet.create({
  text: {
    marginTop: IS_ANDROID ? 0 : 3
  }
});

const Text = ({ children, style, isCapitalize, type, key, onLayout, numberOfLines, onTextLayout, onPress, ellipsizeMode }: TextProps) => {
  return (
    <Tex
      key={key}
      style={[styles.text, validateTypeText(type), style, isCapitalize ? stylesGeneral.textCapitalize : {}]}
      {...{ onLayout, numberOfLines, onTextLayout, onPress, ellipsizeMode }}>
      {children}
    </Tex>
  );
};

export default Text;
