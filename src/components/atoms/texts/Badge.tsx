// Libs
import React, { useEffect, useState } from 'react';
import { StyleSheet, StyleProp, ViewStyle } from 'react-native';

// Atoms
import Text, { TextProps } from '@atoms/texts';
import View from '@atoms/views';

// Config
import { BadgeEnum } from '@config/Enum';

// Constatns
import { THEME_DEFAULT, METRICS, EnumFont } from '@constantsApp';

// Helpers
import { CLog } from '@helpers/Message';

// Props
type BadgeProps = TextProps | { typeBadge: BadgeEnum };

// Constants
const URL_PATH = 'src/components/atoms/texts/Badge.tsx';

// Styles
const styles = StyleSheet.create({
  container: {
    borderRadius: METRICS.BORDER_RADIUS,
    padding: METRICS.PADDING_INSIDE,
    paddingHorizontal: METRICS.CONSTANTS_PADDING
  },
  new: {
    backgroundColor: THEME_DEFAULT.black
  },
  default: {
    backgroundColor: THEME_DEFAULT.primary
  }
});

const Badge = ({ typeBadge, type = EnumFont.octonary, ...props }: BadgeProps) => {
  const [styleBadge, setStyleBadge] = useState<StyleProp<ViewStyle>>();

  const loadStyle = () => {
    let style: StyleProp<ViewStyle> = styles.container;
    CLog(URL_PATH, 'loadStyle()', 'typeBadge', typeBadge);
    switch (typeBadge) {
      case BadgeEnum.new:
        style = {
          ...style,
          ...styles.new
        };
        break;
      default:
        style = {
          ...style,
          ...styles.default
        };
        break;
    }
    setStyleBadge(style);
  };

  useEffect(() => {
    loadStyle();
  }, []);

  return (
    <View style={styleBadge}>
      <Text {...props} {...{ type }} />
    </View>
  );
};

export default Badge;
