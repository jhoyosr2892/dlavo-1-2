// Libs
import React, { useState, SyntheticEvent } from 'react';
import { StyleSheet } from 'react-native';

// Atoms
import SvgIconButton from '@atoms/buttons/SvgIcon';
import Text, { TextProps } from '@atoms/texts';
import View from '@atoms/views';

// Constants
import { NUMBER_LINE, NAME_ICON, THEME_DEFAULT } from '@constantsApp';
import { TypeLibraryIconsSystem } from 'configs/Enum';

const URL_PATH = 'src/components/atoms/texts/TextSeeMore.tsx';

// Props
type TextSeeMoreProps = TextProps;

// Styles
const styles = StyleSheet.create({
  buttonSeeMore: {
    alignSelf: 'center'
  }
});

const TextSeeMore = ({ numberOfLines = NUMBER_LINE.general, ...props }: TextSeeMoreProps) => {
  // numero de lineas a mostrar
  const [numberLine, setNumberLine] = useState<number | undefined>(numberOfLines);
  // si el texto excede el numero de lineas limite
  const [isSeeMore, setIsSeeMore] = useState(false);

  const handleSeeMore = () => {
    if (numberLine) {
      setNumberLine(undefined);
    } else {
      setNumberLine(numberOfLines);
    }
  };

  const validatesIfTextExceedsNumberLine = (e: SyntheticEvent) => {
    if ('lines' in e.nativeEvent) {
      if (e.nativeEvent.lines.length >= numberOfLines) {
        setIsSeeMore(true);
      }
    }
  };

  return (
    <View>
      <Text {...props} numberOfLines={numberLine} ellipsizeMode='tail' onTextLayout={validatesIfTextExceedsNumberLine} />
      {isSeeMore && (
        <SvgIconButton
          style={styles.buttonSeeMore}
          color={THEME_DEFAULT.secondaryFont}
          icon={numberLine ? NAME_ICON.keyboardArrowDown : NAME_ICON.keyboardArrowUp}
          libraryIcon={TypeLibraryIconsSystem.materialIcons}
          onPress={() => handleSeeMore()}
        />
      )}
    </View>
  );
};

export default TextSeeMore;
