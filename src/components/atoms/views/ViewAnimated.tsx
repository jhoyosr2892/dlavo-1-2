// Libs
import React, { ReactNode, LegacyRef } from 'react';
import { StyleProp, ViewStyle } from 'react-native';
import { View, Animation, CustomAnimation } from 'react-native-animatable';

// Constants
import { TIMER_ANIMATION } from '@constantsApp';

// Props
type ViewAnimatedProps = {
  children?: ReactNode | null;
  style?: StyleProp<ViewStyle> | null;
  useNativeDriver?: boolean;
  animation?: Animation | string | CustomAnimation;
  duration?: number;
  onAnimationEnd?: Function;
  delay?: number;
  refViewAnimated?: LegacyRef<any>;
  transition?: any;
};

const ViewAnimated = ({
  children = null,
  style = null,
  useNativeDriver = true,
  animation,
  duration = TIMER_ANIMATION.animatesGeneral,
  onAnimationEnd,
  delay,
  refViewAnimated,
  transition
}: ViewAnimatedProps) => {
  return (
    <View
      ref={refViewAnimated}
      animation={animation}
      transition={transition}
      easing='ease-out'
      useNativeDriver={useNativeDriver}
      style={style}
      duration={duration}
      onAnimationEnd={onAnimationEnd}
      delay={delay}>
      {children}
    </View>
  );
};

export default ViewAnimated;
