// Libs
import React, { ReactNode } from 'react';
import { View, StyleProp, ViewStyle, LayoutChangeEvent } from 'react-native';

// Props
type ViewCustomProps = {
  children?: ReactNode | null;
  style?: StyleProp<ViewStyle> | null;
  onLayout?: (event: LayoutChangeEvent) => void;
};

const ViewAtom = ({ children = null, style = null, onLayout }: ViewCustomProps) => {
  return (
    <View style={style} {...{ onLayout }}>
      {children}
    </View>
  );
};

export default ViewAtom;
