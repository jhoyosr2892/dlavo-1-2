// Libs
import React from 'react';
import { StyleSheet, StyleProp, ViewStyle } from 'react-native';

// Atoms
import LottieView from '@atoms/lottieViews';
import Text from '@atoms/texts';
import View from '@atoms/views';

// Constants
import { ASSETS, METRICS } from '@constantsApp';
import { translate } from 'locales';

// Props
type NotResultViewProps = {
  text?: string;
  style?: StyleProp<ViewStyle> | null;
};

// Styles
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    width: '100%',
    height: '100%',
    padding: METRICS.CONSTANTS_PADDING
  },
  lottieView: {
    flex: 1,
    width: '100%'
  }
});

const NotResultView = ({ text, style }: NotResultViewProps) => {
  return (
    <View style={[styles.container, style]}>
      <Text>{text || translate('VIEWS_ATOMS.NO_RESULTS')}</Text>
      <LottieView style={styles.lottieView} loop={false} source={ASSETS.notResults} />
    </View>
  );
};

export default NotResultView;
