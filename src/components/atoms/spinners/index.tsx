// Libs
import React, { useEffect, useState } from 'react';
import { ViewStyle, StyleProp } from 'react-native';

// Atoms
import LottieViewCustom from '@atoms/lottieViews';

// Helpers
//import { CLog } from '@helpers/message';

// Constants
import { ASSETS, METRICS, EnumSpinner } from '@constantsApp';

//const URL_PATH = 'src/components/atoms/spinners/index.tsx';

// Props
type SpinnerLottieProps = {
  size?: number | string;
  type?: EnumSpinner;
  style?: StyleProp<ViewStyle>;
};

const SpinnerLottie = ({ size, type, style }: SpinnerLottieProps) => {
  const [source, setSource] = useState(ASSETS.spinnerLoading);

  useEffect(() => {
    const updateSource = () => {
      switch (type) {
        case EnumSpinner.primary:
          setSource(ASSETS.spinnerLoading);
          break;
        case EnumSpinner.white:
          setSource(ASSETS.spinnerLoadingWhite);
          break;
        default:
          setSource(ASSETS.spinnerLoading);
          break;
      }
    };
    updateSource();
  }, [type]);

  return (
    <LottieViewCustom
      source={source}
      style={[
        {
          height: size || METRICS.DEVICE_WIDTH / 8,
          width: size || METRICS.DEVICE_WIDTH / 8
        },
        style
      ]}
    />
  );
};

export default SpinnerLottie;
