import { FAMILY_FONTS, NAME_ROUTE, THEME_DEFAULT } from 'configs/Constants';
import React, { ReactNode } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import CloseButtonAtom from '../buttons/CloseButton';

export type CloseHeaderAtomProps = {
  title: string;
  backRoute?: NAME_ROUTE;
  rightElement?: ReactNode;
};

const CloseHeaderAtom = ({ title, backRoute, rightElement }: CloseHeaderAtomProps) => {
  return (
    <View style={styles.header}>
      <View style={{ flex: 0.2 }}>
        <CloseButtonAtom backRoute={backRoute} />
      </View>
      <View style={{ flex: 0.6 }}>
        <Text style={styles.title}>{title}</Text>
      </View>
      <View style={{ flex: 0.2 }}>{rightElement}</View>
    </View>
  );
};

export default CloseHeaderAtom;

const styles = StyleSheet.create({
  header: {
    backgroundColor: THEME_DEFAULT.primary,
    paddingHorizontal: 6,
    paddingVertical: 12,
    flexDirection: 'row',
    elevation: 4,
    borderColor: 'rgb(216,216,216)',
    shadowColor: 'rgb(216,216,216)'
  },
  title: {
    marginTop: 0,
    fontFamily: FAMILY_FONTS.primaryLight,
    fontSize: 20,
    color: THEME_DEFAULT.white,
    fontWeight: '700',
    textAlign: 'center',
    marginLeft: 25
  }
});
