// Libs
import React, { ReactNode } from 'react';
import { StyleProp, ViewStyle } from 'react-native';
import MapView, { Region, MapViewProps, PROVIDER_GOOGLE } from 'react-native-maps';

// Props
type MapViewAtomProps = {
  region?: Region;
  children?: ReactNode;
  style?: StyleProp<ViewStyle>;
  refMap?: string | ((instance: MapView | null) => void) | React.RefObject<MapView> | null | undefined;
  props?: MapViewProps;
  scrollEnabled?: boolean;
  zoomEnabled?: boolean;
  showsUserLocation?: boolean;
};

const MapViewAtom = ({
  region,
  children,
  style,
  refMap,
  scrollEnabled = false,
  zoomEnabled = false,
  showsUserLocation = false,
  props
}: MapViewAtomProps) => {
  return (
    <MapView
      ref={refMap}
      region={region}
      style={style}
      provider={PROVIDER_GOOGLE}
      scrollEnabled={scrollEnabled}
      zoomEnabled={zoomEnabled}
      showsUserLocation={showsUserLocation}
      {...props}>
      {children}
    </MapView>
  );
};

export const MapViewOriginal = MapView;
export default MapViewAtom;
