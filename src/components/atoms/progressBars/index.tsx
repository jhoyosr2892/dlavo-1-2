// Libs
import React from 'react';
import { StyleSheet, StyleProp, ViewStyle } from 'react-native';

// Atoms
import Text from '@atoms/texts';
import View from '@atoms/views';

// Helpers
import { percentageBetweenRanges } from '@helpers/Convert';

// Constants
import { THEME_DEFAULT, RangeProgress, METRICS, EnumFont } from '@constantsApp';

// Props
type ProgressBarProps = {
  range?: RangeProgress;
  value?: number;
  isLight?: boolean;
  stylesContainer?: StyleProp<ViewStyle>;
};

// Styles
const styles = StyleSheet.create({
  constainer: {
    width: '100%',
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  bar: {
    backgroundColor: THEME_DEFAULT.black,
    height: 20,
    borderRadius: 10,
    width: '70%',
    borderWidth: 2,
    justifyContent: 'center',
    marginRight: METRICS.CONSTANTS_MARGIN_INSIDE
  },
  barIndicator: {
    backgroundColor: THEME_DEFAULT.primary,
    height: 16,
    borderRadius: 8
  },
  barWhite: {
    backgroundColor: THEME_DEFAULT.white,
    borderColor: THEME_DEFAULT.white
  }
});

const ProgressBar = ({ range = { min: 0, max: 100 }, value, isLight, stylesContainer }: ProgressBarProps) => {
  return (
    <View style={[styles.constainer, stylesContainer]}>
      <View style={[styles.bar, isLight ? styles.barWhite : null]}>
        <View style={[styles.barIndicator, { width: `${percentageBetweenRanges(range, value)}%` }]} />
      </View>
      <Text type={isLight ? EnumFont.sixth : EnumFont.quinary}>{value}%</Text>
    </View>
  );
};

export default ProgressBar;
