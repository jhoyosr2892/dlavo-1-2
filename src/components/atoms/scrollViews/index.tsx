// Libs
import React, { ReactNode } from 'react';
import { StyleSheet, ScrollView as ScrollV, StyleProp, ViewStyle, RefreshControl } from 'react-native';

// Constants
import { THEME_DEFAULT, METRICS } from '@constantsApp';

// Props
type ScrollViewProps = {
  children?: ReactNode;
  style?: StyleProp<ViewStyle>;
  horizontal?: boolean;
  contentContainerStyle?: StyleProp<ViewStyle>;
  refreshing?: boolean;
  onRefresh?: () => void;
  scrollEnabled?: boolean;
  keyboardShouldPersistTaps?: 'always' | 'never' | 'handled';
};

// Styles
const styles = StyleSheet.create({
  content: {
    padding: METRICS.CONSTANTS_PADDING
  }
});

const ScrollView = ({
  children,
  style,
  horizontal,
  contentContainerStyle,
  refreshing,
  onRefresh,
  scrollEnabled,
  keyboardShouldPersistTaps
}: ScrollViewProps) => {
  return (
    <ScrollV
      refreshControl={
        onRefresh ? <RefreshControl colors={[THEME_DEFAULT.primary]} refreshing={refreshing || false} onRefresh={onRefresh} /> : undefined
      }
      contentContainerStyle={[styles.content, contentContainerStyle]}
      {...{ horizontal, style, scrollEnabled, keyboardShouldPersistTaps }}>
      {children}
    </ScrollV>
  );
};

export default ScrollView;
