// Libs
import React from 'react';
import { StyleSheet, StyleProp, TextStyle } from 'react-native';

// Atoms
import TextField, { TextFieldProps } from '@atoms/textFields';

// Constants
import { NUMBER_LINE, CHARACTER_LENGTH, THEME_DEFAULT } from '@constantsApp';

// Props
type TextAreaProps =
  | {
      multiline?: boolean;
      numberOfLines?: number;
      maxLength?: number;
      style?: StyleProp<TextStyle>;
    }
  | TextFieldProps;

const styles = StyleSheet.create({
  text: {
    textAlignVertical: 'top',
    backgroundColor: THEME_DEFAULT.textArea,
    height: 100
  }
});

const TextArea = ({
  multiline = true,
  numberOfLines = NUMBER_LINE.textArea,
  maxLength = CHARACTER_LENGTH.textArea,
  style,
  ...props
}: TextAreaProps) => {
  return <TextField style={[styles.text, style]} {...props} {...{ multiline, numberOfLines, maxLength }} />;
};

export default TextArea;
