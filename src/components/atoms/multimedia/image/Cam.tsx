import React, { useContext, useRef, useState } from 'react';
import { StyleSheet, TouchableOpacity, Image, View } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { RNCamera, TakePictureResponse } from 'react-native-camera';
import Modal from 'react-native-modal';

/* Config */
import { EnumToast, THEME_DEFAULT } from '@config/Constants';
import { RootContext } from 'configs/ContextApp';

/* Locales */
import { translate } from 'locales';

type CameraModalProps = {
  isShow: boolean;
  onClose: (value?: boolean) => void;
  onPhoto?: (photo: TakePictureResponse) => void;
};

const URL_PATH = 'src/components/atoms/multimedia/image/Cam.tsx';

const CameraModal = ({ isShow, onClose, onPhoto }: CameraModalProps) => {
  const refCamera = useRef<RNCamera>(null);
  const [photo, setPhoto] = useState<TakePictureResponse | null>(null);
  const [flashMode, setFlashMode] = useState<'auto' | 'on' | 'off' | 'torch'>('auto');
  const { showToast } = useContext(RootContext);

  const onCapture = async () => {
    if (refCamera && refCamera.current) {
      const data = await refCamera.current.takePictureAsync({ base64: true, quality: 0.4, width: 1080 });
      if (data && onPhoto) {
        if (data.base64) {
          const x = data.base64.length * (3 / 4) - 2;
          const fileSize = data.base64.length * (3 / 4) - 2;
          if (fileSize < 600000) {
            setPhoto(data);
          } else {
            showToast({ text: translate('CAM_ATOM.FILE_SIZE_BIGGER'), type: EnumToast.error });
          }
        }
      }
    }
  };

  const close = () => {
    onClose();
  };

  const cancel = () => {
    setPhoto(null);
  };

  const ok = () => {
    if (onPhoto && photo) {
      setPhoto(null);
      onPhoto(photo);
    }
  };

  return (
    <Modal isVisible={isShow} coverScreen={true} onSwipeComplete={() => onClose()} swipeDirection='down' onBackdropPress={() => onClose()}>
      <View style={styles.container}>
        {!photo ? (
          <RNCamera
            ref={refCamera}
            style={styles.camera}
            type={RNCamera.Constants.Type.back}
            flashMode={flashMode}
            androidCameraPermissionOptions={{
              title: 'Permission to use camera',
              message: 'We need your permission to use your camera',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel'
            }}
            androidRecordAudioPermissionOptions={{
              title: 'Permission to use audio recording',
              message: 'We need your permission to use your audio',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel'
            }}
            captureAudio={false}>
            <View style={{ position: 'absolute', bottom: 10, width: '100%', justifyContent: 'center' }}>
              <TouchableOpacity onPress={() => onCapture()} style={[styles.capture, styles.cam]}>
                <FontAwesome name='camera' size={25} color={THEME_DEFAULT.white} />
              </TouchableOpacity>
            </View>
            <View style={{ position: 'absolute', top: -15, right: -30 }}>
              <TouchableOpacity onPress={() => close()} style={[styles.capture, styles.close]}>
                <FontAwesome name='close' size={20} color={THEME_DEFAULT.white} />
              </TouchableOpacity>
            </View>
            <View style={{ position: 'absolute', top: 25, width: '100%', justifyContent: 'center', left: 10 }}>
              <TouchableOpacity onPress={() => setFlashMode(flashMode === 'on' || flashMode === 'auto' ? 'off' : 'on')}>
                {
                  flashMode === 'on' || flashMode === 'auto' ?
                  <Ionicons name='flash' size={20} color={THEME_DEFAULT.white} />
                  :
                  <Ionicons name='flash-off' size={20} color={THEME_DEFAULT.white} />
                }
              </TouchableOpacity>
            </View>
          </RNCamera>
        ) : (
          <View>
            <Image source={{ uri: photo?.uri }} style={{ height: '100%', width: '100%' }} />
            <View style={{ position: 'absolute', top: -15, right: -30 }}>
              <TouchableOpacity onPress={() => close()} style={[styles.capture, styles.close]}>
                <FontAwesome name='close' size={20} color={THEME_DEFAULT.white} />
              </TouchableOpacity>
            </View>
            <View style={{ position: 'absolute', bottom: 10, width: '100%', justifyContent: 'space-around', flexDirection: 'row' }}>
              <TouchableOpacity onPress={() => cancel()} style={[styles.capture, styles.cancel]}>
                <FontAwesome name='close' size={20} color={THEME_DEFAULT.white} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => ok()} style={[styles.capture, styles.ok]}>
                <FontAwesome name='check' size={20} color={THEME_DEFAULT.white} />
              </TouchableOpacity>
            </View>
          </View>
        )}
      </View>
    </Modal>
  );
};

export default CameraModal;

const styles = StyleSheet.create({
  container: {
    padding: 0,
    margin: 0,
    width: '100%',
    height: '100%',
    backgroundColor: THEME_DEFAULT.black
  },
  camera: {
    flex: 0,
    width: '100%',
    height: '100%'
  },
  capture: {
    flex: 0,
    backgroundColor: 'rgba(0,0,0,0.3)',
    alignSelf: 'center',
    justifyContent: 'center',
    margin: 20,
    alignItems: 'center'
  },
  cam: {
    borderRadius: 70 / 2,
    height: 70,
    width: 70,
    padding: 15,
    paddingHorizontal: 20
  },
  close: {
    borderRadius: 50 / 2,
    height: 50,
    width: 50
  },
  cancel: {
    borderRadius: 50 / 2,
    height: 50,
    width: 50
  },
  ok: {
    borderRadius: 50 / 2,
    height: 50,
    width: 50,
    backgroundColor: THEME_DEFAULT.primary
  }
});
