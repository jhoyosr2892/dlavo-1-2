// Libs
import React, { useEffect, useState } from 'react';
import { StyleProp, ImageStyle, ImageResizeMode, StyleSheet, ViewStyle } from 'react-native';

// Atoms
import View from '@atoms/views';
import Image from '@atoms/images';
import SvgIconButton from '@atoms/buttons/SvgIcon';

// Config
import { ImageSource } from '@config/Types';
import { TypeLibraryIconsSystem } from '@enum';

// Helpers
import { lockScreenDevice, OrientationDevice } from '@helpers/Device';
//import { CLog } from '@helpers/Message';

// Constants
import { NAME_ICON, METRICS, THEME_DEFAULT } from '@constantsApp';

//const URL_PATH = 'src/components/atoms/multimedia/image/index.tsx';

// Props
type ImageInteractionProps = {
  source?: ImageSource;
  resizeMode?: ImageResizeMode;
  /** Style de la imagen **/
  style?: StyleProp<ImageStyle>;
  /** Style del container **/
  styleContainer?: StyleProp<ViewStyle>;
  /** si tiene la opción de Pantalla completa **/
  fullScreen?: boolean;
};

// Styles
const styles = StyleSheet.create({
  container: {
    backgroundColor: THEME_DEFAULT.black,
    width: '100%',
    height: '100%'
  },
  containerFullscreen: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: METRICS.ELEVATION + 1,
    height: '100%'
  },
  image: {
    width: '100%',
    height: '100%'
  },
  viewOptionsTop: {},
  viewOptionsCenter: {
    flex: 1
  },
  viewOptionsBottom: {},
  viewOptions: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    padding: METRICS.CONSTANTS_PADDING
  },
  svgOptions: { alignSelf: 'flex-end' },
  contentModal: {
    backgroundColor: THEME_DEFAULT.black,
    width: '100%',
    height: '100%'
  }
});

const ImageInteraction = ({ source, styleContainer, style, resizeMode, fullScreen }: ImageInteractionProps) => {
  const [isVertical, setIsVertical] = useState(true);
  const [isFullScreen, setIsFullScreen] = useState(false);

  const handleRotation = () => {
    setIsVertical(!isVertical);
    if (isVertical) {
      lockScreenDevice(OrientationDevice.landscapeLeft);
    } else {
      lockScreenDevice(OrientationDevice.portrait);
    }
  };

  /**
   * @func handleFullScreen
   * @description se encarga de manejar el fullscreen de la pantalla y si esta
   * en pantalla completa y en posición horizontal al salir lo regresa a la
   * posición original vertical.
   */
  const handleFullScreen = () => {
    if (isFullScreen && !isVertical) {
      handleRotation();
    }
    setIsFullScreen(!isFullScreen);
  };

  useEffect(() => {
    return () => {
      lockScreenDevice();
    };
  }, []);

  return (
    <View style={[styles.container, styleContainer, isFullScreen ? styles.containerFullscreen : {}]}>
      {/** Renderiza la imagen **/}
      <Image style={[styles.image, style]} resizeMode={resizeMode || 'contain'} {...{ source }} />

      {/** Mascara de opciones **/}
      <View style={styles.viewOptions}>
        {/** Barra de opciones superior **/}
        <View style={styles.viewOptionsTop}>
          {/** botón que cambia la rotación del componente **/}
          {(!fullScreen || isFullScreen) && (
            <SvgIconButton
              onPress={() => handleRotation()}
              style={styles.svgOptions}
              icon={isVertical ? NAME_ICON.rotateLandspace : NAME_ICON.rotatePortrait}
              libraryIcon={TypeLibraryIconsSystem.materialCommunityIcons}
            />
          )}
        </View>

        {/** Me permite agregar un separador entre la barra de opciones superior
          y la inferior**/}
        <View style={styles.viewOptionsCenter} />

        {/** Barra de opciones inferior **/}
        <View style={styles.viewOptionsBottom}>
          {/** botón que cambia el estado de fullscreen **/}
          {fullScreen && (
            <SvgIconButton
              style={styles.svgOptions}
              libraryIcon={TypeLibraryIconsSystem.materialCommunityIcons}
              icon={isFullScreen ? NAME_ICON.fullScreenExit : NAME_ICON.fullScreen}
              onPress={() => handleFullScreen()}
            />
          )}
        </View>
      </View>
    </View>
  );
};

export default ImageInteraction;
