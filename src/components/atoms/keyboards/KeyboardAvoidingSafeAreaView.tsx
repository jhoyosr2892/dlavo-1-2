// Libs
import React, { ReactNode } from 'react';
import { KeyboardAvoidingView, StyleProp, ViewStyle, StyleSheet } from 'react-native';

// Atoms
import SafeAreaView from '@atoms/safeAreaViews';

// Constants
import { IS_ANDROID, METRICS } from '@constantsApp';

// Props
interface KeyboardAvoidingSafeAreaViewProps {
  children?: ReactNode;
  behavior?: 'height' | 'position' | 'padding';
  keyboardVerticalOffset?: number;
  enabled?: boolean;
  safeAreaView?: StyleProp<ViewStyle>;
  style?: StyleProp<ViewStyle> | null;
}

// Styles
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  safeAreaView: {
    flex: 1,
    width: '100%',
    height: '100%'
  }
});

const KeyboardAvoidingSafeAreaView = ({
  children,
  behavior,
  keyboardVerticalOffset,
  enabled,
  safeAreaView,
  style,
  ...props
}: KeyboardAvoidingSafeAreaViewProps) => {
  return (
    <KeyboardAvoidingView
      behavior={behavior || IS_ANDROID ? undefined : 'padding'}
      keyboardVerticalOffset={keyboardVerticalOffset || METRICS.KEYBOARD_VERTICAL_OFFSET}
      enabled={enabled || !IS_ANDROID}
      style={[styles.container, style]}
      {...props}>
      <SafeAreaView style={[styles.safeAreaView, safeAreaView]}>{children}</SafeAreaView>
    </KeyboardAvoidingView>
  );
};

export default KeyboardAvoidingSafeAreaView;
