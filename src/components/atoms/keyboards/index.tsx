// Libs
import { Keyboard } from 'react-native';

export const dismissKeyboard = () => Keyboard.dismiss();

export const addEventListenerKeyboard = (showEvent?: () => void, hideEvent?: () => void) => {
  if (showEvent) {
    Keyboard.addListener('keyboardDidShow', showEvent);
  }
  if (hideEvent) {
    Keyboard.addListener('keyboardDidHide', hideEvent);
  }
};

export const removeEventListenerKeyboard = (showEvent?: () => void, hideEvent?: () => void) => {
  if (showEvent) {
    Keyboard.removeListener('keyboardDidShow', showEvent);
  }
  if (hideEvent) {
    Keyboard.removeListener('keyboardDidHide', hideEvent);
  }
};
