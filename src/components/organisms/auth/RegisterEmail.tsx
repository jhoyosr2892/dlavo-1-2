import React, { useContext, useRef, useState } from 'react';
import { StyleSheet, TextInput } from 'react-native';

/* Components */
import KeyboardAvoidingSafeAreaView from '@atoms/keyboards/KeyboardAvoidingSafeAreaView';
import Text from '@atoms/texts';
import ViewCustom from '@atoms/views';
import TextField from '@atoms/textFields';
import Button from '@atoms/buttons';
/* Constants */
import { EnumToast, FAMILY_FONTS, INPUT_ACCESSORY_ID, METRICS, SIZE_FONTS, THEME_DEFAULT } from 'configs/Constants';
import { RootContext } from 'configs/ContextApp';
/* Locales */
import { translate } from 'locales';

// Props
interface RegisterEmailOrganismProps {
  sendEmail: (email: string) => void;
}

const RegisterEmailOrganism = ({ sendEmail }: RegisterEmailOrganismProps) => {
  const [valueEmail, setValueEmail] = useState<string | undefined>();
  const inputEmailRef = useRef<TextInput | null>(null);

  // Context
  const { showToast } = useContext(RootContext);

  const onSendEmail = () => {
    if (inputEmailRef && inputEmailRef.current) {
      inputEmailRef.current.blur();
    }
    if (valueEmail) {
      sendEmail(valueEmail);
    } else {
      showToast({ text: translate('REGISTER_EMAIL_SCENE.ERRORS.ENTER_EMAIL'), type: EnumToast.error });
      inputEmailRef.current?.focus();
    }
  };

  return (
    <KeyboardAvoidingSafeAreaView>
      <ViewCustom style={styles.viewContainer}>
        <ViewCustom style={{ marginTop: 50 }}>
          <TextField
            refInput={inputEmailRef}
            value={valueEmail}
            autoFocusTF={true}
            editable={true}
            autoCapitalize='none'
            placeholder={translate('REGISTER_EMAIL_SCENE.EMAIL_PLACEHOLDER')}
            textContentType={'emailAddress'}
            keyboardType={'email-address'}
            returnKeyType={'send'}
            // inputAccessoryId={INPUT_ACCESSORY_ID.down}
            onChange={(text: string) => setValueEmail(text)}
            onSubmitEditing={onSendEmail}
          />
          <Text style={{ marginTop: 30, fontSize: 16 }}>{translate('REGISTER_EMAIL_SCENE.HELP_TEXT')}</Text>
          <Button
            text={translate('REGISTER_EMAIL_SCENE.SEND_EMAIL_BUTTON')}
            textStyle={[styles.buttonLoginText, { marginLeft: 10 }]}
            style={styles.button}
            onPress={onSendEmail}
          />
        </ViewCustom>
      </ViewCustom>
    </KeyboardAvoidingSafeAreaView>
  );
};

export default RegisterEmailOrganism;

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    padding: METRICS.CONSTANTS_PADDING
  },
  button: {
    flexDirection: 'row-reverse',
    backgroundColor: THEME_DEFAULT.primary,
    fontSize: 10,
    padding: METRICS.PADDING_MODAL_ALERT,
    marginVertical: 10,
    marginTop: 50
  },
  buttonLoginText: {
    fontFamily: FAMILY_FONTS.primarySemiBold,
    fontWeight: 'bold',
    fontSize: SIZE_FONTS.primary
  }
});
