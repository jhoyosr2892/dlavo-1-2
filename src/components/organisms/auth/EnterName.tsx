import React, { useContext, useRef, useState } from 'react';
import { StyleSheet, Text, TextInput } from 'react-native';

/* Components */
import KeyboardAvoidingSafeAreaView from '@atoms/keyboards/KeyboardAvoidingSafeAreaView';
import ViewCustom from '@atoms/views';
import TextField from '@atoms/textFields';
import Button from '@atoms/buttons';
/* Constants */
import { EnumToast, FAMILY_FONTS, INPUT_ACCESSORY_ID, METRICS, SIZE_FONTS, THEME_DEFAULT } from 'configs/Constants';
import { RootContext } from 'configs/ContextApp';
import { translate } from 'locales';

// Props
interface EnterNameOrganismProps {
  sendName: (name: string, lastName: string) => void;
}

const EnterNameOrganism = ({ sendName }: EnterNameOrganismProps) => {
  /* Ref */
  const inputNameRef = useRef<TextInput | null>(null);
  const inputLastNameRef = useRef<TextInput | null>(null);
  /* States */
  const [valueName, setValueName] = useState<string | undefined>();
  const [valueLastName, setValueLastName] = useState<string | undefined>();
  const [blurInput, setBlurInput] = useState(false);

  // Context
  const { showToast } = useContext(RootContext);

  const onSendName = () => {
    if (valueName && valueLastName) {
      inputLastNameRef.current?.blur();
      sendName(valueName, valueLastName);
    } else {
      showToast({ text: translate('ENTER_NAME_SCENE.HEADER'), type: EnumToast.error });
      inputNameRef.current?.focus();
    }
  };

  return (
    <KeyboardAvoidingSafeAreaView>
      <ViewCustom style={styles.viewContainer}>
        <ViewCustom style={{ marginTop: 50 }}>
          <Text style={{ marginBottom: 10 }}>{translate('ENTER_NAME_SCENE.NAME_PLACEHOLDER')}</Text>
          <TextField
            refInput={inputNameRef}
            value={valueName}
            autoFocusTF={true}
            editable={true}
            autoCapitalize='words'
            placeholder={translate('ENTER_NAME_SCENE.NAME_PLACEHOLDER')}
            textContentType={'name'}
            returnKeyType={'send'}
            inputAccessoryId={INPUT_ACCESSORY_ID.down}
            containerStyle={{ marginBottom: 25 }}
            onChange={(text: string) => setValueName(text)}
            onSubmitEditing={() => inputLastNameRef.current?.focus()}
            blurOnSubmit={blurInput}
          />
          <Text style={{ marginBottom: 10 }}>{translate('ENTER_NAME_SCENE.LAST_NAME_PLACEHOLDER')}</Text>
          <TextField
            refInput={inputLastNameRef}
            value={valueLastName}
            editable={true}
            autoCapitalize='words'
            placeholder={translate('ENTER_NAME_SCENE.LAST_NAME_PLACEHOLDER')}
            textContentType={'name'}
            returnKeyType={'send'}
            inputAccessoryId={INPUT_ACCESSORY_ID.down}
            onChange={(text: string) => setValueLastName(text)}
            onSubmitEditing={onSendName}
            blurOnSubmit={blurInput}
          />
          <Button
            text={translate('GENERAL.BUTTONS.NEXT')}
            textStyle={[styles.buttonLoginText, { marginLeft: 10 }]}
            style={styles.button}
            onPress={onSendName}
          />
        </ViewCustom>
      </ViewCustom>
    </KeyboardAvoidingSafeAreaView>
  );
};

export default EnterNameOrganism;

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    padding: METRICS.CONSTANTS_PADDING
  },
  button: {
    flexDirection: 'row-reverse',
    backgroundColor: THEME_DEFAULT.primary,
    fontSize: 10,
    padding: METRICS.PADDING_MODAL_ALERT,
    marginVertical: 10,
    marginTop: 50
  },
  buttonLoginText: {
    fontFamily: FAMILY_FONTS.primarySemiBold,
    fontWeight: 'bold',
    fontSize: SIZE_FONTS.primary
  }
});
