// Libs
import React, { useEffect, useState, useContext } from 'react';
import { LayoutChangeEvent, StyleSheet } from 'react-native';

// Components
import Button from '@atoms/buttons';
import Image, { dimensionSourceImageWithBase } from '@atoms/images';
import KeyboardAvoidingSafeAreaView from '@atoms/keyboards/KeyboardAvoidingSafeAreaView';
import View from '@atoms/views';
import Text from '@atoms/texts';
import TouchableO from '@atoms/buttons/TouchableOpacity';
import SvgCss from '@atoms/svgs/SvgCss';

// Contexts
import { RootContext } from '@config/ContextApp';

// Helpers
import { onLayout } from '@helpers/Convert';
import { navigationScreen } from '@helpers/Navigation';

// Constants
import { ASSETS, METRICS, THEME_DEFAULT, EnumFont, FAMILY_FONTS, SIZE_FONTS, NAME_ROUTE } from '@constantsApp';
import { TypeLibraryIconsSystem } from 'configs/Enum';
import { translate } from 'locales';

const URL_PATH = 'src/components/organisms/auth/index.tsx';

// Props
interface AuthOrganismProps {
  login: (email: string, password: string) => void;
}

const AuthOrganism = ({}: AuthOrganismProps) => {
  const [imageResolvedAssetSource, setImageResolvedAssetSource] = useState({ height: 0, width: 0 });
  const [dimensionView, setDimensionView] = useState({ height: 0, width: 0 });

  // Context
  const { showToast } = useContext(RootContext);

  const loadDimensionLogo = () => {
    const { h, w } = dimensionSourceImageWithBase(ASSETS.logo, METRICS.DEVICE_WIDTH - METRICS.CONSTANTS_MARGIN * 4);
    setImageResolvedAssetSource({ height: h, width: w });
  };

  const onLayoutContainer = (e: LayoutChangeEvent) => {
    setDimensionView(onLayout(e));
  };

  useEffect(() => {
    loadDimensionLogo();
  }, []);

  return (
    <KeyboardAvoidingSafeAreaView>
      <View style={styles.viewContainer}>
        <View style={styles.imageView}>
          <Image
            resizeMode={'contain'}
            source={ASSETS.logo}
            style={{
              width: imageResolvedAssetSource.width,
              height: imageResolvedAssetSource.height,
              flex: 1
            }}
          />
        </View>
        <View style={styles.buttons} onLayout={onLayoutContainer}>
          <Button
            text={translate('AUTH_SCENE.SIGN_IN_WHIT_FACEBOOK')}
            textStyle={[styles.buttonLoginText, { marginLeft: 10 }]}
            icon={'facebook-f'}
            libraryIcon={TypeLibraryIconsSystem.fontAwesome}
            style={styles.loginButton}
          />
          <Button
            text={translate('AUTH_SCENE.SIGN_IN_WHIT_APPLE')}
            textStyle={[styles.buttonLoginText, { marginHorizontal: 20 }]}
            icon={'apple'}
            libraryIcon={TypeLibraryIconsSystem.fontAwesome}
            style={styles.loginButton}
          />
          <Button
            text={translate('AUTH_SCENE.EMAIL')}
            textStyle={[styles.buttonLoginText, { marginHorizontal: 76 }]}
            onPress={() => navigationScreen(NAME_ROUTE.registerEmail)}
            icon={'email'}
            libraryIcon={TypeLibraryIconsSystem.materialCommunityIcons}
            style={styles.loginButton}
          />
        <TouchableO style={{ marginBottom: 20 }} onPress={() => navigationScreen(NAME_ROUTE.privacy)}>
          <Text style={{ textAlign: 'center', fontFamily: FAMILY_FONTS.primaryRegular, fontSize: 20, textDecorationLine: 'underline', textDecorationColor: THEME_DEFAULT.primary }}>
            {translate('AUTH_SCENE.PRIVACY')}
          </Text>
        </TouchableO>
          {/* Oval back images */}
          <SvgCss xml={ASSETS.oval99} height={99} width={99} style={[styles.ovalBackImage, { top: 0 }]} />
          <SvgCss xml={ASSETS.oval30} height={30} width={30} style={[styles.ovalBackImage, { top: '24%' }]} />
          <SvgCss xml={ASSETS.oval30} height={30} width={30} style={[styles.ovalBackImage, { bottom: 0, left: '28%' }]} />
          <SvgCss xml={ASSETS.oval30} height={30} width={30} style={[styles.ovalBackImage, { bottom: '10%', right: '10%' }]} />
          <SvgCss xml={ASSETS.oval46} height={46} width={46} style={[styles.ovalBackImage, { top: '2%', right: '26%' }]} />
          <SvgCss xml={ASSETS.oval62} height={62} width={62} style={[styles.ovalBackImage, { top: '20%', right: -10 }]} />
          <SvgCss xml={ASSETS.oval62} height={62} width={62} style={[styles.ovalBackImage, { top: '55%', right: -10 }]} />
          <SvgCss xml={ASSETS.oval268} height={268} width={268} style={[styles.ovalBackImage, { top: '29%' }]} />
        </View>
      </View>
      <View style={styles.footer}>
        <TouchableO style={{ marginBottom: 20 }} onPress={() => navigationScreen(NAME_ROUTE.login)}>
          <Text style={{ textAlign: 'center', fontFamily: FAMILY_FONTS.primaryRegular, fontSize: 20 }}>
            {translate('AUTH_SCENE.HAVE_AN_ACCOUNT')}
          </Text>
        </TouchableO>
        <TouchableO style={{ backgroundColor: THEME_DEFAULT.secondary, height: 50, justifyContent: 'center' }}>
          <Text style={{ textAlign: 'center', fontWeight: 'bold', color: THEME_DEFAULT.white }} type={EnumFont.eleventh}>
            {translate('AUTH_SCENE.JOIN_AS_DLAVO_PARTNER')}
          </Text>
        </TouchableO>
      </View>
    </KeyboardAvoidingSafeAreaView>
  );
};

// Styles
const styles = StyleSheet.create({
  viewContainer: {
    flex: 0.81,
    padding: METRICS.CONSTANTS_PADDING
  },
  imageView: {
    flex: 0.24,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttons: {
    flex: 0.76,
    width: '100%',
    justifyContent: 'center'
  },
  footer: {
    flex: 0.19,
    justifyContent: 'flex-end'
  },
  loginButton: {
    flexDirection: 'row-reverse',
    backgroundColor: THEME_DEFAULT.primary,
    fontSize: 10,
    padding: METRICS.PADDING_MODAL_ALERT,
    marginVertical: 10
  },
  buttonLoginText: {
    fontFamily: FAMILY_FONTS.primarySemiBold,
    fontWeight: 'bold',
    fontSize: SIZE_FONTS.primary
  },
  ovalBackImage: {
    position: 'absolute',
    opacity: 0.7,
    zIndex: -1
  }
});

export default AuthOrganism;
