import React, { useContext, useRef, useState } from 'react';
import { StyleSheet, TextInput } from 'react-native';

/* Components */
import KeyboardAvoidingSafeAreaView from '@atoms/keyboards/KeyboardAvoidingSafeAreaView';
import Text from '@atoms/texts';
import TextField from '@atoms/textFields';
import Button from '@atoms/buttons';
import ViewAtom from '@atoms/views';
/* Constants */
import { EnumToast, FAMILY_FONTS, INPUT_ACCESSORY_ID, METRICS, SIZE_FONTS, THEME_DEFAULT } from 'configs/Constants';
import { RootContext } from 'configs/ContextApp';
import { translate } from 'locales';

// Props
interface props {
  login: (email: string) => void;
}

const LoginOrganism = ({ login }: props) => {
  /* States */
  const [valueEmail, setValueEmail] = useState<string | undefined>();

  /* Ref */
  const inputEmailRef = useRef<TextInput | null>(null);

  // Context
  const { showToast } = useContext(RootContext);

  const onSendEmail = () => {
    if (!valueEmail) {
      showToast({ text: translate('LOGIN_SCENE.ERRORS.EMAIL_IS_REQUIRED'), type: EnumToast.error });
      inputEmailRef.current?.focus();
      return;
    }
    if (valueEmail) {
      login(valueEmail);
    }
  };

  return (
    <KeyboardAvoidingSafeAreaView>
      <ViewAtom style={styles.viewContainer}>
        <ViewAtom style={{ marginTop: 50 }}>
          <Text>{translate('LOGIN_SCENE.EMAIL')}:</Text>
          <TextField
            refInput={inputEmailRef}
            value={valueEmail}
            autoFocusTF={true}
            editable={true}
            autoCapitalize='none'
            placeholder={translate('LOGIN_SCENE.ENTER_EMAIL_PLACEHOLDER')}
            textContentType={'emailAddress'}
            keyboardType={'email-address'}
            returnKeyType={'next'}
            containerStyle={styles.textInput}
            // inputAccessoryId={INPUT_ACCESSORY_ID.in}
            onChange={(text: string) => setValueEmail(text)}
            onSubmitEditing={onSendEmail}
          />
          {/* <Text>{translate('LOGIN_SCENE.PASSWORD')}:</Text>
          <TextField
            refInput={inputPasswordRef}
            value={valuePassword}
            editable={true}
            autoCapitalize='none'
            placeholder={translate('LOGIN_SCENE.ENTER_PASSWORD')}
            textContentType={'password'}
            secureTextEntry={true}
            returnKeyType={'done'}
            containerStyle={styles.textInput}
            inputAccessoryId={INPUT_ACCESSORY_ID.in}
            onChange={(text: string) => setValuePassword(text)}
          /> */}
          {/* <Text style={{ marginTop: 30, fontSize: 16 }}>You will receive a EMAIL with a confirmation number.</Text> */}
          <Button
            text={translate('LOGIN_SCENE.LOGIN_BUTTON')}
            textStyle={[styles.buttonLoginText, { marginLeft: 10 }]}
            style={styles.button}
            onPress={onSendEmail}
          />
        </ViewAtom>
      </ViewAtom>
    </KeyboardAvoidingSafeAreaView>
  );
};

export default LoginOrganism;

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    padding: METRICS.CONSTANTS_PADDING
  },
  textInput: {
    marginBottom: 20,
    marginTop: 5
  },
  button: {
    flexDirection: 'row-reverse',
    backgroundColor: THEME_DEFAULT.primary,
    fontSize: 10,
    padding: METRICS.PADDING_MODAL_ALERT,
    marginVertical: 10,
    marginTop: 50
  },
  buttonLoginText: {
    fontFamily: FAMILY_FONTS.primarySemiBold,
    fontWeight: 'bold',
    fontSize: SIZE_FONTS.primary
  }
});
