import React, { useContext, useEffect, useRef, useState } from 'react';
import { StyleSheet, TextInput } from 'react-native';

/* Components */
import KeyboardAvoidingSafeAreaView from '@atoms/keyboards/KeyboardAvoidingSafeAreaView';
import Text from '@atoms/texts';
import ViewCustom from '@atoms/views';
import Button from '@atoms/buttons';
/* Constants */
import { EnumFont, EnumToast, FAMILY_FONTS, METRICS, SIZE_FONTS, THEME_DEFAULT } from 'configs/Constants';
import { RootContext } from 'configs/ContextApp';
import InputCode from 'components/atoms/textFields/InputCode';
import TouchableO from 'components/atoms/buttons/TouchableOpacity';
import { translate } from 'locales';

// Props
interface EnterCodeOrganismProps {
  sendCode: (email: string) => void;
  reSendCode: () => void;
}

const EnterCodeOrganism = ({ sendCode, reSendCode }: EnterCodeOrganismProps) => {
  /* State */
  const [valueCode, setValueCode] = useState<string | undefined>();
  const [disableResend, setDisableResend] = useState<boolean>(true);
  const [resentTimeout, setResentTimeout] = useState<number>(30);

  // Context
  const { showToast } = useContext(RootContext);

  useEffect(() => {
    const loadData = () => {
      const some = setTimeout(() => {
        setDisableResend(false);
      }, 30000);
      // setInterval(() => {
      //   setResentTimeout(resentTimeout - 1);
      //   if(resentTimeout === 0){
      //     // clearInterval()
      //   }
      // }, 30000);
    };
    return loadData();
  }, []);

  const onSendCode = () => {
    if (valueCode && valueCode.length === 4) {
      sendCode(valueCode);
    } else {
      showToast({ text: translate('ENTER_CODE_SCENE.ERRORS.ENTER_CODE'), type: EnumToast.error });
    }
  };
  const onReSendCode = () => {
    reSendCode();
    setDisableResend(true);
    setTimeout(() => {
      setDisableResend(false);
    }, 30000);
  };

  return (
    <KeyboardAvoidingSafeAreaView>
      <ViewCustom style={styles.viewContainer}>
        <ViewCustom style={{ marginTop: 50 }}>
          {/* Code */}
          <ViewCustom style={{ flexDirection: 'row' }}>
            <InputCode codeLength={4} setValueCode={setValueCode} onSubmitEditing={onSendCode} />
          </ViewCustom>
          {/* Help text */}
          <ViewCustom style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'center' }}>
            <Text style={{ fontSize: 20, textAlign: 'center' }}>{translate('ENTER_CODE_SCENE.HELP_TEXT')}</Text>
            <TouchableO onPress={onReSendCode} disabled={disableResend}>
              <Text
                style={{
                  fontSize: 20,
                  color: disableResend ? THEME_DEFAULT.primaryOpacity : THEME_DEFAULT.primary,
                  borderBottomColor: disableResend ? THEME_DEFAULT.primaryOpacity : THEME_DEFAULT.primary,
                  borderBottomWidth: 1
                }}
                type={EnumFont.eleventh}>
                {translate('ENTER_CODE_SCENE.RESEND_BUTTON')}
              </Text>
            </TouchableO>
          </ViewCustom>
          {/* Button */}
          <Button
            text={translate('GENERAL.BUTTONS.NEXT')}
            textStyle={[styles.buttonLoginText, { marginLeft: 10 }]}
            style={styles.button}
            onPress={onSendCode}
          />
        </ViewCustom>
      </ViewCustom>
    </KeyboardAvoidingSafeAreaView>
  );
};

export default EnterCodeOrganism;

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    padding: METRICS.CONSTANTS_PADDING
  },
  tiContainer: {
    width: 50,
    height: 50
  },
  button: {
    flexDirection: 'row-reverse',
    backgroundColor: THEME_DEFAULT.primary,
    fontSize: 10,
    padding: METRICS.PADDING_MODAL_ALERT,
    marginVertical: 10,
    marginTop: 50
  },
  buttonLoginText: {
    fontFamily: FAMILY_FONTS.primarySemiBold,
    fontWeight: 'bold',
    fontSize: SIZE_FONTS.primary
  }
});
