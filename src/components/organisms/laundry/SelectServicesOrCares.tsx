import React, { useState } from 'react';
import { StyleSheet, Text } from 'react-native';
/* Libs */
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
/* Components */
import BottomButtonAtom from 'components/atoms/buttons/BottomButton';
import ScrollView from 'components/atoms/scrollViews';
import SwitchAtom from 'components/atoms/switch';
import ViewAtom from 'components/atoms/views';
/* Config */
import { THEME_DEFAULT } from 'configs/Constants';
/* Models */
import { SPECIAL_SERVICE_CARES, SPECIAL_SERVICE_CARES_NAMES } from 'models/enums/special-services-cares';
/* Styles */
import { stylesGeneral } from 'styles';
import { SOAPS, SOAP_TYPES_NAMES } from 'models/enums/SoapTypes';
import { translate } from 'locales';

type ServicesOrCaresSelectOrganismProps = {
  addService: (services: SPECIAL_SERVICE_CARES[], soaps: SOAPS[]) => Promise<void>;
};

const SelectServicesOrCaresOrganism = ({ addService }: ServicesOrCaresSelectOrganismProps) => {
  const [services, setServices] = useState<SPECIAL_SERVICE_CARES[]>([]);
  const [soaps, setSoaps] = useState<SOAPS[]>([]);

  const updateSelectedServices = (active: boolean, value: SPECIAL_SERVICE_CARES) => {
    const array = [...services];
    if (active) {
      array.push(value);
    } else {
      const index = array.findIndex(e => e === value);
      if (index !== -1) {
        array.splice(index, 1);
      }
    }
    setServices(array);
  };

  const updateSelectedSoaps = (active: boolean, value: SOAPS) => {
    // const array = [...soaps];
    // if (active) {
    //   array.push(value);
    // } else {
    //   const index = array.findIndex(e => e === value);
    //   if (index !== -1) {
    //     array.splice(index, 1);
    //   }
    // }
    // setSoaps(array);
  };

  return (
    <ViewAtom style={stylesGeneral.viewContainerRootContent}>
      <ScrollView style={styles.viewContainer}>
        <ViewAtom style={{ paddingVertical: 15 }}>
          <Text>* {translate('SELECT_SPECIAL_SERVICES_OR_CARES.SOAPS')}</Text>
        </ViewAtom>
        {/* {SOAP_TYPES_NAMES.map((service, i) => (
          <ViewAtom style={styles.card} key={i.toString()}>
            <ViewAtom style={{ flex: 0.75 }}>
              <Text style={styles.text}>{service.name}</Text>
            </ViewAtom>
            <ViewAtom style={{ flex: 0.25, flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text style={styles.text}>{service.price > 0 ? '$' + service.price : translate('SELECT_SPECIAL_SERVICES_OR_CARES.FREE')}</Text>
              <SwitchAtom onSwitchPress={value => updateSelectedSoaps(value, service.id)} valueSelected={soaps[i] ? true : false} />
            </ViewAtom>
          </ViewAtom>
        ))} */}
        <RadioForm animation={true}>
          {/* To create radio buttons, loop through your array of options */}
          {SOAP_TYPES_NAMES.map((obj, i) => (
            <RadioButton key={i} style={{ marginVertical: 10 }}>
              <ViewAtom style={{ flex: 0.75, justifyContent: 'center' }}>
                <RadioButtonLabel
                  obj={obj}
                  index={i}
                  onPress={val => setSoaps([val])}
                  labelStyle={styles.text}
                  labelWrapStyle={{ width: '100%' }}
                />
              </ViewAtom>
              <ViewAtom style={{ flex: 0.25, flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={[styles.text, { textAlignVertical: 'center' }]}>
                  {obj.price > 0 ? '$' + obj.price : translate('SELECT_SPECIAL_SERVICES_OR_CARES.FREE')}
                </Text>
                <RadioButtonInput
                  obj={obj}
                  index={i}
                  isSelected={soaps[0] === obj.id}
                  onPress={val => setSoaps([val])}
                  // borderWidth={1}
                  buttonInnerColor={THEME_DEFAULT.primary}
                  buttonOuterColor={soaps[0] === obj.id ? THEME_DEFAULT.primary : 'gray'}
                  buttonSize={20}
                  // buttonOuterSize={80}
                  // buttonStyle={{}}
                />
              </ViewAtom>
            </RadioButton>
          ))}
        </RadioForm>
        {/* <ViewAtom style={{width: '100%', backgroundColor: 'red',}}>
        </ViewAtom> */}
        <ViewAtom style={{ paddingVertical: 15 }}>
          <Text>{translate('SELECT_SPECIAL_SERVICES_OR_CARES.ADDITIONAL_SERVICES')}</Text>
        </ViewAtom>
        {SPECIAL_SERVICE_CARES_NAMES.map((service, i) => (
          <ViewAtom style={styles.card} key={i.toString()}>
            <ViewAtom style={{ flex: 0.75 }}>
              <Text style={styles.text}>{service.name}</Text>
            </ViewAtom>
            <ViewAtom style={{ flex: 0.25, flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text style={styles.text}>{service.price > 0 ? '$' + service.price : 'Free'}</Text>
              <SwitchAtom onSwitchPress={value => updateSelectedServices(value, service.id)} />
            </ViewAtom>
          </ViewAtom>
        ))}
      </ScrollView>
      <BottomButtonAtom
        text={translate('GENERAL.BUTTONS.NEXT')}
        disabled={soaps.length === 0}
        onPress={() => addService(services, soaps)}
      />
    </ViewAtom>
  );
};

export default SelectServicesOrCaresOrganism;

const styles = StyleSheet.create({
  viewContainer: {
    padding: 0,
    paddingTop: 30,
    paddingBottom: 75,
    backgroundColor: THEME_DEFAULT.content
  },
  card: {
    borderBottomColor: THEME_DEFAULT.borderLine,
    borderBottomWidth: 1,
    flexDirection: 'row',
    paddingVertical: 15
  },
  text: {
    color: 'gray',
    fontSize: 16
  }
});
