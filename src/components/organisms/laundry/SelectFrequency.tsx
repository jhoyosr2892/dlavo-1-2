import React from 'react';
import { StyleSheet } from 'react-native';

/* Config */
import { ASSETS, METRICS, NAME_ROUTE, SIZE_FONTS, THEME_DEFAULT } from 'configs/Constants';
/* Components */
import TouchableO from 'components/atoms/buttons/TouchableOpacity';
import View from 'components/atoms/views';
import Text from '@atoms/texts';
import { navigationScreen } from 'utils/helpers/Navigation';
import SvgCss from 'components/atoms/svgs/SvgCss';
import { translate } from 'locales';
import { Frequency, ServiceModel } from 'models/service';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { onSelectFrequency } from 'stores/ServiceState';

type SelectFrequencyOrganismProps = {
  data: ServiceModel;
};

const SelectFrequencyOrganism = ({ data }: SelectFrequencyOrganismProps) => {
  const onPressNext = async (frequency: Frequency) => {
    onSelectFrequency(frequency, data);
  };

  return (
    <View style={styles.viewContainer}>
      <TouchableO style={styles.button} onPress={() => onPressNext('single')}>
        <View style={{ flex: 0.75 }}>
          <Text style={styles.title}>{translate('SELECT_FREQUENCY_SCENE.SINGLE_SERVICE')}</Text>
          <Text style={styles.subTitle}>{translate('SELECT_FREQUENCY_SCENE.SINGLE_SERVICE_NOTE')}</Text>
        </View>
        <View style={{ flex: 0.25 }}>
          <SvgCss xml={ASSETS.singleService} />
        </View>
      </TouchableO>
      <TouchableO style={styles.button} onPress={() => onPressNext('frequent')}>
        <View style={{ flex: 0.75 }}>
          <Text style={styles.title}>{translate('SELECT_FREQUENCY_SCENE.FREQUENCY_SERVICE')}</Text>
          <Text style={styles.subTitle}>{translate('SELECT_FREQUENCY_SCENE.FREQUENCY_SERVICE_NOTE')}</Text>
        </View>
        <View style={{ flex: 0.25 }}>
          <SvgCss xml={ASSETS.frequentService} />
        </View>
      </TouchableO>
    </View>
  );
};

export default SelectFrequencyOrganism;

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    padding: METRICS.CONSTANTS_PADDING,
    backgroundColor: THEME_DEFAULT.white,
    paddingTop: 30
  },
  button: {
    flexDirection: 'row',
    width: '100%',
    height: 120,
    backgroundColor: THEME_DEFAULT.white,
    paddingHorizontal: 15,
    paddingVertical: 20,
    borderRadius: 15,
    marginVertical: 10,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 5
  },
  subTitle: {
    fontSize: SIZE_FONTS.secondary,
    color: THEME_DEFAULT.primaryOpacity
  },
  buttonImage: {
    height: '100%',
    width: '100%',
    resizeMode: 'cover',
    borderRadius: 5,
    opacity: 0.6
  }
});
