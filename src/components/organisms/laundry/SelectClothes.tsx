import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
/* Libs */
import uuid from 'react-native-uuid';
/* Models */
import { CLOTHES, typeClothes } from 'models/clothes';
/* Config */
import { ASSETS, THEME_DEFAULT } from 'configs/Constants';
import { TypeLibraryIconsSystem } from 'configs/Enum';
/* Components */
import ViewAtom from 'components/atoms/views';
import SvgCss from 'components/atoms/svgs/SvgCss';
import TouchableO from 'components/atoms/buttons/TouchableOpacity';
import IconCustom from 'components/atoms/icons';
import Image from 'components/atoms/images';
import ScrollView from 'components/atoms/scrollViews';
import BottomButtonAtom from 'components/atoms/buttons/BottomButton';
import { translate } from 'locales';
import { ServiceModel } from 'models/service';
import SpinnerLottie from 'components/atoms/spinners';

type SelectGarmentsOrganismProps = {
  data?: ServiceModel;
  onPressNext: (_selectedClothes: typeClothes[]) => void;
};

const SelectClothesOrganism = ({ data, onPressNext }: SelectGarmentsOrganismProps) => {
  /* State */
  const [clothes, setClothes] = useState<typeClothes[]>(CLOTHES);

  useEffect(() => {
    const loadData = () => {
      console.log(data);
      
      const _clothes = [...clothes];
      _clothes.forEach(_clothe => (_clothe.quantity = 0));
      if (data && data.clothes) {
        data.clothes.forEach((clothe, i) => {
          const _clothe = _clothes.find(b => b.type === clothe.type);
          if (_clothe) {
            _clothe.quantity = clothe.quantity as number;
          }
        });
      }
      setClothes(_clothes);
    };
    loadData();
  }, []);

  const addClothes = (index: number) => {
    const array = [...clothes];
    array[index].quantity++;
    setClothes(array);
    // const selectArray = [...selectedClothes];
    // const indexFound = selectArray.findIndex(i => i.name === array[index].name);
    // if (indexFound === -1) {
    //   selectArray.push(array[index]);
    // }
    // setSelectedClothes(selectArray);
  };
  const subClothes = (index: number) => {
    const array = [...clothes];
    if (array[index].quantity > 0) {
      array[index].quantity = array[index].quantity - 1;
      setClothes(array);
      // const selectArray = [...selectedClothes];
      // const indexFound = selectArray.findIndex(i => i.name === array[index].name);
      // if (indexFound !== -1) {
      //   if (selectArray[indexFound].quantity > 0) {
      //     // selectArray[indexFound].quantity = selectArray[indexFound].quantity - 1;
      //   } else {
      //     selectArray.splice(indexFound, 1);
      //   }
      // }
      // setSelectedClothes(selectArray);
    }
  };

  const _onPressNext = async () => {
    if (data) {
      const _selectedClothes: typeClothes[] = [];
      clothes.forEach(c => {
        if (c.quantity > 0) {
          _selectedClothes.push(c);
        }
      });
      if (data.clothes && data.clothes.length) {
        data.clothes.concat(_selectedClothes);
        // if (data.clothesInfo) {
        //   data.clothesInfo.concat(selectedClothes);
        // } else {
        //   data.clothesInfo = selectedClothes;
        // }
      } else {
        data.id = uuid.v4().toString();
        data.clothes = _selectedClothes;
      }
      onPressNext(_selectedClothes);
    }
  };

  return (
    <ViewAtom style={{ flex: 1 }}>
      {!data && (
        <View style={{ justifyContent: 'center', alignItems: 'center', width: '100%' }}>
          <SpinnerLottie />
        </View>
      )}
      {data && (
        <ScrollView contentContainerStyle={styles.viewContainer}>
          {clothes.map(
            (clothe, i) =>
              data.serviceCategory &&
              clothe.service.indexOf(data.serviceCategory) >= 0 && (
                <ViewAtom style={styles.card} key={i.toString()}>
                  <ViewAtom style={{ flex: 0.3 }}>
                    {clothe.image ? (
                      <SvgCss xml={clothe.image} height={53} fill={'#15224cBF'} />
                    ) : (
                      <Image source={ASSETS.noImage} style={{ height: 53, width: 53, alignSelf: 'center' }} />
                    )}
                  </ViewAtom>
                  <ViewAtom style={{ flex: 0.3, justifyContent: 'center' }}>
                    <Text>{clothe.name}</Text>
                  </ViewAtom>
                  <ViewAtom style={{ flex: 0.3, alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableO onPress={() => subClothes(i)}>
                      <IconCustom name='minus' library={TypeLibraryIconsSystem.simpleLineIcons} color={THEME_DEFAULT.primaryOpacity} />
                    </TouchableO>
                    <Text style={{ fontSize: 25 }}>{clothe.quantity}</Text>
                    <TouchableO onPress={() => addClothes(i)}>
                      <IconCustom name='plus' library={TypeLibraryIconsSystem.simpleLineIcons} color={THEME_DEFAULT.primaryOpacity} />
                    </TouchableO>
                  </ViewAtom>
                </ViewAtom>
              )
            // <Text>
            //   {data.serviceCategory} - {clothe.service.indexOf(data.serviceCategory)}
            // </Text>
          )}
        </ScrollView>
      )}
      {data && (
        <BottomButtonAtom
          text={translate('GENERAL.BUTTONS.NEXT')}
          onPress={_onPressNext}
          disabled={clothes.filter(b => b.quantity > 0).length === 0}
        />
      )}
    </ViewAtom>
  );
};

export default SelectClothesOrganism;

const styles = StyleSheet.create({
  viewContainer: {
    padding: 0,
    paddingTop: 30,
    paddingBottom: 75
  },
  card: {
    backgroundColor: THEME_DEFAULT.white,
    borderBottomColor: THEME_DEFAULT.borderLine,
    borderBottomWidth: 2,
    flexDirection: 'row',
    paddingVertical: 15
  }
});
