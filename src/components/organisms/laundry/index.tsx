import React from 'react';
import { StyleSheet } from 'react-native';
/* Libs */
import uuid from 'react-native-uuid';
/* Config */
import { ASSETS, METRICS, NAME_ROUTE, SIZE_FONTS, SIZE_ICONS, THEME_DEFAULT } from 'configs/Constants';
/* Components */
import View from 'components/atoms/views';
import { Image, Text } from 'react-native-animatable';
import Icon from 'components/atoms/icons';
import { TypeLibraryIconsSystem } from 'configs/Enum';
import TouchableO from 'components/atoms/buttons/TouchableOpacity';
import { navigationScreen } from 'utils/helpers/Navigation';
import ScrollView from 'components/atoms/scrollViews';
import { AddressModel } from 'models/address';
import { translate } from 'locales';
import SvgCss from 'components/atoms/svgs/SvgCss';
import { ServiceModel, ServiceRequestModel } from 'models/service';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { normalize } from 'utils/helpers/Convert';

// Props
interface props {
  addressSelected?: AddressModel;
  bagSelected?: ServiceRequestModel;
}

const LaundryOrganism = ({ addressSelected, bagSelected }: props) => {
  const onPressNext = async (data: ServiceModel) => {
    if (data.serviceCategory) {
      data.id = uuid.v4().toString();
      const bag = await AsyncStorage.getItem('bag');
      if (bag) {
        const _bag: ServiceRequestModel = JSON.parse(bag);
        const service = _bag.services.find(s => s.concurrence);
        if (service) {
          data.concurrence = service.concurrence;
          data.pickUpType = service.pickUpType;
          data.pickUpSingleTime = service.pickUpSingleTime;
          data.deliverySingleTime = service.deliverySingleTime;
          data.frequency = service.frequency;
          data.monthDays = service.monthDays;
          data.weeklyDays = service.weeklyDays;
        }
      }
      switch (data.serviceCategory) {
        case 'WASH_AND_FOLD':
          data.clothes = [];
          data.bags = [];
          data.soapTypes = [];
          data.serviceAdditionals = [];
          data.type = 'WASH_AND_FOLD';
          data.weeklyDays = data.weeklyDays ? data.weeklyDays : [];
          data.monthDays = data.monthDays ? data.monthDays : [];
          navigationScreen(NAME_ROUTE.selectBagQuantity, { data });
          break;
        case 'FOLDING':
          data.clothes = [];
          data.bags = [];
          data.serviceAdditionals = [];
          data.type = 'FOLDING';
          data.soapTypes = ['LIQUID_SOAP'];
          data.weeklyDays = data.weeklyDays ? data.weeklyDays : [];
          data.monthDays = data.monthDays ? data.monthDays : [];
          navigationScreen(NAME_ROUTE.selectBagQuantity, { data });
          break;
        case 'DRY_CLEANING':
          data.soapTypes = ['LIQUID_SOAP'];
          data.serviceAdditionals = [];
          data.bags = [];
          data.clothes = [];
          data.type = 'DRY_CLEANING';
          data.weeklyDays = data.weeklyDays ? data.weeklyDays : [];
          data.monthDays = data.monthDays ? data.monthDays : [];
          navigationScreen(NAME_ROUTE.selectGarments, { data });
          break;
        case 'BULKY_ITEMS':
          data.type = 'BULKY_ITEMS';
          data.bags = [];
          data.clothes = [];
          data.serviceAdditionals = [];
          data.weeklyDays = data.weeklyDays ? data.weeklyDays : [];
          data.monthDays = data.monthDays ? data.monthDays : [];
          navigationScreen(NAME_ROUTE.selectGarments, { data });
          break;
        default:
          break;
      }
    }
  };

  return (
    <View style={styles.viewContainer}>
      <View style={styles.address}>
        <TouchableO style={{ flexDirection: 'row', width: '85%' }} onPress={() => navigationScreen(NAME_ROUTE.address)}>
          <Icon
            name={'map-pin'}
            library={TypeLibraryIconsSystem.fontAwesome}
            size={SIZE_ICONS.generalSmall}
            color={THEME_DEFAULT.primary}
            style={{ textAlignVertical: 'center' }}
          />
          <Text style={{ marginLeft: 20, textAlignVertical: 'center', fontSize: 18 }}>
            {addressSelected ? addressSelected.address : translate('LAUNDRY_SCENE.ADDRESSES')}
          </Text>
        </TouchableO>
        <TouchableO
          onPress={() => navigationScreen(NAME_ROUTE.bag)}
          style={{ width: '15%', justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
          <Icon
            name={'shopping-bag'}
            library={TypeLibraryIconsSystem.fontAwesome}
            size={SIZE_ICONS.generalSmall}
            color={THEME_DEFAULT.primary}
            style={{ textAlignVertical: 'center' }}
          />
          <Text style={{ color: THEME_DEFAULT.primary }}>{bagSelected?.services.length}</Text>
        </TouchableO>
      </View>

      <ScrollView>
        <View style={styles.content}>
          {/* Wash and fold */}
          <TouchableO style={styles.button} onPress={() => onPressNext({ serviceCategory: 'WASH_AND_FOLD' })}>
            <Image source={ASSETS.washFold} style={styles.buttonImage} />
            <Text style={styles.buttonText}>{translate('LAUNDRY_SCENE.SERVICE_TYPES.WASH_AND_FOLD')}</Text>
          </TouchableO>
          {/* Fold */}
          <TouchableO style={styles.button} onPress={() => onPressNext({ serviceCategory: 'FOLDING' })}>
            <SvgCss xml={ASSETS.fold} height={'100%'} fill={'#15224cBF'} />
            <Text style={styles.buttonText}>{translate('LAUNDRY_SCENE.SERVICE_TYPES.FOLD')}</Text>
          </TouchableO>
        </View>
        <View style={styles.content}>
          {/* Dry cleaning */}
          <TouchableO style={styles.button} onPress={() => onPressNext({ serviceCategory: 'DRY_CLEANING' })}>
            <SvgCss xml={ASSETS.dryCleaning} height={'100%'} fill={'#15224cBF'} />
            <Text style={styles.buttonText}>{translate('LAUNDRY_SCENE.SERVICE_TYPES.DRY_CLEANING')}</Text>
          </TouchableO>
          {/* Bulky Items */}
          <TouchableO style={styles.button} onPress={() => onPressNext({ serviceCategory: 'BULKY_ITEMS' })}>
            <SvgCss xml={ASSETS.bulkyItems} height={'100%'} fill={'#15224cBF'} />
            <Text style={styles.buttonText}>{translate('LAUNDRY_SCENE.SERVICE_TYPES.BULKY_ITEMS')}</Text>
          </TouchableO>
        </View>
      </ScrollView>
    </View>
  );
};

export default LaundryOrganism;

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    backgroundColor: THEME_DEFAULT.white
  },
  address: {
    width: '100%',
    padding: METRICS.CONSTANTS_PADDING,
    borderBottomColor: THEME_DEFAULT.primary,
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20
  },
  content: {
    flexDirection: 'row',
    width: '100%',
    marginVertical: 15
  },
  button: {
    width: '45%',
    marginHorizontal: 10,
    height: 200,
    borderRadius: 20,
    padding: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: THEME_DEFAULT.white,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4
  },
  buttonText: {
    fontSize: normalize(SIZE_FONTS.secondary),
    textAlign: 'center',
    color: THEME_DEFAULT.primary,
    fontWeight: 'bold'
  },
  buttonImage: {
    height: '100%',
    width: '100%',
    resizeMode: 'contain'
  }
});
