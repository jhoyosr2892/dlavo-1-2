import Button from 'components/atoms/buttons';
import BottomButtonAtom from 'components/atoms/buttons/BottomButton';
import TouchableO from 'components/atoms/buttons/TouchableOpacity';
import IconCustom from 'components/atoms/icons';
import Image from 'components/atoms/images';
import CameraModal from 'components/atoms/multimedia/image/Cam';
import TextField from 'components/atoms/textFields';
import Text from 'components/atoms/texts';
import ViewAtom from 'components/atoms/views';
import { INPUT_ACCESSORY_ID, THEME_DEFAULT } from 'configs/Constants';
import { TypeLibraryIconsSystem } from 'configs/Enum';
import { translate } from 'locales';
import { additionalNotes, ServiceModel } from 'models/service';
import React, { useRef, useState } from 'react';
import { StyleSheet, TextInput } from 'react-native';
import { TakePictureResponse } from 'react-native-camera';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { stylesGeneral } from 'styles';

type props = {
  data?: ServiceModel;
  _addData: (additionalNotes: additionalNotes) => void;
};

const AdditionalNotesOrganism = ({ _addData, data }: props) => {
  /* States */
  const [specializedInstructions, setSpecializedInstructions] = useState<string>(data?.additionalNotes?.specializedInstructions);
  const [stateOfClothe, setStateOfClothe] = useState<string>(data?.additionalNotes?.stateOfClothe);
  const [showCameraModal, setShowCameraModal] = useState<boolean>(false);
  const [photoIndex, setPhotoIndex] = useState<number>(0);
  const [photos, setPhotos] = useState<any[]>(new Array(4));
  /* Refs */
  const specializedInstructionsRef = useRef<TextInput | null>(null);
  const stateOfClotheRef = useRef<TextInput | null>(null);

  /* Damages Functions */
  const addImage = async (photo: TakePictureResponse) => {
    const copyPhotos = [...photos];
    copyPhotos[photoIndex] = photo.uri;
    setPhotos(copyPhotos);
    setShowCameraModal(false);
  };

  const deleteImage = (index: number) => {
    const copyPhotos = [...photos];
    copyPhotos[index] = undefined;
    setPhotos(copyPhotos);
  };

  return (
    <ViewAtom style={styles.viewContainer}>
      {/* Card */}
      <KeyboardAwareScrollView>
        <ViewAtom style={[styles.card, stylesGeneral.shadowAndElevation]}>
          {/* specializedInstructions*/}
          <ViewAtom style={styles.cardContent}>
            <Text style={styles.cardLabel}>{translate('ADDITIONAL_NOTES.ENTER_NOTES')}</Text>
            <TextField
              refInput={specializedInstructionsRef}
              value={specializedInstructions}
              editable={true}
              blurOnSubmit={false}
              autoCapitalize='words'
              placeholder={translate('ADDITIONAL_NOTES.ENTER_NOTES')}
              textContentType={'name'}
              returnKeyType={'next'}
              inputAccessoryId={INPUT_ACCESSORY_ID.down}
              onSubmitEditing={() => stateOfClotheRef.current?.focus()}
              onChange={(text: string) => setSpecializedInstructions(text)}
            />
          </ViewAtom>
          {/* stateOfClothe */}
          <ViewAtom style={styles.cardContent}>
            <Text style={styles.cardLabel}>{translate('ADDITIONAL_NOTES.ENTER_STATE')}</Text>
            <TextField
              refInput={stateOfClotheRef}
              value={stateOfClothe}
              editable={true}
              autoCapitalize='words'
              placeholder={translate('ADDITIONAL_NOTES.ENTER_STATE')}
              textContentType={'name'}
              returnKeyType={'done'}
              inputAccessoryId={INPUT_ACCESSORY_ID.in}
              onChange={(text: string) => setStateOfClothe(text)}
            />
          </ViewAtom>
          <ViewAtom style={styles.cardContent}>
            <Text style={styles.cardLabel}>{translate('ADDITIONAL_NOTES.ADD_PHOTOS')}</Text>
          </ViewAtom>
        </ViewAtom>
        <ViewAtom style={{ flexDirection: 'row', paddingHorizontal: 20, marginTop: 20, justifyContent: 'space-around' }}>
          {photos[0] === undefined ? (
            <TouchableO
              style={styles.pickerImage}
              onPress={() => {
                setShowCameraModal(true);
                setPhotoIndex(0);
              }}>
              <IconCustom name={'camera'} color={THEME_DEFAULT.primary} />
            </TouchableO>
          ) : (
            <ViewAtom>
              <ViewAtom style={styles.pickerImage}>
                <Image source={{ uri: photos[0] }} resizeMode={'cover'} style={{ height: '100%', width: '100%' }} />
              </ViewAtom>
              <Button icon='remove' libraryIcon={TypeLibraryIconsSystem.fontAwesome} onPress={() => deleteImage(0)}></Button>
            </ViewAtom>
          )}
          {photos[1] === undefined ? (
            <TouchableO
              style={styles.pickerImage}
              onPress={() => {
                setShowCameraModal(true);
                setPhotoIndex(1);
              }}>
              <IconCustom name={'camera'} color={THEME_DEFAULT.primary} />
            </TouchableO>
          ) : (
            <ViewAtom>
              <ViewAtom style={styles.pickerImage}>
                <Image source={{ uri: photos[1] }} resizeMode={'cover'} style={{ height: '100%', width: '100%' }} />
              </ViewAtom>
              <Button icon='remove' libraryIcon={TypeLibraryIconsSystem.fontAwesome} onPress={() => deleteImage(1)}></Button>
            </ViewAtom>
          )}
          {photos[2] === undefined ? (
            <TouchableO
              style={styles.pickerImage}
              onPress={() => {
                setShowCameraModal(true);
                setPhotoIndex(2);
              }}>
              <IconCustom name={'camera'} color={THEME_DEFAULT.primary} />
            </TouchableO>
          ) : (
            <ViewAtom>
              <ViewAtom style={styles.pickerImage}>
                <Image source={{ uri: photos[2] }} resizeMode={'cover'} style={{ height: '100%', width: '100%' }} />
              </ViewAtom>
              <Button icon='remove' libraryIcon={TypeLibraryIconsSystem.fontAwesome} onPress={() => deleteImage(2)}></Button>
            </ViewAtom>
          )}
          {photos[3] === undefined ? (
            <TouchableO
              style={styles.pickerImage}
              onPress={() => {
                setShowCameraModal(true);
                setPhotoIndex(3);
              }}>
              <IconCustom name={'camera'} color={THEME_DEFAULT.primary} />
            </TouchableO>
          ) : (
            <ViewAtom>
              <ViewAtom style={styles.pickerImage}>
                <Image source={{ uri: photos[3] }} resizeMode={'cover'} style={{ height: '100%', width: '100%' }} />
              </ViewAtom>
              <Button icon='remove' libraryIcon={TypeLibraryIconsSystem.fontAwesome} onPress={() => deleteImage(3)}></Button>
            </ViewAtom>
          )}
        </ViewAtom>
      </KeyboardAwareScrollView>
      <BottomButtonAtom
        text={translate('GENERAL.BUTTONS.NEXT')}
        onPress={() => _addData({ specializedInstructions: specializedInstructions, stateOfClothe: stateOfClothe, photos })}
      />
      <CameraModal isShow={showCameraModal} onClose={() => setShowCameraModal(false)} onPhoto={photo => addImage(photo)} />
    </ViewAtom>
  );
};

export default AdditionalNotesOrganism;

const styles = StyleSheet.create({
  viewContainer: {
    padding: 0,
    paddingVertical: 20,
    flex: 1
  },
  card: {
    backgroundColor: THEME_DEFAULT.white,
    marginVertical: 8,
    paddingTop: 10
  },
  cardContent: {
    marginHorizontal: 20,
    paddingVertical: 8
  },
  cardLabel: {
    fontSize: 18,
    fontWeight: 'bold',
    color: THEME_DEFAULT.primary,
    marginBottom: 10
  },
  cardTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: THEME_DEFAULT.primary,
    paddingLeft: 18,
    paddingTop: 20
  },
  pickerImage: {
    borderRadius: 4,
    borderColor: THEME_DEFAULT.primary,
    borderWidth: 2,
    height: 78,
    width: 78,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
