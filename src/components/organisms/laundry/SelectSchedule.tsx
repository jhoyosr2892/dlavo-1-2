/* Libs */
import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import DropDownPicker, { ItemType } from 'react-native-dropdown-picker';
import moment from 'moment';
import DatePicker from 'react-native-date-picker';
import { Picker } from '@react-native-picker/picker';
/* Components */
import TouchableO from 'components/atoms/buttons/TouchableOpacity';
import IconCustom from 'components/atoms/icons';
import Text from 'components/atoms/texts';
import ViewAtom from 'components/atoms/views';
import BottomButtonAtom from 'components/atoms/buttons/BottomButton';
import ScrollView from 'components/atoms/scrollViews';
/* Configs */
import { FAMILY_FONTS, INPUT_ACCESSORY_ID, IS_ANDROID, NAME_ROUTE, THEME_DEFAULT } from 'configs/Constants';
import { TypeLibraryIconsSystem } from 'configs/Enum';
/* Models */
import { AddressModel } from 'models/address';
/* Styles */
import { stylesGeneral } from 'styles';
/* Locales */
import { translate } from 'locales';
/* Utils */
import { navigationScreen } from 'utils/helpers/Navigation';
import { TypePickerNamesObject } from 'models/enums/TypePickers';
import { Concurrence, ServiceModel } from 'models/service';
import { MonthDays, Weekdays } from 'models/time';
import Button from 'components/atoms/buttons';
import I18n from 'i18n-js';
import TextField from 'components/atoms/textFields';

export interface Hour {
  selected: boolean;
  text: string;
  value: string;
}

type props = {
  _addData: ({}: {
    address?: AddressModel;
    typeOfPickUp?: string;
    pickUpDatetime?: number;
    deliveryDatetime?: number;
    monthlyDays: number[];
    weeklyDays: number[];
    concurrence: Concurrence;
    remainingRepetitions: string;
  }) => void;
  address?: AddressModel;
  data: ServiceModel;
};

const SelectScheduleOrganism = ({ _addData, address, data }: props) => {
  /* States */
  const [openTypeOfPickUp, setOpenTypeOfPickUp] = useState(false);
  const [typeOfPickUp, setTypeOfPickUp] = useState(data?.pickUpType ? data?.pickUpType : '');
  const [items, setItems] = useState<ItemType[]>([
    { label: TypePickerNamesObject.AT_DOOR?.name, value: TypePickerNamesObject.AT_DOOR?.id },
    { label: TypePickerNamesObject.AT_ENTRANCE?.name, value: TypePickerNamesObject.AT_ENTRANCE?.id },
    { label: TypePickerNamesObject.AT_LOCKER?.name, value: TypePickerNamesObject.AT_LOCKER?.id },
    { label: TypePickerNamesObject.AT_RECEPTION?.name, value: TypePickerNamesObject.AT_RECEPTION?.id }
  ]);
  const [pickUpDatetime, setPickUpDatetime] = useState(
    data?.pickUpSingleTime ? new Date(data.pickUpSingleTime * 1000) : new Date(new Date().setHours(new Date().getHours() + 1))
  );
  const [deliveryDatetime, setDeliveryDatetime] = useState<Date>(
    data?.deliverySingleTime
      ? new Date(new Date(data.deliverySingleTime * 1000).getDate() - new Date(data.pickUpSingleTime * 1000).getDate())
      : new Date()
  );

  const [selectedTab, setTab] = useState(data?.monthDays?.length > 0 ? 1 : 0);
  const [remainingRepetitions, setRemainingRepetitions] = useState<string>('2');

  const [weeklyDays, setWeeklyDays] = useState<number[]>([0]);
  const [monthlyDays, setMonthlyDays] = useState<number[]>([0]);
  const [weekDays, setWeekDays] = useState<ItemType[]>(Weekdays);

  const [openWeekDayPicker, setOpenWeekDayPicker] = useState(false);
  const [monthDays, setMonthDays] = useState<ItemType[]>(MonthDays);
  const [openMonthDayPicker, setOpenMonthDayPicker] = useState(false);
  const [concurrence, setConcurrence] = useState<Concurrence>(data?.frequency === 'single' ? 'SINGLE_TIME' : 'WEEKLY');

  const deliveryOptions: { label: string; value: any }[] = [
    {
      label: translate('SELECT_SCHEDULE.ONE_DAY'),
      value: 1
    },
    {
      label: translate('SELECT_SCHEDULE.TWO_DAY'),
      value: 2
    },
    {
      label: translate('SELECT_SCHEDULE.THREE_DAY'),
      value: 3
    }
  ];

  const changeTab = (tab: number) => {
    setTab(tab);
    if (tab === 0) {
      setConcurrence('WEEKLY');
    } else {
      setConcurrence('MONTHLY');
    }
  };

  const onAddData = () => {
    _addData({
      address,
      pickUpDatetime: pickUpDatetime.getTime() / 1000,
      // deliveryDatetime: deliveryDatetime
      //   ? new Date(pickUpDatetime).setDate(new Date(pickUpDatetime).getDate() + deliveryDatetime) / 1000
      //   : 0,
      deliveryDatetime: deliveryDatetime.getTime() / 1000,
      typeOfPickUp,
      weeklyDays,
      monthlyDays,
      concurrence,
      remainingRepetitions
    });
  };

  return (
    <ViewAtom style={{ flex: 1 }}>
      <ScrollView contentContainerStyle={{ padding: 0, paddingBottom: 50 }}>
        <ViewAtom style={styles.container}>
          {/* Address */}
          <ViewAtom style={styles.card}>
            <ViewAtom style={[stylesGeneral.shadowAndElevation, { flexDirection: 'row' }]}>
              <IconCustom
                name='map-pin'
                color={THEME_DEFAULT.primary}
                library={TypeLibraryIconsSystem.fontAwesome}
                style={{ marginRight: 18 }}
                size={24}
              />
              <Text style={styles.title}>{translate('SELECT_SCHEDULE.ADDRESS')}</Text>
            </ViewAtom>
          </ViewAtom>
          <ViewAtom style={styles.card}>
            {address ? (
              <ViewAtom style={styles.infoContent}>
                <Text style={{ flex: 0.8 }}>{address.address}</Text>
                <TouchableO
                  style={{ flex: 0.2, justifyContent: 'center' }}
                  onPress={() => navigationScreen(NAME_ROUTE.address, { backRoute: NAME_ROUTE.selectSchedule })}>
                  <Text style={{ fontSize: 16 }}>{translate('GENERAL.BUTTONS.CHANGE')}</Text>
                </TouchableO>
              </ViewAtom>
            ) : (
              <TouchableO
                style={{ flexDirection: 'row' }}
                onPress={() => navigationScreen(NAME_ROUTE.address, { backRoute: NAME_ROUTE.selectSchedule })}>
                <IconCustom
                  name='map-marker-plus'
                  color={THEME_DEFAULT.primaryOpacity}
                  library={TypeLibraryIconsSystem.materialCommunityIcons}
                  style={{ marginRight: 12 }}
                  size={24}
                />
                <Text style={{ color: THEME_DEFAULT.primaryOpacity }}>{translate('SELECT_SCHEDULE.ADD_ADDRESS')}</Text>
              </TouchableO>
            )}
          </ViewAtom>

          {/* Type of pick up */}
          <ViewAtom style={[styles.card, { marginTop: 12 }]}>
            <ViewAtom style={[stylesGeneral.shadowAndElevation, { flexDirection: 'row' }]}>
              <IconCustom
                name='car-estate'
                color={THEME_DEFAULT.primary}
                library={TypeLibraryIconsSystem.materialCommunityIcons}
                style={{ marginRight: 12 }}
                size={24}
              />
              <Text style={styles.title}>{translate('SELECT_SCHEDULE.TYPE_PICK_UP')}</Text>
            </ViewAtom>
          </ViewAtom>
          <ViewAtom style={[styles.card, { zIndex: 1 }]}>
            <DropDownPicker
              open={openTypeOfPickUp}
              value={typeOfPickUp}
              items={items}
              setOpen={setOpenTypeOfPickUp}
              setValue={setTypeOfPickUp}
              setItems={setItems}
              listMode={IS_ANDROID ? 'MODAL' : 'SCROLLVIEW'}
              placeholder={translate('SELECT_SCHEDULE.TYPE_PICK_UP')}
              placeholderStyle={{ color: THEME_DEFAULT.primaryOpacity }}
              containerStyle={{ paddingRight: 10 }}
              style={{ borderColor: THEME_DEFAULT.borderLine }}
            />
          </ViewAtom>

          {/* Pick up day and time */}
          {data?.frequency === 'single' && (
            <ViewAtom>
              <ViewAtom style={[styles.card, { marginTop: 12 }]}>
                <ViewAtom style={[stylesGeneral.shadowAndElevation, { flexDirection: 'row' }]}>
                  <IconCustom
                    name='calendar'
                    color={THEME_DEFAULT.primary}
                    library={TypeLibraryIconsSystem.fontAwesome}
                    style={{ marginRight: 18 }}
                    size={24}
                  />
                  <Text style={styles.title}>{translate('SELECT_SCHEDULE.SELECT_PICK_UP_DATE_TIME')}</Text>
                </ViewAtom>
              </ViewAtom>
              <ViewAtom style={styles.card}>
                <DatePicker
                  date={pickUpDatetime}
                  onDateChange={date => {
                    setPickUpDatetime(date);
                  }}
                  minimumDate={moment().toDate()}
                  minuteInterval={30}
                  locale={I18n.locale}
                />
              </ViewAtom>
            </ViewAtom>
          )}

          {/* delivery and time */}
          {data?.frequency === 'single' && (
            <ViewAtom>
              <ViewAtom style={[styles.card, { marginTop: 12 }]}>
                <ViewAtom style={[stylesGeneral.shadowAndElevation, { flexDirection: 'row' }]}>
                  <IconCustom
                    name='calendar'
                    color={THEME_DEFAULT.primary}
                    library={TypeLibraryIconsSystem.fontAwesome}
                    style={{ marginRight: 18 }}
                    size={24}
                  />
                  <Text style={styles.title}>{translate('SELECT_SCHEDULE.SELECT_DELIVERY_DATE_IN')}</Text>
                </ViewAtom>
              </ViewAtom>
              <ViewAtom style={styles.card}>
                <DatePicker
                  date={deliveryDatetime}
                  onDateChange={setDeliveryDatetime}
                  minimumDate={pickUpDatetime ? moment(pickUpDatetime).add('h', 6).toDate() : moment().add('h', 6).toDate()}
                  minuteInterval={30}
                  locale={I18n.locale}
                  // mode='date'
                />
                {/* <Picker
                  selectedValue={deliveryDatetime}
                  onValueChange={(value, index) => setDeliveryDatetime(value)}
                  mode='dropdown' // Android only
                >
                  <Picker.Item
                    label={translate('SELECT_SCHEDULE.SELECT_DELIVERY_DATE_IN')}
                    style={{ color: THEME_DEFAULT.primaryOpacity }}
                  />
                  {deliveryOptions.map((d, i) => (
                    <Picker.Item
                      label={d.label}
                      // value={new Date(new Date(pickUpDatetime).setDate(new Date(pickUpDatetime).getDate() + d.value))}
                      value={d.value}
                      key={i}
                    />
                  ))}
                </Picker>
                {deliveryDatetime === 1 && <Text style={{ fontSize: 14 }}>{translate('SELECT_SCHEDULE.ADDITIONAL_COST')}</Text>} */}
              </ViewAtom>
            </ViewAtom>
          )}

          {/* Frequently */}
          {data?.frequency === 'frequent' && (
            <ViewAtom>
              {/* Title */}
              <ViewAtom style={[styles.card, { marginTop: 12 }]}>
                <ViewAtom style={[stylesGeneral.shadowAndElevation, { flexDirection: 'row' }]}>
                  <IconCustom
                    name='calendar'
                    color={THEME_DEFAULT.primary}
                    library={TypeLibraryIconsSystem.fontAwesome}
                    style={{ marginRight: 18 }}
                    size={24}
                  />
                  <Text style={styles.title}>{translate('SELECT_SCHEDULE.SELECT_PICK_UP_DATE_TIME')}</Text>
                </ViewAtom>
              </ViewAtom>

              {/* Tabs */}
              <ViewAtom style={styles.card}>
                <ScrollView horizontal={true} contentContainerStyle={{ padding: 0, width: '100%' }}>
                  <Button
                    text={translate('SELECT_SCHEDULE.WEEKLY')}
                    style={[styles.tabButton, stylesGeneral.shadowAndElevation, selectedTab === 0 && styles.activeTab]}
                    textStyle={[styles.tabText, selectedTab === 0 && styles.activeTabText]}
                    onPress={() => changeTab(0)}
                  />
                  <Button
                    text={translate('SELECT_SCHEDULE.MONTHLY')}
                    style={[styles.tabButton, stylesGeneral.shadowAndElevation, selectedTab === 1 && styles.activeTab]}
                    textStyle={[styles.tabText, selectedTab === 1 && styles.activeTabText]}
                    onPress={() => changeTab(1)}
                  />
                </ScrollView>
              </ViewAtom>

              {/* Repetitions */}
              <ViewAtom style={[styles.card, { marginVertical: 12 }]}>
                <Text>{translate('SELECT_SCHEDULE.REMAINING_REPETITIONS')}</Text>
                <TextField
                  value={remainingRepetitions.toString()}
                  editable={true}
                  keyboardType='number-pad'
                  textContentType={'none'}
                  returnKeyType={'done'}
                  inputAccessoryId={INPUT_ACCESSORY_ID.in}
                  onChange={(text: string) => {
                    setRemainingRepetitions(text);
                  }}
                />
              </ViewAtom>

              {/* Weekly */}
              {selectedTab === 0 &&
                weeklyDays.map((weeklyDay, i) => (
                  <ViewAtom style={styles.card} key={i}>
                    <ViewAtom>
                      <Text style={{ marginBottom: 10 }}>{translate('SELECT_SCHEDULE.WEEK_DAY')}</Text>
                      <DropDownPicker
                        open={openWeekDayPicker}
                        setOpen={setOpenWeekDayPicker}
                        items={weekDays}
                        setItems={setWeekDays}
                        value={weeklyDay}
                        setValue={value => {
                          weeklyDays[i] = value(weeklyDay);
                          setWeeklyDays(weeklyDays);
                          console.log(weeklyDays);
                        }}
                        listMode={IS_ANDROID ? 'MODAL' : 'SCROLLVIEW'}
                        placeholder={translate('SELECT_SCHEDULE.WEEK_DAY')}
                        placeholderStyle={{ color: THEME_DEFAULT.primaryOpacity }}
                        containerStyle={{ paddingRight: 10 }}
                        style={{ borderColor: THEME_DEFAULT.borderLine }}
                        zIndex={weeklyDays.length * 1000}
                        zIndexInverse={i + 1 * 1000}
                      />
                    </ViewAtom>

                    {/* <Text style={{ marginTop: 10 }}>{translate('SELECT_SCHEDULE.PICK_UP_TIME')}</Text>
                    <DatePicker
                      date={weeklyDay.pickUpTime}
                      onDateChange={value => {
                        weeklyDay.pickUpTime = value;
                        weeklyDay.deliveryTime = new Date(new Date(value).setHours(new Date(value).getHours() + 6));
                        weeklyDays[i] = weeklyDay;
                        setWeeklyDays(weeklyDays);
                      }}
                      minuteInterval={30}
                      mode='time'
                      locale={I18n.locale}
                    />
                    <Text style={{ marginTop: 10 }}>{translate('SELECT_SCHEDULE.DELIVERY_TIME')}</Text>
                    <DatePicker
                      date={weeklyDay.deliveryTime}
                      onDateChange={value => {
                        weeklyDay.deliveryTime = value;
                        weeklyDays[i] = weeklyDay;
                        setWeeklyDays(weeklyDays);
                      }}
                      minuteInterval={30}
                      mode='time'
                      locale={I18n.locale}
                    /> */}
                  </ViewAtom>
                ))}

              {/* Monthly */}
              {selectedTab === 1 &&
                monthlyDays.map((monthlyDay, i) => (
                  <ViewAtom style={styles.card} key={i}>
                    <Text style={{ marginBottom: 10 }}>{translate('SELECT_SCHEDULE.MONTH_DAY')}</Text>
                    <DropDownPicker
                      open={openMonthDayPicker}
                      value={monthlyDay}
                      items={monthDays}
                      setOpen={setOpenMonthDayPicker}
                      setValue={value => {
                        monthlyDays[i] = value(monthlyDay);
                        setMonthlyDays(monthlyDays);
                      }}
                      setItems={setMonthDays}
                      listMode={IS_ANDROID ? 'MODAL' : 'SCROLLVIEW'}
                      placeholder={translate('SELECT_SCHEDULE.MONTH_DAY')}
                      placeholderStyle={{ color: THEME_DEFAULT.primaryOpacity }}
                      containerStyle={{ paddingRight: 10 }}
                      style={{ borderColor: THEME_DEFAULT.borderLine }}
                    />
                    {/* <Text style={{ marginTop: 10 }}>{translate('SELECT_SCHEDULE.PICK_UP_TIME')}</Text>
                    <DatePicker
                      date={monthlyDay.pickUpTime}
                      // onDateChange={value => (monthlyDay.pickUpTime = value)}
                      onDateChange={value => {
                        monthlyDay.pickUpTime = value;
                        monthlyDay.deliveryTime = new Date(new Date(value).setHours(new Date(value).getHours() + 6));
                        setMonthlyDays(monthlyDays);
                      }}
                      minuteInterval={30}
                      mode='time'
                    />
                    <Text style={{ marginTop: 10 }}>{translate('SELECT_SCHEDULE.DELIVERY_TIME')}</Text>
                    <DatePicker
                      date={monthlyDay.deliveryTime}
                      onDateChange={value => (monthlyDay.deliveryTime = value)}
                      minuteInterval={30}
                      mode='time'
                    /> */}
                  </ViewAtom>
                ))}
              {/* <ViewAtom style={styles.card}>
                <TouchableO
                  style={{ width: 45 }}
                  onPress={() =>
                    setWeeklyDays([
                      ...weeklyDays,
                      { deliveryTime: new Date(), openWeeklyDayPicker: false, pickUpTime: new Date(), weekDay: 0 }
                    ])
                  }>
                  <IconCustom
                    name={'add'}
                    style={{
                      backgroundColor: THEME_DEFAULT.primary,
                      borderRadius: 25,
                      width: 45,
                      height: 45,
                      justifyContent: 'center',
                      textAlign: 'center'
                    }}
                    size={45}
                  />
                </TouchableO>
              </ViewAtom> */}
            </ViewAtom>
          )}
        </ViewAtom>
      </ScrollView>

      <BottomButtonAtom
        text={translate('GENERAL.BUTTONS.NEXT')}
        onPress={() => onAddData()}
        disabled={!address || !typeOfPickUp || (concurrence === 'SINGLE_TIME' && (!deliveryDatetime || !pickUpDatetime))}
      />
    </ViewAtom>
  );
};

export default SelectScheduleOrganism;

const styles = StyleSheet.create({
  container: {
    padding: 0,
    paddingBottom: 50,
    paddingTop: 30
  },
  title: {
    fontFamily: FAMILY_FONTS.primaryBold,
    color: THEME_DEFAULT.primary,
    fontWeight: 'bold',
    maxWidth: '92%'
  },
  card: {
    paddingVertical: 17,
    paddingHorizontal: 14,
    width: '100%',
    backgroundColor: THEME_DEFAULT.white,
    marginBottom: 3
  },
  clotheCard: {
    paddingHorizontal: 19,
    width: '100%',
    backgroundColor: THEME_DEFAULT.white
  },
  infoContent: {
    backgroundColor: THEME_DEFAULT.white,
    borderBottomColor: THEME_DEFAULT.primary,
    borderBottomWidth: 1,
    flexDirection: 'row',
    paddingVertical: 15
  },
  tabButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  tabButton: {
    backgroundColor: THEME_DEFAULT.white,
    flex: 1
  },
  tabText: {
    color: THEME_DEFAULT.primary
  },
  activeTab: {
    backgroundColor: THEME_DEFAULT.primary
  },
  activeTabText: {
    color: THEME_DEFAULT.white
  }
});
