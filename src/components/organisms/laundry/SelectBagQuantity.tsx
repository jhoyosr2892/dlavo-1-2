import BottomButtonAtom from 'components/atoms/buttons/BottomButton';
import TouchableO from 'components/atoms/buttons/TouchableOpacity';
import IconCustom from 'components/atoms/icons';
import SvgCss from 'components/atoms/svgs/SvgCss';
import Text from 'components/atoms/texts';
import ViewAtom from 'components/atoms/views';
import { ASSETS, FAMILY_FONTS, THEME_DEFAULT } from 'configs/Constants';
import { TypeLibraryIconsSystem } from 'configs/Enum';
import { translate } from 'locales';
import { BAGS, typeBags } from 'models/bag';
import { ServiceModel } from 'models/service';
import React, { useEffect, useState } from 'react';
import { StyleSheet } from 'react-native';
import { stylesGeneral } from 'styles';

type props = {
  _addBags: (selectedBag: typeBags[]) => void;
  data?: ServiceModel;
};

const SelectBagQuantityOrganism = ({ _addBags, data }: props) => {
  const [bags, setBags] = useState<typeBags[]>(BAGS);

  useEffect(() => {
    const loadData = () => {
      const _bags = [...bags];
      _bags.forEach(bag => (bag.quantity = 0));
      if (data && data.bags) {
        data.bags.forEach((bag, i) => {
          const _bag = _bags.find(b => b.type === bag.type);
          if (_bag) {
            _bag.quantity = bag.quantity as number;
          }
        });
      }
      setBags(_bags);
    };
    loadData();
  }, []);

  const addClothes = (index: number) => {
    const array = [...bags];
    array[index].quantity += 1;
    setBags(array);
  };
  const subClothes = (index: number) => {
    const array = [...bags];
    if (array[index].quantity > 0) {
      array[index].quantity -= 1;
      setBags(array);
    }
  };

  const onAddBags = () => {
    const _selectBag: typeBags[] = [];
    bags.forEach(b => {
      if (b.quantity > 0) {
        _selectBag.push(b);
      }
    });
    _addBags(_selectBag);
  };

  return (
    <ViewAtom style={{ flex: 1 }}>
      <ViewAtom style={styles.container}>
        <ViewAtom style={styles.card}>
          <ViewAtom style={stylesGeneral.shadowAndElevation}>
            <Text style={styles.title}>{translate('SELECT_BAG_QUANTITY.BAGS_QUANTITY')}</Text>
          </ViewAtom>
          {bags.map(
            (bag, i) =>
              data.serviceCategory &&
              bag.service.indexOf(data.serviceCategory) >= 0 && (
                <ViewAtom style={styles.infoContent} key={i}>
                  <ViewAtom style={{ flex: 0.7, justifyContent: 'flex-end' }}>
                    <Text>{bag.name}</Text>
                  </ViewAtom>
                  <ViewAtom style={{ flex: 0.3, alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableO onPress={() => subClothes(i)}>
                      <IconCustom name='minus' library={TypeLibraryIconsSystem.simpleLineIcons} color={THEME_DEFAULT.primaryOpacity} />
                    </TouchableO>
                    <Text style={{ fontSize: 25 }}>{bag.quantity}</Text>
                    <TouchableO onPress={() => addClothes(i)}>
                      <IconCustom name='plus' library={TypeLibraryIconsSystem.simpleLineIcons} color={THEME_DEFAULT.primaryOpacity} />
                    </TouchableO>
                  </ViewAtom>
                </ViewAtom>
              )
          )}
          <ViewAtom style={{ height: 250, width: '100%' }}>
            <SvgCss xml={ASSETS.bagsprice} />
          </ViewAtom>
        </ViewAtom>
      </ViewAtom>

      <BottomButtonAtom
        text={translate('GENERAL.BUTTONS.NEXT')}
        onPress={onAddBags}
        disabled={bags.filter(b => b.quantity > 0).length === 0}
      />
    </ViewAtom>
  );
};

export default SelectBagQuantityOrganism;

const styles = StyleSheet.create({
  container: {
    padding: 0,
    paddingBottom: 50,
    paddingTop: 30
  },
  title: {
    fontFamily: FAMILY_FONTS.primaryBold,
    color: THEME_DEFAULT.primary,
    fontWeight: 'bold'
  },
  card: {
    paddingVertical: 17,
    paddingHorizontal: 14,
    width: '100%',
    backgroundColor: THEME_DEFAULT.white
  },
  clotheCard: {
    paddingHorizontal: 19,
    width: '100%',
    backgroundColor: THEME_DEFAULT.white
  },
  infoContent: {
    backgroundColor: THEME_DEFAULT.white,
    borderBottomColor: THEME_DEFAULT.borderLine,
    borderBottomWidth: 2,
    flexDirection: 'row',
    paddingTop: 15,
    paddingBottom: 1
    // paddingVertical: 15
  }
});
