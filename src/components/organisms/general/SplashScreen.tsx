// Libs
import React from 'react';
import { StyleSheet } from 'react-native';

// Atoms
import View from '@atoms/views';
import SpinnerLottie from '@atoms/spinners';

// Helpers
//import { CLog } from '@helpers/Message';

// Constants
import { THEME_DEFAULT, EnumSpinner, METRICS } from '@constantsApp';

//const URL_PATH = 'src/components/organisms/splashScreen/index.tsx';

// Props
type SplashScreenProps = {
  /** color inverso (fondo blanco y spinner color primario) **/
  inverse?: boolean;
};

// Styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: METRICS.DEVICE_HEIGHT,
    backgroundColor: THEME_DEFAULT.primary,
    alignItems: 'center',
    justifyContent: 'center'
  },
  styleInverse: {
    backgroundColor: THEME_DEFAULT.content
  }
});

const SplashScreen = ({ inverse }: SplashScreenProps) => {
  return (
    <View style={[styles.container, inverse ? styles.styleInverse : null]}>
      <SpinnerLottie type={inverse ? EnumSpinner.primary : EnumSpinner.white} />
    </View>
  );
};

export default SplashScreen;
