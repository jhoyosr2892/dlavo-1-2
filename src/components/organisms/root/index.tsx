// Libs
import React, { ReactNode, useState, useMemo } from 'react';

// Atoms
import LoadingScreen from '@atoms/modals/LoadingScreen';
import Toast from '@atoms/toasts';
import AlertCustom from '@atoms/modals/AlertCustom';
import AlertSheet from '@atoms/modals/AlertSheet';
import SafeAreaView from '@atoms/safeAreaViews';

// Contexts
import { RootContext, ToastOption, AlertModalOptions, AlertSheetModalOptions } from '@config/ContextApp';

// Helpers
//import { CLog } from '@helpers/Message';

// Styles
import { stylesGeneral } from '@styles/index';

// Constants
import { AnimatedString } from '@constantsApp';

//const URL_PATH = 'src/components/organisms/root/index.tsx';

interface RootProps {
  children?: ReactNode | null;
}

const Root = (props: RootProps) => {
  const { children } = props;
  const { Provider } = RootContext;

  const [nodeToast, setNodeToast] = useState<ReactNode | undefined>();
  const [nodeModalLoading, setNodeModalLoading] = useState<ReactNode | undefined>();
  const [nodeAlert, setNodeAlert] = useState<ReactNode | undefined>();
  const [nodeAlertSheet, setNodeAlertSheet] = useState<ReactNode | undefined>();
  const [nodeCameraModal, setNodeCameraModal] = useState<ReactNode | undefined>(false);

  /****** AlertSheet *******/
  const alertSheetMemo = useMemo(
    () => ({
      showAlert: (option: AlertSheetModalOptions) => {
        const nodeAlertSheetCustom = (
          <AlertSheet
            {...option}
            animateContainer={AnimatedString.fadeIn}
            animateContent={AnimatedString.slideInUp}
            onActionPress={() => alertSheetMemo.hideAlert(option)}
          />
        );
        setNodeAlertSheet(nodeAlertSheetCustom);
      },
      hideAlert: (option: AlertSheetModalOptions) => {
        const nodeAlertSheetCustom = (
          <AlertSheet
            {...option}
            animateContainer={AnimatedString.fadeOut}
            animateContent={AnimatedString.slideOutDown}
            onAnimationEnd={alertSheetMemo.removeAlert}
          />
        );
        setNodeAlertSheet(nodeAlertSheetCustom);
      },
      removeAlert: () => {
        setNodeAlertSheet(undefined);
      }
    }),
    []
  );

  /****** Alerts *******/
  const alertsMemo = useMemo(
    () => ({
      showAlert: (option: AlertModalOptions) => {
        const nodeAlertCustom = (
          <AlertCustom
            {...option}
            animateContainer={AnimatedString.fadeIn}
            animateContent={AnimatedString.zoomIn}
            onActionPress={() => alertsMemo.hideAlert(option)}
          />
        );
        setNodeAlert(nodeAlertCustom);
      },
      hideAlert: (option: AlertModalOptions) => {
        const nodeAlertCustom = (
          <AlertCustom
            {...option}
            animateContainer={AnimatedString.fadeOut}
            animateContent={AnimatedString.zoomOut}
            onAnimationEnd={alertsMemo.removeAlert}
          />
        );
        setNodeAlert(nodeAlertCustom);
      },
      removeAlert: () => {
        setNodeAlert(undefined);
      }
    }),
    []
  );

  /****** Toast ******/
  // TODO: falta agregar la funcionalidad de poder imprimir mas de un toast al mismo tiempo. actualmente solo una a al vez
  const addToast = (node: ReactNode) => {
    setNodeToast(node);
  };

  const removeToast = () => {
    setNodeToast(undefined);
  };

  const hideToast = () => {
    removeToast();
  };

  const showToast = (option: ToastOption) => {
    // CLog(URL_PATH, 'showToast', 'show', toast);
    addToast(
      <Toast
        onPressButton={option.onPress}
        dismountToast={() => {
          hideToast();
        }}
        {...option}
      />
    );
  };

  /****** Loading Screen (Totalmente Funcional) ******/
  // Agregar Modal de Loading
  const addModal = (node: ReactNode) => {
    setNodeModalLoading(node);
  };

  // Remover Modal de Loading
  const removeModal = () => {
    setNodeModalLoading(undefined);
  };

  const hideLoading = () => {
    if (!nodeModalLoading) {
      addModal(
        <LoadingScreen
          animateContainer={AnimatedString.fadeOut}
          animateContent={AnimatedString.zoomOut}
          onAnimationEnd={() => {
            removeModal();
          }}
        />
      );
    }
  };

  // show Modal loading
  const showLoading = () => {
    if (!nodeModalLoading) {
      addModal(<LoadingScreen animateContainer={AnimatedString.fadeIn} animateContent={AnimatedString.zoomIn} />);
    }
  };

  /** Modal de camera **/
  const removeCamera = () => {
    //CLog(URL_PATH, 'removeCamera()', 'remove', 'remove');
    setNodeCameraModal(undefined);
  };

  return (
    <Provider
      value={{
        showToast,
        showLoading,
        hideLoading,
        showAlert: alertsMemo.showAlert,
        showAlertSheet: alertSheetMemo.showAlert,
        removeAlertSheet: alertSheetMemo.removeAlert,
        isShowAlertSheet: nodeAlertSheet ? true : false
      }}>
      <SafeAreaView style={stylesGeneral.viewContainerRoot}>
        {children}
        {nodeCameraModal}
        {nodeToast}
        {nodeAlertSheet}
        {nodeModalLoading}
        {nodeAlert}
      </SafeAreaView>
    </Provider>
  );
};

export default Root;
