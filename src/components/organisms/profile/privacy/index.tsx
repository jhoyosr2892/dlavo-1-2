import { THEME_DEFAULT } from 'configs/Constants';
import React from 'react';
import { ActivityIndicator, View } from 'react-native';

/* Libs */
import { WebView } from 'react-native-webview';

// Props
interface props {}

const PrivacyOrganism = ({}: props) => {
  return (
    <WebView
      source={{ uri: 'https://policy.dlavo.com/' }}
      textZoom={100}
      scalesPageToFit={true}
      //View to show while loading the webpage
      renderLoading={() => (
        <View style={{ position: 'absolute', top: 0, width: '100%', justifyContent: 'center' }}>
          <ActivityIndicator color={THEME_DEFAULT.primary} size='large' />
        </View>
      )}
      //Want to show the view or not|
      startInLoadingState={true}
    />
  );
};

export default PrivacyOrganism;
