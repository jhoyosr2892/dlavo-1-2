import React from 'react';
import { StyleSheet } from 'react-native';

/* Components */
import ViewAtom from 'components/atoms/views';
import Text from 'components/atoms/texts';
import TouchableO from 'components/atoms/buttons/TouchableOpacity';
/* Configs */
import { METRICS, THEME_DEFAULT } from 'configs/Constants';
/* Locales */
import { translate } from 'locales';
/* Helpers */
import { openEmail } from 'utils/helpers/linking';

// Props
interface props {}

const HelpOrganism = ({}: props) => {
  return (
    <ViewAtom>
      <ViewAtom style={styles.viewContainer}>
        <Text>
          {translate('HELP_SCENE.HELP_TEXT')}{' '}
          <TouchableO style={styles.link} onPress={() => openEmail({ mail: 'support@dlavo.com' })}>
            <Text>support@dlavo.com</Text>
          </TouchableO>
        </Text>
      </ViewAtom>
    </ViewAtom>
  );
};

export default HelpOrganism;

const styles = StyleSheet.create({
  viewContainer: {
    padding: METRICS.CONSTANTS_PADDING,
    paddingBottom: 60
  },
  link: {
    marginLeft: 5,
    borderBottomColor: THEME_DEFAULT.black,
    borderBottomWidth: 1
  }
});
