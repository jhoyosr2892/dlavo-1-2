import React, { useRef, useState } from 'react';
import { StyleSheet, TextInput } from 'react-native';

/* Components */
import ViewAtom from 'components/atoms/views';
import Text from 'components/atoms/texts';
import TouchableO from 'components/atoms/buttons/TouchableOpacity';
import Button from '@atoms/buttons';
/* Configs */
import { FAMILY_FONTS, INPUT_ACCESSORY_ID, METRICS, THEME_DEFAULT } from 'configs/Constants';
import { TypeLibraryIconsSystem } from 'configs/Enum';
/* Locales */
import { translate } from 'locales';
/* Models */
import { stylesGeneral } from 'styles';
import BottomButtonAtom from 'components/atoms/buttons/BottomButton';
import TextField from 'components/atoms/textFields';

// Props
interface props {
  checkCouponCode: (couponCode: string | undefined) => void;
}

const CouponsOrganism = ({ checkCouponCode }: props) => {
  /* States */
  const [couponCode, setCouponCode] = useState<string | undefined>();
  /* Refs */
  const couponCodeRef = useRef<TextInput | null>(null);

  return (
    <ViewAtom style={{ flex: 1 }}>
      <ViewAtom style={styles.viewContainer}>
        <ViewAtom style={styles.formInput}>
          <TextField
            refInput={couponCodeRef}
            value={couponCode}
            editable={true}
            autoCapitalize='none'
            textContentType={'oneTimeCode'}
            placeholder={translate('COUPONS_SCENE.ENTER_COUPON_CODE_PLACEHOLDER')}
            containerStyle={{ backgroundColor: THEME_DEFAULT.white }}
            rightButtonProps={{ show: true, nameIcon: 'ticket', libraryIcon: TypeLibraryIconsSystem.fontAwesome }}
            inputAccessoryId={INPUT_ACCESSORY_ID.in}
            onChange={(text: string) => setCouponCode(text)}
          />
        </ViewAtom>
      </ViewAtom>
      <BottomButtonAtom text={translate('GENERAL.BUTTONS.CHECK')} onPress={() => checkCouponCode(couponCode)} />
    </ViewAtom>
  );
};

export default CouponsOrganism;

const styles = StyleSheet.create({
  viewContainer: {
    padding: METRICS.CONSTANTS_PADDING,
    paddingBottom: 60
  },
  formInput: {
    marginVertical: 10
  },
  formText: {
    color: THEME_DEFAULT.primary,
    fontFamily: FAMILY_FONTS.primaryRegular,
    fontSize: 18,
    marginBottom: 10
  }
});
