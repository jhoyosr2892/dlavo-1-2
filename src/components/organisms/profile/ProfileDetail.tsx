import Button from 'components/atoms/buttons';
import BottomButtonAtom from 'components/atoms/buttons/BottomButton';
import TextField from 'components/atoms/textFields';
import TextInputMaskAtom from 'components/atoms/textFields/TextInputMask';
import { ASSETS, INPUT_ACCESSORY_ID, THEME_DEFAULT } from 'configs/Constants';
import { TypeLibraryIconsSystem } from 'configs/Enum';
import { translate } from 'locales';
import { UserModel } from 'models/user/User';
import React, { useRef, useState } from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Masks } from 'react-native-mask-input';
import { SvgUri } from 'react-native-svg';
import UserStorage from 'stores/UserState';

interface props {
  _updateUser: (user: UserModel) => {};
}

const ProfileDetailOrganism = ({ _updateUser }: props) => {
  /* States */
  const [name, setName] = useState<string | undefined>(UserStorage.data?.firstName ? UserStorage.data.firstName : '');
  const [lastName, setLastName] = useState<string | undefined>(UserStorage.data?.lastName ? UserStorage.data.lastName : '');
  const [phoneIndicative, setPhoneIndicative] = useState(null);
  const [phone, setPhone] = useState<string | undefined>(UserStorage.data?.phone ? UserStorage.data.phone : '');
  const [formatPhone, setFormatPhone] = useState<string | undefined>();
  const [email, setEmail] = useState<string | undefined>(UserStorage.data?.email ? UserStorage.data.email : '');

  const [open, setOpen] = useState(false);
  const [countries, setCountries] = useState<any[]>(
    ASSETS.countries.map(e => {
      return {
        // icon: () => <Image source={{ uri: e.flag }} style={{ height: 20, width: 20 }} />,
        icon: () => <SvgUri width={20} height={20} uri={e.flag} />,
        name: e.name,
        dialCode: e.dialCode,
        isoCode: e.isoCode
      };
    })
  );

  /* Refs */
  const inputNameRef = useRef<TextInput | null>(null);
  const inputLastNameRef = useRef<TextInput | null>(null);
  const inputPhoneIndicativeRef = useRef<any | null>(null);
  const inputPhoneRef = useRef<TextInput | null>(null);
  const inputEmailRef = useRef<TextInput | null>(null);

  return (
    <View style={styles.viewContainer}>
      {/* Button */}
      <View style={{ flexDirection: 'row' }}>
        <Button
          text='Gold'
          icon='angle-right'
          libraryIcon={TypeLibraryIconsSystem.fontAwesome}
          colorIcon={THEME_DEFAULT.black}
          textStyle={{ color: THEME_DEFAULT.black }}
          style={{ flexDirection: 'row', paddingHorizontal: 10, backgroundColor: THEME_DEFAULT.white }}
          iconStyle={{ marginLeft: 10 }}
        />
      </View>
      {/* Card */}
      <KeyboardAwareScrollView contentContainerStyle={styles.card}>
        {/* First Name */}
        <View style={styles.cardContent}>
          <Text style={styles.cardLabel}>{translate('PROFILE_DETAIL_SCENE.NAME')}</Text>
          <TextField
            refInput={inputNameRef}
            value={name}
            editable={true}
            autoCapitalize='words'
            placeholder={translate('PROFILE_DETAIL_SCENE.PLACEHOLDER_NAME')}
            textContentType={'name'}
            blurOnSubmit={false}
            returnKeyType={'next'}
            inputAccessoryId={INPUT_ACCESSORY_ID.down}
            onSubmitEditing={() => inputLastNameRef.current?.focus()}
            onChange={(text: string) => setName(text)}
          />
        </View>
        {/* Last name */}
        <View style={styles.cardContent}>
          <Text style={styles.cardLabel}>{translate('PROFILE_DETAIL_SCENE.LAST_NAME')}</Text>
          <TextField
            refInput={inputLastNameRef}
            value={lastName}
            editable={true}
            autoCapitalize='words'
            placeholder={translate('PROFILE_DETAIL_SCENE.PLACEHOLDER_LAST_NAME')}
            textContentType={'name'}
            returnKeyType={'done'}
            inputAccessoryId={INPUT_ACCESSORY_ID.down}
            onSubmitEditing={() => inputPhoneIndicativeRef.current?.focus()}
            onChange={(text: string) => setLastName(text)}
          />
        </View>
        {/* Phone */}
        <View style={[styles.cardContent, { zIndex: 1 }]}>
          <Text style={styles.cardLabel}>{translate('PROFILE_DETAIL_SCENE.PHONE')}</Text>
          <View style={{ flexDirection: 'row', zIndex: 1 }}>
            {/* Indicative */}
            <View style={{ flex: 1, zIndex: 1 }}>
              <DropDownPicker
                open={open}
                value={phoneIndicative}
                items={countries}
                setOpen={setOpen}
                setValue={setPhoneIndicative}
                setItems={setCountries}
                listMode='MODAL'
                placeholder={'+'}
                placeholderStyle={{ color: THEME_DEFAULT.primaryOpacity }}
                containerStyle={{ paddingRight: 10 }}
                style={{ borderColor: THEME_DEFAULT.borderLine, zIndex: 1 }}
                schema={{
                  label: 'name',
                  value: 'dialCode',
                  icon: 'icon'
                }}
                searchable={true}
                searchTextInputProps={{
                  placeholder: translate('GENERAL.BUTTONS.SEARCH')
                }}
                itemKey='name'
              />
            </View>
            {/* Phone input */}
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <TextInputMaskAtom
                // refInput={inputPhoneRef}
                value={phone}
                editable={true}
                autoCapitalize='words'
                placeholder={translate('PROFILE_DETAIL_SCENE.PLACEHOLDER_PHONE')}
                textContentType={'telephoneNumber'}
                returnKeyType={'next'}
                keyboardType={'phone-pad'}
                blurOnSubmit={false}
                inputAccessoryId={INPUT_ACCESSORY_ID.down}
                mask={Masks.USA_PHONE}
                onSubmitEditing={() => inputEmailRef.current?.focus()}
                onChange={(formatted: string, extracted?: string | undefined) => {
                  console.log(extracted);
                  setPhone(extracted);
                  setFormatPhone(formatted);
                }}
              />
            </View>
          </View>
        </View>

        {/* Email */}
        <View style={styles.cardContent}>
          <Text style={styles.cardLabel}>{translate('PROFILE_DETAIL_SCENE.EMAIL')}</Text>
          <TextField
            refInput={inputEmailRef}
            value={email}
            editable={true}
            autoCapitalize='none'
            placeholder={translate('PROFILE_DETAIL_SCENE.PLACEHOLDER_EMAIL')}
            textContentType={'emailAddress'}
            returnKeyType={'done'}
            keyboardType={'email-address'}
            inputAccessoryId={INPUT_ACCESSORY_ID.down}
            onChange={(text: string) => setEmail(text)}
          />
        </View>

        <View style={styles.cardContent}></View>
      </KeyboardAwareScrollView>
      <BottomButtonAtom text={translate('GENERAL.BUTTONS.SAVE')} onPress={() => _updateUser({ firstName: name, lastName, email, phone })} />
    </View>
  );
};

export default ProfileDetailOrganism;

const styles = StyleSheet.create({
  viewContainer: {
    padding: 0,
    paddingVertical: 20,
    flex: 1
  },
  card: {
    backgroundColor: THEME_DEFAULT.white,
    marginVertical: 8,
    paddingTop: 10,
    paddingBottom: 50
  },
  cardContent: {
    marginHorizontal: 20,
    paddingVertical: 8
  },
  cardLabel: {
    fontSize: 18,
    fontWeight: 'bold',
    color: THEME_DEFAULT.primary,
    marginBottom: 10
  },
  cardTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: THEME_DEFAULT.primary,
    paddingLeft: 18,
    paddingTop: 20
  },
  listIcon: {
    marginRight: 15
  },
  borderBottom: {
    borderBottomColor: THEME_DEFAULT.primaryOpacity,
    borderBottomWidth: 1
  }
});
