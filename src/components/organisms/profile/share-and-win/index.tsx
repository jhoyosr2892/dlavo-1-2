import React, { useContext, useRef } from 'react';
import { StyleSheet } from 'react-native';
import Clipboard from '@react-native-clipboard/clipboard';
import { Modalize } from 'react-native-modalize';

/* Components */
import ViewAtom from 'components/atoms/views';
import Text from 'components/atoms/texts';
import TouchableO from 'components/atoms/buttons/TouchableOpacity';
import IconCustom from 'components/atoms/icons';
/* Configs */
import { METRICS, THEME_DEFAULT } from 'configs/Constants';
import { TypeLibraryIconsSystem } from 'configs/Enum';
/* Locales */
import { translate } from 'locales';
/* Styles */
import { stylesGeneral } from 'styles';
import { RootContext } from 'configs/ContextApp';
import { openWhatsApp } from 'utils/helpers/linking';
import { share } from 'utils/helpers/Share';
import UserStorage from 'stores/UserState';

// Props
interface props {}

const ShareAndWinOrganism = ({}: props) => {
  /* Context */
  const { showToast } = useContext(RootContext);
  /* Ref */
  const modalizeRef = useRef<Modalize>(null);

  const copyToClipboard = () => {
    Clipboard.setString('xyz123dlavo');
    showToast({ text: translate('SHARE_AND_WIN_SCENE.TOAST.CODE_COPIED') });
  };

  const onShare = () => {
    share({ message: 'xyz123dlavo', url: '' }, { dialogTitle: 'Hola' });
  };

  const onOpen = () => {
    modalizeRef.current?.open();
  };

  return (
    <ViewAtom>
      <ViewAtom style={styles.viewContainer}>
        <Text>{translate('SHARE_AND_WIN_SCENE.HELP_TEXT')}</Text>

        <ViewAtom style={[styles.card, stylesGeneral.shadowAndElevation]}>
          <ViewAtom style={styles.rowContent}>
            <Text>{UserStorage.data.couponCode}</Text>
            <TouchableO>
              <Text style={{ fontSize: 14 }} onPress={copyToClipboard}>
                {translate('GENERAL.BUTTONS.COPY')}
              </Text>
            </TouchableO>
          </ViewAtom>
        </ViewAtom>

        <ViewAtom style={styles.shareContent}>
          <Text style={styles.shareText}>{translate('SHARE_AND_WIN_SCENE.SHARE_YOUR_CODE')}</Text>
          <ViewAtom style={styles.shareButtons}>
            <TouchableO style={styles.shareButton} onPress={() => openWhatsApp({ text: 'xyz123dlavo' })}>
              <IconCustom name='whatsapp' library={TypeLibraryIconsSystem.fontAwesome} size={32} />
            </TouchableO>
            <TouchableO style={styles.shareButton}>
              <IconCustom name='facebook' library={TypeLibraryIconsSystem.fontAwesome} size={32} />
            </TouchableO>
            <TouchableO style={styles.shareButton}>
              <IconCustom name='mail' library={TypeLibraryIconsSystem.ionicons} size={32} />
            </TouchableO>
            <TouchableO style={[styles.shareButton, { backgroundColor: THEME_DEFAULT.white }]} onPress={onShare}>
              <IconCustom name='more-horizontal' library={TypeLibraryIconsSystem.feather} size={32} color={THEME_DEFAULT.black} />
            </TouchableO>
          </ViewAtom>
        </ViewAtom>

        <ViewAtom style={{ flexDirection: 'row', marginTop: 40 }}>
          <IconCustom
            name='info-outline'
            library={TypeLibraryIconsSystem.materialIcons}
            size={20}
            color={THEME_DEFAULT.primaryOpacity}
            style={{ marginRight: 10 }}
          />
          <Text style={styles.textTerms}>{translate('SHARE_AND_WIN_SCENE.VIEW_THE')}</Text>
          <TouchableO style={styles.termsButton}>
            <Text style={styles.textTerms}>{translate('SHARE_AND_WIN_SCENE.TERMS')}</Text>
          </TouchableO>
        </ViewAtom>

        <ViewAtom>
          <Modalize ref={modalizeRef} modalStyle={{ backgroundColor: 'white' }}>
            <ViewAtom style={{ flex: 1, backgroundColor: 'white' }}>
              <Text>...your content</Text>
            </ViewAtom>
          </Modalize>
        </ViewAtom>
      </ViewAtom>
    </ViewAtom>
  );
};

export default ShareAndWinOrganism;

const styles = StyleSheet.create({
  viewContainer: {
    padding: METRICS.CONSTANTS_PADDING,
    paddingBottom: 60
  },
  card: {
    backgroundColor: THEME_DEFAULT.white,
    padding: METRICS.CONSTANTS_PADDING,
    borderRadius: 8,
    marginVertical: 10
  },
  rowContent: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  shareContent: {
    marginTop: 15
  },
  shareText: {
    textAlign: 'center',
    fontSize: 19,
    marginBottom: 15
  },
  shareButtons: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  shareButton: {
    backgroundColor: THEME_DEFAULT.black,
    borderRadius: 8,
    padding: 8,
    width: 50,
    height: 50,
    alignItems: 'center'
  },
  textTerms: {
    fontSize: 14
  },
  termsButton: {
    marginLeft: 5,
    borderBottomColor: THEME_DEFAULT.black,
    borderBottomWidth: 1
  }
});
