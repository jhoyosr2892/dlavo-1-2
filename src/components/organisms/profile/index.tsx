import TouchableO from 'components/atoms/buttons/TouchableOpacity';
import IconCustom from 'components/atoms/icons';
import ScrollView from 'components/atoms/scrollViews';
import { ASSETS, NAME_ROUTE, THEME_DEFAULT } from 'configs/Constants';
import { AuthContext, RootContext } from 'configs/ContextApp';
import { StyleButtonAlert, TypeLibraryIconsSystem } from 'configs/Enum';
import { translate } from 'locales';
import React, { useContext } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { SvgCss } from 'react-native-svg';
import UserStorage from 'stores/UserState';
import { navigationScreen } from 'utils/helpers/Navigation';

const ProfileOrganism = () => {
  const { authFunction } = useContext(AuthContext);
  const { showAlert } = useContext(RootContext);

  const onBuyMembership = async () => {
    showAlert({
      title: translate('PROFILE_SCENE.SIGN_OUT_ALERT.TITLE'),
      message: translate('PROFILE_SCENE.SIGN_OUT_ALERT.MESSAGE'),
      options: [
        {
          text: translate('GENERAL.WORDS.WORD_YES'),
          onAction: async () => {
            authFunction.signOut();
          }
        },
        {
          text: translate('GENERAL.WORDS.WORD_NOT'),
          style: StyleButtonAlert.cancel
        }
      ]
    });
  };

  return (
    <ScrollView contentContainerStyle={styles.viewContainer}>
      {/* Name */}
      <View>
        <Text style={styles.name}>
          {translate('PROFILE_SCENE.HI')} {UserStorage.data?.firstName}
        </Text>
      </View>
      {/* Score */}
      {/* <View style={[styles.cardContent, { paddingTop: 10 }]}>
        <SvgCss xml={ASSETS.starGray} height={20} width={20} style={{ marginRight: 6 }} />
        <Text>4.95</Text>
      </View> */}
      {/* Button */}
      {/* <View style={{ flexDirection: 'row' }}>
        <Button
          text='6 months'
          icon='angle-right'
          libraryIcon={TypeLibraryIconsSystem.fontAwesome}
          colorIcon={THEME_DEFAULT.black}
          textStyle={{ color: THEME_DEFAULT.black }}
          style={{ flexDirection: 'row', paddingHorizontal: 10, backgroundColor: THEME_DEFAULT.white }}
          iconStyle={{ marginLeft: 10 }}
        />
      </View> */}

      {/* Account */}
      <Text style={styles.cardTitle}>{translate('PROFILE_SCENE.ACCOUNT')}</Text>
      <View style={styles.card}>
        {/* Profile */}
        <TouchableO style={[styles.cardContent, styles.borderBottom]} onPress={() => navigationScreen(NAME_ROUTE.profileDetail)}>
          <SvgCss xml={ASSETS.profile2} height={20} width={20} style={styles.listIcon} />
          <Text>{translate('PROFILE_SCENE.PROFILE')}</Text>
        </TouchableO>
        {/* Address */}
        <TouchableO style={[styles.cardContent, styles.borderBottom]} onPress={() => navigationScreen(NAME_ROUTE.address)}>
          <SvgCss xml={ASSETS.mapPing} height={20} width={20} style={styles.listIcon} />
          <Text>{translate('PROFILE_SCENE.ADDRESSES')}</Text>
        </TouchableO>
        {/* Payment */}
        {/* <TouchableO style={[styles.cardContent, styles.borderBottom]} onPress={() => navigationScreen(NAME_ROUTE.payment)}>
          <SvgCss xml={ASSETS.paymentCard} height={20} width={20} style={styles.listIcon} />
          <Text>{translate('PROFILE_SCENE.PAYMENT')}</Text>
        </TouchableO> */}
        {/* Membership */}
        <TouchableO style={[styles.cardContent, styles.borderBottom]} onPress={() => navigationScreen(NAME_ROUTE.membership)}>
          <SvgCss xml={ASSETS.membership} height={20} width={20} style={styles.listIcon} />
          <Text>{translate('PROFILE_SCENE.MEMBERSHIP')}</Text>
        </TouchableO>
        {/* Coupons */}
        {/* <TouchableO style={[styles.cardContent, styles.borderBottom]} onPress={() => navigationScreen(NAME_ROUTE.coupons)}>
          <SvgCss xml={ASSETS.coupons} height={20} width={20} style={styles.listIcon} />
          <Text>{translate('PROFILE_SCENE.COUPONS')}</Text>
        </TouchableO> */}
        {/* Share and win */}
        <TouchableO style={styles.cardContent} onPress={() => navigationScreen(NAME_ROUTE.shareAndWin)}>
          <SvgCss xml={ASSETS.gift} height={20} width={20} style={styles.listIcon} />
          <Text>{translate('PROFILE_SCENE.SHARE_AND_WIN')}</Text>
        </TouchableO>
      </View>

      {/* Info */}
      <Text style={styles.cardTitle}>{translate('PROFILE_SCENE.INFO')}</Text>
      <View style={styles.card}>
        {/* Join as Dlavo partner */}
        <TouchableO style={[styles.cardContent, styles.borderBottom]}>
          <SvgCss xml={ASSETS.store} height={20} width={20} style={styles.listIcon} />
          <Text>{translate('PROFILE_SCENE.JOIN_AS_DLAVO_PARTNER')}</Text>
        </TouchableO>
        {/* FAQ */}
        <TouchableO style={[styles.cardContent, styles.borderBottom]} onPress={() => navigationScreen(NAME_ROUTE.faq)}>
          <IconCustom
            name='frequently-asked-questions'
            library={TypeLibraryIconsSystem.materialCommunityIcons}
            style={styles.listIcon}
            color={THEME_DEFAULT.black}
            size={20}
          />
          <Text>{translate('PROFILE_SCENE.FAQ')}</Text>
        </TouchableO>
        {/* Help */}
        <TouchableO style={styles.cardContent} onPress={() => navigationScreen(NAME_ROUTE.help)}>
          <SvgCss xml={ASSETS.help} height={20} width={20} style={styles.listIcon} />
          <Text>{translate('PROFILE_SCENE.HELP')}</Text>
        </TouchableO>
      </View>

      {/* Terms */}
      <Text style={styles.cardTitle}>{translate('PROFILE_SCENE.TERMS')}</Text>
      <View style={styles.card}>
        {/* About */}
        <TouchableO style={[styles.cardContent, styles.borderBottom]} onPress={() => navigationScreen(NAME_ROUTE.about)}>
          <SvgCss xml={ASSETS.dlavoLogoOne} height={20} width={20} style={styles.listIcon} fill={THEME_DEFAULT.black} />
          <Text>{translate('PROFILE_SCENE.ABOUT_DLAVO')}</Text>
        </TouchableO>
        {/* Privacy */}
        <TouchableO style={[styles.cardContent]} onPress={() => navigationScreen(NAME_ROUTE.privacy)}>
          <IconCustom
            name='info-outline'
            library={TypeLibraryIconsSystem.materialIcons}
            style={styles.listIcon}
            color={THEME_DEFAULT.black}
            size={20}
          />
          <Text>{translate('PROFILE_SCENE.PRIVACY')}</Text>
        </TouchableO>
      </View>

      {/* Sign out */}
      <View style={styles.card}>
        <TouchableO style={styles.cardContent} onPress={onBuyMembership}>
          <IconCustom
            name='sign-out'
            library={TypeLibraryIconsSystem.fontAwesome}
            color={THEME_DEFAULT.black}
            style={styles.listIcon}
            size={20}
          />
          <Text>{translate('PROFILE_SCENE.SIGN_OUT')}</Text>
        </TouchableO>
      </View>
    </ScrollView>
  );
};

export default ProfileOrganism;

const styles = StyleSheet.create({
  viewContainer: {
    padding: 0,
    paddingVertical: 20
  },
  name: {
    fontSize: 24,
    fontWeight: 'bold',
    paddingHorizontal: 20
  },
  card: {
    flex: 1,
    backgroundColor: THEME_DEFAULT.white,
    marginVertical: 8
  },
  cardContent: {
    flexDirection: 'row',
    marginHorizontal: 20,
    paddingVertical: 20,
    alignItems: 'center'
  },
  cardTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: THEME_DEFAULT.primary,
    paddingLeft: 18,
    paddingTop: 20
  },
  listIcon: {
    marginRight: 15
  },
  borderBottom: {
    borderBottomColor: THEME_DEFAULT.primaryOpacity,
    borderBottomWidth: 1
  }
});
