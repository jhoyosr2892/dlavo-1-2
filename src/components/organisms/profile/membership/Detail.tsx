import React, { useContext } from 'react';
import { StyleSheet, View } from 'react-native';

/* Components */
import ViewAtom from 'components/atoms/views';
import Text from 'components/atoms/texts';
import TouchableO from 'components/atoms/buttons/TouchableOpacity';
/* Configs */
import { METRICS, NAME_ROUTE, THEME_DEFAULT } from 'configs/Constants';
import { StyleButtonAlert, TypeLibraryIconsSystem } from 'configs/Enum';
/* Locales */
import { translate } from 'locales';
/* Models */
import { stylesGeneral } from 'styles';
import { BuyProducts, MembershipModel, TYPES_MEMBERSHIPS } from 'models/membership';
import ScrollView from 'components/atoms/scrollViews';
import IconCustom from 'components/atoms/icons';
import BottomButtonAtom from 'components/atoms/buttons/BottomButton';
import { currencyFormat } from 'utils/helpers/Convert';
import { RootContext } from 'configs/ContextApp';
import { ResponsePaymentMethodModel } from 'models/payment';
import { navigationScreen } from 'utils/helpers/Navigation';

// Props
interface props {
  membership: MembershipModel;
  buyMembership: (infoBuyProduct: BuyProducts) => void;
  paymentMethod: ResponsePaymentMethodModel;
}

const DetailMembershipOrganism = ({ membership, buyMembership, paymentMethod }: props) => {
  const { showAlert } = useContext(RootContext);

  const onBuyMembership = async () => {
    showAlert({
      title: translate('MEMBERSHIP_SCENE.BUY_ALERT.TITLE'),
      message: translate('MEMBERSHIP_SCENE.BUY_ALERT.MESSAGE'),
      options: [
        {
          text: translate('GENERAL.WORDS.WORD_YES'),
          onAction: async () => {
            buyMembership({
              creditCardNumber: paymentMethod.creditCardNumber,
              cvv: paymentMethod.cvv,
              expirationDate: paymentMethod.expirationDate,
              productId: membership.id
            });
          }
        },
        {
          text: translate('GENERAL.WORDS.WORD_NOT'),
          style: StyleButtonAlert.cancel
        }
      ]
    });
  };

  return (
    <ViewAtom style={{ flex: 1 }}>
      {membership && (
        <ScrollView contentContainerStyle={styles.viewContainer}>
          <Text style={styles.cardTitle}>
            {translate('MEMBERSHIP_SCENE.PRE_NOTE') +
              ' ' +
              TYPES_MEMBERSHIPS[membership.type] +
              ' ' +
              (membership.monthDuration < 12
                ? membership.monthDuration + ' ' + translate('GENERAL.TIME.MONTHS').toLocaleLowerCase()
                : 1 + ' ' + translate('GENERAL.TIME.YEAR').toLocaleLowerCase())}
          </Text>
          <ViewAtom>
            <Text style={styles.valueForLabel}>{translate('MEMBERSHIP_SCENE.VALUE_FOR')}</Text>
            <Text style={styles.valueFor}>
              {membership.tokens + ' ' + (membership.tokens === 1 ? translate('MEMBERSHIP_SCENE.BAG') : translate('MEMBERSHIP_SCENE.BAGS'))}
            </Text>
          </ViewAtom>
          <ViewAtom>
            <Text style={styles.priceMembership}>{currencyFormat(membership.price ? membership.price : 0, 0)} USD</Text>
          </ViewAtom>

          {/* Payment */}
          <View style={[styles.card, { marginVertical: 15 }, stylesGeneral.shadowAndElevation]}>
            <Text style={{ marginBottom: 10 }}>{translate('BAG_SCENE.PAYMENT.HEADER')}</Text>
            {paymentMethod.creditCardNumber ? (
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <IconCustom
                  name={'cc-mastercard'}
                  library={TypeLibraryIconsSystem.fontAwesome}
                  color='black'
                  size={18}
                  style={{ marginRight: 14 }}
                />
                <View>
                  <Text>
                    ***
                    {paymentMethod.creditCardNumber[paymentMethod.creditCardNumber.length - 4] +
                      paymentMethod.creditCardNumber[paymentMethod.creditCardNumber.length - 3] +
                      paymentMethod.creditCardNumber[paymentMethod.creditCardNumber.length - 2] +
                      paymentMethod.creditCardNumber[paymentMethod.creditCardNumber.length - 1]}
                  </Text>
                </View>
                <TouchableO
                  style={{ marginLeft: 'auto' }}
                  onPress={() =>
                    navigationScreen(NAME_ROUTE.addPaymentMethod, {
                      backRoute: NAME_ROUTE.pricingDetail,
                      membership,
                      paymentMethod: {
                        cardNumber: paymentMethod.creditCardNumber,
                        cardCvs: paymentMethod.cvv.toString(),
                        cardExpirationDate: paymentMethod.expirationDate,
                        name: paymentMethod.name
                      }
                    })
                  }>
                  <Text>{translate('GENERAL.BUTTONS.CHANGE')}</Text>
                </TouchableO>
              </View>
            ) : (
              // <AddPaymentMethodOrganism {...{ addPaymentMethod: _addPaymentMethod, paymentInfo: {} as PaymentMethodModel }} />
              <TouchableO
                style={{ flexDirection: 'row', alignItems: 'center' }}
                onPress={() => navigationScreen(NAME_ROUTE.addPaymentMethod, { backRoute: NAME_ROUTE.pricingDetail, membership })}>
                <IconCustom
                  name={'cc-mastercard'}
                  library={TypeLibraryIconsSystem.fontAwesome}
                  color='black'
                  size={18}
                  style={{ marginRight: 14 }}
                />
                <Text>{translate('BAG_SCENE.PAYMENT.ADD_PAYMENT_INFO')}</Text>
              </TouchableO>
            )}
          </View>
        </ScrollView>
      )}
      <BottomButtonAtom
        text={translate('MEMBERSHIP_SCENE.BUY_BUTTON')}
        onPress={onBuyMembership}
        disabled={!paymentMethod.creditCardNumber}
      />
    </ViewAtom>
  );
};

export default DetailMembershipOrganism;

const styles = StyleSheet.create({
  viewContainer: {
    padding: METRICS.CONSTANTS_PADDING,
    paddingBottom: 60
  },
  card: {
    backgroundColor: THEME_DEFAULT.white,
    padding: METRICS.CONSTANTS_PADDING,
    borderRadius: 8,
    marginVertical: 10
  },
  cardTitle: {
    color: THEME_DEFAULT.primary,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 10,
    fontSize: 25,
    textTransform: 'uppercase'
  },
  valueForLabel: {
    textAlign: 'center',
    marginTop: 15,
    fontSize: 22,
    color: THEME_DEFAULT.primaryOpacity
  },
  valueFor: {
    textAlign: 'center',
    marginTop: 10,
    fontSize: 25
  },
  priceMembership: {
    textAlign: 'center',
    marginTop: 25,
    fontSize: 35
  }
});
