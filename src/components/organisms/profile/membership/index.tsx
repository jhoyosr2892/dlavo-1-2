import React, { useState } from 'react';
import { StyleSheet } from 'react-native';

/* Libs */
import * as Animatable from 'react-native-animatable';
/* Components */
import ViewAtom from 'components/atoms/views';
import Text from 'components/atoms/texts';
import TouchableO from 'components/atoms/buttons/TouchableOpacity';
/* Configs */
import { METRICS, NAME_ROUTE, THEME_DEFAULT } from 'configs/Constants';
import { TypeLibraryIconsSystem } from 'configs/Enum';
/* Locales */
import { translate } from 'locales';
/* Models */
import { stylesGeneral } from 'styles';
import { MembershipModel, TYPES_MEMBERSHIPS } from 'models/membership';
import ScrollView from 'components/atoms/scrollViews';
import IconCustom from 'components/atoms/icons';
import { navigationScreen } from 'utils/helpers/Navigation';
import { capitalizeFirstLetter, currencyFormat } from 'utils/helpers/Convert';

// Props
interface props {
  memberships?: MembershipModel[];
}

const MembershipOrganism = ({ memberships }: props) => {
  const [activeSection, setActiveSection] = useState(0);
  return (
    <ViewAtom style={{ flex: 1 }}>
      <ScrollView contentContainerStyle={styles.viewContainer}>
        <Animatable.View
          duration={300}
          transition='backgroundColor'
          style={[styles.card, { backgroundColor: activeSection === 1 ? 'rgba(255,255,255,1)' : 'rgba(245,252,255,1)' }]}>
          <TouchableO
            style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}
            onPress={() => setActiveSection(activeSection === 1 ? 0 : 1)}>
            <Animatable.Text
              duration={300}
              easing='ease-out'
              animation={activeSection === 1 ? 'zoomIn' : ''}
              style={{ textAlign: 'center' }}>
              {translate('MEMBERSHIP_SCENE.MONTHLY')}
            </Animatable.Text>
            <IconCustom
              name={activeSection === 1 ? 'caret-down' : 'caret-right'}
              library={TypeLibraryIconsSystem.fontAwesome}
              color={THEME_DEFAULT.black}
            />
          </TouchableO>
        </Animatable.View>
        {activeSection === 1 &&
          memberships?.reverse()?.map(
            (membership, i) =>
              membership.monthDuration === 1 && (
                <Animatable.View duration={300} easing='ease-out' animation={'zoomIn'} key={i}>
                  <TouchableO
                    style={[styles.card, stylesGeneral.shadowAndElevation]}
                    onPress={() => navigationScreen(NAME_ROUTE.pricingDetail, { membership })}>
                    <ViewAtom style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                      <Text style={styles.cardTitle}>
                        {translate('MEMBERSHIP_SCENE.PRE_NOTE') +
                          ' ' +
                          TYPES_MEMBERSHIPS[membership.type] +
                          ' ' +
                          (membership.monthDuration === 1
                            ? membership.monthDuration + ' ' + capitalizeFirstLetter(translate('GENERAL.TIME.MONTH'))
                            : membership.monthDuration < 12
                            ? membership.monthDuration + ' ' + translate('GENERAL.TIME.MONTHS').toLocaleLowerCase()
                            : 1 + ' ' + translate('GENERAL.TIME.YEAR').toLocaleLowerCase())}
                      </Text>
                    </ViewAtom>
                    <Text>
                      {translate('MEMBERSHIP_SCENE.VALUE_FOR') +
                        ' ' +
                        membership.tokens / 3 +
                        ' ' +
                        (membership.tokens / 3 === 1 ? translate('MEMBERSHIP_SCENE.BAG') : translate('MEMBERSHIP_SCENE.BAGS'))}
                    </Text>
                    <Text>{currencyFormat(membership.price ? membership.price : 0, 0)} USD</Text>
                  </TouchableO>
                </Animatable.View>
              )
          )}

        {/* Yearly */}
        <Animatable.View
          duration={300}
          transition='backgroundColor'
          style={[styles.card, { backgroundColor: activeSection === 2 ? 'rgba(255,255,255,1)' : 'rgba(245,252,255,1)' }]}>
          <TouchableO
            style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}
            onPress={() => setActiveSection(activeSection === 2 ? 0 : 2)}>
            <Animatable.Text
              duration={300}
              easing='ease-out'
              animation={activeSection === 2 ? 'zoomIn' : ''}
              style={{ textAlign: 'center' }}>
              {translate('MEMBERSHIP_SCENE.YEARLY')}
            </Animatable.Text>
            <IconCustom
              name={activeSection === 2 ? 'caret-down' : 'caret-right'}
              library={TypeLibraryIconsSystem.fontAwesome}
              color={THEME_DEFAULT.black}
            />
          </TouchableO>
        </Animatable.View>
        {activeSection === 2 &&
          memberships?.reverse().map(
            (membership, i) =>
              membership.monthDuration === 12 && (
                <Animatable.View duration={300} easing='ease-out' animation={'zoomIn'} key={i}>
                  <TouchableO
                    style={[styles.card, stylesGeneral.shadowAndElevation]}
                    onPress={() => navigationScreen(NAME_ROUTE.pricingDetail, { membership })}>
                    <ViewAtom style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                      <Text style={styles.cardTitle}>
                        {translate('MEMBERSHIP_SCENE.PRE_NOTE') +
                          ' ' +
                          TYPES_MEMBERSHIPS[membership.type] +
                          ' ' +
                          (membership.monthDuration < 12
                            ? membership.monthDuration + ' ' + translate('GENERAL.TIME.MONTHS').toLocaleLowerCase()
                            : 1 + ' ' + translate('GENERAL.TIME.YEAR').toLocaleLowerCase())}
                      </Text>
                    </ViewAtom>
                    <Text>
                      {translate('MEMBERSHIP_SCENE.VALUE_FOR') +
                        ' ' +
                        membership.tokens / 3 +
                        ' ' +
                        (membership.tokens / 3 === 1 ? translate('MEMBERSHIP_SCENE.BAG') : translate('MEMBERSHIP_SCENE.BAGS'))}
                    </Text>
                    <Text>{currencyFormat(membership.price ? membership.price : 0, 0)} USD</Text>
                  </TouchableO>
                </Animatable.View>
              )
          )}
      </ScrollView>
    </ViewAtom>
  );
};

export default MembershipOrganism;

const styles = StyleSheet.create({
  viewContainer: {
    padding: METRICS.CONSTANTS_PADDING,
    paddingBottom: 60,
    justifyContent: 'center',
    minHeight: '100%'
  },
  card: {
    backgroundColor: THEME_DEFAULT.white,
    padding: METRICS.CONSTANTS_PADDING,
    borderRadius: 8,
    marginVertical: 10
  },
  cardTitle: {
    color: THEME_DEFAULT.primary,
    fontWeight: 'bold'
  },
  currentMembership: {
    fontWeight: '100',
    fontSize: 16
  }
});
