import React from 'react';
import { ActivityIndicator, StyleSheet } from 'react-native';

/* Configs */
import { METRICS, THEME_DEFAULT } from 'configs/Constants';

/* Libs */
import { WebView } from 'react-native-webview';
import { Text, View } from 'react-native-animatable';
import ViewAtom from 'components/atoms/views';
import { translate } from 'locales';
import ScrollView from 'components/atoms/scrollViews';

// Props
interface props {}

const AboutUsOrganism = ({}: props) => {
  return (
    <ScrollView contentContainerStyle={styles.viewContainer}>
      <Text style={styles.title}>{translate('ABOUT_US_SCENE.TITLE_1')}</Text>
      <Text style={styles.message}>{translate('ABOUT_US_SCENE.MESSAGE_1A')}</Text>
      <Text style={styles.message}>{translate('ABOUT_US_SCENE.MESSAGE_1B')}</Text>
      <Text style={styles.message}>{translate('ABOUT_US_SCENE.MESSAGE_1C')}</Text>
      <Text style={styles.message}>{translate('ABOUT_US_SCENE.MESSAGE_1D')}</Text>
      <Text style={styles.title}>{translate('ABOUT_US_SCENE.TITLE_2')}</Text>
      <Text style={styles.message}>{translate('ABOUT_US_SCENE.MESSAGE_2A')}</Text>
      <Text style={styles.title}>{translate('ABOUT_US_SCENE.TITLE_3')}</Text>
      <Text style={styles.message}>{translate('ABOUT_US_SCENE.MESSAGE_3A')}</Text>
      <Text style={styles.title}>{translate('ABOUT_US_SCENE.TITLE_4')}</Text>
      <Text style={styles.message}>{translate('ABOUT_US_SCENE.MESSAGE_4A')}</Text>
      <Text style={styles.title}>{translate('ABOUT_US_SCENE.TITLE_5')}</Text>
      <Text style={styles.message}>{translate('ABOUT_US_SCENE.MESSAGE_5A')}</Text>
    </ScrollView>
  );
};

export default AboutUsOrganism;

const styles = StyleSheet.create({
  viewContainer: {
    padding: METRICS.CONSTANTS_PADDING,
    paddingBottom: 60
  },
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 5,
    marginTop: 8,
    textAlign: 'center'
  },
  message: {
    textAlign: 'center',
    fontSize: 16,
    marginBottom: 3
  }
});
