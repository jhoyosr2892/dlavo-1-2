import { ActivityIndicator, StyleSheet, View } from 'react-native';
import React from 'react';
import WebView from 'react-native-webview';
import { METRICS, THEME_DEFAULT } from 'configs/Constants';

const FaqOrganisms = () => {
  return (
    <WebView
      source={{ uri: 'https://faq.dlavo.com/' }}
      textZoom={250}
      scalesPageToFit={true}
      renderLoading={() => (
        <View style={{ position: 'absolute', top: 0, width: '100%', justifyContent: 'center' }}>
          <ActivityIndicator color={THEME_DEFAULT.primary} size='large' />
        </View>
      )}
      startInLoadingState={true}
    />
  );
};

export default FaqOrganisms;

const styles = StyleSheet.create({});
