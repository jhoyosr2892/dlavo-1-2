import React, { useRef, useState } from 'react';
import { StyleSheet, TextInput } from 'react-native';

/* Libs */
import CardValidator from 'card-validator';
/* Components */
import ViewAtom from 'components/atoms/views';
import Text from 'components/atoms/texts';
/* Configs */
import { FAMILY_FONTS, INPUT_ACCESSORY_ID, METRICS, THEME_DEFAULT } from 'configs/Constants';
import { TypeLibraryIconsSystem } from 'configs/Enum';
/* Locales */
import { translate } from 'locales';
/* Models */
import { PaymentMethodModel } from 'models/payment';
import BottomButtonAtom from 'components/atoms/buttons/BottomButton';
import TextInputMaskAtom from 'components/atoms/textFields/TextInputMask';
import ScrollView from 'components/atoms/scrollViews';
import TextField from 'components/atoms/textFields';

declare type CreditCardType = {
  niceType?: string;
  type?: string;
  patterns?: Array<number[] | number>;
  gaps?: number[];
  lengths?: number[];
  code?: {
    name: string;
    size: number;
  };
};

// Props
interface props {
  paymentInfo: PaymentMethodModel;
  addPaymentMethod: ({ cardNumber, cardCvs, cardExpirationDate, name }: PaymentMethodModel) => void;
}

const AddPaymentMethodOrganism = ({ paymentInfo, addPaymentMethod }: props) => {
  /* States */
  const [cardNumber, setCardNumber] = useState<string | undefined>(paymentInfo.cardNumber);
  const [cvv, setCvv] = useState<string | undefined>(paymentInfo.cardCvs);
  const [expirationMonth, setExpirationMonth] = useState<string | undefined>(paymentInfo.cardExpirationDate?.split('/')[0]);
  const [expirationYear, setExpirationYear] = useState<string | undefined>(paymentInfo.cardExpirationDate?.split('/')[1]);
  const [cardNumberInfo, setCardNumberInfo] = useState<CreditCardType | null>({ niceType: '' });
  const [nameUser, setNameUser] = useState<string | undefined>(paymentInfo.name);
  /* Refs */
  const cardNumberRef = useRef<TextInput | null>(null);
  const cvvRef = useRef<TextInput | null>(null);
  const expirationMonthRef = useRef<TextInput | null>(null);
  const expirationYearRef = useRef<TextInput | null>(null);
  const nameUserRef = useRef<TextInput | null>(null);

  const validateCardNumber = (formatted: string, extracted?: string | undefined) => {
    const numberValidtion = CardValidator.number(extracted);
    if (numberValidtion.isPotentiallyValid && extracted) {
      setCardNumber(formatted);
      setCardNumberInfo(numberValidtion.card);
    } else {
      setCardNumberInfo({ niceType: translate('ADD_PAYMENT_METHOD_SCENE.INVALID_CARD') });
    }
  };

  return (
    <ViewAtom style={{ flex: 1 }}>
      <ScrollView contentContainerStyle={styles.viewContainer}>
        {/* Card number */}
        <ViewAtom style={styles.formInput}>
          <Text style={styles.formText}>
            {translate('ADD_PAYMENT_METHOD_SCENE.CARD_NUMBER') + (cardNumberInfo?.niceType ? ' - ' + cardNumberInfo?.niceType : '')}
          </Text>
          <TextInputMaskAtom
            refInput={cardNumberRef}
            value={cardNumber}
            editable={true}
            autoCapitalize='none'
            keyboardType='number-pad'
            textContentType={'creditCardNumber'}
            placeholder={'xxxx xxxx xxxx xxxx'}
            mask={'[0000] [0000] [0000] [0000]'}
            blurOnSubmit={false}
            returnKeyType={'next'}
            onSubmitEditing={() => cvvRef.current?.focus()}
            containerStyle={{ backgroundColor: THEME_DEFAULT.white }}
            rightButtonProps={{ show: true, nameIcon: 'card', libraryIcon: TypeLibraryIconsSystem.ionicons }}
            inputAccessoryId={INPUT_ACCESSORY_ID.in}
            onChange={(formatted: string, extracted?: string | undefined) => validateCardNumber(formatted, extracted)}
            setValue={value => {
              setCardNumber(value);
              setCardNumberInfo({ niceType: '' });
            }}
          />
        </ViewAtom>
        {/* CVV */}
        <ViewAtom style={styles.formInput}>
          <Text style={styles.formText}>{translate('ADD_PAYMENT_METHOD_SCENE.CVV')}</Text>
          <TextInputMaskAtom
            refInput={cvvRef}
            value={cvv}
            editable={true}
            autoCapitalize='none'
            keyboardType='number-pad'
            placeholder={'xxxx'}
            mask={'[0000]'}
            blurOnSubmit={false}
            returnKeyType={'next'}
            onSubmitEditing={() => expirationMonthRef.current?.focus()}
            containerStyle={{ backgroundColor: THEME_DEFAULT.white }}
            rightButtonProps={{ show: true, nameIcon: 'key', libraryIcon: TypeLibraryIconsSystem.ionicons }}
            inputAccessoryId={INPUT_ACCESSORY_ID.in}
            onChange={(formatted: string, extracted?: string | undefined) => setCvv(extracted)}
            setValue={setCvv}
          />
        </ViewAtom>

        {/* EXPIRATION DATE */}
        <ViewAtom style={styles.formInput}>
          {/* <TextInputMaskAtom
              refInput={expirationDateRef}
              value={expirationDate}
              editable={true}
              autoCapitalize='none'
              keyboardType='number-pad'
              placeholder={'MM/YY'}
              mask={'[00]{/}[00]'}
              containerStyle={{ backgroundColor: THEME_DEFAULT.white }}
              rightButtonProps={{ show: true, nameIcon: 'calendar', libraryIcon: TypeLibraryIconsSystem.ionicons }}
              inputAccessoryId={INPUT_ACCESSORY_ID.in}
              onChange={(formatted: string, extracted?: string | undefined) => +formatted.split('/') < 13 && setExpirationDate(formatted)}
              setValue={setExpirationDate}
            /> */}

          <ViewAtom style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between' }}>
            <ViewAtom style={{ flex: 1, marginRight: 5 }}>
              <Text style={styles.formText}>{translate('GENERAL.TIME.MONTH')}</Text>
              <TextInputMaskAtom
                refInput={expirationMonthRef}
                value={expirationMonth}
                editable={true}
                autoCapitalize='none'
                keyboardType='number-pad'
                placeholder={'MM'}
                mask={'[00]'}
                blurOnSubmit={false}
                returnKeyType={'next'}
                onSubmitEditing={() => expirationYearRef.current?.focus()}
                containerStyle={{ backgroundColor: THEME_DEFAULT.white }}
                rightButtonProps={{ show: true, nameIcon: 'calendar', libraryIcon: TypeLibraryIconsSystem.ionicons }}
                inputAccessoryId={INPUT_ACCESSORY_ID.in}
                onChange={(formatted: string, extracted?: string | undefined) => {
                  if (parseInt(formatted, 10) > 12) {
                    setExpirationMonth('12');
                  } else if (parseInt(formatted, 10) < 1) {
                    setExpirationMonth('1');
                  } else {
                    setExpirationMonth(formatted);
                  }
                }}
                setValue={setExpirationMonth}
              />
            </ViewAtom>
            <ViewAtom style={{ flex: 1, marginLeft: 5 }}>
              <Text style={styles.formText}>{translate('GENERAL.TIME.YEAR')}</Text>
              <TextInputMaskAtom
                refInput={expirationYearRef}
                value={expirationYear}
                editable={true}
                autoCapitalize='none'
                keyboardType='number-pad'
                placeholder={'YY'}
                mask={'[00]'}
                containerStyle={{ backgroundColor: THEME_DEFAULT.white }}
                rightButtonProps={{ show: true, nameIcon: 'calendar', libraryIcon: TypeLibraryIconsSystem.ionicons }}
                inputAccessoryId={INPUT_ACCESSORY_ID.in}
                onChange={(formatted: string, extracted?: string | undefined) => {
                  setExpirationYear(formatted);
                }}
                setValue={setExpirationYear}
                blurOnSubmit={false}
                returnKeyType={'next'}
                onSubmitEditing={() => nameUserRef.current?.focus()}
              />
            </ViewAtom>
          </ViewAtom>
        </ViewAtom>

        {/* NAME */}
        <ViewAtom style={styles.formInput}>
          <Text style={styles.formText}>{translate('ADD_PAYMENT_METHOD_SCENE.NAME')}</Text>
          <TextField
            refInput={nameUserRef}
            value={nameUser}
            editable={true}
            autoCapitalize='words'
            placeholder={translate('ADD_PAYMENT_METHOD_SCENE.NAME_PLACEHOLDER')}
            returnKeyType={'done'}
            containerStyle={{ backgroundColor: THEME_DEFAULT.white }}
            rightButtonProps={{ show: true, nameIcon: 'user', libraryIcon: TypeLibraryIconsSystem.fontAwesome }}
            inputAccessoryId={INPUT_ACCESSORY_ID.in}
            onChange={text => setNameUser(text)}
          />
        </ViewAtom>
      </ScrollView>
      <BottomButtonAtom
        text={translate('GENERAL.MESSAGES.BUTTON_CONFIRM')}
        onPress={() =>
          addPaymentMethod({
            cardNumber: cardNumber,
            cardCvs: cvv,
            cardExpirationDate: expirationMonth + '/' + expirationYear,
            name: nameUser
          })
        }
        disabled={!cardNumber || !cvv || !expirationMonth || !expirationYear}
      />
    </ViewAtom>
  );
};

export default AddPaymentMethodOrganism;

const styles = StyleSheet.create({
  viewContainer: {
    padding: METRICS.CONSTANTS_PADDING,
    paddingBottom: 60
  },
  formInput: {
    marginVertical: 10
  },
  formText: {
    color: THEME_DEFAULT.primary,
    fontFamily: FAMILY_FONTS.primaryRegular,
    fontSize: 18,
    marginBottom: 10
  }
});
