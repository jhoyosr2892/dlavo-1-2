import React from 'react';
import { StyleSheet } from 'react-native';

/* Components */
import ViewAtom from 'components/atoms/views';
import Text from 'components/atoms/texts';
import IconCustom from 'components/atoms/icons';
import ScrollView from 'components/atoms/scrollViews';
import TouchableO from 'components/atoms/buttons/TouchableOpacity';
/* Configs */
import { METRICS, NAME_ROUTE, SIZE_ICONS, THEME_DEFAULT } from 'configs/Constants';
import { TypeLibraryIconsSystem } from 'configs/Enum';
/* Locales */
import { translate } from 'locales';
/* Models */
import { PaymentMethodModel } from 'models/payment';
import { navigationScreen } from 'utils/helpers/Navigation';

// Props
interface props {
  paymentMethods: PaymentMethodModel[];
  deleteMethod: (index: number) => void;
  onSelectMethod: (index: number) => void;
}

const PaymentMethodsOrganism = ({ paymentMethods, deleteMethod, onSelectMethod }: props) => {
  return (
    <ViewAtom style={styles.container}>
      <ViewAtom style={styles.generalCard}>
        <TouchableO style={styles.addAddressButton} onPress={() => navigationScreen(NAME_ROUTE.addPaymentMethod)}>
          <Text style={{ color: THEME_DEFAULT.primary, fontSize: 18 }}>{translate('ADD_PAYMENT_METHOD_SCENE.CARD_NUMBER')}</Text>
          <IconCustom name='plus' library={TypeLibraryIconsSystem.materialCommunityIcons} color={THEME_DEFAULT.primary} />
        </TouchableO>
        <ScrollView contentContainerStyle={{ padding: 0 }} keyboardShouldPersistTaps='always'>
          {paymentMethods.map((method, index) => (
            <TouchableO key={index} style={styles.card} onPress={() => onSelectMethod(index)}>
              <IconCustom
                name={'map-pin'}
                library={TypeLibraryIconsSystem.fontAwesome}
                size={SIZE_ICONS.generalSmall}
                color={THEME_DEFAULT.primary}
                style={{ textAlignVertical: 'center', flex: 0.1 }}
              />
              <Text style={{ flex: 1, textAlignVertical: 'center' }}>{method.cardNumber}</Text>
              <TouchableO style={styles.deleteButton} onPress={() => onSelectMethod(index)}>
                <IconCustom
                  name={method.selected ? 'radio-button-checked' : 'radio-button-unchecked'}
                  library={TypeLibraryIconsSystem.materialIcons}
                  size={SIZE_ICONS.inputOption}
                  color={THEME_DEFAULT.primary}
                  style={{ textAlignVertical: 'center' }}
                />
              </TouchableO>
            </TouchableO>
          ))}
        </ScrollView>
      </ViewAtom>
    </ViewAtom>
  );
};

export default PaymentMethodsOrganism;

const styles = StyleSheet.create({
  container: {
    marginTop: METRICS.CONSTANTS_MARGIN,
    height: '100%',
    width: '100%'
  },
  generalCard: {
    backgroundColor: THEME_DEFAULT.white,
    padding: 20
  },
  card: {
    paddingVertical: METRICS.CONSTANTS_PADDING,
    borderTopColor: THEME_DEFAULT.primaryOpacity,
    borderTopWidth: 1,
    flexDirection: 'row'
  },
  addAddressButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20
  },
  deleteButton: {
    flex: 0.1,
    justifyContent: 'center',
    padding: 10
  }
});
