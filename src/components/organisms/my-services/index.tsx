import Button from 'components/atoms/buttons';
import FlatList from 'components/atoms/flatLists';
import ScrollView from 'components/atoms/scrollViews';
import { METRICS, THEME_DEFAULT } from 'configs/Constants';
import { ServiceModel } from 'models/service';
import { RequestTypesNamesArray, RequestType, RequestTypes, RequestModel } from 'models/service/Request';
import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';

import { stylesGeneral } from 'styles';
import MyServiceCardOrganism from './MyServiceCard';

type MyServiceOrganismProps = {
  services: RequestModel[] | undefined;
  getServices: (status: RequestTypes) => void;
  refreshing: boolean;
};

const MyServiceOrganism = ({ services, getServices, refreshing }: MyServiceOrganismProps) => {
  const [selectedTab, setTab] = useState(0);
  const [statusSelected, setStatusSelected] = useState<RequestTypes>(RequestTypesNamesArray[0].type);

  const changeTab = (tab: number, status: RequestTypes) => {
    setTab(tab);
    setStatusSelected(status);
    getServices(status);
  };

  return (
    <View style={styles.viewContainer}>
      {/* Buttons */}
      <View>
        <ScrollView horizontal={true} contentContainerStyle={{ padding: 0 }}>
          {RequestTypesNamesArray.map((requestType: RequestType, i: number) => (
            <Button
              key={i}
              text={requestType.name}
              style={[styles.tabButton, stylesGeneral.shadowAndElevation, selectedTab === i && styles.activeTab]}
              textStyle={[styles.tabText, selectedTab === i && styles.activeTabText]}
              onPress={() => changeTab(i, requestType.type)}
            />
          ))}
        </ScrollView>
      </View>
      {/* List */}
      <FlatList
        data={services}
        renderItem={({ item, index }) => <MyServiceCardOrganism service={item} key={index} selectedTab={selectedTab} />}
        keyExtractor={(item, index) => index.toString()}
        onRefresh={() => getServices(statusSelected)}
        refreshing={refreshing}
      />
    </View>
  );
};

export default MyServiceOrganism;

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    padding: METRICS.CONSTANTS_PADDING,
    paddingTop: 30
  },
  tabButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  tabButton: {
    backgroundColor: THEME_DEFAULT.white,
    paddingHorizontal: 20,
    flex: 1
  },
  tabText: {
    color: THEME_DEFAULT.primary
  },
  activeTab: {
    backgroundColor: THEME_DEFAULT.primary
  },
  activeTabText: {
    color: THEME_DEFAULT.white
  }
});
