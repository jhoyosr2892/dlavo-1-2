import BottomButtonAtom from 'components/atoms/buttons/BottomButton';
import TouchableO from 'components/atoms/buttons/TouchableOpacity';
import ScrollView from 'components/atoms/scrollViews';
import SvgCss from 'components/atoms/svgs/SvgCss';
import { ASSETS, METRICS, THEME_DEFAULT } from 'configs/Constants';
import { translate } from 'locales';
import { BagsValues, typeBag } from 'models/bag';
import { ClothesValues } from 'models/clothes';
import { SERVICE_CATEGORY_NAMES, SERVICE_CATEGORY, SERVICE_CATEGORIES } from 'models/enums/ServiceCategory';
import { TypePickerEnum, TypePickerNames } from 'models/enums/TypePickers';
import { RequestModel } from 'models/service/Request';
import moment from 'moment';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { stylesGeneral } from 'styles';

type props = {
  request?: RequestModel;
  total: number;
  bags: { label: string; price: number | undefined }[];
  clothes: { label: string; price: number | undefined }[];
  servicioAdicional: { label: string; price: number | undefined }[];
};

const MyServiceDetailOrganism = ({ request, total, bags, clothes, servicioAdicional }: props) => {
  return (
    <View style={{ flex: 1 }}>
      <ScrollView contentContainerStyle={styles.viewContainer}>
        {/* View bags */}
        {/* <TouchableO style={styles.card}>
          <View style={styles.cardContent}>
            <SvgCss xml={ASSETS.bag} height={20} width={20} style={styles.listIcon} />
            <Text>{translate('MY_SERVICE_DETAIL_SCENE.VIEW_BAGS')}</Text>
          </View>
        </TouchableO> */}
        {/* Partner */}
        {/* <View style={styles.card}>
          <Text style={styles.cardTitle}>{translate('MY_SERVICE_DETAIL_SCENE.PARTNER')}</Text>
          <View style={[styles.cardContent, styles.borderBottom]}>
            <SvgCss xml={ASSETS.avatar} height={20} width={20} style={styles.listIcon} />
            <Text>Eunice Jenkins</Text>
          </View>
          <View style={styles.cardContent}>
            <SvgCss xml={ASSETS.star} height={20} width={20} style={styles.listIcon} />
            <Text>4.95</Text>
          </View>
        </View> */}
        {/* Address */}
        <View style={styles.card}>
          <Text style={styles.cardTitle}>{translate('MY_SERVICE_DETAIL_SCENE.ADDRESS')}</Text>
          <View style={[styles.cardContent, styles.borderBottom]}>
            <SvgCss xml={ASSETS.mapPing} height={20} width={20} style={styles.listIcon} />
            <Text>{request?.address.address}</Text>
          </View>
          {/* <View style={styles.cardContent}>
            <Text>{request?.address.address}</Text>
          </View> */}
        </View>

        {request?.services?.length > 0 && (
          <View style={styles.card}>
            {/* Pickup */}
            <Text style={styles.cardTitle}>{translate('MY_SERVICE_DETAIL_SCENE.SERVICES')}</Text>
            {request?.services?.map((service, i) => (
              <View key={i}>
                <View style={[styles.cardSubContent, styles.borderBottom]}>
                  <Text style={styles.serviceType}>
                    {i + 1}. {service.type ? SERVICE_CATEGORY_NAMES[service.type] : ''}
                  </Text>
                  <View style={{ flex: 1 }}>
                    {(service.type === SERVICE_CATEGORIES.WASH_AND_FOLD || service.type === SERVICE_CATEGORIES.FOLD) && (
                      <View>
                        <Text>{translate('MY_SERVICE_DETAIL_SCENE.BAGS')}: </Text>
                        {service.bags.map((s, j) => (
                          <Text>
                            - {BagsValues[(s.type as typeBag).value]} x {s.quantity}
                          </Text>
                        ))}
                      </View>
                    )}
                    {(service.type === SERVICE_CATEGORIES.BULKY_ITEMS || service.type === SERVICE_CATEGORIES.DRY_CLEANING) && (
                      <View>
                        <Text>{translate('MY_SERVICE_DETAIL_SCENE.CLOTHES')}</Text>
                        {service.clothes.map((s, j) => (
                          <Text>
                            - {ClothesValues[s.type]} x {s.quantity}
                          </Text>
                        ))}
                      </View>
                    )}
                  </View>
                </View>
              </View>
            ))}
          </View>
        )}

        {request?.services?.length > 0 && (
          <View style={styles.card}>
            {/* Pickup */}
            <Text style={styles.cardTitle}>{translate('MY_SERVICE_DETAIL_SCENE.PICKUP')}</Text>
            <View style={[styles.cardContent, styles.borderBottom]}>
              <SvgCss xml={ASSETS.clock} height={20} width={20} style={styles.listIcon} />
              <Text>{request.services[0] ? moment(request.services[0].pickUpSingleTime).format('LLLL') : ''}</Text>
            </View>
            <View style={styles.cardContent}>
              <SvgCss xml={ASSETS.truck} height={20} width={20} style={[styles.listIcon, { transform: [{ rotateY: '180deg' }] }]} />
              <Text>{request.services[0].pickUpType && TypePickerNames[request.services[0].pickUpType]}</Text>
            </View>
            {/* DELIVERY */}
            <View>
              <Text style={styles.cardTitle}>{translate('MY_SERVICE_DETAIL_SCENE.DELIVERY')}</Text>
              <View style={[styles.cardContent, styles.borderBottom]}>
                <SvgCss xml={ASSETS.clock} height={20} width={20} style={styles.listIcon} />
                <Text>{request.services[0] ? moment(request.services[0].deliverySingleTime).format('LLLL') : ''}</Text>
              </View>
              <View style={styles.cardContent}>
                <SvgCss xml={ASSETS.truck} height={20} width={20} style={styles.listIcon} />
                <Text>{request.services[0].pickUpType && TypePickerNames[request.services[0].pickUpType]}</Text>
              </View>
              {request.services[0].additionalNotes && (
                <TouchableO style={styles.cardContent}>
                  <Text>{translate('MY_SERVICE_DETAIL_SCENE.ADDITIONAL_NOTES')}</Text>
                  <Text>{request.services[0].additionalNotes?.specializedInstructions}</Text>
                  <Text>{request.services[0].additionalNotes?.stateOfClothe}</Text>
                </TouchableO>
              )}
            </View>
          </View>
        )}

        {/* TOTAL */}
        {/* <View style={styles.card}>
          <View style={[styles.cardContent, { justifyContent: 'space-between' }, styles.borderBottom]}>
            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{translate('MY_SERVICE_DETAIL_SCENE.TOTAL')}</Text>
            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>$ 162</Text>
          </View>
          <View style={styles.cardContent}>
            <View style={{ flex: 1 }}>
              <View style={[{ justifyContent: 'space-between', flexDirection: 'row' }]}>
                <Text>7.5 lb x 2</Text>
                <Text>$ 97.5</Text>
              </View>
              <View style={[{ justifyContent: 'space-between', flexDirection: 'row' }]}>
                <Text>15 lb x 2</Text>
                <Text>$ 49.5</Text>
              </View>
              <View style={[{ justifyContent: 'space-between', flexDirection: 'row' }]}>
                <Text>Ironing</Text>
                <Text>$ 15</Text>
              </View>
              <View style={[{ justifyContent: 'space-between', flexDirection: 'row' }]}>
                <Text>Fees</Text>
                <Text>$ 0</Text>
              </View>
            </View>
          </View>
        </View> */}

        {/* Resume */}
        <View style={[styles.card, { marginVertical: 15, paddingHorizontal: 10, paddingBottom: 10 }, stylesGeneral.shadowAndElevation]}>
          {/* Bags */}
          {bags.map((b, i) => (
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} key={i}>
              <Text>{b.label}</Text>
              <Text>{b.price} $</Text>
            </View>
          ))}
          {/* Clothes */}
          {clothes.map((b, i) => (
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} key={i}>
              <Text>{b.label}</Text>
              <Text>{b.price} $</Text>
            </View>
          ))}
          {/* Service aditionals */}
          {servicioAdicional.map((b, i) => (
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} key={i}>
              <Text>{b.label}</Text>
              <Text>{b.price} $</Text>
            </View>
          ))}
          <View style={{ width: '100%', height: 1, marginVertical: 20, backgroundColor: THEME_DEFAULT.primary }}></View>
          {/* Total */}
          <View style={[{ flexDirection: 'row', justifyContent: 'space-between' }]}>
            <Text style={{ fontWeight: 'bold', color: THEME_DEFAULT.black }}>{translate('BAG_SCENE.TOTAL')}</Text>
            <Text style={{ fontWeight: 'bold', color: THEME_DEFAULT.black }}>
              <Text style={{ marginRight: 3, fontWeight: 'bold', color: THEME_DEFAULT.black }}>{total}</Text>
              {'\0'} $
            </Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default MyServiceDetailOrganism;

const styles = StyleSheet.create({
  viewContainer: {
    padding: 0,
    paddingVertical: 20
  },
  card: {
    flex: 1,
    backgroundColor: THEME_DEFAULT.white,
    marginVertical: 8
  },
  cardContent: {
    flexDirection: 'row',
    marginHorizontal: 20,
    paddingVertical: 20,
    alignItems: 'center'
  },
  cardSubContent: {
    marginHorizontal: 20,
    paddingVertical: 20
  },
  cardTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: THEME_DEFAULT.primary,
    paddingLeft: 18,
    paddingTop: 20
  },
  listIcon: {
    marginRight: 15
  },
  borderBottom: {
    borderBottomColor: THEME_DEFAULT.primaryOpacity,
    borderBottomWidth: 1
  },
  serviceType: {
    fontWeight: 'bold'
  }
});
