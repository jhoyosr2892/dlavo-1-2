/* Libs */
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
/* Components */
import TouchableO from '@atoms/buttons/TouchableOpacity';
/* Configs */
import { NAME_ROUTE, THEME_DEFAULT } from '@config/Constants';
/* Models */
import { ServiceRequestResponseServer } from '@models/service';
/* Styles */
import { stylesGeneral } from 'styles';
/* Utils */
import { navigationScreen } from '@utils/helpers/Navigation';
import { RequestModel, RequestTypesNames } from 'models/service/Request';
import { translate } from 'locales';
import { SERVICE_CATEGORY_NAMES } from 'models/enums/ServiceCategory';
import { BagsValues } from 'models/bag';
import { ClothesValues } from 'models/clothes';

type MyServiceCardOrganismProps = {
  service: RequestModel;
  selectedTab: number;
};

const MyServiceCardOrganism = ({ service, selectedTab }: MyServiceCardOrganismProps) => {
  return (
    <View style={[styles.card, stylesGeneral.shadowAndElevation]}>
      {service.services &&
        service.services?.map((s, i) => (
          <View key={i}>
            <Text style={styles.quantity}>{SERVICE_CATEGORY_NAMES[s.type]}</Text>
            {s.bags &&
              s.bags.map((b, j) => (
                <Text key={j}>
                  {BagsValues[b.type.value]} x {b.quantity}
                </Text>
              ))}
            {s.clothes &&
              s.clothes.map((c, e) => (
                <Text key={e}>
                  {ClothesValues[c.type]} x {c.quantity}
                </Text>
              ))}
          </View>
        ))}
      {/* <Text style={styles.serviceCategory}>{service && SERVICE_CATEGORY_NAMES[service?.serviceCategory]}</Text> */}
      <Text style={styles.status}>{RequestTypesNames[service.status]}</Text>
      {/* Action buttons */}
      <View style={styles.actionButtons}>
        {/* <TouchableO style={[styles.actionButton, { borderRightWidth: 1, borderRightColor: THEME_DEFAULT.borderLine }]}>
          <Text style={styles.actionButtonText}>SMS</Text>
        </TouchableO> */}
        <TouchableO style={styles.actionButton} onPress={() => navigationScreen(NAME_ROUTE.myServiceDetail, { request: service })}>
          <Text style={styles.actionButtonText}>{translate('MY_SERVICES_SCENE.DETAILS')}</Text>
        </TouchableO>
      </View>
    </View>
  );
};

export default MyServiceCardOrganism;

const styles = StyleSheet.create({
  card: {
    flex: 1,
    backgroundColor: THEME_DEFAULT.white,
    borderRadius: 8,
    marginVertical: 8,
    padding: 10
  },
  quantity: {
    fontSize: 20,
    fontWeight: '700'
  },
  serviceCategory: {
    fontSize: 16
  },
  status: {
    fontSize: 14,
    color: THEME_DEFAULT.primaryOpacity
  },
  actionButtons: {
    flexDirection: 'row',
    borderTopColor: THEME_DEFAULT.borderLine,
    borderTopWidth: 1,
    marginTop: 10,
    paddingTop: 5
  },
  actionButton: {
    flex: 1,
    alignItems: 'center'
  },
  actionButtonText: {
    color: THEME_DEFAULT.primaryOpacity
  }
});
