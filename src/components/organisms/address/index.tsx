import React, { useContext, useState } from 'react';
import { FlatList, StyleSheet } from 'react-native';

/* Libs */
import { GooglePlaceData, GooglePlaceDetail } from 'react-native-google-places-autocomplete';
/* Config */
import { LatLng, METRICS, NAME_ROUTE, SIZE_ICONS, THEME_DEFAULT } from 'configs/Constants';
import { TypeLibraryIconsSystem } from 'configs/Enum';
/* Components */
import ViewAtom from '@atoms/views';
import Text from '@atoms/texts';
import IconCustom from '@atoms/icons';
import TouchableO from '@atoms/buttons/TouchableOpacity';
import GooglePlacesAutocompleteAtom from 'components/atoms/textFields/GooglePlacesAutocomplete';
/* Styles */
import { stylesGeneral } from 'styles';
/* Locales */
import { translate } from 'locales';
/* Helpers */
import { navigationScreen } from '@helpers/Navigation';
/* Models */
import { AddressModel } from 'models/address';
import { CLog } from 'utils/helpers/Message';
import PlaceGoogle, { typesAddressComponent } from 'models/google/Place';
import { getGeocodeReverse } from 'services/app/Google';
import { RootContext } from 'configs/ContextApp';
import BottomButtonAtom from 'components/atoms/buttons/BottomButton';
import { useNavigation } from '@react-navigation/native';

// Props
interface props {
  addresses: AddressModel[];
  deleteAddress: (index: number) => void;
  onSelectAddress: (index: number) => void;
  backRoute?: string;
}

const URL_PATH = 'src/components/organisms/address/index.tsx';

const AddressOrganism = ({ addresses, onSelectAddress, deleteAddress, backRoute }: props) => {
  /* Context */
  const { showAlertSheet, removeAlertSheet } = useContext(RootContext);
  /* State */
  const [showAddressAutocomplete, setShowAddressAutocomplete] = useState<boolean>(false);
  const [address, setAddress] = useState<AddressModel>();
  const [lat, setLatitude] = useState<number>(4.6007172);
  const [lng, setLongitude] = useState<number>(-74.1146825);
  const [zipCode, setZipCode] = useState<string>('');

  const navigation = useNavigation();

  const onSelectAutocompleteAddress = async (data: GooglePlaceData, details: GooglePlaceDetail | null) => {
    if (details) {
      console.log('====================================');
      console.log(data);
      console.log('====================================');
      const addressCopy = { ...address };
      addressCopy.address = details.formatted_address;
      addressCopy.city = '';
      // if (splitAddress[splitAddress.length - 3]) {
      //   addressCopy.city += splitAddress[splitAddress.length - 3] + ', ';
      // }
      // if (splitAddress[splitAddress.length - 2]) {
      //   addressCopy.city += splitAddress[splitAddress.length - 2] + ', ';
      // }
      // if (splitAddress[splitAddress.length - 1]) {
      // }
      addressCopy.city = data.structured_formatting.secondary_text;
      addressCopy.coordinates = `${details.geometry.location.lat}, ${details.geometry.location.lng}`;
      setAddress(addressCopy);
      setLatitude(details.geometry.location.lat);
      setLongitude(details.geometry.location.lng);
      await searchGeocodeReverse({
        coordinate: {
          latitude: details.geometry.location.lat,
          longitude: details.geometry.location.lng
        },
        addressCopy
      });
      setShowAddressAutocomplete(false);
    }
  };

  const searchGeocodeReverse = async ({ coordinate, addressCopy }: { coordinate: LatLng; addressCopy: any }) => {
    const { data, status, message } = await getGeocodeReverse(coordinate);
    //CLog(URL_PATH, 'searchGeocodeReverse()', '{ data, status, messag }', { data, status, message });
    if (status) {
      const placeGoogle = data as PlaceGoogle[];
      if (placeGoogle.length > 0) {
        if (
          placeGoogle[0].data.address_components &&
          placeGoogle[0].data.address_components[placeGoogle[0].data.address_components.length - 1].types.find(
            t => t === typesAddressComponent.postal_code
          )
        ) {
          const _zipCode = placeGoogle[0].data.address_components[placeGoogle[0].data.address_components.length - 1].long_name;
          setZipCode(_zipCode);
          const _address = { ...addressCopy };
          _address.fullZipCode = _zipCode;
          setAddress(_address);
          navigationScreen(NAME_ROUTE.enterAddress, {
            address: _address,
            latitude: coordinate.latitude,
            longitude: coordinate.longitude
          });
        } else if (
          placeGoogle[1].data.address_components &&
          placeGoogle[1].data.address_components[placeGoogle[1].data.address_components.length - 1].types.find(
            t => t === typesAddressComponent.postal_code
          )
        ) {
          const _zipCode = placeGoogle[1].data.address_components[placeGoogle[1].data.address_components.length - 1].long_name;
          setZipCode(_zipCode);
          const _address = { ...addressCopy };
          _address.fullZipCode = _zipCode;
          setAddress(_address);
          navigationScreen(NAME_ROUTE.enterAddress, {
            address: _address,
            latitude: coordinate.latitude,
            longitude: coordinate.longitude
          });
        }
      } else {
        CLog(URL_PATH, 'searchGeocodeReverse()', 'err', 'error2');
      }
    } else {
      CLog(URL_PATH, 'searchGeocodeReverse()', 'message', message);
    }
  };

  const goEnterAddress = () => {
    const _address = { ...address };
    _address.fullZipCode = zipCode;
    setAddress(_address);
    navigationScreen(NAME_ROUTE.enterAddress, {
      address: _address,
      latitude: lat,
      longitude: lng,
      zipCode
    });
    setShowAddressAutocomplete(false);
  };

  const onPressAddress = async (index: number) => {
    showAlertSheet({
      title: translate('ADDRESS_SCENE.ALERT_SHEET.TITLE'),
      options: [
        {
          text: translate('ADDRESS_SCENE.ALERT_SHEET.SELECT'),
          onPress(): void {
            onSelectAddress(index);
          }
        },
        {
          text: translate('ADDRESS_SCENE.ALERT_SHEET.DELETE'),
          onPress(): void {
            deleteAddress(index);
          }
        },
        {
          text: translate('GENERAL.MESSAGES.BUTTON_CANCEL'),
          onPress: () => removeAlertSheet()
        }
      ]
    });
  };

  return (
    <ViewAtom style={styles.container}>
      <ViewAtom style={[styles.generalCard, stylesGeneral.shadowAndElevation]}>
        {!showAddressAutocomplete ? (
          <TouchableO style={styles.addAddressButton} onPress={() => setShowAddressAutocomplete(true)}>
            <Text style={{ color: THEME_DEFAULT.primary, fontSize: 18 }}>{translate('ADDRESS_SCENE.ADD_ADDRESS')}</Text>
            <IconCustom name='plus' library={TypeLibraryIconsSystem.materialCommunityIcons} color={THEME_DEFAULT.primary} />
          </TouchableO>
        ) : (
          <ViewAtom style={{ flex: 1, width: '100%', zIndex: 1 }}>
            <ViewAtom style={{ width: '100%', zIndex: 1, flex: 1, position: 'absolute' }}>
              <GooglePlacesAutocompleteAtom
                onPress={onSelectAutocompleteAddress}
                address={address?.address}
                textInputProps={{ onSubmitEditing: goEnterAddress, returnKeyType: 'send' }}
              />
            </ViewAtom>
          </ViewAtom>
        )}
        <ViewAtom
          style={{
            position: 'absolute',
            top: 120,
            width: '100%',
            alignSelf: 'center',
            backgroundColor: THEME_DEFAULT.white,
            zIndex: showAddressAutocomplete ? 0 : 1,
            height: METRICS.DEVICE_HEIGHT - 220
          }}>
          <FlatList
            contentContainerStyle={{ padding: 0, zIndex: showAddressAutocomplete ? 0 : 1 }}
            keyboardShouldPersistTaps='always'
            data={addresses}
            renderItem={address => (
              <TouchableO key={address.index} style={styles.card} onPress={() => onPressAddress(address.index)}>
                <IconCustom
                  name={'map-pin'}
                  library={TypeLibraryIconsSystem.fontAwesome}
                  size={SIZE_ICONS.generalSmall}
                  color={THEME_DEFAULT.primary}
                  style={{ textAlignVertical: 'center', flex: 0.1 }}
                />
                <Text style={{ flex: 1, textAlignVertical: 'center' }}>{address.item.address}</Text>
                <TouchableO style={styles.deleteButton} onPress={() => onPressAddress(address.index)}>
                  <IconCustom
                    name={address.item.selected ? 'radio-button-checked' : 'radio-button-unchecked'}
                    library={TypeLibraryIconsSystem.materialIcons}
                    size={SIZE_ICONS.inputOption}
                    color={THEME_DEFAULT.primary}
                    style={{ textAlignVertical: 'center' }}
                  />
                </TouchableO>
              </TouchableO>
            )}>
            {/* {addresses.map((address, index) => (
              <TouchableO key={index} style={styles.card} onPress={() => onPressAddress(index)}>
                <IconCustom
                  name={'map-pin'}
                  library={TypeLibraryIconsSystem.fontAwesome}
                  size={SIZE_ICONS.generalSmall}
                  color={THEME_DEFAULT.primary}
                  style={{ textAlignVertical: 'center', flex: 0.1 }}
                />
                <Text style={{ flex: 1, textAlignVertical: 'center' }}>{address.address}</Text>
                <TouchableO style={styles.deleteButton} onPress={() => onPressAddress(index)}>
                  <IconCustom
                    name={address.selected ? 'radio-button-checked' : 'radio-button-unchecked'}
                    library={TypeLibraryIconsSystem.materialIcons}
                    size={SIZE_ICONS.inputOption}
                    color={THEME_DEFAULT.primary}
                    style={{ textAlignVertical: 'center' }}
                  />
                </TouchableO>
              </TouchableO>
            ))} */}
          </FlatList>
          {/* {backRoute !== '' && <BottomButtonAtom onPress={() => navigation.goBack()} text={translate('GENERAL.BUTTONS.NEXT')} />} */}
        </ViewAtom>
      </ViewAtom>
    </ViewAtom>
  );
};

export default AddressOrganism;

const styles = StyleSheet.create({
  container: {
    marginTop: METRICS.CONSTANTS_MARGIN,
    height: '100%',
    width: '100%'
  },
  generalCard: {
    backgroundColor: THEME_DEFAULT.white,
    padding: 20,
    flex: 1
  },
  searchInput: {
    height: 50,
    borderWidth: 1,
    borderColor: THEME_DEFAULT.borderLine,
    marginTop: 25,
    flex: 1
  },
  textInputFocused: {
    borderColor: THEME_DEFAULT.borderLineFocused
  },
  card: {
    paddingVertical: METRICS.CONSTANTS_PADDING,
    borderTopColor: THEME_DEFAULT.primaryOpacity,
    borderTopWidth: 1,
    flexDirection: 'row'
  },
  deleteButton: {
    flex: 0.1,
    justifyContent: 'center',
    padding: 10
  },
  addAddressButton: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});
