import React, { Dispatch, SetStateAction, useRef } from 'react';

/* Libs */
import { GestureResponderEvent, StyleSheet, TextInput } from 'react-native';
import MapView, { Region, Marker } from 'react-native-maps';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
/* Components */
import MapViewAtom from '@components/atoms/maps';
import ViewAtom from '@components/atoms/views';
import Text from 'components/atoms/texts';
import TextField from 'components/atoms/textFields';
import BottomButtonAtom from 'components/atoms/buttons/BottomButton';
/* Configs */
import { ASSETS, FAMILY_FONTS, INPUT_ACCESSORY_ID, LatLng, METRICS, THEME_DEFAULT } from '@config/Constants';
/* Models */
import { AddressModel } from 'models/address';
/* Locales */
import { translate } from 'locales';
import GooglePlacesAutocompleteAtom from 'components/atoms/textFields/GooglePlacesAutocomplete';
import { GooglePlaceData, GooglePlaceDetail } from 'react-native-google-places-autocomplete';
import { getGeocodeReverse } from 'services/app/Google';
import PlaceGoogle, { typesAddressComponent } from 'models/google/Place';
import { CLog } from 'utils/helpers/Message';
import Image from 'components/atoms/images';

// Props
interface props {
  address?: AddressModel;
  setAddress: Dispatch<SetStateAction<AddressModel | undefined>>;
  addAddress: () => void;
  lat: number;
  lng: number;
  setLatitude: (lat: number) => void;
  setLongitude: (lng: number) => void;
  showSearch?: boolean;
}

const URL_PATH = 'src/components/organisms/address/EnterAddress.tsx';

const EnterAddressOrganism = ({ address, setAddress, addAddress, lat, lng, setLatitude, setLongitude, showSearch }: props) => {
  /* Ref */
  const zipAddressRef = useRef<TextInput | null>(null);
  const cityRef = useRef<TextInput | null>(null);
  const stateRef = useRef<TextInput | null>(null);
  const zipCodeRef = useRef<TextInput | null>(null);
  const additionalInstructionRef = useRef<TextInput | null>(null);
  const mapRef = useRef<MapView | null>(null);

  const onSetAddress = (newAddress: AddressModel) => {
    const addressCopy = { ...address };
    if (newAddress.address !== undefined) {
      addressCopy.address = newAddress.address;
    }
    if (newAddress.city !== undefined) {
      addressCopy.city = newAddress.city;
    }
    if (newAddress.fullZipCode !== undefined) {
      addressCopy.fullZipCode = newAddress.fullZipCode;
    }
    if (newAddress.additionalInstructions !== undefined) {
      addressCopy.additionalInstructions = newAddress.additionalInstructions;
    }
    if (newAddress.coordinates !== undefined) {
      addressCopy.coordinates = newAddress.coordinates;
    }
    setAddress(addressCopy);
  };

  const onRegionChange = async (region: Region) => {
    const mapCamera = await mapRef.current?.getCamera();
    if (mapCamera) {
      setLatitude(mapCamera?.center.latitude);
      setLongitude(mapCamera?.center.longitude);
    }
  };

  const onTouchEndCapture = async (event: GestureResponderEvent) => {
    searchGeocodeReverse({ coordinate: { latitude: lat, longitude: lng } });
  };

  const searchGeocodeReverse = async ({ coordinate }: { coordinate: LatLng }) => {
    const { data, status, message } = await getGeocodeReverse(coordinate);
    //CLog(URL_PATH, 'searchGeocodeReverse()', '{ data, status, messag }', { data, status, message });
    if (status) {
      const placeGoogle = data as PlaceGoogle[];
      if (placeGoogle.length > 0 && placeGoogle[0].data.address_components) {
        if (
          placeGoogle[0].data.address_components[placeGoogle[0].data.address_components.length - 1].types.find(
            t => t === typesAddressComponent.postal_code
          )
        ) {
          const _zipCode = placeGoogle[0].data.address_components[placeGoogle[0].data.address_components.length - 1].long_name;
          const addressCopy = { ...address };
          addressCopy.address = placeGoogle[0].data.formattedAddress;
          const splitAddress = placeGoogle[0].data.formattedAddress.split(',');
          addressCopy.city = '';
          if (splitAddress[splitAddress.length - 3]) {
            addressCopy.city += splitAddress[splitAddress.length - 3] + ', ';
          }
          if (splitAddress[splitAddress.length - 2]) {
            addressCopy.city += splitAddress[splitAddress.length - 2] + ', ';
          }
          if (splitAddress[splitAddress.length - 1]) {
            addressCopy.city += splitAddress[splitAddress.length - 1];
          }
          addressCopy.fullZipCode = _zipCode;
          onSetAddress(addressCopy);
        }
      }
    } else {
      CLog(URL_PATH, 'searchGeocodeReverse()', 'message', message);
    }
  };

  const onSelectAutocompleteAddress = async (data: GooglePlaceData, details: GooglePlaceDetail | null) => {
    if (details) {
      const addressCopy = { ...address };
      addressCopy.address = details.formatted_address;
      const splitAddress = details.formatted_address.split(',');
      addressCopy.city = '';
      if (splitAddress[splitAddress.length - 3]) {
        addressCopy.city += splitAddress[splitAddress.length - 3] + ', ';
      }
      if (splitAddress[splitAddress.length - 2]) {
        addressCopy.city += splitAddress[splitAddress.length - 2] + ', ';
      }
      if (splitAddress[splitAddress.length - 1]) {
        addressCopy.city += splitAddress[splitAddress.length - 1];
      }

      addressCopy.coordinates = `${details.geometry.location.lat}, ${details.geometry.location.lng}`;
      onSetAddress(addressCopy);
      setLatitude(details.geometry.location.lat);
      setLongitude(details.geometry.location.lng);
      await searchGeocodeReverse({
        coordinate: {
          latitude: details.geometry.location.lat,
          longitude: details.geometry.location.lng
        }
      });
      await mapRef.current?.setCamera({ center: { latitude: lat, longitude: lng } });
    }
  };

  return (
    <ViewAtom style={{ flex: 1 }}>
      {showSearch && (
        <ViewAtom style={{ height: 100 }}>
          <ViewAtom style={{ position: 'absolute', width: '100%', height: 500 }}>
            <GooglePlacesAutocompleteAtom
              onPress={onSelectAutocompleteAddress}
              address={address ? (address as AddressModel).address : ''}
            />
          </ViewAtom>
        </ViewAtom>
      )}
      <MapViewAtom
        refMap={mapRef}
        zoomEnabled={true}
        scrollEnabled={true}
        props={{
          onTouchEndCapture,
          onRegionChange,
          initialRegion: {
            latitude: lat,
            longitude: lng,
            latitudeDelta: 0.001,
            longitudeDelta: 0.001
          }
        }}
        style={styles.map}>
        <Marker
          onDrag={e => console.log(e)}
          draggable
          coordinate={{
            latitude: lat,
            longitude: lng
          }}>
          <Image source={ASSETS.washFoldPin} style={{ width: 28, height: 28 }} resizeMode='contain' />
        </Marker>
      </MapViewAtom>
      <KeyboardAwareScrollView contentContainerStyle={styles.viewContainer} keyboardShouldPersistTaps='handled'>
        <ViewAtom style={styles.formInput}>
          <Text style={styles.formText}>*{translate('ENTER_ADDRESS.ADDRESS')}</Text>
          <TextField
            refInput={zipAddressRef}
            value={address?.address.split(',')[0]}
            editable={true}
            autoCapitalize='none'
            textContentType={'fullStreetAddress'}
            returnKeyType={'next'}
            containerStyle={styles.input}
            blurOnSubmit={false}
            rightButtonProps={{ show: true }}
            inputAccessoryId={INPUT_ACCESSORY_ID.down}
            onSubmitEditing={() => cityRef.current?.focus()}
            onChange={(text: string) => onSetAddress({ address: text })}
          />
        </ViewAtom>
        <ViewAtom style={styles.formInput}>
          <Text style={styles.formText}>*{translate('ENTER_ADDRESS.CITY')}</Text>
          <TextField
            refInput={cityRef}
            value={address?.address?.split(',').length > 2 ? address?.address?.split(',')[1] : address?.address?.split(',')[0]}
            editable={true}
            autoCapitalize='words'
            textContentType={'addressCity'}
            returnKeyType={'next'}
            containerStyle={styles.input}
            blurOnSubmit={false}
            rightButtonProps={{ show: true }}
            inputAccessoryId={INPUT_ACCESSORY_ID.down}
            onSubmitEditing={() => zipCodeRef.current?.focus()}
            onChange={(text: string) => onSetAddress({ city: text })}
          />
        </ViewAtom>
        <ViewAtom style={styles.formInput}>
          <Text style={styles.formText}>*{translate('ENTER_ADDRESS.STATE')}</Text>
          <TextField
            refInput={stateRef}
            value={
              address?.address?.split(',').length > 2
                ? address?.address?.split(',')[2].split(address?.fullZipCode?.toString())[0]
                : address?.address?.split(',')[1].split(address?.fullZipCode?.toString())[0]
            }
            editable={true}
            autoCapitalize='words'
            textContentType={'addressCity'}
            returnKeyType={'next'}
            containerStyle={styles.input}
            blurOnSubmit={false}
            rightButtonProps={{ show: true }}
            inputAccessoryId={INPUT_ACCESSORY_ID.down}
            onSubmitEditing={() => zipCodeRef.current?.focus()}
            onChange={(text: string) => onSetAddress({ city: text })}
          />
        </ViewAtom>
        <ViewAtom style={styles.formInput}>
          <Text style={styles.formText}>*{translate('ENTER_ADDRESS.ZIP_CODE')}</Text>
          <TextField
            refInput={zipCodeRef}
            value={address?.fullZipCode?.toString()}
            editable={true}
            autoCapitalize='words'
            textContentType={'postalCode'}
            returnKeyType={'done'}
            keyboardType={'number-pad'}
            containerStyle={styles.input}
            rightButtonProps={{ show: true }}
            inputAccessoryId={INPUT_ACCESSORY_ID.down}
            onChange={(text: string) => onSetAddress({ fullZipCode: text })}
          />
        </ViewAtom>
        {/* <ViewAtom style={styles.formInput}>
          <Text style={styles.formText}>{translate('ENTER_ADDRESS.ADDITIONAL_INSTRUCTIONS')}</Text>
          <TextField
            refInput={additionalInstructionRef}
            value={address?.additionalInstructions}
            editable={true}
            autoCapitalize='sentences'
            textContentType={'streetAddressLine1'}
            containerStyle={styles.input}
            rightButtonProps={{ show: true }}
            inputAccessoryId={INPUT_ACCESSORY_ID.in}
            onChange={(text: string) => onSetAddress({ additionalInstructions: text })}
          />
        </ViewAtom> */}
      </KeyboardAwareScrollView>
      <BottomButtonAtom onPress={() => addAddress()} text={translate('GENERAL.MESSAGES.BUTTON_CONFIRM')} />
    </ViewAtom>
  );
};

export default EnterAddressOrganism;

const styles = StyleSheet.create({
  map: {
    height: 200
  },
  viewContainer: {
    padding: METRICS.CONSTANTS_PADDING,
    backgroundColor: THEME_DEFAULT.white,
    paddingBottom: 60,
    overflow: 'hidden'
  },
  formInput: {
    marginVertical: 10
  },
  formText: {
    color: THEME_DEFAULT.primary,
    letterSpacing: 2,
    fontFamily: FAMILY_FONTS.primaryRegular
  },
  input: {
    borderWidth: 0,
    borderBottomWidth: 1,
    paddingLeft: 0,
    borderBottomColor: THEME_DEFAULT.primary,
    backgroundColor: THEME_DEFAULT.white
  }
});
