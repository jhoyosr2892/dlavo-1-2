import BottomButtonAtom from 'components/atoms/buttons/BottomButton';
import TouchableO from 'components/atoms/buttons/TouchableOpacity';
import IconCustom from 'components/atoms/icons';
import Image from 'components/atoms/images';
import ScrollView from 'components/atoms/scrollViews';
import SvgCss from 'components/atoms/svgs/SvgCss';
import TextField from 'components/atoms/textFields';
import ViewAtom from 'components/atoms/views';
import { ASSETS, FAMILY_FONTS, INPUT_ACCESSORY_ID, NAME_ROUTE, THEME_DEFAULT } from 'configs/Constants';
import { TypeLibraryIconsSystem } from 'configs/Enum';
import { translate } from 'locales';
import { CLOTHES } from 'models/clothes';
import { SERVICE_CATEGORIES, SERVICE_CATEGORY, SERVICE_CATEGORY_NAMES } from 'models/enums/ServiceCategory';
import { SPECIAL_SERVICE_CARES_I18 } from 'models/enums/special-services-cares';
import { TypePickerNames } from 'models/enums/TypePickers';
import { ServiceRequestModel } from 'models/service';
import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { stylesGeneral } from 'styles';
import { navigationScreen } from 'utils/helpers/Navigation';
import moment from 'moment';
import { Weekdays } from 'models/time';
import { differenceBetweenTwoDates } from 'utils/helpers/Convert';

type BagOrganismProps = {
  bag: ServiceRequestModel | undefined;
  deleteClothe: (index: number, serviceCategory: SERVICE_CATEGORY) => void;
  deleteBag: (index: number, serviceCategory: SERVICE_CATEGORY) => void;
  deleteAll: () => void;
  _createRequest: (coupon?: string) => void;
  total: number;
  bags: { label: string; price: number | undefined }[];
  clothes: { label: string; price: number | undefined }[];
  servicioAdicional: { label: string; price: number | undefined }[];
};

const BagOrganism = ({
  bag,
  deleteClothe,
  deleteBag,
  deleteAll,
  _createRequest,
  total,
  bags,
  clothes,
  servicioAdicional
}: BagOrganismProps) => {
  const [coupon, setCoupon] = useState<string>();

  return (
    <ViewAtom style={{ flex: 1 }}>
      <ScrollView contentContainerStyle={styles.container}>
        {bag?.services.map((service, i) => (
          <View key={i.toString()} style={[styles.content, stylesGeneral.shadowAndElevation]}>
            {/* Service name */}
            <View style={[styles.card, styles.header, stylesGeneral.shadowAndElevation]}>
              {service.serviceCategory === SERVICE_CATEGORIES.DRY_CLEANING ? (
                <SvgCss xml={ASSETS.dryCleaning} height={40} width={40} style={{ marginRight: 12 }} fill={'#15224cBF'} />
              ) : service.serviceCategory === SERVICE_CATEGORIES.FOLD ? (
                <SvgCss xml={ASSETS.fold} height={40} width={40} style={{ marginRight: 12 }} fill={'#15224cBF'} />
              ) : service.serviceCategory === SERVICE_CATEGORIES.BULKY_ITEMS ? (
                <SvgCss xml={ASSETS.bulkyItems} height={40} width={40} style={{ marginRight: 12 }} fill={'#15224cBF'} />
              ) : (
                <Image source={ASSETS.washFold} style={styles.image} />
              )}
              <Text style={styles.title}>{SERVICE_CATEGORY_NAMES[service.serviceCategory]}</Text>
            </View>

            {/* Clothes */}
            {(service.serviceCategory === SERVICE_CATEGORIES.DRY_CLEANING || service.serviceCategory === SERVICE_CATEGORIES.BULKY_ITEMS) &&
              service.clothes?.map((clothe, ci) => (
                <View style={styles.clotheCard} key={ci.toString()}>
                  <View style={styles.clotheContent}>
                    {CLOTHES.find(c => clothe.type === c.type) && CLOTHES.find(c => clothe.type === c.type)?.image ? (
                      <SvgCss
                        xml={CLOTHES.find(c => clothe.type === c.type)?.image ?? ASSETS.tShit}
                        height={40}
                        width={40}
                        fill={THEME_DEFAULT.primary}
                        style={{ marginRight: 12 }}
                      />
                    ) : (
                      <Image source={ASSETS.noImage} style={styles.image} />
                    )}
                    <View style={styles.infoContent}>
                      <View style={{ flex: 1 }}>
                        {/* Name and quantity*/}
                        <Text>
                          {clothe.name} x {clothe.quantity}
                        </Text>
                        {/* Services */}
                        <Text>
                          {service.serviceAdditionals?.map((serviceAdditional, si) => (
                            <Text style={styles.serviceName} key={si}>
                              {SPECIAL_SERVICE_CARES_I18[serviceAdditional]}
                              {service.serviceAdditionals?.length ? service.serviceAdditionals?.length - 1 > si && ', ' : ', '}
                            </Text>
                          ))}
                        </Text>
                      </View>
                      <TouchableO style={styles.deleteButton} onPress={() => deleteClothe(ci, service.serviceCategory)}>
                        <IconCustom name='trash' library={TypeLibraryIconsSystem.foundation} size={24} color={THEME_DEFAULT.black} />
                      </TouchableO>
                    </View>
                  </View>
                </View>
              ))}

            {/* Bags */}
            {(service.serviceCategory === SERVICE_CATEGORIES.WASH_AND_FOLD || service.serviceCategory === SERVICE_CATEGORIES.FOLD) &&
              service.bags?.map((bag, ci) => (
                <View style={styles.clotheCard} key={ci.toString()}>
                  <View style={styles.clotheContent}>
                    <View style={styles.infoContent}>
                      <View style={{ flex: 1 }}>
                        {/* Name and quantity*/}
                        <Text>
                          {bag.name} x {bag.quantity}
                        </Text>
                        {/* Services */}
                        {service.serviceAdditionals?.map((serviceAdditional, si) => (
                          <Text style={styles.serviceName} key={si}>
                            {SPECIAL_SERVICE_CARES_I18[serviceAdditional]}
                            {service.serviceAdditionals?.length ? service.serviceAdditionals?.length - 1 > si && ', ' : ', '}
                          </Text>
                        ))}
                      </View>
                      <TouchableO style={styles.deleteButton} onPress={() => deleteBag(ci, service.serviceCategory)}>
                        <IconCustom name='trash' library={TypeLibraryIconsSystem.foundation} size={24} color={THEME_DEFAULT.black} />
                      </TouchableO>
                    </View>
                  </View>
                </View>
              ))}

            {/* Add bag clothes */}
            {(service.serviceCategory === SERVICE_CATEGORIES.DRY_CLEANING ||
              service.serviceCategory === SERVICE_CATEGORIES.BULKY_ITEMS) && (
              <TouchableO
                style={styles.card}
                onPress={() => {
                  navigationScreen(NAME_ROUTE.selectGarments, {
                    data: service,
                    backRoute: NAME_ROUTE.bag
                  });
                }}>
                <Text>{translate('BAG_SCENE.ADD_MORE_GARMENTS')}</Text>
              </TouchableO>
            )}

            {/* Add bag quantity */}
            {(service.serviceCategory === SERVICE_CATEGORIES.WASH_AND_FOLD || service.serviceCategory === SERVICE_CATEGORIES.FOLD) && (
              <TouchableO
                style={styles.card}
                onPress={() => {
                  navigationScreen(NAME_ROUTE.selectBagQuantity, {
                    data: service,
                    backRoute: NAME_ROUTE.bag
                  });
                }}>
                <Text>{translate('BAG_SCENE.ADD_MORE_BAGS')}</Text>
              </TouchableO>
            )}

            {/* Additional notes */}
            {service.additionalNotes && (
              <TouchableO
                style={[styles.card]}
                onPress={() => navigationScreen(NAME_ROUTE.additionalNotes, { data: service, updated: true })}>
                <Text>{translate('MY_SERVICE_DETAIL_SCENE.ADDITIONAL_NOTES')}</Text>
                {service.additionalNotes?.specializedInstructions !== '' && <Text>{service.additionalNotes?.specializedInstructions}</Text>}
                {service.additionalNotes?.stateOfClothe !== '' && <Text>{service.additionalNotes?.stateOfClothe}</Text>}
              </TouchableO>
            )}
          </View>
        ))}

        {/* Dates */}
        {bag?.services?.length > 0 && (
          <View style={[styles.card, { marginVertical: 15 }, stylesGeneral.shadowAndElevation]}>
            {bag?.services[0].frequency === 'frequent' ? (
              <View>
                <Text style={{ color: 'red' }}>{bag.services[0].concurrence}</Text>
                {/* Frequent service title */}
                <Text style={styles.cardTitle}>
                  {translate('BAG_SCENE.FREQUENT_SERVICE') + ' '}
                  {bag.services[0].weeklyDays?.length > 0 && translate('BAG_SCENE.WEEKLY')}
                  {bag.services[0].monthDays?.length > 0 && translate('BAG_SCENE.MONTHLY')}
                </Text>
                {bag.services[0].concurrence === 'WEEKLY' ? (
                  <View style={{ marginTop: 15 }}>
                    <Text style={{ color: THEME_DEFAULT.primary }}>
                      {translate('SELECT_SCHEDULE.WEEK_DAY')}:&nbsp;
                      {bag.services[0].weeklyDays?.map((e, i) => (
                        <Text key={i}>{Weekdays.find(w => w.value === e)?.label}</Text>
                      ))}
                    </Text>
                    <Text style={{ color: THEME_DEFAULT.primary }}>
                      {translate('SELECT_SCHEDULE.REMAINING_REPETITIONS')}:&nbsp;
                      {bag.services[0].repetitions}
                    </Text>
                  </View>
                ) : (
                  <View style={{ marginTop: 15 }}>
                    <Text style={{ color: THEME_DEFAULT.primary }}>
                      {translate('SELECT_SCHEDULE.MONTH_DAY')}:&nbsp;
                      {bag.services[0].monthDays?.map((e, i) => (
                        <Text key={i}>{e}</Text>
                      ))}
                    </Text>
                    <Text style={{ color: THEME_DEFAULT.primary }}>
                      {translate('SELECT_SCHEDULE.REMAINING_REPETITIONS')}:&nbsp;
                      {bag.services[0].repetitions}
                    </Text>
                  </View>
                )}
              </View>
            ) : (
              <View>
                {/* Pickup */}
                <Text style={styles.cardTitle}>{translate('MY_SERVICE_DETAIL_SCENE.PICKUP')}</Text>
                <View style={[styles.cardContent, styles.borderBottom]}>
                  <SvgCss xml={ASSETS.clock} height={20} width={20} style={styles.listIcon} />
                  {bag.services[0].pickUpSingleTime && (
                    <Text>{bag.services[0] ? moment(bag.services[0].pickUpSingleTime * 1000).format('LLLL') : ''}</Text>
                  )}
                  {/* {bag.services[0].monthDays?.map((e, i) => (
                <Text key={i}>
                  {e.monthDay}, {e.pickUpTime}
                </Text>
              ))}
              {bag.services[0].weeklyDays?.map((e, i) => (
                <Text key={i}>
                  {Weekdays.find(w => w.value === e.weekDay)?.label}, {e.pickUpTime}
                </Text>
              ))} */}
                </View>
                <TouchableO
                  style={styles.cardContent}
                  onPress={() => navigationScreen(NAME_ROUTE.selectSchedule, { data: bag?.services[0], backRoute: NAME_ROUTE.bag })}>
                  <SvgCss xml={ASSETS.truck} height={20} width={20} style={[styles.listIcon, { transform: [{ rotateY: '180deg' }] }]} />
                  <Text>{bag.services[0].pickUpType && TypePickerNames[bag.services[0].pickUpType]}</Text>
                </TouchableO>
                {/* DELIVERY */}
                <View>
                  <Text style={styles.cardTitle}>{translate('MY_SERVICE_DETAIL_SCENE.DELIVERY')}</Text>
                  <View style={[styles.cardContent, styles.borderBottom]}>
                    <SvgCss xml={ASSETS.clock} height={20} width={20} style={styles.listIcon} />
                    {bag.services[0].deliverySingleTime && (
                      <Text>{bag.services[0] ? moment(bag.services[0].deliverySingleTime * 1000).format('LLLL') : ''}</Text>
                    )}
                    {bag.services[0].monthDays?.map((e, i) => (
                      <Text key={i}>
                        {e.monthDay}, {e.deliveryTime}
                      </Text>
                    ))}
                    {bag.services[0].weeklyDays?.map((e, i) => (
                      <Text key={i}>
                        {Weekdays.find(w => w.value === e.weekDay)?.label}, {e.deliveryTime}
                      </Text>
                    ))}
                  </View>
                  <TouchableO style={styles.cardContent}>
                    <SvgCss xml={ASSETS.truck} height={20} width={20} style={styles.listIcon} />
                    <Text>{bag.services[0].pickUpType && TypePickerNames[bag.services[0].pickUpType]}</Text>
                  </TouchableO>
                </View>
              </View>
            )}
          </View>
        )}

        {/* Add service */}
        <TouchableO style={styles.card} onPress={() => navigationScreen(NAME_ROUTE.laundry)}>
          <Text>{translate('BAG_SCENE.ADD_SERVICE')}</Text>
        </TouchableO>
        {/* Empty bag */}
        <TouchableO style={styles.card} onPress={deleteAll}>
          <Text>{translate('BAG_SCENE.EMPTY_BAG')}</Text>
        </TouchableO>

        {/* Payment */}
        <View style={[styles.card, { marginVertical: 15 }, stylesGeneral.shadowAndElevation]}>
          <Text style={{ marginBottom: 10 }}>{translate('BAG_SCENE.PAYMENT.HEADER')}</Text>
          {bag?.creditCardNumber ? (
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <IconCustom
                name={'cc-mastercard'}
                library={TypeLibraryIconsSystem.fontAwesome}
                color='black'
                size={18}
                style={{ marginRight: 14 }}
              />
              <View>
                <Text>
                  ***
                  {bag.creditCardNumber[bag.creditCardNumber.length - 4] +
                    bag.creditCardNumber[bag.creditCardNumber.length - 3] +
                    bag.creditCardNumber[bag.creditCardNumber.length - 2] +
                    bag.creditCardNumber[bag.creditCardNumber.length - 1]}
                </Text>
              </View>
              <TouchableO
                style={{ marginLeft: 'auto' }}
                onPress={() =>
                  navigationScreen(NAME_ROUTE.addPaymentMethod, {
                    backRoute: NAME_ROUTE.bag,
                    paymentMethod: { cardNumber: bag.creditCardNumber, cardCvs: bag.cvv, cardExpirationDate: bag.expirationDate }
                  })
                }>
                <Text>{translate('GENERAL.BUTTONS.CHANGE')}</Text>
              </TouchableO>
            </View>
          ) : (
            <TouchableO
              style={{ flexDirection: 'row', alignItems: 'center' }}
              onPress={() => navigationScreen(NAME_ROUTE.addPaymentMethod, { backRoute: NAME_ROUTE.bag })}>
              <IconCustom
                name={'cc-mastercard'}
                library={TypeLibraryIconsSystem.fontAwesome}
                color='black'
                size={18}
                style={{ marginRight: 14 }}
              />
              <Text>{translate('BAG_SCENE.PAYMENT.ADD_PAYMENT_INFO')}</Text>
            </TouchableO>
          )}
        </View>

        {/* Coupons */}
        <View style={[styles.card, { marginVertical: 15 }, stylesGeneral.shadowAndElevation]}>
          <Text style={{ marginBottom: 10 }}>{translate('BAG_SCENE.COUPONS')}</Text>
          {/* Coupon */}
          <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center' }}>
            <IconCustom
              name='card-giftcard'
              library={TypeLibraryIconsSystem.materialIcons}
              color={THEME_DEFAULT.primary}
              style={{ marginRight: 5 }}
              size={23}
            />
            <TextField
              value={coupon}
              editable={true}
              autoCapitalize='words'
              placeholder={translate('BAG_SCENE.ENTER_COUPON_PLACEHOLDER')}
              textContentType={'oneTimeCode'}
              // keyboardType={'number-pad'}
              returnKeyType={'done'}
              inputAccessoryId={INPUT_ACCESSORY_ID.down}
              onChange={(text: string) => setCoupon(text)}
              containerStyle={{ flex: 0.98 }}
            />
          </View>
        </View>

        {/* Resume */}
        <View style={[styles.card, { marginVertical: 15 }, stylesGeneral.shadowAndElevation]}>
          {/* Bags */}
          {bags.map((b, i) => (
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} key={i}>
              <Text>{b.label}</Text>
              <Text>$ {b.price}</Text>
            </View>
          ))}
          {/* Clothes */}
          {clothes.map((b, i) => (
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} key={i}>
              <Text>{b.label}</Text>
              <Text>$ {b.price}</Text>
            </View>
          ))}
          {/* Service aditionals */}
          {servicioAdicional.map((b, i) => (
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} key={i}>
              <Text>{b.label}</Text>
              <Text>$ {b.price}</Text>
            </View>
          ))}
          {/* Extra cost by one day */}
          {differenceBetweenTwoDates(
            new Date(bag?.services[0].deliverySingleTime * 1000),
            new Date(bag?.services[0].pickUpSingleTime * 1000)
          ) === 1 && (
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text>{translate('BAG_SCENE.EXTRA_COST_BY_ONE_DAY')}</Text>
              <Text>$ 15</Text>
            </View>
          )}
          {/* Impuestos */}
          {total > 0 && (
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text>{translate('BAG_SCENE.TAXES')}</Text>
              <Text>5.6%</Text>
            </View>
          )}
          <View style={{ width: '100%', height: 1, marginVertical: 20, backgroundColor: THEME_DEFAULT.primary }}></View>
          {/* Total */}
          <View style={[{ flexDirection: 'row', justifyContent: 'space-between' }]}>
            <Text style={{ fontWeight: 'bold', color: THEME_DEFAULT.black }}>{translate('BAG_SCENE.TOTAL')}</Text>
            <Text style={{ fontWeight: 'bold', color: THEME_DEFAULT.black }}>
              <Text style={{ marginRight: 3, fontWeight: 'bold', color: THEME_DEFAULT.black }}>
                $ {(Math.round((total + total * 0.056) * 100) / 100).toFixed(2)}
              </Text>
            </Text>
          </View>
          {total < 30 && (
            <Text style={{ color: THEME_DEFAULT.error, textAlign: 'center', marginTop: 5 }}>
              {translate('BAG_SCENE.TOTAL_COST_MIN_NOTE')}
            </Text>
          )}
        </View>
      </ScrollView>

      <BottomButtonAtom
        text={translate('BAG_SCENE.CREATE_REQUEST')}
        onPress={() => _createRequest(coupon)}
        disabled={!bag || !bag.creditCardNumber || total < 30}
      />
    </ViewAtom>
  );
};

export default BagOrganism;

const styles = StyleSheet.create({
  container: {
    padding: 0,
    paddingBottom: 50,
    paddingTop: 30
  },
  content: {
    marginBottom: 15
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  image: {
    height: 40,
    width: 40,
    resizeMode: 'contain',
    marginRight: 12
  },
  title: {
    fontFamily: FAMILY_FONTS.primaryBold,
    color: THEME_DEFAULT.primary,
    fontWeight: 'bold'
  },
  card: {
    paddingVertical: 17,
    paddingHorizontal: 14,
    width: '100%',
    backgroundColor: THEME_DEFAULT.white
  },
  clotheCard: {
    paddingHorizontal: 19,
    width: '100%',
    backgroundColor: THEME_DEFAULT.white
  },
  clotheContent: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: THEME_DEFAULT.borderLine,
    borderBottomWidth: 1,
    paddingVertical: 12
  },
  serviceName: {
    fontFamily: FAMILY_FONTS.primaryRegular,
    color: THEME_DEFAULT.primaryOpacity,
    fontSize: 11
  },
  infoContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1
  },
  deleteButton: {
    justifyContent: 'center',
    marginRight: 5,
    paddingHorizontal: 5
  },
  borderBottom: {
    borderBottomColor: THEME_DEFAULT.primaryOpacity,
    borderBottomWidth: 1
  },
  cardTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: THEME_DEFAULT.primary,
    paddingTop: 20
  },
  cardContent: {
    flexDirection: 'row',
    paddingVertical: 20,
    alignItems: 'center'
  },
  listIcon: {
    marginRight: 15
  }
});
