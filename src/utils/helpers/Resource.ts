// /* eslint-disable camelcase */
// // Helpers
// import { durationHumanize } from '@helpers/Convert';
import { CLog } from '@helpers/Message';

// // Models
// import Meeting from '@models/course/Meeting';
// import ResourceTask, { ResourceTaskResponseServer, ResourceTaskResponseServerSecondary } from '@models/course/ResourceTask';
// import ResourceType, { ResourceTypeResponseServer } from '@models/course/ResourceType';
// import StatusResource from '@models/course/StatusResource';
// import Test, { TestResponseServer, TestResponseServerSecondary } from '@models/course/Test';

// // Constants
// import { STATUS_RESOURCE } from '@constantsApp';
// const URL_PATH = 'src/models/course/Resource.ts';

// export interface ResourceModel {
//   id: number;
//   number?: number;
//   idRelation?: number;
//   /** descripción sobre el recurso **/
//   description: string;
//   /** nombre **/
//   name: string;
//   /** tiempo limite para hacer el recurso pero crudo como llegar del server **/
//   timeLimit?: number;
//   /** tiempo limite para hacer el recurso pero entendible al usuario **/
//   timeLimitDisplay?: string;
//   type: ResourceType;
//   /** Url donde se encuentra alojado el recurso **/
//   source?: string;
//   /** estado del curso (finalizado, en proceso, terminado) **/
//   status: StatusResource;
//   /** modlidad del recurso (precencial o virtual) **/
//   modality: string;
//   /** Tareas asociadas al recurso **/
//   tasks: ResourceTask[];
//   /** si el recurso es de tipo Zoom **/
//   meeting?: Meeting;
//   /** si el recurso es de tipo test **/
//   test?: Test;
//   /** progreso del rescurso **/
//   progress?: number;
// }

// export interface ResourceOnly {
//   id: number;
//   name: string;
//   description: string;
//   /** password de la reunion **/
//   meetingPassword?: string;
//   /** id de la reunion **/
//   meetingId?: string;
//   /** hora de inicio de la reunion **/
//   meetingStartTime?: string;
//   resourceUrl?: string;
//   /** miniatura del recurso **/
//   resourceThumbnail?: string;
//   size?: number;
//   resourceType: ResourceTypeResponseServer;
//   mode: string;
//   duration: string;
//   tasks?: ResourceTaskResponseServer[];
//   /** Recurso de tipo evaluacion **/
//   evaluation?: TestResponseServer;
// }

// export interface ResourceOnlySecondary {
//   id: number;
//   name: string;
//   description: string;
//   /** password de la reunion **/
//   meetingPassword?: string;
//   /** id de la reunion **/
//   meetingId?: string;
//   /** hora de inicio de la reunion **/
//   meetingStartTime?: string;
//   resourceUrl?: string;
//   /** miniatura del recurso **/
//   resourceThumbnail?: string;
//   size?: number;
//   resourceType: ResourceTypeResponseServer;
//   evaluation?: number;
//   mode: string;
//   duration: string;
//   tasks?: ResourceTaskResponseServer[];
// }

// export interface ResourceResponseServerSecondary {
//   id: number;
//   resource: ResourceOnlySecondary;
//   /** Tareas asociadas al recurso **/
//   tasks: ResourceTaskResponseServerSecondary[];
//   /** recurso cuando es de tipo test **/
//   evaluation?: TestResponseServerSecondary;
//   progress: number;
// }

// export interface ResourceResponseServer {
//   id: number;
//   resource: ResourceOnly;
// }

// class Resource {
//   data: ResourceModel;

//   constructor(data: ResourceModel) {
//     this.data = data;
//   }

//   /**
//    * @method formatData
//    * @description formatea la data que llega del servidor a la que se maneja en
//    * la APP.
//    * @param {ResourceResponseServer} data
//    * @return {ResourceModel}
//    */
//   static formatData(data: ResourceResponseServer, index: number): ResourceModel {
//     const duration = parseInt(data.resource.duration, 10);
//     const isMeeting = data.resource.meetingPassword && data.resource.meetingId && data.resource.meetingStartTime;
//     const status: StatusResource = new StatusResource(STATUS_RESOURCE.withoutStarting);
//     //CLog(URL_PATH, 'formatData()', 'data', data);
//     const formattedData: ResourceModel = {
//       id: data.resource.id,
//       number: index + 1,
//       idRelation: data.id,
//       name: data.resource.name,
//       description: data.resource.description,
//       timeLimit: duration,
//       timeLimitDisplay: durationHumanize(duration),
//       type: new ResourceType(ResourceType.formatData(data.resource.resourceType)),
//       source: data.resource.resourceUrl?.search(/(zoom.us)/g) === -1 ? data.resource.resourceUrl : undefined,
//       status,
//       modality: data.resource.mode,
//       tasks: data.resource.tasks
//         ? data.resource.tasks?.map((task: ResourceTaskResponseServer) => new ResourceTask(ResourceTask.formatData(task)))
//         : [],
//       meeting: isMeeting ? new Meeting(Meeting.formatData(data.resource)) : undefined,
//       test: data.resource.evaluation ? new Test(Test.formatData(data.resource.evaluation)) : undefined
//     };
//     //CLog(URL_PATH, 'formatData()', 'formattedData', formattedData);
//     return formattedData;
//   }

//   static formatDataSecondary(data: ResourceResponseServerSecondary, index: number): ResourceModel {
//     const duration = parseInt(data.resource.duration, 10);
//     const isMeeting = data.resource.meetingPassword && data.resource.meetingId && data.resource.meetingStartTime;
//     let status: StatusResource = new StatusResource(STATUS_RESOURCE.withoutStarting);
//     CLog(URL_PATH, 'formatDataSecondary()', 'data', data);
//     if (data.tasks?.length > 0) {
//       if (data.tasks.every(task => task.completed)) {
//         status = new StatusResource(STATUS_RESOURCE.completed);
//       } else if (data.tasks.some(task => task.completed)) {
//         status = new StatusResource(STATUS_RESOURCE.inProgress);
//       } else {
//         status = new StatusResource(STATUS_RESOURCE.withoutStarting);
//       }
//     } else {
//       if (data.progress === 100 || (data.evaluation?.attempt || 0) === (data.evaluation?.evaluation.attempts || 0)) {
//         status = new StatusResource(STATUS_RESOURCE.completed);
//       } else if (data.progress > 0) {
//         status = new StatusResource(STATUS_RESOURCE.inProgress);
//       } else {
//         status = new StatusResource(STATUS_RESOURCE.withoutStarting);
//       }
//     }
//     const formattedData: ResourceModel = {
//       id: data.resource.id,
//       number: index + 1,
//       idRelation: data.id,
//       name: data.resource.name,
//       description: data.resource.description,
//       timeLimit: duration,
//       timeLimitDisplay: durationHumanize(duration),
//       type: new ResourceType(ResourceType.formatData(data.resource.resourceType)),
//       source: data.resource.resourceUrl?.search(/(zoom.us)/g) === -1 ? data.resource.resourceUrl : undefined,
//       status,
//       modality: data.resource.mode,
//       tasks: data.tasks.map((task: ResourceTaskResponseServerSecondary) => new ResourceTask(ResourceTask.formatDataSecondary(task))),
//       meeting: isMeeting ? new Meeting(Meeting.formatData(data.resource)) : undefined,
//       test: data.evaluation ? new Test(Test.formatDataSecondary(data.evaluation)) : undefined,
//       progress: data.progress
//     };
//     return formattedData;
//   }

//   /**
//    * @description Fomatea la data como sale del storage o de un json a formato
//    * usado en la app.
//    */
//   static formatDataStorage(data: ResourceModel): ResourceModel {
//     const formattedData: ResourceModel = {
//       ...data,
//       type: new ResourceType(data.type.data),
//       status: new StatusResource(data.status.data),
//       modality: data.modality,
//       tasks: data.tasks.map(task => new ResourceTask(task.data)),
//       meeting: data.meeting ? new Meeting(data.meeting.data) : undefined,
//       test: data.test ? new Test(data.test.data) : undefined
//     };
//     return formattedData;
//   }

//   /**
//    * @description cantidad de Tareas terminadas en el recurso.
//    */
//   finishedTask() {
//     return this.data.tasks.filter(task => task.data.checked).length;
//   }

//   /**
//    * @description actualizar test
//    */
//   updateTest(test: Test) {
//     this.data.test = test;
//     if (test.data.approved) {
//       this.data.progress = 100;
//     } else if (test.data.attemps === test.data.attempsMade) {
//       this.data.progress = 100;
//     } else if (test.data.qualification && test.data.qualification > 0) {
//       this.data.progress = test.data.qualification;
//     } else {
//       this.data.progress = 0;
//     }
//   }

//   /**
//    * @description actualiza el status
//    */
//   updateStatus() {
//     let status: StatusResource = new StatusResource(STATUS_RESOURCE.withoutStarting);
//     if (this.data.tasks && this.data.tasks?.length > 0) {
//       if (this.data.tasks.every(task => task.data.checked)) {
//         status = new StatusResource(STATUS_RESOURCE.completed);
//       } else if (this.data.tasks?.some(task => task.data.checked)) {
//         status = new StatusResource(STATUS_RESOURCE.inProgress);
//       } else {
//         status = new StatusResource(STATUS_RESOURCE.withoutStarting);
//       }
//     } else {
//       if (this.data.progress === 100) {
//         status = new StatusResource(STATUS_RESOURCE.completed);
//       } else if (this.data.progress === 0) {
//         status = new StatusResource(STATUS_RESOURCE.withoutStarting);
//       } else {
//         status = new StatusResource(STATUS_RESOURCE.inProgress);
//       }
//     }
//     this.data.status = status;
//   }
// }

// export default Resource;
