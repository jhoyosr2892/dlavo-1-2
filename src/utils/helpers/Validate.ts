// Libs
import { ImageRequireSource, ImageURISource, TextStyle } from 'react-native';

// Configs
import { Resource, EnumFormatDateTime } from '@config/Enum';
import { ResourceSource } from '@config/Types';
import moment from '@config/Moment';

// Helpers
import { CLog } from '@helpers/Message';

// Styles
import { stylesGeneral } from 'styles';

// Constants
import { AnimationObject, ASSETS, EnumFont } from '@constantsApp'
const URL_PATH = 'src/utils/helpers/Validate.ts';


export const validateSourceImage = (source?: null | number | string | ImageURISource): ImageRequireSource | ImageURISource => {
  try {
    const typeSource = typeof source;
    if (!source || source === null) {
      return ASSETS.monogramPng;
    } else if (typeSource === 'string') {
      return { uri: source } as ImageURISource;
    } else if (typeSource === 'number') {
      return source as number;
    } else if (typeSource === 'object') {
      const sourceConvert = source as ImageURISource;
      if (sourceConvert.uri) {
        return source as ImageURISource;
      }
    }
    return ASSETS.logo;
  } catch (err) {
    CLog(URL_PATH, 'validateSource()', 'err', err);
    return ASSETS.logo;
  }
};

/**
 * @description Valida si el source lottie es correcto
 * @param {string|AnimationObject|{uri:string}} source 
 * @returns Animation lottie
 */
export const validateSourceLottie = (source?: string | AnimationObject | { uri: string }): AnimationObject | { uri: string } | string => {
  try {
    const typeSource = typeof source;
    if (typeSource === 'string') {
      return { uri: source } as { uri: string };
    } else if (typeSource === 'object') {
      return source as AnimationObject;
    }
    return ASSETS.spinnerLoading;
  } catch (err) {
    CLog(URL_PATH, 'validateSourceLottie()', 'err', err);
    return ASSETS.spinnerLoading;
  }
};

/**
 * @function validateTypeText
 * @description valida el tipo de estilo del texto a mostrar
 * @param {EnumFont} enumFont
 * @return {TextStyle}
 */
export const validateTypeText = (enumFont?: EnumFont): TextStyle => {
  let typeFont: TextStyle = {};
  try {
    switch (enumFont) {
      case EnumFont.primary:
        typeFont = stylesGeneral.textLightBigColorSecondary;
        break;
      case EnumFont.secondary:
        typeFont = stylesGeneral.textLightSmallColorPrimary;
        break;
      case EnumFont.tertiary:
        typeFont = stylesGeneral.textLightBigColorPrimary;
        break;
      case EnumFont.quaternary:
        typeFont = stylesGeneral.textLightSmallColorSecondary;
        break;
      case EnumFont.quinary:
        typeFont = stylesGeneral.textRegularSmallColorSecondary;
        break;
      case EnumFont.sixth:
        typeFont = stylesGeneral.textRegularSmallColorPrimary;
        break;
      case EnumFont.septenary:
        typeFont = stylesGeneral.textRegularBigColorSecondary;
        break;
      case EnumFont.octonary:
        typeFont = stylesGeneral.textRegularBigColorPrimary;
        break;
      case EnumFont.nonary:
        typeFont = stylesGeneral.textRegularExtraBigColorPrimary;
        break;
      case EnumFont.tenth:
        typeFont = stylesGeneral.textRegularExtraBigColorSecondary;
        break;
      default:
        typeFont = stylesGeneral.textLightBigColorSecondary;
        break;
    }
  } catch (err) {
    CLog(URL_PATH, 'validateTypeText()', 'err', err);
  }
  return typeFont;
};


/**
 * @function isPdfDocDocx
 * @description valida si la url contiene un pdf, un doc o un docx.
 * @param {string} url
 * @return {boolean}
 */
export const urlIsPdfDocDocx = (url: string): boolean => {
  if (
    url.toLowerCase().endsWith('.pdf') ||
    url.toLowerCase().endsWith('.ppt') ||
    url.toLowerCase().endsWith('.pptx') ||
    url.toLowerCase().endsWith('.docx') ||
    url.toLowerCase().endsWith('.doc') ||
    url.toLowerCase().endsWith('.xls') ||
    url.toLowerCase().endsWith('.xlsx') ||
    url.toLowerCase().endsWith('download')
  ) {
    return true;
  } else {
    return false;
  }
};

/**
 * @function whatKindOfMultimediaFileIsIt
 * @description en base a una url indica que tipo se recuro multimedia es
 * un video, imagen, un audio.
 * @param {string} url
 * @return {Resource | undefined}
 */
export const whatKindOfMultimediaFileIsIt = (url: string): Resource | undefined => {
  try {
    if (url.match(/(mp4|mkv|mov|wmv|flv|avi|mpd|m3u8|ism)/gi)) {
      return Resource.video;
    } else if (url.match(/(jpg|jpeg|png|tiff|tif|bmp)/gi)) {
      return Resource.image;
    } else if (url.match(/(mp3|wav|aiff|ogg)/gi)) {
      return Resource.audio;
    } else if (url.match(/(ppt|pptx|doc|docx|xls|xlsx|pdf)/gi)) {
      return Resource.documentSupport;
    } else if (url.match(/(html|website|php)/gi)) {
      return Resource.webSite;
    }
    return undefined;
  } catch (err) {
    CLog(URL_PATH, 'whatKindOfMultimediaFileIsIt()', 'err', err);
    return undefined;
  }
};

/**
 * @function whatKindOfResourceIsIt
 * @description en base al source del recurso indica que tipo de recurso es
 * tenga en cuenta que esto se basa en la url del uri, o si se le pasa un string
 * cuando se pasa un requires al devolver en algunos casos un tipo numerico
 * no es posible determinar el tipo de recurso ( si ve esto y cree capas
 * por favor agregar la funcionalidad).
 * @param {ResourceSource} source
 * @return {Resource | undefined}
 */
export const whatKindOfResourceIsIt = (source: ResourceSource): Resource | undefined => {
  try {
    if (source) {
      if (typeof source === 'object') {
        if (source.uri) {
          return whatKindOfMultimediaFileIsIt(source.uri);
        }
      } else if (typeof source === 'string') {
        return whatKindOfMultimediaFileIsIt(source);
      } else {
        return undefined;
      }
    }
  } catch (err) {
    CLog(URL_PATH, 'urlIsTypeResource()', 'err', err);
    return undefined;
  }
};

/***************** Time and Date *********************/
/**
 * @description Compara si una fecha es superior o igual a otra.
 * @return {boolean}
 */
export const isSameOrAfter = ({ date, dateCompare }: { date?: string; dateCompare?: string }): boolean => {
  //CLog(URL_PATH, 'isSameOrAfter()', '{ date, dateCompare }', { date, dateCompare });
  try {
    const momentNow = moment().format(EnumFormatDateTime.YYYYMMDDHHmm);
    return moment(date || momentNow).isSameOrAfter(dateCompare || momentNow);
  } catch (err) {
    return moment().isSameOrAfter(dateCompare);
  }
};


