// Libs
import React from 'react';
import { Linking, StyleSheet, Text as T } from 'react-native';
import { RouteProp, DrawerActions, NavigationContainerRef } from '@react-navigation/native';
import { StackNavigationProp, StackNavigationOptions } from '@react-navigation/stack';
import { DrawerNavigationOptions } from '@react-navigation/drawer';

// Atoms
import Icon from '@atoms/icons';
//import Image from '@atoms/images';
import Text from '@atoms/texts';

// Configs
import { valueof } from '@config/Types';

// Helpers
import { validateTypeText } from '@helpers/Validate';
import { CLog } from '@helpers/Message';

// Constants
import { NAME_ROUTE, THEME_DEFAULT, EnumFont, SIZE_ICONS, NAME_ICON, METRICS } from '@constantsApp';
import { TypeLibraryIconsSystem } from 'configs/Enum';
import { SERVICE_CATEGORY } from 'models/enums/ServiceCategory';
import { typeClothes } from 'models/clothes';
import { AddressModel } from 'models/address';
import { ServiceModel, ServiceRequestModel } from 'models/service';
import { PaymentMethodModel } from 'models/payment';
const URL_PATH = 'src/utils/helpers/navigation.tsx';

export const navigationRef = React.createRef<NavigationContainerRef<any>>();

/**
 * navigate screen
 *
 * @param {string} screen
 * @param {object} params
 */
export function navigationScreen(name: NAME_ROUTE, params: object = {}): void {
  name && navigationRef.current?.navigate(name, params);
}

/**
 * Go sign out
 */
export function goSignOut() {
  navigationRef.current?.resetRoot({
    index: 0,
    routes: [{ name: 'SignOut' }]
  });
}

/**
 * Go sign In
 */
export function goSignIn() {
  navigationRef.current?.resetRoot({
    index: 0,
    routes: [{ name: 'SignIn' }]
  });
}

export type OptionsParams = {
  /** solo funciona en los stack no en drawer
   * pone el header sobre el screen
   */
  transparentHeader?: boolean;
  /** ocultar el titulo **/
  hideTitle?: boolean;
  //
  hideHeader?: boolean;
  title?: string;
  isDrawer?: boolean;
  iconDrawer?: string;
  iconLibrary?: TypeLibraryIconsSystem;
};

/** se pasa al parámetro de params de la navegación **/
export type StackParamList = valueof<RootStackParamList>;

/** si llega modificar algo de aqui recuerde actualizar el linking para
 * los deeplinks**/
export type RootStackParamList = {
  Home?: undefined;
  Auth?: undefined;
  Drawer?: undefined;
  EnterName?: undefined;
  RegisterEmail?: undefined;
  EnterCode?: undefined;
  Tabs?: undefined;
  LaundryTab?: undefined;
  MyServicesTab?: undefined;
  PricingTab?: undefined;
  ProfileTab?: undefined;
  Laundry?: undefined;
  SelectGarments?: undefined;
  SelectFrequency?: { data: ServiceModel };
  ServiceOrCaresSelect?: { serviceCategory: SERVICE_CATEGORY; clothes: typeClothes[]; data: ServiceModel };
  Bag?: { paymentMethod: { cardCvs: number; cardExpirationDate: string; cardNumber: string } };
  Address?: undefined;
  EnterAddress?: { address: AddressModel; latitude: number; longitude: number };
  MyServices?: undefined;
  MyServiceDetail?: undefined;
  Pricing?: undefined;
  Profile?: undefined;
  ProfileDetail?: undefined;
  Payment?: undefined;
  Membership?: undefined;
  Coupons?: undefined;
  ShareAndWin?: undefined;
  JoinAsDlavoPartner?: undefined;
  Help?: undefined;
  About?: undefined;
  Privacy?: undefined;
  AddPaymentMethod?: { backRoute: NAME_ROUTE; paymentMethod: PaymentMethodModel };
  SelectBagQuantity?: undefined;
  SelectSchedule?: undefined;
  AdditionalNotes?: { data: ServiceModel; address: AddressModel; updated: boolean };
  PricingDetail?: undefined;
};

export type StackNavigationPropScreens = StackNavigationProp<
  RootStackParamList,
  | NAME_ROUTE.drawer
  | NAME_ROUTE.home
  | NAME_ROUTE.auth
  | NAME_ROUTE.enterName
  | NAME_ROUTE.enterCode
  | NAME_ROUTE.registerEmail
  | NAME_ROUTE.tabs
  | NAME_ROUTE.laundryTab
  | NAME_ROUTE.myServicesTab
  | NAME_ROUTE.pricingTab
  | NAME_ROUTE.profileTab
  | NAME_ROUTE.laundry
  | NAME_ROUTE.selectFrequency
  | NAME_ROUTE.selectGarments
  | NAME_ROUTE.serviceOrCaresSelect
  | NAME_ROUTE.bag
  | NAME_ROUTE.address
  | NAME_ROUTE.enterAddress
  | NAME_ROUTE.myServices
  | NAME_ROUTE.myServiceDetail
  | NAME_ROUTE.pricing
  | NAME_ROUTE.profile
  | NAME_ROUTE.profileDetail
  | NAME_ROUTE.payment
  | NAME_ROUTE.membership
  | NAME_ROUTE.coupons
  | NAME_ROUTE.shareAndWin
  | NAME_ROUTE.joinAsDlavoPartner
  | NAME_ROUTE.help
  | NAME_ROUTE.about
  | NAME_ROUTE.privacy
  | NAME_ROUTE.addPaymentMethod
  | NAME_ROUTE.selectBagQuantity
  | NAME_ROUTE.selectSchedule
  | NAME_ROUTE.additionalNotes
  | NAME_ROUTE.pricingDetail
>;

export type OptionsScreensParams = {
  route?: RouteProp<
    RootStackParamList,
    | NAME_ROUTE.drawer
    | NAME_ROUTE.home
    | NAME_ROUTE.auth
    | NAME_ROUTE.enterName
    | NAME_ROUTE.enterCode
    | NAME_ROUTE.registerEmail
    | NAME_ROUTE.tabs
    | NAME_ROUTE.laundryTab
    | NAME_ROUTE.myServicesTab
    | NAME_ROUTE.pricingTab
    | NAME_ROUTE.profileTab
    | NAME_ROUTE.laundry
    | NAME_ROUTE.selectFrequency
    | NAME_ROUTE.selectGarments
    | NAME_ROUTE.serviceOrCaresSelect
    | NAME_ROUTE.bag
    | NAME_ROUTE.address
    | NAME_ROUTE.enterAddress
    | NAME_ROUTE.myServices
    | NAME_ROUTE.myServiceDetail
    | NAME_ROUTE.pricing
    | NAME_ROUTE.profile
    | NAME_ROUTE.profileDetail
    | NAME_ROUTE.payment
    | NAME_ROUTE.membership
    | NAME_ROUTE.coupons
    | NAME_ROUTE.shareAndWin
    | NAME_ROUTE.joinAsDlavoPartner
    | NAME_ROUTE.help
    | NAME_ROUTE.about
    | NAME_ROUTE.privacy
    | NAME_ROUTE.addPaymentMethod
    | NAME_ROUTE.selectBagQuantity
    | NAME_ROUTE.selectSchedule
    | NAME_ROUTE.additionalNotes
    | NAME_ROUTE.pricingDetail
  >;
  navigation?: StackNavigationPropScreens;
  params?: OptionsParams;
};

// Styles
const styles = StyleSheet.create({
  iconHeader: {
    marginHorizontal: METRICS.CONSTANTS_MARGIN,
    minWidth: 50
  }
});

const OptionsParamsDefault: OptionsParams = {
  hideTitle: false,
  transparentHeader: false,
  hideHeader: false,
  title: undefined,
  isDrawer: false,
  iconDrawer: 'rocket',
  iconLibrary: undefined
};

/**
 * @function optionsScreens
 * @description dependiendo de los parámetros pasados el screen cambia.
 */
export const optionsScreens = ({ route, params = OptionsParamsDefault }: OptionsScreensParams): StackNavigationOptions => {
  let options: StackNavigationOptions = {};
  //CLog(URL_PATH, 'optionsScreens', '{ route, params }', { route, params });
  // si se muestra el header del screen
  if (route?.name === NAME_ROUTE.drawer || params?.hideHeader) {
    options = {
      ...options,
      headerShown: false
    };
  }

  // nombre del screen
  if (params?.title) {
    options = {
      ...options,
      title: params.title
    };
  }

  options = {
    ...options,
    // oculta el nombre del screen
    headerTitle: params.hideTitle
      ? ''
      : props => (
          <Text
            numberOfLines={2}
            style={{
              color: props.tintColor,
              fontWeight: '700',
              fontSize: 20,
              textAlign: 'center',
              marginLeft: 25
            }}>
            {props.children}
          </Text>
        ),
    // lo vuelve transparente al encabezado
    headerTransparent: params.transparentHeader,
    // headerLeft: () => {
    //   return <Icon style={styles.iconHeader} onPress={() => {}} name={NAME_ICON.arrowBackCircle} />;
    // },
    ///
    // headerBackTitleVisible: false,
    headerTitleAlign: 'center',
    headerStyle: {
      backgroundColor: THEME_DEFAULT.primary
    },
    headerTintColor: THEME_DEFAULT.primaryFont,
    headerTitleStyle: [validateTypeText(EnumFont.octonary)],
    // headerTitleContainerStyle: {  }
    headerTitleAllowFontScaling: false,
    headerBackTitleVisible: false
  };

  return options;
};

export const optionsDrawer = ({ route, params = OptionsParamsDefault, navigation }: OptionsScreensParams): DrawerNavigationOptions => {
  //CLog(URL_PATH, 'optionsDrawer()', '{ route, params }', { route, params, navigation });
  let options: DrawerNavigationOptions = {};
  // si se muestra el header del screen
  if (route?.name === NAME_ROUTE.drawer || params?.hideHeader) {
    options = {
      ...options,
      headerShown: false
    };
  }

  // nombre del screen
  if (params?.title) {
    options = {
      ...options,
      title: params.title
    };
  }

  options = {
    ...options,
    // Header
    headerLeft: () => <></>,
    headerRight: ({ tintColor }: { tintColor?: string }) => (
      <Icon
        style={styles.iconHeader}
        color={tintColor}
        onPress={() => navigation?.dispatch(DrawerActions.openDrawer())}
        name={NAME_ICON.menu}
        library={TypeLibraryIconsSystem.materialCommunityIcons}
      />
    ),
    headerTitleAlign: 'center',
    headerStyle: {
      backgroundColor: THEME_DEFAULT.primary
    },
    headerTintColor: THEME_DEFAULT.primaryFont,
    headerTitleStyle: validateTypeText(EnumFont.octonary),
    // Menu lateral
    drawerLabel: ({ color }: { color: string; focused: boolean }) => (
      <Text style={{ color }} type={EnumFont.sixth}>
        {params.title || route?.name}
      </Text>
    ),
    drawerIcon: ({ color }: { color: string; size: number; focused: boolean }) => (
      <Icon name={params.iconDrawer || NAME_ICON.debug} size={SIZE_ICONS.inputOption} library={params.iconLibrary} {...{ color }} />
    )
  };
  return options;
};

/**
 * @function openMenu
 * @description Abre el menu lateral de la APP
 * @param {OptionsScreensParams} { navigation }
 */
export const openMenu = (navigation: StackNavigationPropScreens) => {
  navigation.dispatch(DrawerActions.openDrawer());
};

/**
 * @function canOpenURL
 * @description valida si es posible abrir la url
 * @param {string} url
 * @return {Promise<boolean>}
 */
export const canOpenURL = async (url: string): Promise<boolean> => {
  return await Linking.canOpenURL(url);
};

/**
 * @function openURL
 * @description funcion que abre la url con alguna app instalada en el
 * dispotivo.
 * @param {string} url
 * @return {Promise<{message?: string; status?: boolean}>}
 */
export const openURL = async (url: string): Promise<{ message?: string; status?: boolean }> => {
  try {
    const isCanOpen = await canOpenURL(url);
    if (isCanOpen) {
      await Linking.openURL(url);
      return { status: true };
    } else {
      return { message: 'No es posible abrir el enlace', status: false };
    }
  } catch (err) {
    CLog(URL_PATH, 'openURL()', 'err', err);
    return { message: 'Ocurrió un error al abrir el enlace', status: false };
  }
};
