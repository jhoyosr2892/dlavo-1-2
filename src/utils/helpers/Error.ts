/* eslint-disable camelcase */
import axios from 'axios';

import { CLog } from '@helpers/Message';

import { ResponseApp } from '@services/app';
import { translate } from 'locales';

const URL_PATH = 'src/utils/helpers/Error.ts';

export const handleErrorServiceApi = (e: Error): ResponseApp => {
  try {
    if (axios.isAxiosError(e)) {
      CLog(URL_PATH, 'handleErrorServiceApi()', 'e.response?.data.message', e.response?.data.message);
      if (e.response?.data && e.response.data.message) {
        console.log(e.response?.data.message)
        return { status: false, message: translate('GENERAL.ERRORS.' + e.response?.data.message) };
      } else {
        return { status: false, message: translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
      }
    } else {
      return { status: false, message: e.message || 'Error ...' };
    }
    // return { status: false, message: 'Error ...' };
  } catch (err: any) {
    CLog(URL_PATH, 'handleErrorServiceApi()', 'err.message', err.message);
    return { status: false, message: 'Error...' };
  }
};
