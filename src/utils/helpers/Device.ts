// Libs
import Orientation from 'react-native-orientation-locker';

// Helpers
import { CLog } from '@helpers/Message';

// Constants
const URL_PATH = 'src/utils/helpers/Device.ts';

// Enum
// El tipo de orientación del dispositivo
export enum OrientationDevice {
  portrait = 'portrait',
  landscapeLeft = 'landscapeLeft'
}

/**
 * @function lockScreenDevice
 * @description Permite poner la posición de la pantalla en una orientación.
 * @param {OrientationDevice} orientation
 */
export const lockScreenDevice = (orientation?: OrientationDevice) => {
  try {
    switch (orientation) {
      case OrientationDevice.portrait:
        Orientation.lockToPortrait();
        break;
      case OrientationDevice.landscapeLeft:
        Orientation.lockToLandscapeLeft();
        break;
      default:
        Orientation.lockToPortrait();
        break;
    }
  } catch (err) {
    CLog(URL_PATH, 'lockScreenDevice()', 'err', err);
  }
};
