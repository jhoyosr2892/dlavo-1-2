// Libs
import { PermissionsAndroid } from 'react-native';

// Helpers
import { CLog } from '@helpers/Message';

// Constants
import { NAME_APP, IS_ANDROID } from '@constantsApp';
import { translate } from 'locales';

const URL_PATH = 'src/utils/helpers/Permission.ts';

type ResponsePermissions = {
  message?: string;
  status?: boolean;
};

/**
 * @function requestWriteExternalStorage
 * @description Solicita al usuario de dispotivos android el permiso para guardar
 * en memoria informacion.
 */
export const requestWriteExternalStorage = async (): Promise<ResponsePermissions> => {
  let responsePermissions: ResponsePermissions;
  try {
    if (IS_ANDROID) {
      const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
        title: `${NAME_APP} Permiso Almacenar en Memoria`,
        message: `${NAME_APP} solicita el permiso para guardar, los certificados en la memoria del dispositivo.`,
        buttonNeutral: 'Preguntar después',
        buttonNegative: 'Cancelar',
        buttonPositive: 'OK'
      });
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        responsePermissions = { status: true };
      } else {
        responsePermissions = { message: 'No se permitió guardar en memoria', status: false };
      }
    } else {
      responsePermissions = { message: 'No es android', status: true };
    }
  } catch (err) {
    CLog(URL_PATH, 'requestWriteExternalStorage()', 'err', err);
    responsePermissions = { message: 'Error al conceder el permiso.', status: false };
  }
  return responsePermissions;
};


export async function requestLocationPermission() {
  let responsePermissions: ResponsePermissions;
  try {
    if (IS_ANDROID) {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: translate('PERMISSION.LOCATION.TITLE', { app: NAME_APP }),
          message: translate('PERMISSION.LOCATION.MESSAGE', { app: NAME_APP }),
          buttonNeutral: translate('PERMISSION.NEUTRAL_BUTTON'),
          buttonNegative: translate('GENERAL.MESSAGES.BUTTON_CANCEL'),
          buttonPositive: translate('GENERAL.MESSAGES.BUTTON_OK')
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the location")
        responsePermissions = { status: true };
      } else {
        console.log("location permission denied")
        responsePermissions = { message: translate('PERMISSION.LOCATION.ERRORS.NOT_ACCESS_PERMISSION'), status: false };
      }
    } else {
      responsePermissions = { status: true };
    }
  } catch (err) {
    console.warn(err)
    responsePermissions = { message: translate('PERMISSION.LOCATION.ERRORS.ERROR_ACCESS_PERMISSION'), status: false };
  }
  return responsePermissions;
};
