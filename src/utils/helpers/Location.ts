// Libs
import Geolocation from 'react-native-geolocation-service';
// Constants
import { optionsGPS } from '@config/Constants';
import { requestLocationPermission } from './Permission';

//method to access the user's location
export const getPositionGPS = async (): Promise<Geolocation.GeoPosition | Geolocation.GeoError | undefined> => {
  try {
    const locationPermission = await requestLocationPermission();
    if (locationPermission.status) {
      const geolocationPositionPromise = new Promise<Geolocation.GeoPosition | Geolocation.GeoError>((resolve, reject) => {
        Geolocation.getCurrentPosition(
          (position) => {
            resolve(position)
          },
          (error) => {
            // See error code charts below.
            console.log(error.code, error.message);
            reject()
          },
          optionsGPS
        );
      });
      const result = await geolocationPositionPromise;
      return result;
    } else {
      return;
    }
  } catch (e) {
    return;
  }
};
