// constants
import { DEVELOPMENT } from '@constantsApp';
import { TypeLogSystem } from '@enum';

/**
 * @func CLog
 * @description funcion usada para imprimir los mensajes de distinto tipo de la app {@link TypeLogSystem}
 * @param {string} urlPath path desde donde se escribe el console
 * @param {string} functionInvokes función desde donde se invoca el console
 * @param {string} type nombre de la variable que almacena el dato a imprimir
 * @param {string} message dato a imprimir
 * @param {TypeLogSystem} typeLog
 */
export function CLog(
  urlPath?: string | null,
  functionInvokes?: string | null,
  type?: string | null,
  message?: any | null,
  typeLog?: TypeLogSystem | null
) {
  // DEBUG: recordar des-comentar esto cuando se suba a producción o se lance un push.
  if (DEVELOPMENT) {
    switch (typeLog) {
      case TypeLogSystem.warning:
        console.warn(
          `🛠 ${urlPath || 'undefined'} ⏩ ⏩ ⏩ ${functionInvokes || 'undefined'} ⏩ ⏩ ${type || 'undefined'} ⏩`,
          message,
          '🛑'
        );
        break;
      case TypeLogSystem.error:
        console.error(
          `🚨 ${urlPath || 'undefined'} ⏩ ⏩ ⏩ ${functionInvokes || 'undefined'} ⏩ ⏩ ${type || 'undefined'} ⏩`,
          message,
          '🛑'
        );
        break;
      case TypeLogSystem.success:
        console.log(
          `✅ ${urlPath || 'undefined'} ⏩ ⏩ ⏩ ${functionInvokes || 'undefined'} ⏩ ⏩ ${type || 'undefined'} ⏩`,
          message,
          '🛑'
        );
        break;
      default:
        console.log(
          `🍀 ${urlPath || 'undefined'} ⏩ ⏩ ⏩ ${functionInvokes || 'undefined'} ⏩ ⏩ ${type || 'undefined'} ⏩`,
          message,
          '🛑'
        );
        break;
    }
  }
}
