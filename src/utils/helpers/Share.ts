import { Share, ShareContent, ShareOptions } from 'react-native';

export const share = async (content: ShareContent, options?: ShareOptions) => {
  try {
    const result = await Share.share({
      message:
        'React Native | A framework for building native apps using React',
      url: ''
    });
    if (result.action === Share.sharedAction) {
      if (result.activityType) {
        // shared with activity type of result.activityType
      } else {
        // shared
      }
    } else if (result.action === Share.dismissedAction) {
      // dismissed
    }
  } catch (error) {
    return error;
  }
}