import { Dimensions, Platform, PixelRatio } from 'react-native';
// Libs
import { LayoutChangeEvent } from 'react-native';
import { DurationInputArg1, DurationInputArg2 } from 'moment';

// Configs
import { TimeUnits } from '@config/Enum';
import moment from '@config/Moment';

// Helpers
import { CLog } from '@helpers/Message';

// Constants
import { DimensionsComponentType, RangeProgress, EnumFormatDateTime } from '@constantsApp';

const URL_PATH = 'src/utils/helpers/Convert.ts';

/**** Google ****/
const encodeNumber = (num: number) => {
  let encodeString = '';

  while (num >= 0x20) {
    encodeString += String.fromCharCode((0x20 | (num & 0x1f)) + 63);
    num >>= 5;
  }

  encodeString += String.fromCharCode(num + 63);
  return encodeString;
};

const encodeSignedNumber = (num: number) => {
  let sgnNum = num << 1;

  if (num < 0) {
    sgnNum = ~sgnNum;
  }

  return encodeNumber(sgnNum);
};

const encodePoint = (plat: number, plng: number, lat: number, lng: number) => {
  let dlng = 0;
  let dlat = 0;

  const late5 = Math.round(lat * 1e5);
  const plate5 = Math.round(plat * 1e5);

  const lnge5 = Math.round(lng * 1e5);
  const plnge5 = Math.round(plng * 1e5);

  dlng = lnge5 - plnge5;
  dlat = late5 - plate5;

  return encodeSignedNumber(dlat) + encodeSignedNumber(dlng);
};

const createEncodings = (coords: { latitude: number; longitude: number }[]) => {
  let i = 0;

  let plat = 0;
  let plng = 0;

  let encodedPoints = '';

  for (i = 0; i < coords.length; ++i) {
    const lat = coords[i].latitude;
    const lng = coords[i].longitude;

    encodedPoints += encodePoint(plat, plng, lat, lng);

    plat = lat;
    plng = lng;
  }

  return encodedPoints;
};
/*********************/
/********* Remplazo ************/
const hexToUrlColor = (color: string) => {
  return color.replace('#', '0x');
};

/**
 * @description convierte una numero del 1 al 7 en su correspondiente orden
 * en el abecedario.
 */
export const orderOfNumbersToAlphabet = (num: number): string => {
  try {
    if (num === 1) {
      return 'a. ';
    } else if (num === 2) {
      return 'b. ';
    } else if (num === 3) {
      return 'c. ';
    } else if (num === 4) {
      return 'd. ';
    } else if (num === 5) {
      return 'e. ';
    } else if (num === 6) {
      return 'f. ';
    } else if (num === 7) {
      return 'g. ';
    } else {
      return 'a. ';
    }
  } catch (err) {
    CLog(URL_PATH, 'orderOfNumbersToAlphabet()', 'err', err);
    return 'a.';
  }
};

/**
 * @function arrayStringToStringList
 * @description valida si el valor ingresado es un array de string y si así
 * es lo devuelve en una lista con indice numérico y con salto de linea. si el
 * dato ingresado no es un array de string simplemente retorna el mismo valor
 * ingresado.
 * @param {string[] | string | number} strArray
 * @return {string | number}
 */
export const arrayStringToStringList = (strArray: string[] | string | number): string | number => {
  let stringFormatt: string | number = '';
  if (typeof strArray === 'object') {
    //CLog(URL_PATH, 'arrayStringToStringList()', 'strArray', strArray);
    if (strArray.length > 0) {
      strArray.forEach((str, index) => {
        if (strArray.length === index + 1) {
          stringFormatt += `${index + 1}. ${str}`;
        } else {
          stringFormatt += `${index + 1}. ${str}\n`;
        }
      });
    } else {
      stringFormatt = 0;
    }
    return stringFormatt;
  } else {
    //CLog(URL_PATH, 'arrayStringToStringList()', 'strArray', strArray);
    stringFormatt = strArray;
    return stringFormatt;
  }
};

export { createEncodings as createEncodingsPolyline, hexToUrlColor };


/************/
/********** Time and Date **********/
/**
 * @function duration
 * @description convierte una unidad de tiempo a otra
 * @param {number} time
 * @return {string | undefined}
 */
export const durationSecondToDuration = (time: number): string | undefined => {
  try {
    const durationSTD = moment.duration(time, 'seconds');
    const secondsSTD = durationSTD.seconds();
    const minutesSTD = durationSTD.minutes();
    const hoursSTD = durationSTD.hours();
    if (hoursSTD !== 0) {
      return `${hoursSTD < 10 ? `0${hoursSTD}` : hoursSTD}:${minutesSTD < 10 ? `0${minutesSTD}` : minutesSTD}:${secondsSTD < 10 ? `0${secondsSTD}` : secondsSTD
        }`;
    } else {
      return `${minutesSTD < 10 ? `0${minutesSTD}` : minutesSTD}:${secondsSTD < 10 ? `0${secondsSTD}` : secondsSTD}`;
    }
  } catch (err) {
    CLog(URL_PATH, 'duration()', 'err', err);
    return undefined;
  }
};

/**
 * @function durationHumanize
 * @description convierte un numero de minutos en tiempo que falta para que
 * comienzan o hace cuanto termino. Pero de forma mas humana.
 * ejm: 3 Horas
 * @param {number} timeDuration
 * @return {string | undefined}
 */
export const durationHumanize = (timeDuration: number): string | undefined => {
  try {
    //CLog(URL_PATH, 'durationHumanize()', 'momentInMinutes', moment().format('LLLL'));
    return moment.duration(timeDuration, 'minutes').humanize();
  } catch (err) {
    CLog(URL_PATH, 'durationHumanize()', 'err', err);
    return undefined;
  }
};

/**
 * @function timeDoingHumanize
 * @description convierte los minutos a un formato mas humano.
 * ejm. 1 Hora, 30 Minutos
 * @param {number} timeDuration
 * @return {string | undefined}
 */
export const timeDoingHumanize = (timeDuration: number): string | undefined => {
  try {
    //CLog(URL_PATH, 'timeDoingHumanize', 'momentInMinutes', moment().format('LLLL'));
    let formantDuration: string | undefined;
    if (timeDuration < 60) {
      formantDuration = `${timeDuration === 1 ? '1 Minuto' : `${timeDuration} Minutos`}`;
    } else if (timeDuration < 1440) {
      const hoursD = parseInt(`${timeDuration / 60}`, 10);
      const minutesD = timeDuration % 60;
      let minutesDS = '';

      if (minutesD !== 0) {
        minutesDS = `, ${minutesD === 1 ? '1 Minuto' : `${minutesD} Minutos`}`;
      }
      formantDuration = `${hoursD === 1 ? '1 Hora' : `${hoursD} Horas`}${minutesDS}`;
    } else if (timeDuration >= 1440) {
      const daysD = parseInt(`${timeDuration / 1440}`, 10);
      const hoursD = parseInt(`${(timeDuration % 1440) / 60}`, 10);
      const minutesD = (timeDuration % 1440) % 60;
      let hoursDS = '';
      let minutesDS = '';

      if (hoursD !== 0) {
        hoursDS = `, ${hoursD === 1 ? '1 Hora' : `${hoursD} Horas`}`;
      }
      if (minutesD !== 0) {
        minutesDS = `, ${minutesD === 1 ? '1 Minuto' : `${minutesD} Minutos`}`;
      }
      formantDuration = `${daysD === 1 ? '1 Día' : `${daysD} Días`}${hoursDS}${minutesDS}`;
    }
    return formantDuration;
  } catch (err) {
    CLog(URL_PATH, 'timeDoingHumanize()', 'err', err);
    return undefined;
  }
};

/**
 * @function dateToDateDisplay
 * @description convierte la fecha y hora que llega del servidor a un formato
 * mas legible para el usuario.
 * @param {string} dateTime
 * @param {outDateTime} EnumFormatDateTime
 * @return {string}
 */
export const dateToDateDisplay = (dateTime: string, outDateTime?: EnumFormatDateTime): string => {
  return moment(dateTime).format(outDateTime || EnumFormatDateTime.DMMMMYYYY);
};

/**
 * @description convierte de moment a iso 8601 string
 * @param {string} date
 * @return {string} ISO_8601
 * @return {undefined}
 */
export const toISOString = (date: string): string | undefined => {
  try {
    return moment(date).toISOString();
  } catch (err) {
    CLog(URL_PATH, 'toISOString()', 'err', err);
    return undefined;
  }
};

/**
 * @description obtiene la hora actual del dispositivo en formato ISO_8601
 * @return {string}
 */
export const nowDateISO = (): string => {
  return moment().toISOString();
};

/**
 * @description le suma una unidad definida a la fecha que se le pase como
 * parámetro.
 * @return {string}
 */
export const addUnitTime = ({
  date,
  quantity = 1,
  unity = TimeUnits.hours,
  format = EnumFormatDateTime.YYYYMMDDHHmm
}: {
  date: string;
  quantity?: DurationInputArg1;
  unity?: DurationInputArg2;
  format?: EnumFormatDateTime;
}): string => {
  try {
    return moment(date).add(quantity, unity).format(format);
  } catch (err) {
    return moment().format(format);
  }
};

/**
 * @description le sustrae una unidad definida a la fecha que se le pasa como
 * parámetro.
 * @return {string}
 */
export const subtractUnitTime = ({
  date,
  quantity = 1,
  unity = TimeUnits.hours,
  format = EnumFormatDateTime.YYYYMMDDHHmm
}: {
  date: string;
  quantity?: DurationInputArg1;
  unity?: DurationInputArg2;
  format?: EnumFormatDateTime;
}): string => {
  try {
    return moment(date).subtract(quantity, unity).format(format);
  } catch (err) {
    return moment().format(format);
  }
};

/**
 * @description convierte la data de la que se tiene en el server a como se usa
 * en la app.
 */
export const dateServerToApp = (date: string): string => {
  try {
    return moment(date).format(EnumFormatDateTime.YYYYMMDDHHmm);
  } catch (err) {
    CLog(URL_PATH, 'dateServerToApp()', 'err', err);
    return moment().format(EnumFormatDateTime.YYYYMMDDHHmm);
  }
};

/************/
/********** Rangos y Dominios **********/
export const percentageBetweenRanges = (range?: RangeProgress, value?: number) => {
  try {
    let rangeCurrent: RangeProgress = {
      min: 0,
      max: 100
    };
    let valueCurrent = 0;

    if (range) {
      rangeCurrent = range;
    }
    if (value) {
      valueCurrent = value;
    }
    return (valueCurrent * 100) / rangeCurrent.max!;
  } catch (err) {
    CLog(URL_PATH, 'percentageBetweenRanges()', 'err', err);
  }
};

/************/
/********** Actions ************/
export const onLayout = (e: LayoutChangeEvent): DimensionsComponentType => {
  try {
    //CLog(URL_PATH, 'onLayout()', '{ height, width }', { height: e.nativeEvent.layout.height, width: e.nativeEvent.layout.width });
    return {
      height: e.nativeEvent.layout.height,
      width: e.nativeEvent.layout.width
    };
  } catch (err) {
    CLog(URL_PATH, 'onLayout()', 'err', err);
    return {
      height: 0,
      width: 0
    };
  }
};

/*************/
/****** Numerós *******/
/**
 * @function roundNumber
 * @description redondeo de un numero
 * @param {number} num
 * @return {number}
 */
export const roundNumber = (num: number): number => {
  return Math.round(num);
};

/**
 * @function roundUpNumber
 * @description redondeo hacia arriba de un numero
 * @param {number} num
 * @return {number}
 */
export const roundUpNumber = (num: number): number => {
  return Math.ceil(num);
};

/**
 * @function roundDownNumber
 * @description redondeo hacia abajo de un numero
 * @param {number} num
 * @return {number}
 */
export const roundDownNumber = (num: number): number => {
  return Math.floor(num);
};

/** File **/
export const blobToFile = (theBlob: Blob, fileName: string, type: string): File => {
  return new File([theBlob], fileName, { type, lastModified: Date.now() });
};


export function currencyFormat(num: number, fixedDigits = 2) {
  return '$' + num.toFixed(fixedDigits).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}



const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');

// based on iphone 5s's scale
const scale = SCREEN_WIDTH / 320;

export function normalize(size: number) {
  const newSize = size * scale
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(newSize))
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
  }
}


/**
 * @function differenceBetweenTwoDates
 * @description retorna la diferencia entre dos fechas
 * @param {Date} dateOne
 * @param {Date} dateTwo
 * @return {number}
 */
export const differenceBetweenTwoDates = (dateOne: Date, dateTwo: Date): number => {
  return Math.ceil(Math.abs(dateOne.getTime() - dateTwo.getTime()) / 86400000);
};


export const capitalizeFirstLetter = (string: string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};
