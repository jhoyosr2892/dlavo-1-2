import { Linking, Platform } from 'react-native';
import I18n from 'i18n-js';

/**
 * Open url
 * @param {string} link
 */
export const handleClick = (link: string) => {
  Linking.canOpenURL(link).then(supported => {
    if (supported) {
      Linking.openURL(link);
      return true;
    } else {
      return { status: false, message: I18n.t('LINKING_HELPER.ERROR_OPEN_LINK', { link }) };
    }
  });
};

/**
 * Open phone link
 * @param {{
 * phone:string
 * }} param0
 */
export const openTel = ({ phone }: { phone: string }) => {
  handleClick(`tel:${phone}`);
};

/**
 * Open mail link
 * @param {{mail:string}} param0
 */
export const openEmail = ({ mail }: { mail: string }) => {
  handleClick(`mailto:${mail}`);
};

/**
 * Open whatsApp link
 * @param {{
 * phone:string;
 * text:string;
 * }} param0
 */
export const openWhatsApp = ({ phone, text = '' }: { phone?: string; text?: string }) => {
  let query = '';
  if (phone && phone.includes('+')) {
    query += `phone=${phone}`;
  }
  if (text) {
    query += `text=${text}`;
  }
  if (query) {
    handleClick(`https://api.whatsapp.com/send?${query}`);
  }
};

/**
 * Open instagram link
 * @param {{
 * username:string
 * }} param0
 */
export const openInstagram = ({ username }: { username: string }) => {
  const newUsername = username.replace('@', '').toLowerCase().normalize('NFD').replace(/\s+/g, '');
  handleClick(`https://instagram.com/${newUsername}`);
};

/**
 * Open facebook link
 * @param {{username:sting}} param0
 */
export const openFacebook = ({ username }: { username: string }) => {
  const formatUsername = username
    .replace(/ /g, '')
    .toLowerCase()
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '');
  handleClick(`https://facebook.com/${formatUsername}`);
};

/**
 * Open youTube link
 * @param {{username:string}} param0
 */
export const openYouTube = ({ username }: { username: string }) => {
  handleClick(`https://www.youtube.com/c/${username}`);
};

/**
 * Open google maps
 * @param {{
 * lat:string;
 * lng:string;
 * label?:string;
 * }} param0
 */
export const openMaps = ({ lat, lng, label }: { lat: string; lng: string; label?: string }) => {
  const scheme = Platform.select({ ios: 'maps:0,0?q=', android: 'geo:0,0?q=' });
  const latLng = `${lat},${lng}`;
  const url = Platform.select({
    ios: `${scheme}${label}@${latLng}`,
    android: `${scheme}${latLng}(${label})`
  });
  if (url) {
    handleClick(url);
  }
};
