import { ServiceRequestModel } from './../../models/service/index';
/* eslint-disable camelcase */
import { AxiosResponse } from 'axios';
//import DeviceInfo from 'react-native-device-info';

import { api } from '@services/apis';
import { ResponseApp } from '@services/app';

import { handleErrorServiceApi } from '@helpers/Error';
import { CLog } from '@helpers/Message';
import { RequestTypes } from 'models/service/Request';

//import FirebaseToken from '@stores/Firebase';

const URL_PATH = 'src/services/apis/ServiceRequest.ts';

class ServiceRequestProvider {

  static async create(data: ServiceRequestModel): Promise<AxiosResponse<any> | ResponseApp> {
    try {
      const response = await api().post('service-requests', data);
      CLog(URL_PATH, 'create()', 'response', response);
      return response;
    } catch (e: any) {
      CLog(URL_PATH, 'create()', 'error', e);
      return handleErrorServiceApi(e);
    }
  }

  static async getServiceRequests(status?: RequestTypes): Promise<AxiosResponse<any> | ResponseApp> {
    try {
      let query = '';
      if (status) {
        query += 'status=' + status;
      }
      const response = await api().get('service-requests?' + query);
      CLog(URL_PATH, 'getServiceRequests()', 'response', response);
      return response;
    } catch (e: any) {
      CLog(URL_PATH, 'getServiceRequests()', 'error', e);
      return handleErrorServiceApi(e);
    }
  }
}

export default ServiceRequestProvider;
