import { AxiosResponse } from 'axios';

import { API_GOOGLE, GOOGLE_API_KEY, LatLng } from '@constantsApp';

import { handleErrorServiceApi } from '@helpers/Error';
import { CLog } from '@helpers/Message';

import { api } from '@services/apis';
import { ResponseApp } from '@services/app';

const URL_PATH = 'src/services/apis/Google.ts';

class GoogleProvider {
  static async queryAutocomplete(input: string): Promise<ResponseApp | AxiosResponse> {
    try {
      const inputFormat = encodeURIComponent(input);
      CLog(URL_PATH, 'querySelector()', 'inputFormat', inputFormat);
      return await api({ url: API_GOOGLE }).get(`place/queryautocomplete/json?key=${GOOGLE_API_KEY}&input=${inputFormat}&language=es-419`);
    } catch (e: any) {
      return handleErrorServiceApi(e);
    }
  }

  static async getPlaceDetail(idPlace: string): Promise<ResponseApp | AxiosResponse> {
    try {
      return await api({ url: API_GOOGLE }).get(
        `place/details/json?place_id=${idPlace}&key=${GOOGLE_API_KEY}&fields=name,formatted_address,geometry,place_id`
      );
    } catch (e: any) {
      return handleErrorServiceApi(e);
    }
  }

  /**
   * The term geocoding generally refers to translating a human-readable address into a location
   * on a map. The process of doing the opposite, translating a location on the map into a human
   * readable address, is known as reverse geocoding.
   */
  static async getGeocodeReverse({ latitude, longitude }: LatLng): Promise<ResponseApp | AxiosResponse> {
    try {
      const data = await api({ url: API_GOOGLE }).get(`geocode/json?key=${GOOGLE_API_KEY}&latlng=${latitude},${longitude}`);
      return data;
    } catch (e: any) {
      return handleErrorServiceApi(e);
    }
  }
}

export default GoogleProvider;
