import { UserModel } from '@models/user/User';
import { handleErrorServiceApi } from '@helpers/Error';
import { CLog } from '@helpers/Message';
// Libs
import axios, { AxiosResponse } from 'axios';
//import FormData from 'form-data';

// Api
import { api } from '@services/apis';

// Storage
import { UserIdSession } from '@stores/UserState';
import { ResponseApp } from 'services/app';
//import DeviceStorage from '@stores/Session';
import uuid from 'react-native-uuid';
import User from 'models/user/User';

const URL_PATH = 'src/services/apis/User.ts';

class UserProvider {
  /**
   * @description obtiene los datos del perfil de usuario
   * @return {Promise<AxiosResponse <any>>}
   */
  static async profile(): Promise<AxiosResponse<any>> {
    return await api().get(`users/find/${UserIdSession.id}`);
  }

  static async validateCode(code: string): Promise<AxiosResponse<any> | ResponseApp> {
    try {
      const data = {
        token: code
      };
      CLog(URL_PATH, 'validateCode()', 'data', data);
      const response = await api().post('users/validate', data);
      CLog(URL_PATH, 'validateCode()', 'response', response);
      return response;
    } catch (e: any) {
      CLog(URL_PATH, 'validateCode()', 'error', e);
      return handleErrorServiceApi(e);
    }
  }

  static async register(email: string): Promise<AxiosResponse<any> | ResponseApp> {
    try {
      const data = {
        id: uuid.v4().toString(),
        email: email.toLowerCase(),
        roles: ["ROLE_USER"]
      };
      CLog(URL_PATH, 'register()', 'data', data);
      const response = await api().post('users', data);
      // CLog(URL_PATH, 'register()', 'response', response);
      CLog(URL_PATH, 'register()', 'data', response.data);
      return response;
    } catch (e: any) {
      CLog(URL_PATH, 'register()', 'error', e);
      return handleErrorServiceApi(e);
    }
  }

  static async updateUser(user: UserModel, token: string): Promise<AxiosResponse<any> | ResponseApp> {
    try {
      CLog(URL_PATH, 'updateUser()', 'data', user);
      const response = await api().put('users', user, { headers: { Authorization: token } });
      CLog(URL_PATH, 'updateUser()', 'response', response);
      return response;
    } catch (e: any) {
      CLog(URL_PATH, 'updateUser()', 'error', e);
      return handleErrorServiceApi(e);
    }
  }

  static async getCurrentUser(token?: string): Promise<AxiosResponse<any> | ResponseApp> {
    try {
      let headers = {};
      if (token) {
        headers = { Authorization: token };
      }
      const response = await api().get('users', { headers });
      CLog(URL_PATH, 'getCurrentUser()', 'response.data', response);
      return response;
    } catch (e: any) {
      CLog(URL_PATH, 'getCurrentUser()', 'error', e);
      return handleErrorServiceApi(e);
    }
  }

  static async addNotificationToken(token?: string): Promise<AxiosResponse<any> | ResponseApp> {
    try {
      const response = await api().put('service-requests/users/add-notifications-token', { notificationsToken: token });
      CLog(URL_PATH, 'addNotificationToken()', 'response.data', response);
      return response;
    } catch (e: any) {
      CLog(URL_PATH, 'addNotificationToken()', 'error', e);
      return handleErrorServiceApi(e);
    }
  }

}

export default UserProvider;
