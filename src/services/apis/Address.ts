import { AddressModel } from 'models/address';
import { AxiosResponse } from 'axios';

import { api } from '@services/apis';
import { ResponseApp } from '@services/app';

import { handleErrorServiceApi } from '@helpers/Error';
import { CLog } from '@helpers/Message';


const URL_PATH = 'src/services/apis/AddressProvider.ts';

class AddressProvider {
  static async getUserAddress(): Promise<AxiosResponse<any> | ResponseApp> {
    try {
      const response = await api().get('addresses');
      CLog(URL_PATH, 'getUserAddress()', 'response', response);
      return response;
    } catch (e: any) {
      CLog(URL_PATH, 'getUserAddress()', 'error', e);
      return handleErrorServiceApi(e);
    }
  }
  static async createAddress(address: AddressModel): Promise<AxiosResponse<any> | ResponseApp> {
    try {
      CLog(URL_PATH, 'createAddress()', 'address', address);
      const response = await api().post('addresses', address);
      CLog(URL_PATH, 'createAddress()', 'response', response);
      return response;
    } catch (e: any) {
      CLog(URL_PATH, 'createAddress()', 'error', e);
      return handleErrorServiceApi(e);
    }
  }
  static async deleteAddress(addressId: string): Promise<AxiosResponse<any> | ResponseApp> {
    try {
      CLog(URL_PATH, 'createAddress()', 'address', addressId);
      const response = await api().delete('addresses/' + addressId);
      CLog(URL_PATH, 'createAddress()', 'response', response);
      return response;
    } catch (e: any) {
      CLog(URL_PATH, 'createAddress()', 'error', e);
      return handleErrorServiceApi(e);
    }
  }
}

export default AddressProvider;
