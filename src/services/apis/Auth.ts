/* eslint-disable camelcase */
import { AxiosResponse } from 'axios';
//import DeviceInfo from 'react-native-device-info';

import { api } from '@services/apis';
import { ResponseApp } from '@services/app';

import { handleErrorServiceApi } from '@helpers/Error';
import { CLog } from '@helpers/Message';

//import FirebaseToken from '@stores/Firebase';

const URL_PATH = 'src/services/apis/Auth.ts';

class AuthProvider {
  static async login(email: string, password: string): Promise<AxiosResponse<any> | ResponseApp> {
    try {
      const data = {
        email: email.toLowerCase(),
        password: password
      };
      CLog(URL_PATH, 'login()', 'data', data);
      const response = await api().post('auth', data);
      CLog(URL_PATH, 'login()', 'response', response);
      return response;
    } catch (e: any) {
      CLog(URL_PATH, 'login()', 'error', e);
      return handleErrorServiceApi(e);
    }
  }

  static async requestCode(email: string): Promise<AxiosResponse<any> | ResponseApp> {
    try {
      const data = {
        email: email.toLowerCase()
      };
      CLog(URL_PATH, 'requestCode()', 'data', data);
      const response = await api().post('auth', data);
      CLog(URL_PATH, 'requestCode()', 'response', response);
      return response;
    } catch (e: any) {
      CLog(URL_PATH, 'requestCode()', 'error', e);
      return handleErrorServiceApi(e);
    }
  }
}

export default AuthProvider;
