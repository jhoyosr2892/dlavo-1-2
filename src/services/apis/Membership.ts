/* eslint-disable camelcase */
import { AxiosResponse } from 'axios';
//import DeviceInfo from 'react-native-device-info';

import { api } from '@services/apis';
import { ResponseApp } from '@services/app';

import { handleErrorServiceApi } from '@helpers/Error';
import { CLog } from '@helpers/Message';
import { BuyProducts, ResponseMembershipModel } from 'models/membership';

//import FirebaseToken from '@stores/Firebase';

const URL_PATH = 'src/services/apis/Membership.ts';

class MembershipProvider {

  static async getMemberships(): Promise<AxiosResponse<ResponseMembershipModel[]> | ResponseApp> {
    try {
      const response = await api().get('products');
      CLog(URL_PATH, 'getMemberships()', 'response', response);
      return response;
    } catch (e: any) {
      CLog(URL_PATH, 'getMemberships()', 'error', e);
      return handleErrorServiceApi(e);
    }
  }

  static async buyMembership(data: BuyProducts): Promise<AxiosResponse<any> | ResponseApp> {
    try {
      CLog(URL_PATH, 'requestCode()', 'data', data);
      const response = await api().post('products', data);
      CLog(URL_PATH, 'requestCode()', 'response', response);
      return response;
    } catch (e: any) {
      CLog(URL_PATH, 'requestCode()', 'error', e);
      return handleErrorServiceApi(e);
    }
  }

}

export default MembershipProvider;
