/* eslint-disable camelcase */
// Libs
import axios from 'axios';
import RNFetchBlob from 'rn-fetch-blob';

// Helpers
import { requestWriteExternalStorage } from '@helpers/Permission';
import { CLog } from '@helpers/Message';

// Stores
import DeviceToken from '@stores/Session';

// Constants
import { API_URL, ResourceTypeExtension } from '@constantsApp';
import I18n from 'i18n-js';

const URL_PATH = 'src/services/apis/index.ts';

// Params API
interface ApiParams {
  url?: string;
  v2?: boolean;
  v3?: boolean;
  headers?: object;
  multipart?: boolean;
}

export interface ApiDownloadFileParams {
  /** lo que complementa a la urlBase recuerde colocar el / al inicio de la url **/
  url?: string;
  /** url base para descargar el archivo **/
  urlBase?: string;
  /** extension de como de guarda el archivo **/
  extensionFile?: ResourceTypeExtension;
  /** nombre como de guarda el archivo **/
  nameFile?: string;
}

export interface ApiDownloadResponse {
  status?: boolean;
  message?: string;
  data?: any;
}

// Meta
export interface ApiMeta {
  pagination: {
    total: number;
    count: number;
    perPage: number;
    currentPage: number;
    totalPages: number;
  };
}

export const API_META = {
  pagination: {
    total: 0,
    count: 0,
    perPage: 0,
    currentPage: 0,
    totalPages: 0
  }
};

// Meta response
interface ApiMetaResponse {
  pagination: {
    total: number;
    count: number;
    per_page: number;
    current_page: number;
    total_pages: number;
  };
}

/**
 * Configuration API
 *
 * @param {*} params
 *
 */
export const api = (params?: ApiParams) => {
  // const { url = API_URL, headers, v2, v3 } = params;
  const url: string = params ? params.url || API_URL : API_URL;
  const v2: boolean | undefined = params ? params.v2 || false : false;
  const v3: boolean | undefined = params ? params.v3 || false : false;
  const headers: object | undefined = params ? params.headers || undefined : undefined;
  const apiInstance = axios.create({
    baseURL: url,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      language: I18n.locale,
      // post: {
      //   'Content-Type': 'application/json'
      // }
    }
  });

  const token = DeviceToken.token;

  if (token !== null) {
    apiInstance.defaults.headers.Authorization = `${token}`;
  }

  if (headers) {
    Object.assign(apiInstance.defaults.headers, headers);
  }

  if (params?.multipart) {
    apiInstance.defaults.headers['Content-Type'] = 'multipart/form-data';
  }

  if (v2) {
    apiInstance.defaults.headers.Accept = 'application/vnd.api.v2+json';
  }

  if (v3) {
    apiInstance.defaults.headers.Accept = 'application/vnd.api.v3+json';
  }

  return apiInstance;
};

export const apiDownloadFile = async ({
  nameFile = 'descarga',
  extensionFile = ResourceTypeExtension.pdf,
  url,
  urlBase = API_URL
}: ApiDownloadFileParams): Promise<ApiDownloadResponse> => {
  let apiResponse: ApiDownloadResponse;
  try {
    const { message, status } = await requestWriteExternalStorage();
    CLog(URL_PATH, 'apiResponseBlob', '{ message, status }', { message, status });
    if (status) {
      const dirs = RNFetchBlob.fs.dirs;
      const { respInfo, data } = await RNFetchBlob.config({
        /** NOTE: seria bueno dejarle al usuario la opción para configurar donde
         *guardar lo que se descarga
         */
        path: `${dirs.DownloadDir}/${nameFile}${extensionFile}`
      }).fetch('GET', `${urlBase}${url || ''}`, {
        Authorization: `Bearer ${DeviceToken.token}`,
        'Content-Type': 'blob'
      });
      //CLog(URL_PATH, 'apiDownloadFile()', '{ respInfo, data }', { respInfo, data });
      if (respInfo.status === 200) {
        apiResponse = { message: `guardado en ${data}`, status: true, data: { data, respInfo } };
      } else {
        CLog(URL_PATH, 'apiDownloadFile()', '{ respInfo, data }', { data, respInfo });
        apiResponse = { message: `${nameFile} no se pudo descargar`, status: false };
      }
    } else {
      CLog(URL_PATH, 'apiDownloadFile()', 'message', message);
      apiResponse = { message, status: false };
    }
  } catch (err) {
    CLog(URL_PATH, 'apiDownloadFile()', 'err', err);
    apiResponse = { message: 'Error al descargar el archivo', status: false };
  }
  return apiResponse;
};

export const metaResponseAtMeta = (data: ApiMetaResponse): ApiMeta => {
  // CLog(URL_PATH, 'metaResponseAtMeta()', 'data', data);
  return {
    pagination: {
      total: data.pagination.total,
      count: data.pagination.count,
      perPage: data.pagination.per_page,
      currentPage: data.pagination.current_page,
      totalPages: data.pagination.total_pages
    }
  };
};
