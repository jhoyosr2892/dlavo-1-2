export interface ResponseApp<T = any> {
  status: boolean;
  message?: string | null;
  data?: T;
}
