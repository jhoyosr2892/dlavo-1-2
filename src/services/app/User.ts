import { UserModel, UserResponseServer } from '@models/user/User';
import DeviceToken from '@stores/Session';
// Providers
//import UserProvider from '@services/apis/User';

// Helpers
import { CLog } from '@helpers/Message';

// Services
import UserProvider from '@services/apis/User';
import { ResponseApp } from '@services/app';

// Storages
import UserStorage from '@stores/UserState';
import { translate } from 'locales';

// DEBUG: quitar esto cuando ya se tenga establecido el servidor
//import DataServerFake from '@scenes/debug/DataServerFake.json';

// Constants
const URL_PATH = 'src/services/app/User.ts';

export const loadProfileSession = async (): Promise<ResponseApp> => {
  let responseApp: ResponseApp;
  try {
    const { data, status } = await UserProvider.profile();
    //const { data, status } = DataServerFake.userProfile;
    //CLog(URL_PATH, 'loadProfileSession()', '{ data, status }', { data: data, status });
    if (status === 200) {
      UserStorage.update(data);
      //CLog(URL_PATH, 'loadProfileSession()', 'UserStorage.data', UserStorage.data);
      responseApp = { status: true, message: null };
      return responseApp;
    } else {
      CLog(URL_PATH, 'loadProfileSession()', '{ data, status }', { data, status });
      responseApp = { status: false, message: data.message || 'Error al cargar los datos del usuario' };
      return responseApp;
    }
  } catch (e: any) {
    CLog(URL_PATH, 'loadProfileSession()', 'e', e);
    responseApp = { status: false, message: e.message || 'Error al cargar los datos del usuario.' };
    return responseApp;
  }
};

export const validateCode = async (code: string): Promise<ResponseApp> => {
  let responseApp: ResponseApp;
  try {
    const { data, status, message } = await UserProvider.validateCode(code);
    if (status === 200) {
      // await setTokenUserSession(data.token);
      // await loadUserAddress();
      responseApp = { data, status: true, message: message };
      return responseApp;
    } else {
      responseApp = { status: false, message: message || 'Error al ingresar' };
      return responseApp;
    }
  } catch (e: any) {
    responseApp = { status: false, message: e.message || 'Error...' };
    return responseApp;
  }
};

export const register = async (email: string): Promise<ResponseApp> => {
  let responseApp: ResponseApp;
  try {
    const { data, status, message } = await UserProvider.register(email);
    if (status === 201) {
      responseApp = { data, status: true, message: message };
      return responseApp;
    } else {
      responseApp = { status: false, message: message || 'Error al ingresar' };
      return responseApp;
    }
  } catch (e: any) {
    responseApp = { status: false, message: e.message || translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
    return responseApp;
  }
};

export const updateUser = async (user: UserModel, token = DeviceToken.token ? DeviceToken.token : ''): Promise<ResponseApp> => {
  let responseApp: ResponseApp;
  try {
    const { data, status, message } = await UserProvider.updateUser(user, token);
    if (status === 200) {
      console.log('data: ', data);
      if (UserStorage.data) {
        const userStorage: UserResponseServer = {
          firstName: user.firstName,
          lastName: user.lastName,
          email: user.email,
          id: UserStorage.data.id ? UserStorage.data.id : 0,
          username: UserStorage.data.username,
          phone: user.phone
        };
        UserStorage.update(userStorage);
      }
      console.log(UserStorage.data)
      responseApp = { status: true, message: message };
      return responseApp;
    } else {
      responseApp = { status: false, message: message || translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
      return responseApp;
    }
  } catch (e: any) {
    responseApp = { status: false, message: e.message || translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
    return responseApp;
  }
};

export const getCurrentUser = async (token?: string): Promise<ResponseApp> => {
  let responseApp: ResponseApp;
  try {
    const { data, status, message } = await UserProvider.getCurrentUser(token);
    if (status === 200) {
      CLog(URL_PATH, 'getCurrentUser()', 'data', data);
      UserStorage.update(data);
      responseApp = { status: true, message: message };
      return responseApp;
    } else {
      responseApp = { status: false, message: message || translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
      return responseApp;
    }
  } catch (e: any) {
    responseApp = { status: false, message: e.message || translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
    return responseApp;
  }
};

export const addNotificationToken = async (token?: string): Promise<ResponseApp> => {
  let responseApp: ResponseApp;
  try {
    const { data, status, message } = await UserProvider.addNotificationToken(token);
    CLog(URL_PATH, 'addNotificationToken()', '{ data, status, message }', { data: data, status, message: message });
    if (status === 200) {
      CLog(URL_PATH, 'addNotificationToken()', 'data', data);
      responseApp = { status: true, message: null, data };
      return responseApp;
    } else {
      CLog(URL_PATH, 'addNotificationToken()', '{ data, status }', { data, status });
      responseApp = { status: false, message: data ? data?.message : translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
      return responseApp;
    }
  } catch (e: any) {
    CLog(URL_PATH, 'addNotificationToken()', 'e', e);
    responseApp = { status: false, message: e.message || translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
    return responseApp;
  }
};
