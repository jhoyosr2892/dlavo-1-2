import { StatusCodeGoogle } from '@config/Enum';
import { LatLng, THEME_DEFAULT, GOOGLE_API_KEY, URL_MAPS_STATIC } from '@constantsApp';

import { hexToUrlColor, createEncodingsPolyline } from '@helpers/Convert';
import { CLog } from '@helpers/Message';

import PlaceGoogle, { PlaceGeocodeResponseServer } from '@models/google/Place';
import Prediction, { PredictionResponseServer } from '@models/google/Prediction';

import GoogleProvider from '@services/apis/Google';
import { ResponseApp } from '@services/app';

const URL_PATH = 'src/services/app/Google.ts';

/**
 * @type MarkerMapStatic
 * @description https://developers.google.com/maps/documentation/maps-static/start#MarkerStyles
 */
export type MarkerMapStatic = {
  size?: 'tiny' | 'mid' | 'small';
  // (optional) specifies a 24-bit color (example: color=0xFFFFCC) or a predefined color from the set
  color?: 'black' | 'brown' | 'green' | 'purple' | 'yellow' | 'blue' | 'gray' | 'orange' | 'red' | 'white' | string;
  /** specifies a single uppercase alphanumeric character from the set
   * {A-Z, 0-9}. (The requirement for uppercase characters is new to this
   * version of the API.) Note that default and mid sized markers are the only
   * markers capable of displaying an alphanumeric-character parameter. tiny
   * and small markers are not capable of displaying an alphanumeric-character.
   */
  label?: string;
  custom?: {
    icon: string;
    anchor?:
    | 'top'
    | 'bottom'
    | 'left'
    | 'right'
    | 'center'
    | 'topleft'
    | 'topright'
    | 'bottomleft'
    | 'bottomright'
    | { x: number; y: number };
  };
  location: LatLng;
};

/**
 * @type PathMapStatic
 * @description https://developers.google.com/maps/documentation/maps-static/start#Paths
 * @example path=color:0xffffffff|weight:10
 */
export type PathMapStatic = {
  // Path Styles
  weight?: number;
  color?: 'black' | 'brown' | 'green' | 'purple' | 'yellow' | 'blue' | 'gray' | 'orange' | 'red' | 'white' | string;
  // https://developers.google.com/maps/documentation/utilities/polylinealgorithm
  enc?: string;
  locations: LatLng[];
};

/**
 * @type MapStaticParams
 * @description https://developers.google.com/maps/documentation/maps-static/start#URL_Parameters
 */
type MapStaticParams = {
  // Location Parameters
  center?: LatLng; // required if markers not present
  zoom?: number; // required if markers not present
  // Map Parameters
  size?: {
    height: number;
    width: number;
  };
  language?: string;
  // Feature Parameters
  markers?: MarkerMapStatic[];
  // https://developers.google.com/maps/documentation/maps-static/start#Paths
  path?: PathMapStatic;
};

export const getPlaceDetail = async (idPlace: string): Promise<ResponseApp> => {
  try {
    const { status, data, message } = await GoogleProvider.getPlaceDetail(idPlace);
    if (status === 200) {
      if (data.status === StatusCodeGoogle.ok) {
        return { status: true, data: new PlaceGoogle(PlaceGoogle.formatedData(data)) };
      } else if (data.status === StatusCodeGoogle.zeroResult) {
        return { status: true, message: 'No hay resultado.', data: [] };
      } else if (data.status === StatusCodeGoogle.overQueryLimit) {
        return { status: false, message: 'La cuota de solicitudes se cumplió.' };
      } else if (data.status === StatusCodeGoogle.unknownError) {
        return await getPlaceDetail(idPlace);
      } else {
        return { status: false, message: 'Lo que se busca no tiene sentido.' };
      }
    } else {
      return { status: false, message };
    }
  } catch (e) {
    return { status: false, message: e.message };
  }
};

export const queryAutocompletePlace = async (input: string): Promise<ResponseApp> => {
  try {
    const { status, data, message } = await GoogleProvider.queryAutocomplete(input);
    CLog(URL_PATH, 'queryAutocompletePlace()', '{ status, data, message }', { status, data, message });
    if (status === 200) {
      if (data.status === StatusCodeGoogle.ok) {
        return {
          status: true,
          data: data.predictions.map((prediction: PredictionResponseServer) => new Prediction(Prediction.formatedData(prediction)))
        };
      } else if (data.status === StatusCodeGoogle.zeroResult) {
        return { status: true, message: 'No hay resultados.', data: [] };
      } else if (data.status === StatusCodeGoogle.overQueryLimit) {
        return { status: false, message: 'La cuota de solicitudes se cumplió.' };
      } else if (data.status === StatusCodeGoogle.unknownError) {
        return await queryAutocompletePlace(input);
      } else {
        return { status: false, message: 'Lo que se busca no tiene sentido.' };
      }
    } else {
      return { status: false, message };
    }
  } catch (e) {
    return { status: false, message: e.message };
  }
};

/**
 * The term geocoding generally refers to translating a human-readable address into a location
 * on a map. The process of doing the opposite, translating a location on the map into a human
 * readable address, is known as reverse geocoding.
 */
export const getGeocodeReverse = async (latlng: LatLng): Promise<ResponseApp> => {
  try {
    const { status, data, message } = await GoogleProvider.getGeocodeReverse(latlng);
    // console.log('data: ', data.results[0].address_components);
    if (status === 200) {
      if (data.status === StatusCodeGoogle.ok) {
        return {
          status: true,
          data: data.results.map((result: PlaceGeocodeResponseServer) => new PlaceGoogle(PlaceGoogle.formatedDataGeocode(result)))
        };
      } else if (data.status === StatusCodeGoogle.zeroResult) {
        return { status: true, message: 'No hay resultados.', data: [] };
      } else if (data.status === StatusCodeGoogle.overQueryLimit) {
        return { status: false, message: 'La cuota de solicitudes se cumplió.' };
      } else if (data.status === StatusCodeGoogle.unknownError) {
        return await getGeocodeReverse(latlng);
      } else {
        return { status: false, message: 'Lo que se busca no tiene sentido.' };
      }
    } else {
      return { status: false, message };
    }
  } catch (e) {
    return { status: false, message: e.message };
  }
};

export const getMapStatic = ({ center, zoom, size, markers, path }: MapStaticParams) => {
  let urlMapStatic: string = URL_MAPS_STATIC;
  if (center) {
    urlMapStatic += `center=${center.latitude},${center.longitude}`;
  }
  if (zoom) {
    urlMapStatic += `&zoom=${zoom}`;
  }

  urlMapStatic += `&size=${Math.round(size?.width || 0) || 200}x${Math.round(size?.height || 0) || 200}&scale=2`;

  if (markers) {
    markers.forEach((marker, index) => {
      if (marker.size || marker.color || marker.label) {
        urlMapStatic += `&markers=${marker.size ? `size:${marker.size}|` : 'size:mid|'}${marker.color ? `color:${hexToUrlColor(marker.color)}|` : `color:${hexToUrlColor(THEME_DEFAULT.primary)}|`
          }${marker.label ? `label:${marker.label}` : ''}`;
      }
      if (marker.custom) {
        urlMapStatic += `&markers=${marker.custom ? `anchor:${marker.custom.anchor}|` : ''}${marker.custom ? `icon:${marker.custom.icon}` : ''
          }`;
      }
      if (!marker.custom && !marker.size && !marker.color && !marker.label) {
        urlMapStatic += `&markers=label:${index + 1}|color:${hexToUrlColor(THEME_DEFAULT.primary)}|${marker.location.latitude},${marker.location.longitude
          }`;
      } else {
        urlMapStatic += `|${marker.location.latitude},${marker.location.longitude}`;
      }
    });
  }
  if (path) {
    urlMapStatic += `&path=${path.color ? `color:${hexToUrlColor(path.color)}|` : `color:${hexToUrlColor(THEME_DEFAULT.primary)}|`}${path.weight ? `weight:${path.weight}|` : ''
      }enc:${createEncodingsPolyline(path.locations)}`;
  }
  urlMapStatic += `&format=png32&key=${GOOGLE_API_KEY}`;
  //CLog(URL_PATH, 'getMapStatic()', 'urlMapStatic', urlMapStatic);
  return urlMapStatic;
};
