import { addAddress } from './../../stores/Address';
import Address, { AddressModel } from '@models/address';
import { AddressResponseServer } from './../../models/address/index';
// Providers
//import UserProvider from '@services/apis/User';

// Helpers
import { CLog } from '@helpers/Message';

// Services
import { ResponseApp } from '@services/app';

// Storages
import UserStorage from '@stores/UserState';
import AddressProvider from 'services/apis/Address';
import { translate } from 'locales';


// Constants
const URL_PATH = 'src/services/app/Address.ts';

export const loadUserAddress = async (): Promise<ResponseApp> => {
  let responseApp: ResponseApp;
  try {
    const { data, status } = await AddressProvider.getUserAddress();
    CLog(URL_PATH, 'loadUserAddress()', '{ data, status }', { data: data, status });
    if (status === 200) {
      data?.forEach((address: AddressResponseServer) => {
        addAddress(new Address(Address.formatData(address)));
      });
      CLog(URL_PATH, 'loadUserAddress()', 'UserStorage.data', UserStorage.data);
      responseApp = { status: true, message: null };
      return responseApp;
    } else {
      CLog(URL_PATH, 'loadUserAddress()', '{ data, status }', { data, status });
      responseApp = { status: false, message: data.message || 'Error al cargar las direcciones del usuario' };
      return responseApp;
    }
  } catch (e: any) {
    CLog(URL_PATH, 'loadUserAddress()', 'e', e);
    responseApp = { status: false, message: e.message || 'Error al cargar  las direcciones del usuario.' };
    return responseApp;
  }
};

export const createAddress = async (address: AddressModel): Promise<ResponseApp> => {
  let responseApp: ResponseApp;
  try {
    CLog(URL_PATH, 'createAddress()', '{ address  }', { address });
    const { data, status, message } = await AddressProvider.createAddress(address);
    CLog(URL_PATH, 'createAddress()', '{ data, status }', { data: data, status });
    if (status === 201) {
      responseApp = { status: true, message: null };
      return responseApp;
    } else {
      CLog(URL_PATH, 'createAddress()', '{ data, status }', { data, status });
      responseApp = { status: false, message: message || translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
      return responseApp;
    }
  } catch (e: any) {
    CLog(URL_PATH, 'createAddress()', 'e', e);
    responseApp = { status: false, message: e.message || translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
    return responseApp;
  }
};

export const deleteAddress = async (addressId: string): Promise<ResponseApp> => {
  let responseApp: ResponseApp;
  try {
    CLog(URL_PATH, 'createAddress()', '{ address  }', { addressId });
    const { data, status, message } = await AddressProvider.deleteAddress(addressId);
    CLog(URL_PATH, 'createAddress()', '{ data, status }', { data: data, status });
    if (status === 204) {
      responseApp = { status: true, message: null };
      return responseApp;
    } else {
      CLog(URL_PATH, 'createAddress()', '{ data, status }', { data, status });
      responseApp = { status: false, message: message || translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
      return responseApp;
    }
  } catch (e: any) {
    CLog(URL_PATH, 'createAddress()', 'e', e);
    responseApp = { status: false, message: e.message || translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
    return responseApp;
  }
};
