import { translate } from 'locales';
import { BuyProducts, Membership, MembershipModel } from 'models/membership';
import MembershipProvider from 'services/apis/Membership';
import { CLog } from 'utils/helpers/Message';
import { ResponseApp } from ".";

// Debug
import DataFake from '../debug/DataFake.json';

// Constants
const URL_PATH = 'src/services/app/Membership.ts';


/**
 * Carga todas las membresias
 */
export const getMemberships = async () => {
  let responseApp: ResponseApp;
  try {
    // const { data, status } = DataFake.getMemberships;
    const { data, status } = await MembershipProvider.getMemberships();
    if (status) {
      if (data.length > 0) {
        const _data: MembershipModel[] = [];
        data.forEach(e => {
          _data.push(Membership.formatData(e));
        });
        responseApp = {
          status: true,
          data: _data
        };
      } else {
        responseApp = {
          status: false,
          message: translate('MEMBERSHIP_SCENE.ERRORS.MEMBERSHIPS_NOT_FOUND')
        };
      }
    } else {
      CLog(URL_PATH, 'getMemberships()', '{ data, status }', { data, status });
      responseApp = {
        status: false,
        message: data.message ? data.message : translate('MEMBERSHIP_SCENE.ERRORS.ERROR_GET_MEMBERSHIPS')
      };
    }
  } catch (err: any) {
    CLog(URL_PATH, 'getMemberships()', 'err', err);
    responseApp = { status: false, message: err.message || translate('MEMBERSHIP_SCENE.ERRORS.ERROR_GET_MEMBERSHIPS') };
  }
  return responseApp;
};


export const buyMembership = async (infoBuyProduct: BuyProducts): Promise<ResponseApp> => {
  let responseApp: ResponseApp;
  try {
    CLog(URL_PATH, 'buyMembership()', '{ BuyProducts  }', infoBuyProduct);
    const { data, status, message } = await MembershipProvider.buyMembership(infoBuyProduct);
    CLog(URL_PATH, 'buyMembership()', '{ data, status }', { data: data, status });
    if (status === 200) {
      responseApp = { status: true, message: null };
      return responseApp;
    } else {
      CLog(URL_PATH, 'buyMembership()', '{ data, status }', { data, status });
      responseApp = { status: false, message: message || translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
      return responseApp;
    }
  } catch (e: any) {
    CLog(URL_PATH, 'buyMembership()', 'e', e);
    responseApp = { status: false, message: e.message || translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
    return responseApp;
  }
};