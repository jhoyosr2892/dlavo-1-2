import { ServiceRequestModel } from './../../models/service/index';
import { RequestTypes } from 'models/service/Request';

// Helpers
import { CLog } from '@helpers/Message';

// Services
import { ResponseApp } from '@services/app';

// Storages
import ServiceRequestProvider from 'services/apis/ServiceRequest';
import { translate } from 'locales';


// Constants
const URL_PATH = 'src/services/app/ServiceRequest.ts';


export const createServiceRequest = async (serviceRequest: ServiceRequestModel): Promise<ResponseApp> => {
  let responseApp: ResponseApp;
  try {
    CLog(URL_PATH, 'createServiceRequest()', '{ serviceRequest } }', { serviceRequest: JSON.stringify(serviceRequest, null, '\t') });
    const { data, status, message } = await ServiceRequestProvider.create(serviceRequest);
    CLog(URL_PATH, 'createServiceRequest()', '{ data, status, message }', { data: data, status, message: message });
    if (status === 201) {
      responseApp = { status: true, message: null };
      return responseApp;
    } else {
      CLog(URL_PATH, 'createServiceRequest()', '{ data, status }', { data, status });
      responseApp = { status: false, message: message || translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
      return responseApp;
    }
  } catch (e: any) {
    CLog(URL_PATH, 'createServiceRequest()', 'e', e);
    responseApp = { status: false, message: e.message || translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
    return responseApp;
  }
};

export const getServiceRequests = async (requestStatus?: RequestTypes): Promise<ResponseApp> => {
  let responseApp: ResponseApp;
  try {
    const { data, status, message } = await ServiceRequestProvider.getServiceRequests(requestStatus);
    CLog(URL_PATH, 'getServiceRequests()', '{ data, status, message }', { data: data, status, message: message });
    if (status === 200) {
      CLog(URL_PATH, 'getServiceRequests()', 'data', data);
      responseApp = { status: true, message: null, data };
      return responseApp;
    } else {
      CLog(URL_PATH, 'getServiceRequests()', '{ data, status }', { data, status });
      responseApp = { status: false, message: data ? data?.message : translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
      return responseApp;
    }
  } catch (e: any) {
    CLog(URL_PATH, 'getServiceRequests()', 'e', e);
    responseApp = { status: false, message: e.message || translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
    return responseApp;
  }
};
