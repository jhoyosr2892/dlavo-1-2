/* eslint-disable camelcase */
// Providers
import AuthProvider from '@services/apis/Auth';

// Helpers
import { CLog } from '@helpers/Message';

// Services
import { ResponseApp } from '@services/app';
import { loadProfileSession } from '@services/app/User';

// Storages
import { setTokenUserSession, removeTokenUserSession } from '@stores/Session';
import { setIdUserSession, removeIdUserSession } from '@stores/UserState';
import { loadUserAddress } from './Address';
import { translate } from 'locales';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { deleteAllAddress } from 'stores/Address';

// Constants
const URL_PATH = 'src/services/app/Auth.ts';

export const onLogin = async (email: string, password: string): Promise<ResponseApp> => {
  let responseApp: ResponseApp;
  try {
    const { data, status, message } = await AuthProvider.login(email, password);
    CLog(URL_PATH, 'onLogin()', '{ data, status }', { data, status });
    if (status === 200) {
      // Guarda el token y id de la session del usuario en el storages y en el
      // state
      await setIdUserSession(data.id);
      await setTokenUserSession(data.token);
      await loadUserAddress();
      // Carga la data del usuario de la session en una variable de estado global.
      // const { status: statusLPS, message } = await loadProfileSession();
      // if (statusLPS) {
      //   responseApp = { status: true, message: message };
      //   return responseApp;
      // } else {
      //   CLog(URL_PATH, 'onLogin()', '{ message, status }', { message, status: statusLPS });
      //   responseApp = { status: true, message: message || 'Error al cargar los datos del usuario' };
      //   return responseApp;
      // }
      responseApp = { status: true, message: message };
      return responseApp;
    } else {
      //CLog(URL_PATH, 'onLogin()', '{ data, status } 2', { data, status });
      responseApp = { status: false, message: message || 'Error al ingresar' };
      return responseApp;
    }
  } catch (e: any) {
    CLog(URL_PATH, 'onLogin()', 'e', e);
    responseApp = { status: false, message: e.message || translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
    return responseApp;
  }
};

export const onLogout = async (): Promise<ResponseApp> => {
  let responseApp: ResponseApp;
  try {
    //CLog(URL_PATH, 'onLogout()', '{ data, status }', { data, status });
    await removeTokenUserSession();
    await removeIdUserSession();
    await AsyncStorage.clear();
    await deleteAllAddress();
    responseApp = { status: true, message: translate('AUTH_SCENE.TOAST.SIGN_OUT_SUCCESSFULLY') };
    return responseApp;
  } catch (e: any) {
    CLog(URL_PATH, 'onLogout()', 'e', e);
    responseApp = { status: false, message: e.message || translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
    return responseApp;
  }
};

export const requestCode = async (email: string): Promise<ResponseApp> => {
  let responseApp: ResponseApp;
  try {
    const { data, status, message } = await AuthProvider.requestCode(email);
    if (status === 200) {
      responseApp = { data, status: true, message: message };
      return responseApp;
    } else {
      responseApp = { status: false, message: message || 'Error al ingresar' };
      return responseApp;
    }
  } catch (e: any) {
    responseApp = { status: false, message: e.message || translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION') };
    return responseApp;
  }
};
