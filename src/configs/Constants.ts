/* Libs */
import { Dimensions, Platform, StatusBar } from 'react-native';

// Assets
import LoadingLottie from '../assets/animations/loading.json';
import LoadingWhiteLottie from '@assets/animations/loadingLight.json';
import NotResultsLottie from '@assets/animations/notResults.json';
import CongratulationLottie from '@assets/animations/congratulation.json';
import CondolenceLottie from 'assets/animations/condolence.json';
import Oval268 from '@assets/default/Oval-268.svg';
import Oval99 from '@assets/default/Oval-99.svg';
import Oval62 from '@assets/default/Oval-62.svg';
import Oval46 from '@assets/default/Oval-46.svg';
import Oval30 from '@assets/default/Oval-30.svg';
import Laundry from '@assets/default/laundry.svg';
import MyServices from '@assets/default/washing-machine.svg';
import Pricing from '@assets/default/pricing.svg';
import Profile from '@assets/default/user.svg';
import SingleService from '@assets/default/DLAVO ICONArtboard 185.svg';
import FrequentService from '@assets/default/DLAVO ICONArtboard 184.svg';
import TShit from '@assets/default/DLAVO ICONArtboard 128.svg';
import Shits from '@assets/default/DLAVO ICONArtboard 126.svg';
import Jackets from '@assets/default/DLAVO ICONArtboard 142.svg';
import Pants from '@assets/default/DLAVO ICONArtboard 137.svg';
import Dress from '@assets/default/DLAVO ICONArtboard 153_1.svg';
import Bag from '@assets/default/bag.svg';
import Avatar from '@assets/default/avatar.svg';
import Star from '@assets/default/star.svg';
import MapPing from '@assets/default/map-ping.svg';
import Clock from '@assets/default/clock.svg';
import Truck from '@assets/default/truck.svg';
import Payment from '@assets/default/payment.svg';
import StarGray from '@assets/default/star-gray.svg';
import Profile2 from '@assets/default/profile-2.svg';
import PaymentCard from '@assets/default/payment-card.svg';
import Membership from '@assets/default/membership.svg';
import Coupons from '@assets/default/coupons.svg';
import Gift from '@assets/default/gift.svg';
import Store from '@assets/default/store.svg';
import Help from '@assets/default/help.svg';
import About from '@assets/default/about.svg';
import Info from '@assets/default/info.svg';
import DlavoLogoOne from '@assets/default/DLAVO ICONArtboard 161_1.svg';
import BlanketBed from '@assets/default/blanket-bed.svg';
import BulkyItems from '@assets/default/DLAVO_Bulky-Items-79.svg';
import Bagsprice from '@assets/default/Bagsprice.svg';
// import Fold from '@assets/default/Fold_ICON-77.svg';
import Fold from '@assets/default/DLAVO ICONArtboard 166.svg';
import DryCleaning from '@assets/default/DLAVO ICONArtboard 170.svg';
/* Clothe icons */
import Pillow from '@assets/default/clothes_icons/Icono_Almohada.svg';
import Scarf from '@assets/default/clothes_icons/Icono_Bufanda.svg';
import Curtain from '@assets/default/clothes_icons/Icono_Cortina.svg';
import Duvet from '@assets/default/clothes_icons/Icono_Duvet.svg';
import Covers from '@assets/default/clothes_icons/Icono_Fundas.svg';
import BedSheets from '@assets/default/clothes_icons/Icono_Sabanas.svg';
import Napkin from '@assets/default/clothes_icons/Icono_Servilleta.svg';
import BathMat from '@assets/default/clothes_icons/Icono_Tapete de baño.svg';
import Towels from '@assets/default/clothes_icons/Icono_Toallas.svg';
/* Json */
import Countries from '@assets/json/countries.json';

/**
 * ******************************
 *
 * APP INFO
 *
 * ******************************
 */
/**
 * Vesion y build de la app
 */
export const VERSION_AND_BUILD = {
  version: '0.0.1',
  codePush: Platform.OS === 'android' ? '1' : '1',
  build: Platform.OS === 'android' ? '1' : '1'
};
// Name App
export const NAME_APP = 'Dlavo';

/**
 * ******************************
 *
 * STORAGE
 *
 * ******************************
 */
// Token que indica el espacio en memoria en el que se guarda el toque del dispotivo
export const DEVICE_TOKEN = 't0k34pPC3cKC4r';

/** Key con la que se busca o set el id del usuario de la session **/
export const KEY_USER_SESSION = 'k3yCk3c4rR';

/** Key con la que se busca o set el id del usuario de la session **/
export const KEY_USER = 'FP9s3TFGgX';

/**
 * GOOGLE API KEY
 */
export const GOOGLE_API_KEY = 'AIzaSyCmhPGFusuT6fgqbhBTbHS3U8A_lFs4fRI';
export const API_GOOGLE = 'https://maps.googleapis.com/maps/api/';

/**
 * @name URL_GOOGLE_APIS_BASE
 * @description la base de todas las apis de google maps
 */
export const URL_GOOGLE_APIS_BASE = `https://maps.googleapis.com/maps/api/`;

/**
 * @name URL_MAPS_STATIC
 * @description es la url usada para traer una imagen de los mapas
 * @see https://developers.google.com/maps/documentation/maps-static/start
 */
export const URL_MAPS_STATIC = `${URL_GOOGLE_APIS_BASE}staticmap?`;

/**
 * @description usado principalmente en lo mapas
 */
export type LatLng = {
  latitude: number;
  longitude: number;
};

/**
 * ******************************
 *
 * SERVER CONFIG
 *
 * ******************************
 */
/**
 * Development mode
 *
 * @description Constante que indica si la app esta en modo desarrollo
 */
export const DEVELOPMENT = true;
/**
 * Url base
 */
export const URL_BASE = 'http://3.131.143.220:3000';
// export const URL_BASE = 'http://192.168.100.139:3000';

/**
 * Url de la api
 *
 * @description Constante que identifica la URL base para el consumo de servicios REST
 */
export const API_URL = DEVELOPMENT ? `${URL_BASE}/api/` : `${URL_BASE}/api/`;

/**
 * ******************************
 *
 * THEME
 *
 * ******************************
 */
/**
 * Constante de opacidad
 */
export const OPACITY = 0.5;

/**
 * Colores
 */
export const THEME_DEFAULT = {
  //primary: DEVELOPMENT ? '#02B0F5' : '#FFB632',
  //primaryDark: DEVELOPMENT ? '#018AC2' : '#E88C23',
  //primaryLight: DEVELOPMENT ? '#4CC6F7' : '#FFDF26',
  primary: '#15224c',
  primaryOpacity: `rgba(21,34,76, ${OPACITY})`,
  primaryDark: '#0a1026',
  primaryLight: '#5d78d2',
  secondary: '#0abfd0',
  secondaryLight: '#73ecf8',
  secondaryDark: '#045f68',
  // general
  content: '#f2f2f2',
  white: '#ffffff',
  black: '#000000',
  colorsGradient: ['#0D42E6', '#582EC7', '#A917A4'],
  colorsGradientDisable: ['#97A2A8', '#97A2A8'], // '#253566', '#2F2647', '#F56CF0'
  backgroundTransparent: `rgba(0,0,0,${OPACITY})`,
  borderLine: '#b0b0b0', // También usado en algunos iconos
  borderLineFocused: '#606060',
  shadow: '#6f6f6f',
  buttonNeutral: '#f4f4f4',
  buttonDisable: '#97A2A8',
  transparent: 'rgba(0, 0, 0, 0)',
  textArea: '#e8e8e8', // Fondo del textArea
  backgroundCard: '#e8e8e8',
  // info
  error: '#E80C22',
  success: '#00A331',
  warn: '#E8BF0C',
  // fonts
  primaryFont: '#ffffff',
  secondaryFont: '#272A31'
};

/**
 * Nombres de iconos que se pueden usar en la app
 */
export const NAME_ICON = {
  /** recargar
   * @library TypeLibraryIconsSystem.ionicons
   * @see WebView.tsx
   */
  reload: 'reload',
  /**
   * @library TypeLibraryIconsSystem.materialIcons
   * Usado por los botones que sobresalen por encima del teclado de ios
   * (subir) **/
  keyboardArrowUp: 'keyboard-arrow-up',
  /** Usado por los botones que sobresalen por encima del teclado de ios
   * (bajar) **/
  keyboardArrowDown: 'keyboard-arrow-down',
  /**
   * En cabezado de los screens para regresar al anterior
   * @library TypeLibraryIconsSystem.ionicons
   */
  arrowBackCircle: 'arrow-back-circle',
  /** Usado para simbolizar el la X **/
  close: 'close',
  /** Usado por el textfield para ver una contraseña **/
  eye: 'eye',
  eyeOff: 'eye-off',
  /**
   * @library TypeLibraryIconsSystem.ionicons
   * @see Drawer.tsx simboliza los cursos
   */
  courses: 'list',
  /**
   * @library TypeLibraryIconsSystem.simpleLineIcons
   * @see Drawer.tsx simboliza el perfil del usuario
   */
  profile: 'user',
  /**
   * @library TypeLibraryIconsSystem.simpleLineIcons
   * @see Drewer.tsx simboliza el acerca de nosotros
   */
  aboutUs: 'people',
  /**
   * @library TypeLibraryIconsSystem.ionicons
   * @see Drawer.tsx simboliza mis certificados
   */
  myCertificates: 'newspaper-outline',
  /**
   * @library TypeLibraryIconsSystem.materialIcons
   * @see Drawer.tsx simboliza los eventos programados por el usuario en su
   * calendario
   */
  eventsCalendar: 'date-range',
  /**
   * @library TypeLibraryIconsSystem.ionicons
   * @see Drawer.tsx simboliza debug
   */
  debug: 'rocket',
  /**
   * @library TypeLibraryIconsSystem.ionicons
   * @see detailCourse
   */
  time: 'time',
  /**
   * @library TypeLibraryIconsSystem.ionicons
   * @see detailCourse
   */
  barChart: 'bar-chart',
  /**
   * Usado en el header de los screens del drawer para mostrar el menu lateral
   * @library TypeLibraryIconsSystem.materialCommunityIcons
   */
  menu: 'microsoft-xbox-controller-menu',
  /**
   * button de refresh usano el el perfil del usuario
   * @library TypeLibraryIconsSystem.materialCommunityIcons
   */
  refresh: 'refresh-circle',
  /**
   * @library TypeLibraryIconsSystem.MaterialIcons
   */
  editImage: 'image-edit-outline',
  /**
   * @library TypeLibraryIconsSystem.materialCommunityIcons
   */
  editProfile: 'account-edit',
  /**
   * icono de cerrar sesion
   * @library TypeLibraryIconsSystem.materialCommunityIcons
   */
  logout: 'logout',
  /**
   * icons de ir usado en eventHistory card
   * @library TypeLibraryIconsSystem.materialIcons
   */
  arrowForwardIos: 'arrow-forward-ios',
  /**
   * @library TypeLibraryIconsSystem.materialCommunityIcons
   * @see Controls controles de video
   */
  rotatePortrait: 'phone-rotate-portrait',
  rotateLandspace: 'phone-rotate-landscape',
  skipNext: 'skip-next',
  skipPrevious: 'skip-previous',
  play: 'play',
  pause: 'pause',
  volumeMute: 'volume-mute',
  volume: 'volume-source',
  fullScreen: 'fullscreen',
  fullScreenExit: 'fullscreen-exit',
  dotsVertical: 'dots-vertical',
  /**
   * Camera
   * @library TypeLibraryIconsSystem.materialCommunityIcons
   */
  flash: 'flash',
  flashOff: 'flash-off',
  flashAuto: 'flash-auto',
  cameraFront: 'camera-front-variant',
  cameraRear: 'camera-rear-variant',
  /** @library TypeLibraryIconsSystem.ionicons **/
  reloadCircle: 'reload-circle',
  checkCircle: 'checkmark-circle',
  closeCircle: 'close-circle'
};

/**
 * Tamaños de iconos de la app
 */
export const SIZE_ICONS = {
  general: 30,
  generalSmallContainer: 26,
  generalSmall: 19,
  buttons: 40,
  inputOption: 23,
  /**
   * @see GraficText
   */
  infoDetail: 50
};

/**
 * familia de fuentes
 */
export const FAMILY_FONTS = {
  primaryRegular: 'SFProDisplay-Regular',
  primaryLight: 'SFProDisplay-Medium',
  primaryBold: 'SFProDisplay-Bold',
  primarySemiBold: 'SFProDisplay-Semibold'
};

/**
 * Tamaños de textos de la app
 */
export const SIZE_FONTS = {
  primary: 20,
  secondary: 15,
  tertiary: 30
};

/**
 * @enum EnumFont
 * typos de Fuentes
 * @description
 * primary - stylesGeneral.textLightBigColorSecondary
 * secondary - stylesGeneral.textLightSmallColorPrimary;
 * tertiary - stylesGeneral.textLightBigColorPrimary;
 * quaternary - stylesGeneral.textLightSmallColorSecondary;
 * quinary - stylesGeneral.textRegularSmallColorSecondary;
 * sixth - stylesGeneral.textRegularSmallColorPrimary;
 * septenary - stylesGeneral.textRegularBigColorSecondary;
 * octonary - stylesGeneral.textRegularBigColorPrimary
 * default - stylesGeneral.textLightBigColorSecondary;
 * nonary - stylesGeneral.textRegularExtraBigColorPrimary;
 * tenth - stylesGeneral.textRegularExtraBigColorSecondary;
 * eleventh - stylesGeneral.textSemiBoldBigColorPrimary
 * twelfth - stylesGeneral.textSemiBoldBigColorSecondary;
 * thirteenth - stylesGeneral.textSemiBoldSmallColorPrimary;
 * fourteenth - stylesGeneral.textSemiBoldSmallColorSecondary;
 * fifteenth - stylesGeneral.textSemiBoldExtraBigColorPrimary;
 * sixteenth - stylesGeneral.textSemiBoldExtraBigColorSecondary;
 * seventeenth - stylesGeneral.textBoldBigColorPrimary;
 * eighteenth - stylesGeneral.textBoldBigColorSecondary;
 * nineteenth - styles.textBoldSmallColorPrimary;
 * twentieth - styles.textBoldSmallColorSecondary;
 * twentyOne - styles.textBoldExtraBigColorPrimary;
 * twentyTwo - styles.textBoldExtraBigColorSecondary;
 * */
export enum EnumFont {
  primary = 1, // stylesGeneral.textLightBigColorSecondary
  secondary = 2, // stylesGeneral.textLightSmallColorPrimary;
  tertiary = 3, // stylesGeneral.textLightBigColorPrimary;
  quaternary = 4, // stylesGeneral.textLightSmallColorSecondary;
  quinary = 5, // stylesGeneral.textRegularSmallColorSecondary;
  sixth = 6, // stylesGeneral.textRegularSmallColorPrimary;
  septenary = 7, // stylesGeneral.textRegularBigColorSecondary;
  octonary = 8, // stylesGeneral.textRegularBigColorPrimary;
  nonary = 9, // styles.textRegularExtraBigColorPrimary;
  tenth = 10, // styles.textRegularExtraBigColorSecondary;
  eleventh = 11, // stylesGeneral.textSemiBoldBigColorPrimary
  twelfth = 12, // stylesGeneral.textSemiBoldBigColorSecondary;
  thirteenth = 13, // stylesGeneral.textSemiBoldSmallColorPrimary;
  fourteenth = 14, // stylesGeneral.textSemiBoldSmallColorSecondary;
  fifteenth = 15, // stylesGeneral.textSemiBoldExtraBigColorPrimary;
  sixteenth = 16, // stylesGeneral.textSemiBoldExtraBigColorSecondary;
  seventeenth = 17, // stylesGeneral.textBoldBigColorPrimary;
  eighteenth = 18, // stylesGeneral.textBoldBigColorSecondary;
  nineteenth = 19, // styles.textBoldSmallColorPrimary;
  twentieth = 20, // styles.textBoldSmallColorSecondary;
  twentyOne = 21, // styles.textBoldExtraBigColorPrimary;
  twentyTwo = 22 // styles.textBoldExtraBigColorSecondary;
}

/**
 * Tipos de timer
 */
export const TIMER_ANIMATION = {
  toast: 6000,
  animatesGeneral: 500,
  animatesAlert: 200
};

/**
 * @const TIMER_OUT
 * @description Tiempo de los timeout de la app
 */
export const TIMER_OUT = {
  carousel: 500,
  controlMultimedia: 3000
};

/**
 * Constantes para las Métricas
 */
export const IS_ANDROID: boolean = Platform.OS === 'android';
export const { height, width } = Dimensions.get('window');
export const METRICS = {
  ANDROID_STATUSBAR: 24,
  DEVICE_HEIGHT: height,
  DEVICE_WIDTH: width,
  DEVICE_WIDTH_SCREEN: Dimensions.get('screen').width,
  DEVICE_HEIGHT_SCREEN: Dimensions.get('screen').height,
  BAR_INFO_HEIGHT: StatusBar.currentHeight,
  KEYBOARD_VERTICAL_OFFSET: IS_ANDROID ? 0 : 60, // el height del InputAccessoryView.
  DOT_CAROUSEL: 10,
  HEIGHT_HEADER_BANNER: height / 4,
  /** stroke line svg history **/
  STROKE_WIDTH: 2,
  /** radio circunferencia svg history **/
  R_CIRCLE: 6,
  // padding
  PADDING_INSIDE: 5,
  PADDING_RESOURCE_CARD: 30,
  CONSTANTS_PADDING_LIST: 5,
  CONSTANTS_PADDING: 15,
  CONSTANTS_PADDING_BUTTON: 5,
  CONSTANTS_PADDING_BUTTON_ICON: 3,
  PADDING_MODAL_ALERT: 10,
  PADDING_ALERT_SHEET: 5,
  // margins
  MARGIN_VIEW_IMAGE: 50,
  CONSTANTS_MARGIN: 20,
  CONSTANTS_MARGIN_INSIDE: 5, // dentro de los componentes de un View
  CONSTANTS_MARGIN_LIST: 5,
  CONSTANTS_MARGIN_INSIDE_MODAL_ALERT_LEFT: 10,
  MARGIN_ALERT_SHEET: 5,
  // border
  BORDER_FRAMEWORK: 3,
  BORDER_CARD: 2,
  BORDER_RADIUS: 5,
  BORDER_RADIUS_ALERT_SHEET: 20,
  // Elevations
  ELEVATION: 3
};

/**
 */
export const COMPONENT_DIMENSIONS = {
  itemCarousel: {
    category: { width: METRICS.DEVICE_WIDTH - METRICS.CONSTANTS_MARGIN * 2, height: METRICS.DEVICE_HEIGHT / 8 },
    course: { width: METRICS.DEVICE_WIDTH - METRICS.CONSTANTS_MARGIN * 2 },
    module: { width: METRICS.DEVICE_WIDTH - METRICS.CONSTANTS_MARGIN * 2, height: METRICS.DEVICE_HEIGHT / 8 }
  },
  itemModal: {
    animation: { height: METRICS.DEVICE_HEIGHT / 4 }
  }
};

/**
 * Assets usados en la app
 * @const ASSETS
 */
export const ASSETS = {
  // Animations
  spinnerLoading: LoadingLottie,
  spinnerLoadingWhite: LoadingWhiteLottie,
  notResults: NotResultsLottie,
  congratulationAnimation: CongratulationLottie,
  condolenceAnimation: CondolenceLottie,
  // Logos
  logo: require('@assets/logo/logo.png'),
  monogramSvg: require('@assets/logo/logo.png'),
  monogramPng: require('@assets/logo/logo.png'),
  /* Default */
  userProfileSecondary: require('@assets/default/UserProfileSecondary.png'),
  oval268: Oval268,
  oval99: Oval99,
  oval62: Oval62,
  oval46: Oval46,
  oval30: Oval30,
  laundry: Laundry,
  myServices: MyServices,
  pricing: Pricing,
  profile: Profile,
  wash: require('@assets/default/wash.png'),
  washFold: require('@assets/default/wash-fold.png'),
  washFoldPin: require('@assets/default/wash-fold-pin.png'),
  noImage: require('@assets/default/no-image.png'),
  singleService: SingleService,
  frequentService: FrequentService,
  tShit: TShit,
  shits: Shits,
  jackets: Jackets,
  pants: Pants,
  dress: Dress,
  bag: Bag,
  avatar: Avatar,
  star: Star,
  mapPing: MapPing,
  clock: Clock,
  truck: Truck,
  payment: Payment,
  starGray: StarGray,
  profile2: Profile2,
  paymentCard: PaymentCard,
  membership: Membership,
  coupons: Coupons,
  gift: Gift,
  store: Store,
  help: Help,
  about: About,
  info: Info,
  dlavoLogoOne: DlavoLogoOne,
  blanketBed: BlanketBed,
  bulkyItems: BulkyItems,
  fold: Fold,
  dryCleaning: DryCleaning,
  bagsprice: Bagsprice,
  /* Clothe items */
  pillow: Pillow,
  scarf: Scarf,
  curtain: Curtain,
  duvet: Duvet,
  covers: Covers,
  bedSheets: BedSheets,
  napkin: Napkin,
  bathMat: BathMat,
  towels: Towels,
  /* Json */
  countries: Countries
};

/**
 * Estilos de los botones del alert
 */
export enum StyleButtonAlert {
  cancel,
  default
}

/**
 * Color del spinner lottie
 */
export enum EnumSpinner {
  white = 'white',
  primary = 'primary'
}

/**
 * Animated
 */
export enum AnimatedString {
  fadeIn = 'fadeIn',
  fadeOut = 'fadeOut',
  slideInUp = 'slideInUp',
  slideOutDown = 'slideOutDown',
  slideInRigth = 'slideInRight',
  slideInLeft = 'slideInLeft',
  zoomIn = 'zoomIn',
  zoomOut = 'zoomOut'
}

/**
 * *************
 * ANIMATION
 * *************
 */

/**
 * @interface AnimationObject
 * @description Serialized animation as generated from After Effects
 * @see LottieView
 */
export interface AnimationObject {
  v: string;
  fr: number;
  ip: number;
  op: number;
  w: number;
  h: number;
  nm: string;
  ddd: number;
  assets: any[];
  layers: any[];
}

/**
 * @type Range
 * @description es usado para calcular el progreso de la barra de un curso
 */
export type RangeProgress = {
  min?: number;
  max?: number;
};

/**
 * @type DimensionsComponent
 * @description dimensiones del componentes
 */
export type DimensionsComponentType = {
  height: number;
  width: number;
};

/**
 * @enum EnumFormatDateTime
 * @description formato de entrada y salida de fecha y hora en la APP
 */
export enum EnumFormatDateTime {
  DMMMMYYYY = 'D MMMM YYYY',
  DDDDDMMMMYYYY = 'dddd[,] D MMM YYYY',
  hma = 'h:m a',
  hhmma = 'hh:mm a',
  YYYYMMDDHHmm = 'YYYY-MM-DD HH:mm'
}

/**
 * id del accesorio del teclado (IOS)
 */
export const INPUT_ACCESSORY_ID = {
  close: 'close',
  in: 'in',
  inDown: 'inDown',
  down: 'down'
};

/**
 * @const NUMBER_LINE
 * @description numero de lineas maximo de un texto
 */
export const NUMBER_LINE = {
  descriptionResource: 5,
  general: 3,
  textArea: 6
};

/**
 * @description numero de caracteres maximo de un textArea o textField
 */
export const CHARACTER_LENGTH = {
  textArea: 250
};

/**
 * @enum EnumToast
 * @description Para establecer que tipo de toas es si es de error, éxito o
 * advertencia.
 */
export enum EnumToast {
  error,
  success,
  warn
}

/**
 * @enum ResourceTypeExtension
 * @description tipos de recusos usados con su repectiva extencion
 */
export enum ResourceTypeExtension {
  // Document
  pdf = '.pdf',
  word = '.docx',
  exel = '.xlsx',
  powerPoint = '.ppt',
  // Video
  mp4 = '.mp4',
  mkv = '.mkv',
  // mp3
  mp3 = '.mp3'
}

/**
 * Nombre de las rutas de la app
 */
export enum NAME_ROUTE {
  // Drawer
  drawer = 'Drawer',
  /* Tabs */
  tabs = 'Tabs',
  laundryTab = 'LaundryTab',
  myServicesTab = 'MyServicesTab',
  pricingTab = 'PricingTab',
  profileTab = 'ProfileTab',
  // Screen
  home = 'Home',
  /************** Modulo Auth **************/
  auth = 'Auth',
  login = 'Login',
  registerEmail = 'RegisterEmail',
  enterCode = 'EnterCode',
  enterName = 'EnterName',
  /************** Modulo Auth Partner **************/
  partnerRegisterEmail = 'PartnerRegisterEmail',
  partnerEnterCode = 'PartnerEnterCode',
  partnerEnterInfo = 'PartnerEnterInfo',
  /************** Laundry **************/
  laundry = 'Laundry',
  selectFrequency = 'SelectFrequency',
  selectGarments = 'SelectGarments',
  serviceOrCaresSelect = 'ServiceOrCaresSelect',
  bag = 'Bag',
  selectBagQuantity = 'SelectBagQuantity',
  selectSchedule = 'SelectSchedule',
  additionalNotes = 'AdditionalNotes',
  /************** Address **************/
  address = 'Address',
  enterAddress = 'EnterAddress',
  /************** MyServices **************/
  myServices = 'MyServices',
  myServiceDetail = 'MyServiceDetail',
  /************** Pricing **************/
  pricing = 'Pricing',
  pricingDetail = 'PricingDetail',
  /************** Profile **************/
  profile = 'Profile',
  profileDetail = 'ProfileDetail',
  payment = 'Payment',
  membership = 'Membership',
  coupons = 'Coupons',
  shareAndWin = 'ShareAndWin',
  joinAsDlavoPartner = 'JoinAsDlavoPartner',
  help = 'Help',
  about = 'About',
  privacy = 'Privacy',
  faq = 'Faq',
  /************** Payment **************/
  addPaymentMethod = 'AddPaymentMethod'
}

// Options GPS
export const optionsGPS = {
  enableHighAccuracy: true,
  timeout: 15000,
  maximumAge: 10000
};

