
/**
 * @enum TypeLogSystem
 * @description Typo de console del sistema
 */
export enum TypeLogSystem {
  error = 1,
  warning = 2,
  success = 3,
  develop = 4
}

/**
 * Typo de librería de iconos de la app
 * @enum {number} TypeLibraryIconsSystem
 * @readonly
 */
export enum TypeLibraryIconsSystem {
  ionicons,
  fontAwesome,
  materialIcons,
  materialCommunityIcons,
  simpleLineIcons,
  foundation,
  feather
}

/**
 * Text content type
 */
export enum TypeTextContent {
  none = 'none',
  name = 'name',
  password = 'password',
  telephoneNumber = 'telephoneNumber',
  newPassword = 'newPassword',
  username = 'username',
  emailAddress = 'emailAddress'
}

/**
 * Animated
 */
export enum AnimatedString {
  fadeIn = 'fadeIn',
  fadeOut = 'fadeOut',
  slideInUp = 'slideInUp',
  slideOutDown = 'slideOutDown',
  slideInRigth = 'slideInRight',
  slideInLeft = 'slideInLeft',
  zoomIn = 'zoomIn',
  zoomOut = 'zoomOut'
}



/**
 * ******************************
 * 
 * BUTTONS
 * 
 * ******************************
 */
/**
 * Typos de botones
 */
export enum EnumTypeButton {
  normal,
  neutral
}
/**
 * Estilos de los botones del alert
 */
export enum StyleButtonAlert {
  cancel,
  default
}

/** Tipos de recursos a visualizar **/
export enum Resource {
  video = 'video',
  image = 'image',
  audio = 'audio',
  pdf = 'pdf',
  documentSupport = 'document',
  webSite = 'webSite',
  meeting = 'meeting',
  exam = 'exam'
}




/**
 * @enum EnumFormatDateTime
 * @description formato de entrada y salida de fecha y hora en la APP
 */
export enum EnumFormatDateTime {
  DMMMMYYYY = 'D MMMM YYYY',
  DDDDDMMMMYYYY = 'dddd[,] D MMM YYYY',
  hma = 'h:m a',
  hhmma = 'hh:mm a',
  YYYYMMDDHHmm = 'YYYY-MM-DD HH:mm'
}

/** Estado del recurso **/
export enum StatusResource {
  /** finalizado **/
  finalized = 1,
  /** sin comenzar **/
  withoutStarting = 3,
  /** en proceso **/
  inProcess = 2
}

/** unidades de tiempo **/
export enum TimeUnits {
  days = 'days',
  minutes = 'minutes',
  hours = 'hours'
}

export enum BadgeEnum {
  new = 'new'
}

export enum StatusCamera {
  play = 0,
  pause = 1
}

export enum MaskCamera {
  portrait
}

/** status de respuesta de los sericios de google **/
export enum StatusCodeGoogle {
  /** indicates that no errors occurred and at least one result was returned. **/
  ok = 'OK',
  /** indicates that the search was successful but returned no results. This may occur if the
   * search was passed a bounds in a remote location. **/
  zeroResult = 'ZERO_RESULTS',
  /** indicates that you are over your quota. **/
  overQueryLimit = 'OVER_QUERY_LIMIT',
  /** indicates that your request was denied, generally because the key parameter is missing or
   * invalid. **/
  requestDenied = 'REQUEST_DENIED',
  /** generally indicates that the input parameter is missing. **/
  invalidRequest = 'INVALID_REQUEST',
  /** indicates a server-side error; trying again may be successful. **/
  unknownError = 'UNKNOWN_ERROR',
  /** indicates that the referenced location (place_id) was not found in the Places database. **/
  notFound = 'NOT_FOUND'
}