// Libs
import { ImageURISource } from 'react-native';

// Enums
import { Resource } from '@config/Enum';


/** Source que se usa en los videos **/
export type VideoSource = { uri?: string; headers?: { [key: string]: string }; type: 'mpd' | 'm3u8' | 'ism' } | number;

/** Source que se usa en las Imagenes **/
export type ImageSource = string | null | ImageURISource | number;

/** Source que se usa en los documentos **/
export type DocumentSource = string;

/** Source que se usa en los sitios web **/
export type WebSiteSource = string;

/** Source de los recursos **/
export type ResourceSource = VideoSource | ImageSource | DocumentSource | WebSiteSource | undefined;

/** Tipos de recursos usados (en el Viewer) **/
export type ResourceViewer = {
  /** Recurso, url, requires, etc... **/
  source: ResourceSource;
  /** nombre que se muestra en la parte superior **/
  name?: string;
  /** cuando no se describe la el tipos en la extension de la url **/
  type?: Resource;
};

/** extrae los valores de algún typo **/
export type valueof<T> = T[keyof T];
