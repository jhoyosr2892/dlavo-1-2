// Libs
import { createContext, ReactNode } from 'react';
import { StyleProp, TextStyle } from 'react-native';

// Atoms
import { AlertOptions } from '@atoms/modals/AlertCustom';
import { OptionsAlertSheet } from '@atoms/modals/AlertSheet';

// Constants
import { EnumToast } from '@constantsApp';

// Props Modal
export interface AlertModalOptions {
  title?: string;
  message?: string | ReactNode;
  styleMessage?: StyleProp<TextStyle>;
  options?: Array<AlertOptions>;
}

// Props AlertSheet
export interface AlertSheetModalOptions {
  infoNote?: string;
  errorNote?: string;
  successIcon?: boolean;
  title?: string;
  message?: string;
  options?: Array<OptionsAlertSheet>;
  cancel?: boolean;
}

// Option Toast
export interface ToastOption {
  text?: string | null;
  textButton?: string | null;
  type?: EnumToast;
  duration?: number;
  onPress?: () => void;
}

interface AuthContextState {
  authFunction: {
    signIn: (email: string, password: string) => void;
    signOut: () => void;
  };
  authState: {};
}

interface RootContext {
  showToast: (option: ToastOption) => void;
  showLoading: () => void;
  hideLoading: () => void;
  showAlert: (option: AlertModalOptions) => void;
  showAlertSheet: (option: AlertSheetModalOptions) => void;
  removeAlertSheet: () => void;
  isShowAlertSheet: boolean;
}

export const AuthContext = createContext({} as AuthContextState);
export const RootContext = createContext({} as RootContext);
