import moment from 'moment';
import 'moment/min/locales.min';

moment.locale('es');

export default moment;
