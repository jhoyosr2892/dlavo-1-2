// Helpers
import { CLog } from '@helpers/Message';

// Storages
import { openRealm } from '@stores/index';

// Model
import Address, { AddressModel } from '@models/address';

// Constants
const URL_PATH = 'src/stores/Address.ts';


/**
 * @function addAddress
 * @description Es esta función permite agregar un nuevo elemento a la base
 * de datos.
 * @param {AddressModel} (AddressModel)['../../models/address']
 * @return {undefined | Address}
 */
export const addAddress = async (newElement: AddressModel): Promise<undefined | Address> => {
  let newAddress: undefined | AddressModel;
  const realm = await openRealm({});

  if (realm) {
    try {
      realm.write(() => {
        const addressResult = (realm.create(Address.schema.name, newElement) as unknown) as AddressModel;
        CLog(URL_PATH, 'addAddress()', 'addressResult', addressResult);
        newAddress = new Address(addressResult);
        CLog(URL_PATH, 'addAddress()', 'newAddress', newAddress);
      });
    } catch (err) {
      CLog(URL_PATH, 'addAddress()', 'err', err);
      realm.close();
    }
  }

  CLog(URL_PATH, 'addAddress()', 'newAddress 2', newAddress);
  return newAddress;
};


/**
 * @function getAddressById
 * @description retorna el objecto si es encontrado.
 * @param {id} el id de la dirección
 * @return {undefined | Address}
 */
export const getAddressById = async (id: number): Promise<undefined | Address> => {
  let address: undefined | AddressModel[];
  let addressFound: undefined | Address;
  const realm = await openRealm({});

  if (realm) {
    try {
      address = (realm
        .objects(Address.schema.name)
        .filtered(`id = "${id}"`)
        .slice(0, 1) as unknown) as AddressModel[];

      if (address.length) {
        addressFound = new Address(address[0]);
      }
    } catch (err) {
      CLog(URL_PATH, 'getAddressById()', 'err', err);
      realm.close();
    }
  }

  return addressFound;
};


/**
 * @function getAddressSelected
 * @description retorna el objecto si es encontrado.
 * @return {undefined | Address}
 */
export const getAddressSelected = async (): Promise<undefined | Address> => {
  let address: undefined | AddressModel[];
  let addressFound: undefined | Address;
  const realm = await openRealm({});
  if (realm) {
    try {
      address = (realm
        .objects(Address.schema.name)
        .filtered(`selected = true`)
        .slice(0, 1) as unknown) as AddressModel[];

      if (address.length) {
        addressFound = new Address(address[0]);
      } else {
        realm.close();
      }
    } catch (err) {
      CLog(URL_PATH, 'getAddressSelected()', 'err', err);
      realm.close();
    }
  }

  return addressFound;
};

/**
 * @function getAddress
 * @description retorna las direcciones.
 * @return {undefined | Address}
 */
export const getAddress = async (): Promise<Address[]> => {
  let address: undefined | AddressModel[];
  let addressFound: Address[] = [];
  const realm = await openRealm({});

  if (realm) {
    try {
      address = (realm.objects(Address.schema.name) as unknown) as AddressModel[];
      address.forEach(a => addressFound.push(new Address(a)));
    } catch (err) {
      CLog(URL_PATH, 'getAddress()', 'err', err);
      realm.close();
    }
  }

  return addressFound;
};


/**
 * @function getLastAddress
 * @description obtener la ultima dirección
 * @return {undefined | Address}
 */
export const getLastAddress = async (): Promise<undefined | Address> => {
  let addressLast: undefined | Address;
  const realm = await openRealm({});

  if (realm) {
    try {
      const addressFound = (realm.objects(Address.schema.name) as unknown) as AddressModel[];
      if (addressFound.length) {
        const address = addressFound.pop();
        if (address) {
          addressLast = new Address(address);
        }
      }
    } catch (err) {
      CLog(URL_PATH, 'getLastAddress()', 'err', err);
      realm.close();
    }
  }
  return addressLast;
};


/**
 * @function deleteAddressById
 * @description actualiza el objeto buscado o creado.
 * @param {AddressModel} newAddress recurso a actualizar
 * @return {boolean} retorna true si se actualizó con éxito el recurso
 */
export const updateAddress = async (newAddress: AddressModel): Promise<boolean> => {
  let isUpdate = false;
  const realm = await openRealm({});

  if (realm) {
    try {
      realm.write(() => {
        let address = realm.objects(Address.schema.name).filtered(`id = "${newAddress.id}"`)[0] as AddressModel;
        if (address) {
          address = newAddress;
          isUpdate = true;
        }
      });
    } catch (err) {
      CLog(URL_PATH, 'deleteAddressById()', 'err', err);
      realm.close();
    }
  }
  return isUpdate;
};


/**
 * @function selectAddress
 * @description actualiza el objeto buscado o creado.
 * @param {id} id del recurso a seleccionar
 * @return {boolean} retorna true si se actualizó con éxito el recurso
 */
export const selectAddress = async (id?: string | number[]): Promise<boolean> => {
  let isUpdate = false;
  const realm = await openRealm({});

  if (realm) {
    try {
      realm.write(() => {
        let address = realm.objects(Address.schema.name).filtered(`id = "${id}"`)[0] as AddressModel;
        if (address) {
          address.selected = true;
          isUpdate = true;
        }
        const addressFound = (realm.objects(Address.schema.name).filtered(`id != "${id}"`) as unknown) as AddressModel[];
        addressFound.forEach(address => address.selected = false);
      });
    } catch (err) {
      CLog(URL_PATH, 'deleteAddressById()', 'err', err);
      realm.close();
    }
  }
  return isUpdate;
};

/**
 * @function deleteAddressById
 * @description borrar el objeto buscado o creado.
 * @param {id} id del recurso a borrar
 * @return {boolean} retorna true si se borro con éxito el recurso
 */
export const deleteAddressById = async (id?: string | number[]): Promise<boolean> => {
  let isDelete = false;
  const realm = await openRealm({});

  if (realm) {
    try {
      realm.write(() => {
        const address = realm.objects(Address.schema.name).filtered(`id = "${id}"`).slice(0, 1);
        if (address.length) {
          realm.delete(address);
          isDelete = true;
        }
      });
    } catch (err) {
      CLog(URL_PATH, 'deleteAddressById()', 'err', err);
      realm.close();
    }
  }
  return isDelete;
};


/**
 * @function deleteAllAddress
 * @description Borrar todos los elementos address
 * @return {boolean}
 */
export const deleteAllAddress = async (): Promise<boolean> => {
  let isDelete = false;
  const realm = await openRealm({});

  if (realm) {
    try {
      realm.write(() => {
        const addressFound = realm.objects(Address.schema.name);
        realm.delete(addressFound);
      });
      isDelete = true;
    } catch (err) {
      CLog(URL_PATH, 'deleteAllAddress()', 'err', err);
      realm.close();
    }
  }
  return isDelete;
};
