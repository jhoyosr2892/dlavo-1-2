// Libs
import AsyncStorage from '@react-native-async-storage/async-storage';
import { makeAutoObservable } from 'mobx';

// Helpers
import { CLog } from '@helpers/Message';

// Constants
import { DEVICE_TOKEN, KEY_USER_SESSION } from '@constantsApp';

const URL_PATH = 'src/stores/session.ts';
const TOKEN: string | null = null;

/**
 * @class DeviceToken
 * @decription clase que adminstra el token de session del usuario.
 */
class DeviceToken {
  static token: string | null = TOKEN;

  constructor() {
    makeAutoObservable(this);
  }

  static update(value: string) {
    this.token = value;
  }

  static destroy() {
    this.token = null;
  }
}

export default DeviceToken;

// Guarda el token del usuario de la session.
export const setTokenUserSession = async (value: string) => {
  try {
    // CLog(URL_PATH, 'setTokenUserSession()', 'value', value);
    await AsyncStorage.setItem(DEVICE_TOKEN, value);
    DeviceToken.update(value);
    // CLog(URL_PATH, 'setTokenUserSession()', 'DeviceToken.token', DeviceToken.token);
  } catch (e) {
    CLog(URL_PATH, 'setTokenUserSession()', 'e', e);
  }
};

// Obtiene el token del usuario de la session.
export const getTokenUserSession = async () => {
  try {
    const value = await AsyncStorage.getItem(DEVICE_TOKEN);
    if (value !== null) {
      DeviceToken.update(value);
    }
  } catch (e) {
    CLog(URL_PATH, 'getTokenUserSession()', 'e', e);
  }
};

// Remover el token del usuario de la session.
export const removeTokenUserSession = async () => {
  try {
    await AsyncStorage.removeItem(DEVICE_TOKEN);
    DeviceToken.destroy();
  } catch (e) {
    CLog(URL_PATH, 'removeTokenUserSession()', 'e', e);
  }
  //CLog(URL_PATH, 'removeTokenUserSession()', 'Done', 'se elimino el token del AsyncStorage');
};
