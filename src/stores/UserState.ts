// Libs
import AsyncStorage from '@react-native-async-storage/async-storage';
import { makeAutoObservable } from 'mobx';

// Helpers
import { CLog } from '@helpers/Message';

// Model
import UserData, { UserModel, UserResponseServer } from '@models/user/User';

// Constants
import { KEY_USER, KEY_USER_SESSION } from '@constantsApp';

const URL_PATH = 'src/stores/session.ts';
class UserStorage extends UserData {
  static data: UserModel;

  constructor(data: UserModel) {
    super(data);
    this.data = super.data;
    makeAutoObservable(this);
  }

  static update(data: UserResponseServer) {
    this.data = UserData.formatData(data);
  }
}
// Guarda el usuario de la session.
export const setUserData = async (value: UserResponseServer) => {
  try {
    // CLog(URL_PATH, 'setIdUserSession()', 'value', value);
    await AsyncStorage.setItem(KEY_USER, `${value}`);
    UserStorage.update(value);
  } catch (e) {
    CLog(URL_PATH, 'setUser()', 'e', e);
  }
};

/**
 * @description es usado cuando al hacer el login el servidor retorna el id
 * se usuario que luego se usa para perdir al servidor cada que abre la app
 * la informacion del usuario.
 */
export class UserIdSession {
  static id: number | null = null;

  constructor() {
    makeAutoObservable(this);
  }

  static update(value: number) {
    this.id = value;
  }

  static destroy() {
    this.id = null;
  }
}

// Guarda el id del usuario de la session.
export const setIdUserSession = async (value: number) => {
  try {
    // CLog(URL_PATH, 'setIdUserSession()', 'value', value);
    await AsyncStorage.setItem(KEY_USER_SESSION, `${value}`);
    UserIdSession.update(value);
    // CLog(URL_PATH, 'setIdUserSession()', 'UserIdSession.token', UserIdSession.token);
  } catch (e) {
    CLog(URL_PATH, 'setIdUserSession()', 'e', e);
  }
};

// Obtiene el id del usuario de la session.
export const getIdUserSession = async () => {
  try {
    const value = await AsyncStorage.getItem(KEY_USER_SESSION);
    if (value !== null) {
      UserIdSession.update(parseInt(value, 10));
    }
  } catch (e) {
    CLog(URL_PATH, 'getIdUserSession()', 'e', e);
  }
};

// Remover el id del usuario de la session.
export const removeIdUserSession = async () => {
  try {
    await AsyncStorage.removeItem(KEY_USER_SESSION);
    UserIdSession.destroy();
  } catch (e) {
    CLog(URL_PATH, 'removeIdUserSession()', 'e', e);
  }
  //CLog(URL_PATH, 'removeIdUserSession()', 'Done', 'se elimino el token del AsyncStorage');
};

export default UserStorage;
