import uuid from 'react-native-uuid';
import { getAddressSelected } from './Address';
// Helpers
import { CLog } from '@helpers/Message';

// Models, Class
import Service, { Frequency, ServiceModel, ServiceRequestModel } from '@models/service';
import AsyncStorage from '@react-native-async-storage/async-storage';

// Storages
import { openRealm } from '@stores/index';
import { navigationScreen } from 'utils/helpers/Navigation';
import { NAME_ROUTE } from 'configs/Constants';

// Constants
const URL_PATH = 'src/stores/schemas/BagState';

/**
 * @function addService
 * @description Es esta función permite agregar un nuevo elemento a la base
 * de datos.
 * @param {ServiceModel} (ServiceModel)['../../models/service']
 * @return {undefined | Bag}
 */
export const addService = async (newElement: ServiceModel): Promise<undefined | Service> => {
  CLog(URL_PATH, 'addService()', 'newElement', newElement);
  let newService: undefined | ServiceModel;
  const realm = await openRealm({ schema: [Service.schema] });

  if (realm) {
    try {
      realm.write(() => {
        const serviceResult = (realm.create(Service.schema.name, {
          ...newElement,
          date: new Date()
        }) as unknown) as ServiceModel;
        //CLog(URL_PATH, 'addService()', 'serviceResult', serviceResult);
        newService = new Service(serviceResult);
        //CLog(URL_PATH, 'addService()', 'newService', newService);
      });
    } catch (err) {
      CLog(URL_PATH, 'addService()', 'err', err);
    }
  }

  //CLog(URL_PATH, 'addService()', 'newService 2', newService);
  return newService;
};

/**
 * @function getServiceById
 * @description retorna el objecto si es encontrado.
 * @param {id} el id del servicio
 * @return {undefined | Service}
 */
export const getServiceById = async (id: number): Promise<undefined | Service> => {
  let service: undefined | ServiceModel[];
  let serviceFound: undefined | Service;
  const realm = await openRealm({ schema: [Service.schema] });

  if (realm) {
    try {
      service = (realm
        .objects(Service.schema.name)
        .filtered(`id = "${id}"`)
        .sorted('date')
        .slice(0, 1) as unknown) as ServiceModel[];

      if (service.length) {
        serviceFound = new Service(service[0]);
        // CLog(URL_PATH, 'getserviceById()', 'serviceFound.date', serviceFound.date);
      }
    } catch (err) {
      CLog(URL_PATH, 'getService()', 'err', err);
    }
  }

  return serviceFound;
};

/**
 * @function deleteServiceById
 * @description borrar el objeto buscado o creado.
 * @param {id} id del recurso a borrar
 * @return {boolean} retorna true si se borro con éxito el recurso
 */
export const deleteServiceById = async (id: number): Promise<boolean> => {
  let isDelete = false;
  const realm = await openRealm({ schema: [Service.schema] });

  if (realm) {
    try {
      realm.write(() => {
        const service = realm.objects(Service.schema.name).filtered('id = "$0"', id).sorted('date', true).slice(0, 1);
        realm.delete(service);
        isDelete = true;
      });
    } catch (err) {
      CLog(URL_PATH, 'deleteServiceById()', 'err', err);
    }
  }
  return isDelete;
};

/**
 * @function getLastService
 * @description obtener el ultimo service
 * @return {undefined | Service}
 */
export const getLastService = async (): Promise<undefined | Service> => {
  let serviceLast: undefined | Service;
  const realm = await openRealm({ schema: [Service.schema] });

  if (realm) {
    try {
      const serviceFound = (realm.objects(Service.schema.name).sorted('date', true) as unknown) as ServiceModel[];
      // console.warn(serviceFound)
      //CLog(URL_PATH, 'getLastService()', 'serviceFound', serviceFound);
      if (serviceFound.length) {
        serviceLast = new Service(serviceFound[0]);
      }
    } catch (err) {
      CLog(URL_PATH, 'getLastService()', 'err', err);
    }
  }
  //CLog(URL_PATH, 'getLastService()', 'serviceLast', serviceLast);
  return serviceLast;
};

/**
 * @function deleteAllService
 * @description Borrar todos los elementos service
 * @return {boolean}
 */
export const deleteAllService = async (): Promise<boolean> => {
  let isDelete = false;
  const realm = await openRealm({ schema: [Service.schema] });

  if (realm) {
    try {
      realm.write(() => {
        const serviceFound = realm.objects(Service.schema.name);
        realm.delete(serviceFound);
      });
      isDelete = true;
    } catch (err) {
      CLog(URL_PATH, 'deleteAllService()', 'err', err);
    }
  }
  return isDelete;
};


/* FUNCIONES TEMPORALES */

export const updateBagStorage = async (service: ServiceModel) => {
  let bagStorage = await AsyncStorage.getItem('bag');
  if (bagStorage) {
    const bagClone: ServiceRequestModel = JSON.parse(bagStorage);
    const serviceIndex = bagClone.services.findIndex((b: ServiceModel) => b.serviceCategory === service?.serviceCategory);
    if (serviceIndex !== -1) {
      bagClone.services[serviceIndex] = service;
    } else {
      bagClone.services.push(service);
    }
    if (bagClone) {
      bagClone.services.forEach(s => {
        s.concurrence = service.concurrence;
        s.pickUpType = service.pickUpType;
        s.pickUpSingleTime = service.pickUpSingleTime;
        s.deliverySingleTime = service.deliverySingleTime;
      });
    }
    await AsyncStorage.setItem('bag', JSON.stringify(bagClone));
  } else {
    const address = await getAddressSelected();
    if (address && address.id) {
      const newBag: ServiceRequestModel = {
        id: uuid.v4().toString(),
        address: address.id.toString(),
        services: [service]
      };
      await AsyncStorage.setItem('bag', JSON.stringify(newBag));
    }
  }
};


export const onSelectFrequency = async (frequency: Frequency, data: ServiceModel) => {
  if (data.serviceCategory) {
    const _bag = await AsyncStorage.getItem('bag');
    data.frequency = frequency;
    switch (data.serviceCategory) {
      case 'DRY_CLEANING':
        if (_bag) {
          navigationScreen(NAME_ROUTE.additionalNotes, { data });
        } else {
          navigationScreen(NAME_ROUTE.selectSchedule, { data });
        }
        break;
      case 'FOLDING':
        if (_bag) {
          navigationScreen(NAME_ROUTE.additionalNotes, { data });
        } else {
          navigationScreen(NAME_ROUTE.selectSchedule, { data });
        }
        break;
      case 'WASH_AND_FOLD':
        navigationScreen(NAME_ROUTE.serviceOrCaresSelect, { data });
        break;
      case 'BULKY_ITEMS':
        navigationScreen(NAME_ROUTE.serviceOrCaresSelect, { data });
        break;
      default:
        break;
    }
  }
};
