// Helpers
import { CLog } from '@helpers/Message';

// Storages
import { openRealm } from '@stores/index';

// Model
import PaymentMethod, { PaymentMethodModel } from '@models/payment';

// Constants
const URL_PATH = 'src/stores/PaymentMethod.ts';


/**
 * @function addPaymentMethod
 * @description Es esta función permite agregar un nuevo elemento a la base
 * de datos.
 * @param {PaymentMethodModel} (PaymentMethodModel)['../../models/paymentMethod']
 * @return {undefined | PaymentMethod}
 */
export const addPaymentMethod = async (newElement: PaymentMethodModel): Promise<undefined | PaymentMethod> => {
  let newPaymentMethod: undefined | PaymentMethodModel;
  const realm = await openRealm({});

  if (realm) {
    try {
      realm.write(() => {
        const paymentMethodResult = (realm.create(PaymentMethod.schema.name, newElement) as unknown) as PaymentMethodModel;
        CLog(URL_PATH, 'addPaymentMethod()', 'paymentMethodResult', paymentMethodResult);
        newPaymentMethod = new PaymentMethod(paymentMethodResult);
        CLog(URL_PATH, 'addPaymentMethod()', 'newPaymentMethod', newPaymentMethod);
      });
    } catch (err) {
      CLog(URL_PATH, 'addPaymentMethod()', 'err', err);
      realm.close();
    }
  }

  CLog(URL_PATH, 'addPaymentMethod()', 'newPaymentMethod 2', newPaymentMethod);
  return newPaymentMethod;
};


/**
 * @function getPaymentById
 * @description retorna el objecto si es encontrado.
 * @param {id} el id de la dirección
 * @return {undefined | PaymentMethod}
 */
export const getPaymentById = async (id: number): Promise<undefined | PaymentMethod> => {
  let paymentMethod: undefined | PaymentMethodModel[];
  let paymentFound: undefined | PaymentMethod;
  const realm = await openRealm({});

  if (realm) {
    try {
      paymentMethod = (realm
        .objects(PaymentMethod.schema.name)
        .filtered(`id = "${id}"`)
        .slice(0, 1) as unknown) as PaymentMethodModel[];

      if (paymentMethod.length) {
        paymentFound = new PaymentMethod(paymentMethod[0]);
      }
    } catch (err) {
      CLog(URL_PATH, 'getPaymentById()', 'err', err);
      realm.close();
    }
  }

  return paymentFound;
};


/**
 * @function getPaymentSelected
 * @description retorna el objecto si es encontrado.
 * @return {undefined | PaymentMethod}
 */
export const getPaymentSelected = async (): Promise<undefined | PaymentMethod> => {
  let paymentMethod: undefined | PaymentMethodModel[];
  let paymentFound: undefined | PaymentMethod;
  const realm = await openRealm({});
  if (realm) {
    try {
      paymentMethod = (realm
        .objects(PaymentMethod.schema.name)
        .filtered(`selected = true`)
        .slice(0, 1) as unknown) as PaymentMethodModel[];

      if (paymentMethod.length) {
        paymentFound = new PaymentMethod(paymentMethod[0]);
      }
    } catch (err) {
      CLog(URL_PATH, 'getPaymentSelected()', 'err', err);
      realm.close();
    }
  }

  return paymentFound;
};

/**
 * @function getPaymentMethods
 * @description retorna los objetos encontrados.
 * @return {undefined | PaymentMethod}
 */
export const getPaymentMethods = async (): Promise<PaymentMethod[]> => {
  let paymentMethod: undefined | PaymentMethodModel[];
  let paymentFound: PaymentMethod[] = [];
  const realm = await openRealm({});
  if (realm) {
    try {
      paymentMethod = (realm.objects(PaymentMethod.schema.name) as unknown) as PaymentMethodModel[];
      paymentMethod.forEach(a => paymentFound.push(new PaymentMethod(a)));
    } catch (err) {
      CLog(URL_PATH, 'getPaymentMethods()', 'err', err);
      realm.close();
    }
  }

  return paymentFound;
};

/**
 * @function updatePayment
 * @description actualiza el objeto buscado o creado.
 * @param {PaymentMethodModel} newPaymentMethod recurso a actualizar
 * @return {boolean} retorna true si se actualizó con éxito el recurso
 */
export const updatePayment = async (newPaymentMethod: PaymentMethodModel): Promise<boolean> => {
  let isUpdate = false;
  const realm = await openRealm({});

  if (realm) {
    try {
      realm.write(() => {
        let paymentMethod = realm.objects(PaymentMethod.schema.name).filtered(`id = "${newPaymentMethod.id}"`)[0] as PaymentMethodModel;
        if (paymentMethod) {
          paymentMethod = newPaymentMethod;
          isUpdate = true;
        }
      });
    } catch (err) {
      CLog(URL_PATH, 'updatePayment()', 'err', err);
      realm.close();
    }
  }
  return isUpdate;
};


/**
 * @function selectPayment
 * @description actualiza el objeto buscado o creado.
 * @param {id} id del recurso a seleccionar
 * @return {boolean} retorna true si se actualizó con éxito el recurso
 */
export const selectPayment = async (id?: string | number[]): Promise<boolean> => {
  let isUpdate = false;
  const realm = await openRealm({});

  if (realm) {
    try {
      realm.write(() => {
        let paymentMethod = realm.objects(PaymentMethod.schema.name).filtered(`id = "${id}"`)[0] as PaymentMethodModel;
        if (paymentMethod) {
          paymentMethod.selected = true;
          isUpdate = true;
        }
        const paymentFound = (realm.objects(PaymentMethod.schema.name).filtered(`id != "${id}"`) as unknown) as PaymentMethodModel[];
        paymentFound.forEach(paymentMethod => paymentMethod.selected = false);
      });
    } catch (err) {
      CLog(URL_PATH, 'selectPayment()', 'err', err);
      realm.close();
    }
  }
  return isUpdate;
};

/**
 * @function deletePaymentById
 * @description borrar el objeto buscado o creado.
 * @param {id} id del recurso a borrar
 * @return {boolean} retorna true si se borro con éxito el recurso
 */
export const deletePaymentById = async (id?: string | number[]): Promise<boolean> => {
  let isDelete = false;
  const realm = await openRealm({});

  if (realm) {
    try {
      realm.write(() => {
        const paymentMethod = realm.objects(PaymentMethod.schema.name).filtered(`id = "${id}"`).slice(0, 1);
        if (paymentMethod.length) {
          realm.delete(paymentMethod);
          isDelete = true;
        }
      });
    } catch (err) {
      CLog(URL_PATH, 'deletePaymentById()', 'err', err);
      realm.close();
    }
  }
  return isDelete;
};

/**
 * @function getLastPayment
 * @description obtener la ultima dirección
 * @return {undefined | PaymentMethod}
 */
export const getLastPayment = async (): Promise<undefined | PaymentMethod> => {
  let paymentLast: undefined | PaymentMethod;
  const realm = await openRealm({});

  if (realm) {
    try {
      const paymentFound = (realm.objects(PaymentMethod.schema.name) as unknown) as PaymentMethodModel[];
      if (paymentFound.length) {
        const paymentMethod = paymentFound.pop();
        if (paymentMethod) {
          paymentLast = new PaymentMethod(paymentMethod);
        }
      }
    } catch (err) {
      CLog(URL_PATH, 'getLastPayment()', 'err', err);
      realm.close();
    }
  }
  return paymentLast;
};


/**
 * @function deleteAllPayments
 * @description Borrar todos los elementos paymentMethod
 * @return {boolean}
 */
export const deleteAllPayments = async (): Promise<boolean> => {
  let isDelete = false;
  const realm = await openRealm({});

  if (realm) {
    try {
      realm.write(() => {
        const paymentFound = realm.objects(PaymentMethod.schema.name);
        realm.delete(paymentFound);
      });
      isDelete = true;
    } catch (err) {
      CLog(URL_PATH, 'deleteAllPayments()', 'err', err);
      realm.close();
    }
  }
  return isDelete;
};
