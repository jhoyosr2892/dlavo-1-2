// Libs
import Realm from 'realm';

// Helpers
import { CLog } from '@helpers/Message';

/* Schemas names */
import { AddressSchema } from '../models/address';
import { PaymentMethodSchema } from '../models/payment';

// Constants
const URL_PATH = 'src/stores/schemas/index.ts';
export const VERSION_SCHEMA = 1.2;
export const REALM_PATH = 'Realm.dlavo';
export const SCHEMAS = [AddressSchema, PaymentMethodSchema];

/**
 * @description abre una instancia de realm para un modelo o modelos
 * determinados.
 */
export const openRealm = async ({ schemaVersion = VERSION_SCHEMA, schema, path = REALM_PATH }: Realm.Configuration) => {
  try {
    /** función asyncrona **/
    const realm = await Realm.open({ schemaVersion, schema: schema ? schema : SCHEMAS, path });
    return realm;
    /** función syncrona **/
    //return new Realm({ schemaVersion, schema, path });
  } catch (error) {
    console.warn(error)
    CLog(URL_PATH, 'openRealm', 'error', error);
    return undefined;
  }
};

export default Realm;
