import 'react-native-gesture-handler';
import React, { useEffect } from 'react';

// Organisms
import Root from '@organisms/root';
import NavigationContainerApp from 'navigations';
import { setI18nConfig } from 'locales';
import { IS_ANDROID } from 'configs/Constants';
import { Alert, NativeModules } from 'react-native';
import messaging from '@react-native-firebase/messaging';
import moment from 'moment';

declare const global: { HermesInternal: null | {} };

const App = () => {
  const deviceLanguage: string = IS_ANDROID
    ? NativeModules.I18nManager.localeIdentifier
    : NativeModules.SettingsManager.settings.AppleLocale || NativeModules.SettingsManager.settings.AppleLanguages[0]; //iOS 13

  const language = deviceLanguage.split('_')[0];
  if (language === 'es' || language === 'en') {
    setI18nConfig(language);
    moment.locale(language);
  } else {
    setI18nConfig('en');
    moment.locale('en');
  }

  // setI18nConfig('en');
  // moment.locale('en');

  useEffect(() => {
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
    });

    return unsubscribe;
  }, []);

  return (
    <Root>
      <NavigationContainerApp />
    </Root>
  );
};

export default App;
