// libs
import { StyleSheet } from 'react-native';

// constants
import { FAMILY_FONTS, METRICS, SIZE_FONTS, THEME_DEFAULT } from '@constantsApp';

export const stylesGeneral = StyleSheet.create({
  /****** Root *******/
  viewContainerRoot: {
    flex: 1,
    backgroundColor: THEME_DEFAULT.primary
  },
  viewContainerRootContent: {
    flex: 1,
    backgroundColor: THEME_DEFAULT.content
  },
  /******* Font ******/
  // fonts variant (2 gamas de la misas fuente, 3 Sizes, 2 colores).
  textLightBigColorPrimary: {
    fontFamily: FAMILY_FONTS.primaryLight,
    fontSize: SIZE_FONTS.primary,
    color: THEME_DEFAULT.primaryFont
  },
  textLightBigColorSecondary: {
    fontFamily: FAMILY_FONTS.primaryLight,
    fontSize: SIZE_FONTS.primary,
    color: THEME_DEFAULT.secondaryFont
  },
  textLightSmallColorPrimary: {
    fontFamily: FAMILY_FONTS.primaryLight,
    fontSize: SIZE_FONTS.secondary,
    color: THEME_DEFAULT.primaryFont
  },
  textLightSmallColorSecondary: {
    fontFamily: FAMILY_FONTS.primaryLight,
    fontSize: SIZE_FONTS.secondary,
    color: THEME_DEFAULT.secondaryFont
  },
  textRegularBigColorPrimary: {
    fontFamily: FAMILY_FONTS.primaryRegular,
    fontSize: SIZE_FONTS.primary,
    color: THEME_DEFAULT.primaryFont
  },
  textRegularBigColorSecondary: {
    fontFamily: FAMILY_FONTS.primaryRegular,
    fontSize: SIZE_FONTS.primary,
    color: THEME_DEFAULT.secondaryFont
  },
  textRegularSmallColorPrimary: {
    fontFamily: FAMILY_FONTS.primaryRegular,
    fontSize: SIZE_FONTS.secondary,
    color: THEME_DEFAULT.primaryFont
  },
  textRegularSmallColorSecondary: {
    fontFamily: FAMILY_FONTS.primaryRegular,
    fontSize: SIZE_FONTS.secondary,
    color: THEME_DEFAULT.secondaryFont
  },
  textRegularExtraBigColorPrimary: {
    fontFamily: FAMILY_FONTS.primaryRegular,
    fontSize: SIZE_FONTS.tertiary,
    color: THEME_DEFAULT.primaryFont
  },
  textRegularExtraBigColorSecondary: {
    fontFamily: FAMILY_FONTS.primaryRegular,
    fontSize: SIZE_FONTS.tertiary,
    color: THEME_DEFAULT.secondaryFont
  },
  textSemiBoldBigColorPrimary: {
    fontFamily: FAMILY_FONTS.primarySemiBold,
    fontSize: SIZE_FONTS.primary,
    color: THEME_DEFAULT.primaryFont
  },
  textSemiBoldBigColorSecondary: {
    fontFamily: FAMILY_FONTS.primarySemiBold,
    fontSize: SIZE_FONTS.primary,
    color: THEME_DEFAULT.secondaryFont
  },
  textSemiBoldSmallColorPrimary: {
    fontFamily: FAMILY_FONTS.primarySemiBold,
    fontSize: SIZE_FONTS.secondary,
    color: THEME_DEFAULT.primaryFont
  },
  textSemiBoldSmallColorSecondary: {
    fontFamily: FAMILY_FONTS.primarySemiBold,
    fontSize: SIZE_FONTS.secondary,
    color: THEME_DEFAULT.secondaryFont
  },
  textSemiBoldExtraBigColorPrimary: {
    fontFamily: FAMILY_FONTS.primarySemiBold,
    fontSize: SIZE_FONTS.tertiary,
    color: THEME_DEFAULT.primaryFont
  },
  textSemiBoldExtraBigColorSecondary: {
    fontFamily: FAMILY_FONTS.primarySemiBold,
    fontSize: SIZE_FONTS.tertiary,
    color: THEME_DEFAULT.secondaryFont
  },
  textBoldBigColorPrimary: {
    fontFamily: FAMILY_FONTS.primaryBold,
    fontSize: SIZE_FONTS.primary,
    color: THEME_DEFAULT.primaryFont
  },
  textBoldBigColorSecondary: {
    fontFamily: FAMILY_FONTS.primaryBold,
    fontSize: SIZE_FONTS.primary,
    color: THEME_DEFAULT.secondaryFont
  },
  textBoldSmallColorPrimary: {
    fontFamily: FAMILY_FONTS.primaryBold,
    fontSize: SIZE_FONTS.secondary,
    color: THEME_DEFAULT.primaryFont
  },
  textBoldSmallColorSecondary: {
    fontFamily: FAMILY_FONTS.primaryBold,
    fontSize: SIZE_FONTS.secondary,
    color: THEME_DEFAULT.secondaryFont
  },
  textBoldExtraBigColorPrimary: {
    fontFamily: FAMILY_FONTS.primaryBold,
    fontSize: SIZE_FONTS.tertiary,
    color: THEME_DEFAULT.primaryFont
  },
  textBoldExtraBigColorSecondary: {
    fontFamily: FAMILY_FONTS.primaryBold,
    fontSize: SIZE_FONTS.tertiary,
    color: THEME_DEFAULT.secondaryFont
  },
  // transforms fonts
  textCapitalize: {
    textTransform: 'capitalize'
  },
  /***** View *****/
  containerView: {
    height: '100%',
    width: '100%'
  },
  viewContainer: {
    flex: 1,
    padding: METRICS.CONSTANTS_PADDING,
    backgroundColor: THEME_DEFAULT.white,
    paddingTop: 30
  },
  viewContainerWithOutBack: {
    flex: 1,
    padding: METRICS.CONSTANTS_PADDING,
    paddingTop: 30
  },
  /***** elevation o sombra (toast, button) *****/
  shadowAndElevation: {
    // android
    elevation: METRICS.ELEVATION,
    // ios
    shadowColor: THEME_DEFAULT.black,
    shadowOffset: { width: 0, height: METRICS.ELEVATION },
    shadowOpacity: 0.5,
    shadowRadius: METRICS.ELEVATION
  },
  /******* List ********/
  listDefault: {
    marginHorizontal: METRICS.CONSTANTS_MARGIN
  },
  /****** Card ******/
  cardItemList: {
    borderRadius: METRICS.BORDER_RADIUS,
    borderWidth: METRICS.BORDER_FRAMEWORK,
    borderColor: THEME_DEFAULT.borderLine,
    marginBottom: METRICS.CONSTANTS_MARGIN_LIST,
    padding: METRICS.CONSTANTS_PADDING_LIST,
    backgroundColor: THEME_DEFAULT.textArea
  },
  /*** ImageBackground ( card category carousel, detail course ) ***/
  contentCenterImageBackground: {
    flexDirection: 'row',
    backgroundColor: THEME_DEFAULT.backgroundTransparent,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: METRICS.BORDER_RADIUS
  }
});
