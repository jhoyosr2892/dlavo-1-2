import React, { useEffect, useState } from 'react';
import { StackScreenProps } from '@react-navigation/stack';

/* Components */
import MembershipOrganism from '@organisms/profile/membership';
/* Helpers */
import { RootStackParamList } from 'utils/helpers/Navigation';
/* Configs */
import { NAME_ROUTE } from 'configs/Constants';
import { MembershipModel } from 'models/membership';
import { getMemberships } from 'services/app/Membership';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.pricing>;

const PricingScene = ({}: sceneProps) => {
  /* States */
  const [memberships, setMemberships] = useState<MembershipModel[]>();

  useEffect(() => {
    const getData = () => {
      loadMemberships();
    };
    return getData();
  }, []);

  const loadMemberships = async () => {
    const { status, data } = await getMemberships();
    if (status) {
      setMemberships(data);
    }
  };

  return <MembershipOrganism {...{ memberships }} />;
};

export default PricingScene;
