import React, { useEffect, useState } from 'react';
/* Libs */
import { StackScreenProps } from '@react-navigation/stack';
/* Components */
import SelectGarmentsOrganism from 'components/organisms/laundry/SelectClothes';
/* Config */
import { NAME_ROUTE } from 'configs/Constants';
/* Helpers */
import { navigationScreen, RootStackParamList } from 'utils/helpers/Navigation';
/* models */
import { ServiceModel } from 'models/service';
import { View } from 'react-native';
import SpinnerLottie from 'components/atoms/spinners';
import { typeClothes } from 'models/clothes';
import { onSelectFrequency, updateBagStorage } from 'stores/ServiceState';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.selectGarments>;

const SelectClothesScene = ({ route, navigation }: sceneProps) => {
  /* State */
  const [data, setData] = useState<ServiceModel>();
  const [navigateRoute, setNavigateRoute] = useState<NAME_ROUTE>(NAME_ROUTE.selectFrequency);

  useEffect(() => {
    const loadServiceCategory = () => {
      const unsubscribe = navigation.addListener('focus', async () => {
        if (route.params) {
          const { data, backRoute } = route.params;
          setData(data);
          if (backRoute) {
            setNavigateRoute(backRoute);
          }
        }
      });
      return unsubscribe;
    };
    loadServiceCategory();
  }, [route, navigation]);

  const onPressNext = async (_selectedClothes: typeClothes[]) => {
    if (data) {
      data.clothes = _selectedClothes;
      if (navigateRoute !== NAME_ROUTE.selectFrequency) {
        await updateBagStorage(data);
        navigationScreen(navigateRoute, { data: data });
      } else {
        onSelectFrequency('single', data);
      }
    }
  };

  return !data ? (
    <View style={{ justifyContent: 'center', alignItems: 'center', width: '100%' }}>
      <SpinnerLottie />
    </View>
  ) : (
    <SelectGarmentsOrganism {...{ data, onPressNext }} />
  );
};

export default SelectClothesScene;
