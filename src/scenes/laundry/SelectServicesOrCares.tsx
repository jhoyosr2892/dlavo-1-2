import React, { useEffect, useState } from 'react';

/* Libs */
import { StackScreenProps } from '@react-navigation/stack';
/* Constants */
import { NAME_ROUTE } from 'configs/Constants';
/* Models */
import { SPECIAL_SERVICE_CARES } from 'models/enums/special-services-cares';
import { typeQuantity, ServiceModel } from 'models/service';
import { SOAPS } from 'models/enums/SoapTypes';
/* Utils */
import { navigationScreen, RootStackParamList } from 'utils/helpers/Navigation';
/* Components */
import SelectServicesOrCaresOrganism from 'components/organisms/laundry/SelectServicesOrCares';
import AsyncStorage from '@react-native-async-storage/async-storage';

const URL_PATH = 'src/scenes/laundry/ServicesOrCaresSelectScene.tsx';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.serviceOrCaresSelect>;

const ServicesOrCaresSelectScene = ({ navigation, route }: sceneProps) => {
  /* State */
  const [data, setData] = useState<ServiceModel>();

  useEffect(() => {
    const loadServiceCategory = () => {
      if (route.params) {
        const { data } = route.params;
        setData(data);
      }
    };
    loadServiceCategory();
  }, [route, navigation]);

  const _addService = async (services: SPECIAL_SERVICE_CARES[], soapTypes: SOAPS[]) => {
    const arrayClothes: typeQuantity[] = [];
    const arrayClothesInfo: typeQuantity[] = [];
    if (data) {
      data.clothes?.forEach(clothe => {
        arrayClothes.push({ type: clothe.type, quantity: clothe.quantity });
        arrayClothesInfo.push({ type: clothe.type, quantity: clothe.quantity, name: clothe.name });
      });
      data.serviceAdditionals = services && services.length > 0 ? services : [];
      data.soapTypes = soapTypes;
      const _bag = await AsyncStorage.getItem('bag');
      if (_bag) {
        navigationScreen(NAME_ROUTE.additionalNotes, { data });
      } else {
        navigationScreen(NAME_ROUTE.selectSchedule, { data: data });
      }
    }
  };

  return <SelectServicesOrCaresOrganism {...{ addService: _addService }} />;
};

export default ServicesOrCaresSelectScene;
