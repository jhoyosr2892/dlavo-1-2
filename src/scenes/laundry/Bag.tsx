import React, { useContext, useEffect, useState } from 'react';
/* Components */
import BagOrganism from 'components/organisms/bag';
import { StackScreenProps } from '@react-navigation/stack';
import { navigationScreen, RootStackParamList } from 'utils/helpers/Navigation';
import { EnumToast, NAME_ROUTE } from 'configs/Constants';
import { ServiceRequestModel } from 'models/service';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { SERVICE_CATEGORY } from 'models/enums/ServiceCategory';
import { createServiceRequest } from 'services/app/ServiceRequest';
import { RootContext } from 'configs/ContextApp';
import { translate } from 'locales';
import { ServicesAdditional } from 'models/enums/ServiceAdditional';
import { useFocusEffect } from '@react-navigation/native';
import { BackHandler, View } from 'react-native';
import SpinnerLottie from 'components/atoms/spinners';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.bag>;

const BagScene = ({ route, navigation }: sceneProps) => {
  const [loading, setLoading] = useState<boolean>(true);
  const [bag, setBag] = useState<ServiceRequestModel | undefined>();
  const { showToast, showLoading, hideLoading } = useContext(RootContext);
  const [total, setTotal] = useState(0);
  const [bags, setBags] = useState<{ label: string; price: number | undefined }[]>([]);
  const [clothes, setClothes] = useState<{ label: string; price: number | undefined }[]>([]);
  const [servicioAdicional, setServicioAdicional] = useState<{ label: string; price: number | undefined }[]>([]);

  useEffect(() => {
    const loadServices = async () => {
      showLoading();
      let paymentInfo: {
        creditCardNumber: string;
        cvv: number;
        expirationDate: string;
      } = {
        creditCardNumber: '',
        cvv: 0,
        expirationDate: ''
      };

      if (route.params) {
        const { paymentMethod } = route.params;
        if (paymentMethod) {
          paymentInfo = {
            creditCardNumber: paymentMethod.cardNumber,
            cvv: paymentMethod.cardCvs,
            expirationDate: paymentMethod.cardExpirationDate
          };
        }
      }

      const _bag = await AsyncStorage.getItem('bag');
      if (_bag) {
        const bagCopy = JSON.parse(_bag) as ServiceRequestModel;
        console.log(bagCopy.services[0]);
        
        if (paymentInfo.creditCardNumber) {
          bagCopy.creditCardNumber = paymentInfo.creditCardNumber;
          bagCopy.cvv = paymentInfo.cvv;
          bagCopy.expirationDate = paymentInfo.expirationDate;
          bagCopy.uniquePayment = true;
          setBag(bagCopy);
        } else {
          setBag(bagCopy);
        }
        await AsyncStorage.setItem('bag', JSON.stringify(bagCopy));
        determineTotal(bagCopy);
      }
      hideLoading();
      setLoading(false);
    };
    loadServices();
  }, [route, navigation]);

  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        navigationScreen(NAME_ROUTE.laundry);
        return true;
      };
      BackHandler.addEventListener('hardwareBackPress', onBackPress);
      return () => BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [])
  );

  const determineTotal = (bagInfo: ServiceRequestModel) => {
    let _total = 0;
    let _bags: { label: string; price: number | undefined }[] = [];
    let _clothes: { label: string; price: number | undefined }[] = [];
    let _servicioAdicional: { label: string; price: number | undefined }[] = [];
    bagInfo.services.forEach(s => {
      switch (s.serviceCategory) {
        case 'BULKY_ITEMS':
          s.clothes?.forEach(c => {
            if (c.price) {
              _total += (c.price as number) * (c.quantity as number);
            }
            _clothes.push({
              label: `${c.name} x ${c.quantity}`,
              price: (c.price as number) * (c.quantity as number)
            });
          });
          break;
        case 'DRY_CLEANING':
          s.clothes?.forEach(c => {
            if (c.price) {
              _total += (c.price as number) * (c.quantity as number);
            }
            _clothes.push({
              label: `${c.name} x ${c.quantity}`,
              price: (c.price as number) * (c.quantity as number)
            });
          });
          break;
        case 'FOLDING':
          s.bags?.forEach(c => {
            if (c.price) {
              _total += (c.price as number) * (c.quantity as number);
            }
            _bags.push({
              label: `${c.name} x ${c.quantity}`,
              price: (c.price as number) * (c.quantity as number)
            });
          });
          break;
        case 'WASH_AND_FOLD':
          s.bags?.forEach(c => {
            if (c.price) {
              _total += (c.price as number) * (c.quantity as number);
            }
            _bags.push({
              label: `${c.name} x ${c.quantity}`,
              price: (c.price as number) * (c.quantity as number)
            });
          });
          break;
        default:
          break;
      }
      s.serviceAdditionals?.forEach(sa => {
        const servicioAdicional = ServicesAdditional.find(service => service.name === sa);
        if (servicioAdicional) {
          _total += servicioAdicional.price;
          _servicioAdicional.push({
            label: `${servicioAdicional.label}`,
            price: servicioAdicional.price
          });
        }
      });
    });
    if (
      new Date(bagInfo?.services[0].deliverySingleTime * 1000).getDate() - new Date(bagInfo?.services[0].pickUpSingleTime * 1000).getDate() <=
      1
    ) {
      _total += 15;
    }
    setTotal(_total);
    setBags(_bags);
    setClothes(_clothes);
    setServicioAdicional(_servicioAdicional);
  };

  const deleteClothe = async (index: number, serviceCategory: SERVICE_CATEGORY) => {
    if (bag) {
      const cloneBag = { ...bag };
      const indexService = cloneBag.services.findIndex(b => b.serviceCategory === serviceCategory);
      if (indexService !== -1) {
        if (cloneBag.services[indexService].clothes.length > 1) {
          cloneBag.services[indexService].clothes.splice(index, 1);
          cloneBag.services[indexService].clothesInfo?.splice(index, 1);
          setBag(cloneBag);
          determineTotal(cloneBag);
          AsyncStorage.setItem('bag', JSON.stringify(cloneBag));
        } else if (cloneBag.services.length > 1 && cloneBag.services[indexService].clothes.length === 1) {
          cloneBag.services.splice(indexService, 1);
          setBag(cloneBag);
          determineTotal(cloneBag);
          AsyncStorage.setItem('bag', JSON.stringify(cloneBag));
        } else if (cloneBag.services.length === 1) {
          deleteAll();
        }
      }
    }
  };
  const deleteBag = async (index: number, serviceCategory: SERVICE_CATEGORY) => {
    if (bag) {
      const cloneBag = { ...bag };
      const indexService = cloneBag.services.findIndex(b => b.serviceCategory === serviceCategory);
      console.log(cloneBag.services);
      if (indexService !== -1) {
        if (cloneBag.services[indexService].bags) {
          if (cloneBag.services[indexService].bags.length > 1) {
            cloneBag.services[indexService].bags.splice(index, 1);
            setBag(cloneBag);
            determineTotal(cloneBag);
            AsyncStorage.setItem('bag', JSON.stringify(cloneBag));
          } else if (cloneBag.services.length > 1 && cloneBag.services[indexService].bags.length === 1) {
            cloneBag.services.splice(indexService, 1);
            setBag(cloneBag);
            determineTotal(cloneBag);
            AsyncStorage.setItem('bag', JSON.stringify(cloneBag));
          } else if (cloneBag.services.length === 1) {
            deleteAll();
          }
        }
      }
    }
  };
  const deleteAll = async () => {
    AsyncStorage.removeItem('bag');
    setBag(undefined);
    setTotal(0);
    setBags([]);
    setClothes([]);
    setServicioAdicional([]);
  };

  const _createRequest = async (coupon?: string) => {
    showLoading();
    if (bag) {
      try {
        bag.couponCode = coupon;
        const _bag = { ...bag };
        _bag.services.forEach(s => {
          s.clothes?.forEach(c => (c.image = ''));
          s.bags?.forEach(c => (c.image = ''));
        });
        const { status, data, message } = await createServiceRequest(bag);
        if (status) {
          showToast({ text: translate('BAG_SCENE.TOAST.SERVICE_CREATED_SUCCESSFULLY'), type: EnumToast.success });
          AsyncStorage.removeItem('bag');
          setBag(undefined);
          navigation.popToTop();
          navigationScreen(NAME_ROUTE.myServicesTab);
        } else {
          showToast({ text: message, type: EnumToast.error });
        }
      } catch (e) {
        showToast({ text: translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION'), type: EnumToast.error });
      }
    }
    hideLoading();
  };

  return <BagOrganism {...{ bag, deleteClothe, deleteBag, deleteAll, _createRequest, total, bags, clothes, servicioAdicional }} />;
  // return loading ? (
  //   <View style={{ justifyContent: 'center', alignItems: 'center', width: '100%' }}>
  //     <SpinnerLottie />
  //   </View>
  // ) : (
  //   <BagOrganism {...{ bag, deleteClothe, deleteBag, deleteAll, _createRequest, total, bags, clothes, servicioAdicional }} />
  // );
};

export default BagScene;
