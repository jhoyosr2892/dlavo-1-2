import React, { useEffect, useState } from 'react';

/* Libs */
import { StackScreenProps } from '@react-navigation/stack';
/* Components */
import SelectFrequencyOrganism from 'components/organisms/laundry/SelectFrequency';
/* Utils */
import { RootStackParamList } from 'utils/helpers/Navigation';
/* Constants */
import { NAME_ROUTE } from 'configs/Constants';
/* Models */
import { ServiceModel } from 'models/service';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.selectFrequency>;

const SelectFrequencyScene = ({ route, navigation }: sceneProps) => {
  const [data, setData] = useState<ServiceModel>({} as ServiceModel);
  useEffect(() => {
    const loadServiceCategory = () => {
      if (route.params) {
        const { data } = route.params;
        setData(data);
      }
    };
    loadServiceCategory();
  }, [route, navigation]);
  return <SelectFrequencyOrganism {...{ data }} />;
};

export default SelectFrequencyScene;
