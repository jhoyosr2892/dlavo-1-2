import React, { useEffect, useState } from 'react';
/* Libs */
import { StackScreenProps } from '@react-navigation/stack';
/* Components */
import LaundryOrganism from 'components/organisms/laundry';
/* Constants */
import { NAME_ROUTE } from 'configs/Constants';
/* Models */
import { AddressModel } from 'models/address';
/* Stores */
import { getAddressSelected } from 'stores/Address';
/* Utils */
import { RootStackParamList } from 'utils/helpers/Navigation';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { ServiceRequestModel } from 'models/service';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.laundry>;

const LaundryScene = ({ route, navigation }: sceneProps) => {
  /* States */
  const [addressSelected, setAddressSelected] = useState<AddressModel>();
  const [bagSelected, setBag] = useState<ServiceRequestModel>();

  useEffect(() => {
    loadData();
  }, [route, navigation]);

  const loadData = () => {
    const unsubscribe = navigation.addListener('focus', async () => {
      const addressSaved = await getAddressSelected();
      setAddressSelected(addressSaved);
      const _bag = await AsyncStorage.getItem('bag');
      if (_bag) {
        setBag(JSON.parse(_bag));
      }
    });
    return unsubscribe;
  };

  return <LaundryOrganism {...{ addressSelected, bagSelected }} />;
};

export default LaundryScene;
