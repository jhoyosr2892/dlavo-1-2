import React, { useContext, useEffect, useState } from 'react';
import { StackScreenProps } from '@react-navigation/stack';

import SelectScheduleOrganism from 'components/organisms/laundry/SelectSchedule';
import { NAME_ROUTE } from 'configs/Constants';
import { Concurrence, ServiceModel } from 'models/service';
import { navigationScreen, RootStackParamList } from 'utils/helpers/Navigation';
import { AddressModel } from 'models/address';
import { RootContext } from 'configs/ContextApp';
import uuid from 'react-native-uuid';
import { getAddressSelected } from 'stores/Address';
import moment from 'moment';
import { View } from 'react-native';
import SpinnerLottie from 'components/atoms/spinners';
import { updateBagStorage } from 'stores/ServiceState';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.selectSchedule>;

const SelectScheduleScene = ({ route, navigation }: sceneProps) => {
  /* State */
  const [data, setData] = useState<ServiceModel>({} as ServiceModel);
  const [address, setAddress] = useState<AddressModel>();
  const [loading, setLoading] = useState<boolean>(true);
  const [navigateRoute, setNavigateRoute] = useState<NAME_ROUTE>(NAME_ROUTE.additionalNotes);

  useEffect(() => {
    const loadServiceCategory = () => {
      const unsubscribe = navigation.addListener('focus', async () => {
        if (route.params) {
          const { data, backRoute } = route.params;
          setData(data);
          if (backRoute) {
            setNavigateRoute(backRoute);
          }
          setLoading(false);
        }
        _getAddressSelected();
      });
      return unsubscribe;
    };
    loadServiceCategory();
  }, [route, navigation]);

  const _getAddressSelected = async () => {
    const addressFound = await getAddressSelected();
    if (addressFound) {
      setAddress(addressFound);
      return addressFound;
    }
  };

  const _addData = async ({
    address,
    typeOfPickUp,
    deliveryDatetime,
    pickUpDatetime,
    weeklyDays,
    monthlyDays,
    concurrence,
    remainingRepetitions
  }: {
    address?: AddressModel;
    typeOfPickUp?: string;
    pickUpDatetime?: number;
    deliveryDatetime?: number;
    weeklyDays?: number[];
    monthlyDays?: number[];
    concurrence?: Concurrence;
    remainingRepetitions: string;
  }) => {
    if (data) {
      data.id = uuid.v4().toString();
      data.type = data.serviceCategory;
      data.concurrence = concurrence;
      data.pickUpType = typeOfPickUp;
      if (data.frequency === 'frequent') {
        data.repetitions = parseInt(remainingRepetitions, 10);
      }
      if (concurrence === 'SINGLE_TIME') {
        data.pickUpSingleTime = pickUpDatetime;
        data.deliverySingleTime = deliveryDatetime;
      }
      if (concurrence === 'WEEKLY') {
        data.weeklyDays = weeklyDays;
      }
      if (concurrence === 'MONTHLY') {
        data.monthDays = monthlyDays;
      }
      if (navigateRoute !== NAME_ROUTE.additionalNotes) {
        await updateBagStorage(data);
      }
      navigationScreen(navigateRoute, { data, address });
    }
  };

  return loading ? (
    <View style={{ justifyContent: 'center', alignItems: 'center', width: '100%' }}>
      <SpinnerLottie />
    </View>
  ) : (
    <SelectScheduleOrganism {...{ _addData, address, data }} />
  );
};

export default SelectScheduleScene;
