import { StackScreenProps } from '@react-navigation/stack';
import SpinnerLottie from 'components/atoms/spinners';
import SelectBagQuantityOrganism from 'components/organisms/laundry/SelectBagQuantity';
import { NAME_ROUTE } from 'configs/Constants';
import { typeBags } from 'models/bag';
import { ServiceModel } from 'models/service';
import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import { onSelectFrequency, updateBagStorage } from 'stores/ServiceState';
import { navigationScreen, RootStackParamList } from 'utils/helpers/Navigation';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.selectBagQuantity>;

const SelectBagQuantityScene = ({ route, navigation }: sceneProps) => {
  const [data, setData] = useState<ServiceModel>();
  const [navigateRoute, setNavigateRoute] = useState<NAME_ROUTE>(NAME_ROUTE.selectFrequency);

  useEffect(() => {
    const loadServiceCategory = () => {
      if (route.params) {
        const { data, backRoute } = route.params;
        setData(data);
        if (backRoute) {
          setNavigateRoute(backRoute);
        }
      }
    };
    loadServiceCategory();
  }, [route, navigation]);

  const _addBags = async (selectedBags: typeBags[]) => {
    if (data) {
      data.bags = selectedBags;
      if (navigateRoute !== NAME_ROUTE.selectFrequency) {
        await updateBagStorage(data);
        navigationScreen(navigateRoute, { data: data });
      } else {
        onSelectFrequency('single', data);
      }
    }
  };

  return !data ? (
    <View style={{ justifyContent: 'center', alignItems: 'center', width: '100%' }}>
      <SpinnerLottie />
    </View>
  ) : (
    <SelectBagQuantityOrganism {...{ _addBags, data }} />
  );
};

export default SelectBagQuantityScene;
