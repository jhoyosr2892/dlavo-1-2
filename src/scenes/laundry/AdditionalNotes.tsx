import React, { useContext, useEffect, useState } from 'react';

/* Libs */
import { StackScreenProps } from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage';
import uuid from 'react-native-uuid';
/* Components */
import AdditionalNotesOrganism from 'components/organisms/laundry/AdditionalNotes';
/* Utils */
import { navigationScreen, RootStackParamList } from 'utils/helpers/Navigation';
/* Configs */
import { EnumToast, NAME_ROUTE } from 'configs/Constants';
import { RootContext } from 'configs/ContextApp';
/* Models */
import { additionalNotes, ServiceModel, ServiceRequestModel } from 'models/service';
import { AddressModel } from 'models/address';
import { translate } from 'locales';
import { View } from 'react-native';
import SpinnerLottie from 'components/atoms/spinners';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.additionalNotes>;

const AdditionalNotesScene = ({ navigation, route }: sceneProps) => {
  /* States */
  const [data, setData] = useState<ServiceModel>();
  const [address, setAddress] = useState<AddressModel>();
  const [_updated, setUpdated] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(true);
  /* Context */
  const { showToast } = useContext(RootContext);

  useEffect(() => {
    const loadServiceCategory = () => {
      if (route.params) {
        const { data, address, updated } = route.params;
        setData(data);
        setAddress(address);
        setUpdated(updated);
      }
      setLoading(false);
    };
    loadServiceCategory();
  }, [route, navigation]);

  const _addData = async (additionalNotes: additionalNotes) => {
    if (data) {
      data.additionalNotes = {
        specializedInstructions: additionalNotes.specializedInstructions,
        stateOfClothe: additionalNotes.stateOfClothe,
        photos: []
      };
      const bag = await AsyncStorage.getItem('bag');
      if (bag) {
        const bagClone: ServiceRequestModel = JSON.parse(bag);
        // const serviceIndex = bagClone.services.findIndex((b: ServiceModel) => b.serviceCategory === data?.serviceCategory);
        // if (serviceIndex !== -1) {
        //   bagClone.services[serviceIndex] = data;
        // } else {
        // }
        if (_updated) {
          const serviceIndex = bagClone.services.findIndex(s => s.id === data.id);
          if (serviceIndex !== -1) {
            bagClone.services[serviceIndex] = data;
          } else {
            bagClone.services.push(data);
          }
        } else {
          bagClone.services.push(data);
        }
        setBag(bagClone);
      } else {
        if (address && address.id) {
          const newBag: ServiceRequestModel = {
            id: uuid.v4().toString(),
            address: address.id.toString(),
            services: [data]
          };
          setBag(newBag);
        }
      }
    }
  };

  const setBag = async (bag: ServiceRequestModel) => {
    await AsyncStorage.setItem('bag', JSON.stringify(bag));
    showToast({ text: translate('BAG_SCENE.TOAST.SERVICE_ADD_SUCCESSFULLY'), type: EnumToast.success });
    navigationScreen(NAME_ROUTE.bag, { data: data });
  };

  return loading ? (
    <View style={{ justifyContent: 'center', alignItems: 'center', width: '100%' }}>
      <SpinnerLottie />
    </View>
  ) : (
    <AdditionalNotesOrganism {...{ _addData, data }} />
  );
};

export default AdditionalNotesScene;
