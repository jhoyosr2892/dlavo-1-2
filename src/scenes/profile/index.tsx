/* Libs */
import React from 'react';
import { StackScreenProps } from '@react-navigation/stack';
/* Utils */
import { RootStackParamList } from 'utils/helpers/Navigation';
/* Configs */
import { NAME_ROUTE } from 'configs/Constants';
/* Components */
import ProfileOrganism from 'components/organisms/profile';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.profile>;

const ProfileScene = ({}: sceneProps) => {
  return <ProfileOrganism />;
};

export default ProfileScene;
