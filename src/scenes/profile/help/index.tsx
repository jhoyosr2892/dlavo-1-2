import React from 'react';
import { StackScreenProps } from '@react-navigation/stack';

/* Components */
import HelpOrganism from '@organisms/profile/help';
/* Helpers */
import { RootStackParamList } from 'utils/helpers/Navigation';
/* Configs */
import { NAME_ROUTE } from 'configs/Constants';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.help>;

const HelpScene = ({}: sceneProps) => {
  return <HelpOrganism />;
};

export default HelpScene;
