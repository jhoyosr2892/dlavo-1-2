import React from 'react';
import { StackScreenProps } from '@react-navigation/stack';

/* Components */
import CouponsOrganism from '@organisms/profile/coupons';
/* Helpers */
import { RootStackParamList } from 'utils/helpers/Navigation';
/* Configs */
import { NAME_ROUTE } from 'configs/Constants';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.coupons>;

const CouponsScene = ({}: sceneProps) => {
  const checkCouponCode = (couponCode: string | undefined) => {};

  return <CouponsOrganism {...{ checkCouponCode }} />;
};

export default CouponsScene;
