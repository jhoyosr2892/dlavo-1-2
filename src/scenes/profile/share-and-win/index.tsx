import React from 'react';
import { StackScreenProps } from '@react-navigation/stack';

/* Components */
import ShareAndWinOrganism from '@organisms/profile/share-and-win';
/* Helpers */
import { RootStackParamList } from 'utils/helpers/Navigation';
/* Configs */
import { NAME_ROUTE } from 'configs/Constants';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.shareAndWin>;

const ShareAndWinScene = ({}: sceneProps) => {
  return <ShareAndWinOrganism />;
};

export default ShareAndWinScene;
