import { StackScreenProps } from '@react-navigation/stack';
import React, { useContext } from 'react';
/* Utils */
import { RootStackParamList } from 'utils/helpers/Navigation';
/* Configs */
import { EnumToast, NAME_ROUTE } from 'configs/Constants';
/* Components */
import ProfileDetailOrganism from 'components/organisms/profile/ProfileDetail';
import { RootContext } from 'configs/ContextApp';
import { translate } from 'locales';
import { UserModel } from 'models/user/User';
import { updateUser } from 'services/app/User';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.profile>;

const ProfileDetailScene = ({}: sceneProps) => {
  const { showToast, showLoading, hideLoading } = useContext(RootContext);

  const _updateUser = async (user: UserModel) => {
    showLoading();
    try {
      console.log('user to update : ', user);

      const response = await updateUser(user);
      if (response.status) {
        showToast({ text: translate('PROFILE_SCENE.TOAST.USER_UPDATED'), type: EnumToast.success });
      } else {
        showToast({
          text: response.message ? response.message : translate('PROFILE_SCENE.ERRORS.USER_NOT_UPDATED'),
          type: EnumToast.error
        });
      }
    } catch (e) {
      showToast({ text: translate('PROFILE_SCENE.ERRORS.USER_NOT_UPDATED'), type: EnumToast.error });
    }
    hideLoading();
  };

  return <ProfileDetailOrganism {...{ _updateUser }} />;
};

export default ProfileDetailScene;
