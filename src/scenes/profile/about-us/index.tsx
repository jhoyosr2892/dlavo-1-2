import React from 'react';
import { StackScreenProps } from '@react-navigation/stack';

/* Components */
import AboutUsOrganism from '@organisms/profile/about-us';
/* Helpers */
import { RootStackParamList } from 'utils/helpers/Navigation';
/* Configs */
import { NAME_ROUTE } from 'configs/Constants';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.about>;

const AboutUsScene = ({}: sceneProps) => {
  return <AboutUsOrganism />;
};

export default AboutUsScene;
