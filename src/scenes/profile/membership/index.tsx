import React, { useEffect, useState } from 'react';
import { StackScreenProps } from '@react-navigation/stack';

/* Components */
import MembershipOrganism from '@organisms/profile/membership';
/* Helpers */
import { RootStackParamList } from 'utils/helpers/Navigation';
/* Configs */
import { NAME_ROUTE } from 'configs/Constants';
import { MembershipModel } from 'models/membership';
import { getMemberships } from 'services/app/Membership';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.membership>;

const MembershipScene = ({}: sceneProps) => {
  const [memberships, setMemberships] = useState<MembershipModel[] | undefined>();
  useEffect(() => {
    const getData = () => {
      loadMemberships();
    };
    return getData();
  });

  const loadMemberships = async () => {
    // const response = await getMemberships();
    // if (response.status) {
    //   setMemberships(response.data);
    // }
  };

  return <MembershipOrganism {...{ memberships }} />;
};

export default MembershipScene;
