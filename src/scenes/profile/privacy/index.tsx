import React from 'react';
import { StackScreenProps } from '@react-navigation/stack';

/* Components */
import PrivacyOrganism from '@organisms/profile/privacy';
/* Helpers */
import { RootStackParamList } from 'utils/helpers/Navigation';
/* Configs */
import { NAME_ROUTE } from 'configs/Constants';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.privacy>;

const PrivacyScene = ({}: sceneProps) => {
  return <PrivacyOrganism />;
};

export default PrivacyScene;
