import React from 'react';
import FaqOrganisms from 'components/organisms/profile/faq';

const FaqScene = () => {
  return <FaqOrganisms />;
};

export default FaqScene;
