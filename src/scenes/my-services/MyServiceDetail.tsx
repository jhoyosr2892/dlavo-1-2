/* Libs */
import React, { useCallback, useEffect, useState } from 'react';
import { StackScreenProps } from '@react-navigation/stack';

// Libs
import database from '@react-native-firebase/database';
/* Components */
import MyServiceDetailOrganism from 'components/organisms/my-services/MyServiceDetail';
/* Configs */
import { NAME_ROUTE } from 'configs/Constants';
/* Utils */
import { RootStackParamList } from 'utils/helpers/Navigation';
import { RequestModel } from 'models/service/Request';
import { View } from 'react-native';
import SpinnerLottie from 'components/atoms/spinners';
import { useFocusEffect } from '@react-navigation/native';
import { ServiceRequestModel, ServiceResponseModel } from 'models/service';
import { ServicesAdditional } from 'models/enums/ServiceAdditional';
import { BagsValues } from 'models/bag';
import { ClothesPrices, ClothesValues } from 'models/clothes';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.myServiceDetail>;

const MyServiceDetailScene = ({ route, navigation }: sceneProps) => {
  /* States */
  const [request, setRequest] = useState<RequestModel>({} as RequestModel);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [total, setTotal] = useState(0);
  const [bags, setBags] = useState<{ label: string; price: number | undefined }[]>([]);
  const [clothes, setClothes] = useState<{ label: string; price: number | undefined }[]>([]);
  const [servicioAdicional, setServicioAdicional] = useState<{ label: string; price: number | undefined }[]>([]);

  useEffect(() => {
    const loadAndSetData = () => {
      if (route.params) {
        const { request } = route.params;
        console.log('request.id: ', request);
        setRequest(request);
        determineTotal(request);
        setIsLoading(false);
      }
    };
    return loadAndSetData();
  }, [route, navigation]);

  const determineTotal = (bagInfo: ServiceRequestModel) => {
    let _total = 0;
    let _bags: { label: string; price: number | undefined }[] = [];
    let _clothes: { label: string; price: number | undefined }[] = [];
    let _servicioAdicional: { label: string; price: number | undefined }[] = [];
    bagInfo.services?.forEach(s => {
      switch (s.type) {
        case 'BULKY_ITEMS':
          s.clothes?.forEach(c => {
            if (c.type) {
              _total += (ClothesPrices[c.type] as number) * (c.quantity as number);
            }
            _clothes.push({
              label: `${ClothesValues[c.type]} x ${c.quantity}`,
              price: c.type ? (ClothesPrices[c.type] as number) * (c.quantity as number) : 0
            });
          });
          break;
        case 'DRY_CLEANING':
          s.clothes?.forEach(c => {
            if (c.price) {
              _total += (c.price as number) * (c.quantity as number);
            }
            _clothes.push({
              label: `${c.name} x ${c.quantity}`,
              price: (c.price as number) * (c.quantity as number)
            });
          });
          break;
        case 'FOLDING':
          s.bags?.forEach(c => {
            if (c.type) {
              _total += (ClothesPrices[c.type] as number) * (c.quantity as number);
            }
            _bags.push({
              label: `${ClothesValues[c.type]} x ${c.quantity}`,
              price: c.type ? (ClothesPrices[c.type] as number) * (c.quantity as number) : 0
            });
          });
          break;
        case 'WASH_AND_FOLD':
          s.bags?.forEach(c => {
            if (c.type) {
              _total += (c.type.price.value as number) * (c.quantity as number);
            }
            _bags.push({
              label: `${BagsValues[c.type.value]} x ${c.quantity}`,
              price: (c.type.price.value as number) * (c.quantity as number)
            });
          });
          break;
        default:
          break;
      }
      s.serviceAdditionals?.forEach(sa => {
        const servicioAdicional = ServicesAdditional.find(service => service.name === sa);
        if (servicioAdicional) {
          _total += servicioAdicional.price;
          _servicioAdicional.push({
            label: `${servicioAdicional.label}`,
            price: servicioAdicional.price
          });
        }
      });
    });
    setTotal(_total);
    setBags(_bags);
    setClothes(_clothes);
    setServicioAdicional(_servicioAdicional);
  };

  return isLoading ? (
    <View style={{ justifyContent: 'center', alignItems: 'center', width: '100%' }}>
      <SpinnerLottie />
    </View>
  ) : (
    <MyServiceDetailOrganism {...{ request, total, bags, clothes, servicioAdicional }} />
  );
};

export default MyServiceDetailScene;
