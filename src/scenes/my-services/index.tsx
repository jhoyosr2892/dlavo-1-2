/* Libs */
import React, { useEffect, useState } from 'react';
import { StackScreenProps } from '@react-navigation/stack';

// Libs
import database from '@react-native-firebase/database';
/* Components */
import MyServiceOrganism from 'components/organisms/my-services';
/* Models */
import { ServiceModel, ServiceResponseModel } from 'models/service';
/* Utils */
import { RootStackParamList } from 'utils/helpers/Navigation';
/* Configs */
import { NAME_ROUTE } from 'configs/Constants';
/* Services */
import { getServiceRequests } from '@services/app/ServiceRequest';
import { RequestModel, RequestTypes } from 'models/service/Request';
import { ServiceRequestResponseServer } from 'models/service/RequestService';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.myServices>;

const MyServicesScene = ({ route, navigation }: sceneProps) => {
  const [services, setServices] = useState<RequestModel[]>();
  const [refreshing, setRefreshing] = useState(true);

  useEffect(() => {
    const loadServices = async () => {
      getServices();
    };
    loadServices();
  }, [route, navigation]);

  const getServices = async (type?: RequestTypes) => {
    setRefreshing(true);
    try {
      const { status, data } = await getServiceRequests(type);
      if (status) {
        console.log('data: \n', data);
        const _requests: RequestModel[] = data;
        if (_requests.length !== services?.length) {
          for (const request of _requests) {
            const snapshot = await database()
              .ref('services')
              .orderByChild('request')
              .equalTo(request.id as string)
              .once('value');
            if (snapshot.val() !== null) {
              const response = Object.values<ServiceResponseModel>(snapshot.val());
              request.services = response;
            }
          }
          console.log(_requests);
          setServices(_requests.reverse());
        }
      }
    } catch (e) {
      console.log(e);
    }
    setRefreshing(false);
  };

  return <MyServiceOrganism services={services} getServices={getServices} refreshing={refreshing} />;
};

export default MyServicesScene;
