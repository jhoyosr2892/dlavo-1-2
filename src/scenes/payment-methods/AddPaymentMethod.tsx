import React, { useContext, useEffect, useState } from 'react';
import { StackScreenProps } from '@react-navigation/stack';

/* Components */
import AddPaymentMethodOrganism from '@organisms/payment-methods/AddPaymentMethod';
/* Helpers */
import { navigationScreen, RootStackParamList } from 'utils/helpers/Navigation';
/* Configs */
import { NAME_ROUTE } from 'configs/Constants';
/* Store */
import { PaymentMethodModel } from 'models/payment';
import { RootContext } from 'configs/ContextApp';
import { View } from 'react-native';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.addPaymentMethod>;

const AddPaymentMethodScene = ({ navigation, route }: sceneProps) => {
  /* Context */
  const { showLoading, hideLoading } = useContext(RootContext);
  /* States */
  const [paymentInfo, setPaymentInfo] = useState<PaymentMethodModel>({ cardCvs: '', cardExpirationDate: '', cardNumber: '' });
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    const loadServices = async () => {
      if (route.params) {
        const { paymentMethod } = route.params;
        if (paymentMethod) {
          setPaymentInfo(paymentMethod);
        }
      }
      setLoading(false);
    };
    loadServices();
  }, [route, navigation]);

  const onAddPaymentMethod = async ({ cardNumber, cardCvs, cardExpirationDate, name }: PaymentMethodModel) => {
    showLoading();
    if (route.params) {
      const { backRoute } = route.params;
      console.log('name: ', name);

      navigationScreen(backRoute, {
        ...route.params,
        paymentMethod: {
          cardNumber: cardNumber?.replace(/\s/g, ''),
          cardCvs,
          cardExpirationDate,
          name
        }
      });
    } else {
      navigationScreen(NAME_ROUTE.payment);
    }
    hideLoading();
  };

  return loading ? <View></View> : <AddPaymentMethodOrganism {...{ addPaymentMethod: onAddPaymentMethod, paymentInfo: paymentInfo }} />;
};

export default AddPaymentMethodScene;
