import React, { useEffect, useState } from 'react';
import { StackScreenProps } from '@react-navigation/stack';

/* Components */
import PaymentMethodsOrganism from '@organisms/payment-methods';
/* Helpers */
import { RootStackParamList } from 'utils/helpers/Navigation';
/* Configs */
import { NAME_ROUTE } from 'configs/Constants';
/* Models */
import { AddressModel } from 'models/address';
/* Store */
import { deletePaymentById, getPaymentMethods, selectPayment } from 'stores/PaymentMethod';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.payment>;

const PaymentMethodsScene = ({ navigation, route }: sceneProps) => {
  /* States */
  const [paymentMethods, setPaymentMethods] = useState<AddressModel[]>([]);

  useEffect(() => {
    loadData();
  }, [route, navigation]);

  const loadData = () => {
    const unsubscribe = navigation.addListener('focus', getSavedPaymentMethods);
    return unsubscribe;
  };

  const getSavedPaymentMethods = async () => {
    const paymentMethodsSaved = await getPaymentMethods();
    setPaymentMethods(paymentMethodsSaved);
  };

  const deleteMethod = async (index: number) => {
    if (paymentMethods[index].id !== undefined) {
      const isDelete = await deletePaymentById(paymentMethods[index].id);
      if (isDelete) {
        const copy = [...paymentMethods];
        copy.splice(index, 1);
        setPaymentMethods(copy);
      }
    }
  };

  const onSelectMethod = async (index: number) => {
    if (paymentMethods[index].id !== undefined) {
      const isUpdate = await selectPayment(paymentMethods[index].id);
      if (isUpdate) {
        getSavedPaymentMethods();
      }
    }
  };

  return <PaymentMethodsOrganism {...{ paymentMethods, deleteMethod, onSelectMethod }} />;
};

export default PaymentMethodsScene;
