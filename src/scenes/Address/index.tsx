import React, { useContext, useEffect, useState } from 'react';
import { StackScreenProps } from '@react-navigation/stack';

/* Components */
import AddressOrganism from '@organisms/address';
/* Helpers */
import { RootStackParamList } from 'utils/helpers/Navigation';
/* Configs */
import { EnumToast, NAME_ROUTE } from 'configs/Constants';
/* Models */
import { AddressModel } from 'models/address';
/* Store */
import { deleteAddressById, getAddress, selectAddress } from 'stores/Address';
import { RootContext } from 'configs/ContextApp';
import { deleteAddress } from 'services/app/Address';
import { translate } from 'locales';
import { useFocusEffect } from '@react-navigation/native';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.address>;

const AddressScene = ({ navigation, route }: sceneProps) => {
  /* Context */
  const { showLoading, hideLoading, showToast } = useContext(RootContext);
  const [listAddress, setListAddress] = useState<AddressModel[]>([]);
  const [backRoute, setBackRoute] = useState<string>('');

  useFocusEffect(() => {
    navigation.addListener('focus', getSavedAddress);
    if (route.params) {
      const { backRoute } = route.params;
      if (backRoute) {
        setBackRoute(backRoute);
      }
    }
  });

  const getSavedAddress = async () => {
    const addressSaved = await getAddress();
    setListAddress(addressSaved);
  };

  const onDeleteAddress = async (index: number) => {
    showLoading();
    try {
      if (listAddress[index].id !== undefined) {
        const { status, message } = await deleteAddress(listAddress[index].id as string);
        if (status) {
          const isDelete = await deleteAddressById(listAddress[index].id);
          if (isDelete) {
            const arrayAddress = [...listAddress];
            arrayAddress.splice(index, 1);
            setListAddress(arrayAddress);
          }
          showToast({ text: translate('ADDRESS_SCENE.TOAST.ADDRESS_DELETED'), type: EnumToast.success });
        } else {
          showToast({ text: message, type: EnumToast.error });
        }
      }
    } catch (err) {}
    hideLoading();
  };

  const onSelectAddress = async (index: number) => {
    showLoading();
    if (listAddress[index].id !== undefined) {
      const isUpdate = await selectAddress(listAddress[index].id);
      if (isUpdate) {
        getSavedAddress();
        if (backRoute !== '') {
          navigation.goBack();
        }
      }
    }
    hideLoading();
  };

  return <AddressOrganism {...{ addresses: listAddress, deleteAddress: onDeleteAddress, onSelectAddress, backRoute }} />;
};

export default AddressScene;
