import React, { useContext, useEffect, useState } from 'react';

/* Libs */
import Geolocation from 'react-native-geolocation-service';
import uuid from 'react-native-uuid';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { StackScreenProps } from '@react-navigation/stack';
/* Components */
import AddressModalOrganism from '@organisms/address/EnterAddress';
/* Configs */
import { EnumToast, NAME_ROUTE } from 'configs/Constants';
import { RootContext } from 'configs/ContextApp';
/* Helpers */
import { navigationScreen, RootStackParamList } from '@helpers/Navigation';
import { getPositionGPS } from '@helpers/Location';
/* Models */
import { AddressModel } from 'models/address';
/* Stores */
import { addAddress, getAddressSelected } from 'stores/Address';
import { translate } from 'locales';
import { createAddress } from 'services/app/Address';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.enterAddress>;

const EnterAddressScene = ({ navigation, route }: sceneProps) => {
  /* Context */
  const { showLoading, hideLoading, showToast } = useContext(RootContext);
  /* State */
  const [loadingData, setLoadingData] = useState<boolean>(true);
  const [address, setAddress] = useState<AddressModel>();
  const [lat, setLatitude] = useState<number>(4.6007172);
  const [lng, setLongitude] = useState<number>(-74.1146825);
  const [showSearch, setShowSearch] = useState(false);

  useEffect(() => {
    loadData();
  }, [navigation, route]);

  const loadData = async () => {
    if (route.params) {
      const { address, latitude, longitude } = route.params;
      if (address) {
        setAddress(address);
        setLatitude(latitude);
        setLongitude(longitude);
      } else {
        setShowSearch(true);
        await getCurrentLocation();
      }
    } else {
      await getCurrentLocation();
    }
    setLoadingData(false);
  };

  const getCurrentLocation = async () => {
    const currentPosition = await AsyncStorage.getItem('currentPosition');
    if (currentPosition) {
      const parsed = JSON.parse(currentPosition);
      parsed.id = uuid.v4();
      setAddress(parsed);
    } else {
      showLoading();
      const position = await getPositionGPS();
      if (position) {
        const geoOptions = position as Geolocation.GeoPosition;
        const currentPositionAddress: AddressModel = {
          id: uuid.v4(),
          address: '',
          additionalInstructions: '',
          city: ''
        };
        setLatitude(geoOptions.coords.latitude);
        setLongitude(geoOptions.coords.longitude);
        setAddress(currentPositionAddress);
        AsyncStorage.setItem('currentPosition', JSON.stringify(currentPositionAddress));
      }
      hideLoading();
    }
  };

  const onAddAddress = async () => {
    if (address && validAddress(address)) {
      showLoading();
      address.id = uuid.v4();
      // if (!address.id) {
      // }
      if (!address.selected) {
        address.selected = false;
      }
      const response = await createAddress(address);
      if (response.status) {
        try {
          const addressSaved = await getAddressSelected();
          // AsyncStorage.removeItem('bag');
          if (!addressSaved) {
            address.selected = true;
          }
          const newAddress = await addAddress(address);
          if (newAddress) {
            showToast({ text: translate('ENTER_ADDRESS.TOAST.ADDRESS_CREATED'), type: EnumToast.success });
          } else {
            console.log(newAddress);
          }
        } catch {
          showToast({ text: response.message, type: EnumToast.error });
        }
        navigationScreen(NAME_ROUTE.address);
      } else {
        showToast({ text: response.message, type: EnumToast.error });
        console.log(response);
      }
      hideLoading();
    }
  };

  const validAddress = (address: AddressModel) => {
    if (!address.address) {
      showToast({ text: translate('ENTER_ADDRESS.ERRORS.ADDRESS_IS_REQUIRED'), type: EnumToast.error });
      return;
    }
    if (!address.city) {
      showToast({ text: translate('ENTER_ADDRESS.ERRORS.CITY_IS_REQUIRED'), type: EnumToast.error });
      return;
    }
    if (!address.fullZipCode) {
      showToast({ text: translate('ENTER_ADDRESS.ERRORS.ZIP_CODE_IS_REQUIRED'), type: EnumToast.error });
      return;
    }
    if (address.fullZipCode.length < 5) {
      showToast({ text: translate('ENTER_ADDRESS.ERRORS.ZIP_CODE_IS_INVALID'), type: EnumToast.error });
      return;
    }
    return true;
  };

  return <AddressModalOrganism {...{ address, setAddress, addAddress: onAddAddress, lat, lng, setLatitude, setLongitude, showSearch }} />;
};

export default EnterAddressScene;
