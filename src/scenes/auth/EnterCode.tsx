import React, { useContext, useEffect, useState } from 'react';
import { StyleSheet } from 'react-native';
/* Components */
import EnterCodeOrganism from '@organisms/auth/EnterCode';
import { AuthContext, RootContext } from 'configs/ContextApp';
import { EnumToast, NAME_ROUTE } from 'configs/Constants';
import { navigationScreen, RootStackParamList } from 'utils/helpers/Navigation';
import { translate } from 'locales';
import { addNotificationToken, getCurrentUser, validateCode } from 'services/app/User';
import { StackScreenProps } from '@react-navigation/stack';
import { requestCode } from 'services/app/Auth';
import { UserResponseServer } from 'models/user/User';
import { setTokenUserSession } from 'stores/Session';
import { loadUserAddress } from 'services/app/Address';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.enterCode>;

const EnterCodeScene = ({ route, navigation }: sceneProps) => {
  /* States */
  const [email, setEmail] = useState('');
  const [isLogin, setIsLogin] = useState<boolean>();
  const [user, setUser] = useState<UserResponseServer>();
  /* Context */
  const { showLoading, showToast, hideLoading } = useContext(RootContext);
  const { authFunction } = useContext(AuthContext);

  useEffect(() => {
    const loadData = () => {
      if (route.params) {
        const { user, isLogin }: { user: UserResponseServer; isLogin: boolean } = route.params;
        if (user && user.email) {
          setEmail(user.email);
          setUser(user);
        }
        setIsLogin(isLogin);
      }
    };
    return loadData();
  }, [route, navigation]);

  const sendCode = async (code: string) => {
    showLoading();
    try {
      const { status, data, message } = await validateCode(code);
      if (status) {
        if (isLogin) {
          const { message, status } = await getCurrentUser(data.token);
          if (!status) {
            showToast({ text: message, type: EnumToast.error });
          }
          await setTokenUserSession(data.token);
          await loadUserAddress();
          
          await addNotificationToken((Math.random() + 1).toString(36).substring(7));
          authFunction.signIn('', '');
        } else {
          navigationScreen(NAME_ROUTE.enterName, { user, token: data.token });
        }
      } else {
        showToast({ text: message, type: EnumToast.error });
      }
    } catch (e) {
      showToast({ text: translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION'), type: EnumToast.error });
    }
    hideLoading();
  };

  const reSendCode = async () => {
    showLoading();
    try {
      const response = await requestCode(email);
      if (response.status) {
        showToast({ text: translate('ENTER_CODE_SCENE.TOAST.CODE_SENT_SUCCESSFULLY'), type: EnumToast.success });
        navigationScreen(NAME_ROUTE.enterCode, { email });
      } else {
        showToast({ text: response.message, type: EnumToast.error });
      }
    } catch (e) {
      showToast({ text: translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION'), type: EnumToast.error });
    }
    hideLoading();
  };

  return <EnterCodeOrganism {...{ sendCode, reSendCode }} />;
};

export default EnterCodeScene;

const styles = StyleSheet.create({});
