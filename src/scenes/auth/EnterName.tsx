import React, { useContext, useEffect, useMemo, useState } from 'react';
import { StyleSheet } from 'react-native';
/* Components */
import EnterNameOrganism from '@organisms/auth/EnterName';
import { RootStackParamList } from 'utils/helpers/Navigation';
import { NAME_ROUTE } from 'configs/Constants';
import { StackScreenProps } from '@react-navigation/stack';
import { AuthContext } from 'configs/ContextApp';
import { updateUser } from 'services/app/User';
import { UserResponseServer } from 'models/user/User';
import UserStorage, { setUserData } from 'stores/UserState';
import { setTokenUserSession } from 'stores/Session';
import { loadUserAddress } from 'services/app/Address';

// Props
type EnterNameSceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.enterName>;

const EnterNameScene = ({ route, navigation }: EnterNameSceneProps) => {
  const { authFunction } = useContext(AuthContext);
  const [user, setUser] = useState<UserResponseServer>();
  const [currentToken, setToken] = useState<string>('');

  useEffect(() => {
    const loadData = () => {
      if (route.params) {
        const { user, token }: { user: UserResponseServer; token: string } = route.params;
        if (user) {
          setUser(user);
        }
        setToken(token);
      }
    };
    loadData();
  }, [route, navigation]);

  const onSendName = async (name: string, lastName: string) => {
    const response = await updateUser({ firstName: name, lastName }, currentToken);
    if (response.status) {
      if (user) {
        user.firstName = name;
        user.lastName = lastName;
        setUserData(user);
      }
      await setTokenUserSession(currentToken);
      await loadUserAddress();
      authFunction.signIn('', '');
    }
  };

  return <EnterNameOrganism sendName={onSendName} />;
};

export default EnterNameScene;

const styles = StyleSheet.create({});
