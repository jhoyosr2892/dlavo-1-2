import React, { useContext } from 'react';
import { StyleSheet } from 'react-native';
/* Components */
import LoginOrganism from 'components/organisms/auth/Login';
/* Configs */
import { AuthContext, RootContext } from 'configs/ContextApp';
import { requestCode } from 'services/app/Auth';
import { EnumToast, NAME_ROUTE } from 'configs/Constants';
import { navigationScreen } from 'utils/helpers/Navigation';
import { translate } from 'locales';
import { UserResponseServer } from 'models/user/User';

const LoginScene = () => {
  const { showLoading, showToast, hideLoading } = useContext(RootContext);

  const login = async (email: string) => {
    showLoading();
    try {
      const { status, data, message } = await requestCode(email.trim());
      if (status) {
        showToast({ text: translate('REGISTER_EMAIL_SCENE.TOAST.CODE_REQUESTED') });
        const user: UserResponseServer = {
          id: data.id
        };
        navigationScreen(NAME_ROUTE.enterCode, { email, isLogin: true });
      } else {
        showToast({ text: message, type: EnumToast.error });
      }
    } catch (e) {
      showToast({ text: translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION'), type: EnumToast.error });
    }
    hideLoading();
  };

  return <LoginOrganism {...{ login }} />;
};

export default LoginScene;

const styles = StyleSheet.create({});
