// Libs
import React, { useMemo, useContext } from 'react';

// Contexts
import { AuthContext } from '@config/ContextApp';

// Organisms
import AuthOrganisms from '@organisms/auth';

const AuthScene = () => {
  const { authFunction } = useContext(AuthContext);

  const loginMemo = useMemo(
    () => ({
      onLogin: async (email: string, password: string) => {
        authFunction.signIn(email, password);
      }
    }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return <AuthOrganisms login={loginMemo.onLogin} />;
};

export default AuthScene;
