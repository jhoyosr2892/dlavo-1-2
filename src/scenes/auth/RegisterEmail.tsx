import React, { useContext } from 'react';
import { StyleSheet } from 'react-native';
/* Components */
import RegisterEmailOrganism from '@organisms/auth/RegisterEmail';
import { navigationScreen } from 'utils/helpers/Navigation';
import { EnumToast, NAME_ROUTE } from 'configs/Constants';
import { RootContext } from 'configs/ContextApp';
import { register } from 'services/app/User';
import { translate } from 'locales';
import { UserResponseServer } from 'models/user/User';

const RegisterEmailScene = () => {
  // Context
  const { showLoading, hideLoading, showToast } = useContext(RootContext);

  const sendEmail = async (email: string) => {
    showLoading();
    try {
      const { status, data, message } = await register(email.trim());
      if (status) {
        showToast({ text: translate('REGISTER_EMAIL_SCENE.TOAST.CODE_REQUESTED') });
        const user: UserResponseServer = {
          id: data.id
        };
        console.log('data in register: ', data);
        navigationScreen(NAME_ROUTE.enterCode, { user });
      } else {
        showToast({ text: message, type: EnumToast.error });
      }
    } catch (e) {
      showToast({ text: translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION'), type: EnumToast.error });
    }
    hideLoading();
  };

  return <RegisterEmailOrganism {...{ sendEmail }} />;
};

export default RegisterEmailScene;

const styles = StyleSheet.create({});
