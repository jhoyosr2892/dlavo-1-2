import React, { useContext, useEffect, useState } from 'react';
import { StackScreenProps } from '@react-navigation/stack';

/* Components */
import DetailMembershipOrganism from 'components/organisms/profile/membership/Detail';
/* Helpers */
import { navigationScreen, RootStackParamList } from 'utils/helpers/Navigation';
/* Configs */
import { EnumToast, NAME_ROUTE } from 'configs/Constants';
import { BuyProducts, MembershipModel } from 'models/membership';
import { PaymentMethodModel, ResponsePaymentMethodModel } from 'models/payment';
import { RootContext } from 'configs/ContextApp';
import { buyMembership } from 'services/app/Membership';
import { translate } from 'locales';

// Props
type sceneProps = StackScreenProps<RootStackParamList, NAME_ROUTE.pricingDetail>;

const DetailMembershipScene = ({ navigation, route }: sceneProps) => {
  /* States */
  const [loadingData, setLoadingData] = useState<boolean>(true);
  const [_membership, setMembership] = useState<MembershipModel>({} as MembershipModel);
  const [paymentMethod, setPaymentMethod] = useState<ResponsePaymentMethodModel>({} as ResponsePaymentMethodModel);
  /* Context */
  const { showLoading, hideLoading, showToast } = useContext(RootContext);

  useEffect(() => {
    if (route.params) {
      const { membership, paymentMethod }: { membership: MembershipModel; paymentMethod: PaymentMethodModel } = route.params;
      setMembership(membership);
      if (paymentMethod) {
        console.log(paymentMethod);

        setPaymentMethod({
          creditCardNumber: paymentMethod.cardNumber ?? '',
          cvv: paymentMethod.cardCvs ? parseInt(paymentMethod.cardCvs, 10) : 0,
          expirationDate: paymentMethod.cardExpirationDate ?? '',
          name: paymentMethod.name ?? ''
        });
      }
    }
    setLoadingData(false);
  }, [navigation, route]);

  const _buyMembership = async (infoBuyProduct: BuyProducts) => {
    showLoading();
    try {
      const { status, message } = await buyMembership(infoBuyProduct);
      if (status) {
        showToast({ text: translate('MEMBERSHIP_SCENE.TOAST.MEMBERSHIP_BUY'), type: EnumToast.success });
        navigationScreen(NAME_ROUTE.pricing);
      } else {
        showToast({ text: message, type: EnumToast.error });
      }
    } catch (e) {
      showToast({ text: translate('GENERAL.ERRORS.GENERAL_ERROR_ACTION'), type: EnumToast.error });
    }
    hideLoading();
  };

  return <DetailMembershipOrganism membership={_membership} {...{ loadingData, buyMembership: _buyMembership, paymentMethod }} />;
};

export default DetailMembershipScene;
