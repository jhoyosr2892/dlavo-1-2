import { translate } from 'locales';
import { Domain } from '.';

export type TypePicker = 'AT_DOOR' | 'AT_LOCKER' | 'AT_ENTRANCE' | 'AT_RECEPTION';

export const TypePickerNamesArray: Domain<TypePicker>[] = [
  {
    id: 'AT_DOOR',
    name: translate('ENUMS.TYPE_PICKER.AT_DOOR')
  },
  {
    id: 'AT_LOCKER',
    name: translate('ENUMS.TYPE_PICKER.AT_LOCKER')
  },
  {
    id: 'AT_ENTRANCE',
    name: translate('ENUMS.TYPE_PICKER.AT_ENTRANCE')
  },
  {
    id: 'AT_RECEPTION',
    name: translate('ENUMS.TYPE_PICKER.AT_RECEPTION')
  }
];

export type TypePickerEnum = {
  AT_DOOR?: string;
  AT_LOCKER?: string;
  AT_ENTRANCE?: string;
  AT_RECEPTION?: string;
};
export const TypePickerNames: TypePickerEnum = {
  AT_DOOR: translate('ENUMS.TYPE_PICKER.AT_DOOR'),
  AT_LOCKER: translate('ENUMS.TYPE_PICKER.AT_LOCKER'),
  AT_ENTRANCE: translate('ENUMS.TYPE_PICKER.AT_ENTRANCE'),
  AT_RECEPTION: translate('ENUMS.TYPE_PICKER.AT_RECEPTION')
};
export type TypePickerEnumObject = {
  AT_DOOR?: Domain<TypePicker>;
  AT_LOCKER?: Domain<TypePicker>;
  AT_ENTRANCE?: Domain<TypePicker>;
  AT_RECEPTION?: Domain<TypePicker>;
};
export const TypePickerNamesObject: TypePickerEnumObject = {
  AT_DOOR: { id: 'AT_DOOR', name: translate('ENUMS.TYPE_PICKER.AT_DOOR') },
  AT_LOCKER: { id: 'AT_LOCKER', name: translate('ENUMS.TYPE_PICKER.AT_LOCKER') },
  AT_ENTRANCE: { id: 'AT_ENTRANCE', name: translate('ENUMS.TYPE_PICKER.AT_ENTRANCE') },
  AT_RECEPTION: { id: 'AT_RECEPTION', name: translate('ENUMS.TYPE_PICKER.AT_RECEPTION') }
};
