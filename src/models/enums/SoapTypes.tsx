import { translate } from 'locales';

export type SOAPS = 'LIQUID_SOAP' | 'POWDER_SOAP' | 'SHEETS_SOAP';

export type SoapType = {
  id: SOAPS;
  name: string;
  price: number;
  label: string;
  value: SOAPS;
};

export const SOAP_TYPES_NAMES: SoapType[] = [
  {
    id: 'LIQUID_SOAP',
    name: translate('SPECIAL_SERVICES_OR_CARES.LIQUID_SOAP'),
    price: 0,
    label: translate('SPECIAL_SERVICES_OR_CARES.LIQUID_SOAP'),
    value: 'LIQUID_SOAP'
  },
  {
    id: 'POWDER_SOAP',
    name: translate('SPECIAL_SERVICES_OR_CARES.POWDER_SOAP'),
    price: 0,
    label: translate('SPECIAL_SERVICES_OR_CARES.POWDER_SOAP'),
    value: 'POWDER_SOAP'
  },
  {
    id: 'SHEETS_SOAP',
    name: translate('SPECIAL_SERVICES_OR_CARES.SHEETS_SOAP'),
    price: 0,
    label: translate('SPECIAL_SERVICES_OR_CARES.SHEETS_SOAP'),
    value: 'SHEETS_SOAP'
  }
];

export type SoapTypes = {
  LIQUID_SOAP: string;
  POWDER_SOAP: string;
  SHEETS_SOAP: string;
};

export const SOAP_TYPES_I18: SoapTypes = {
  LIQUID_SOAP: translate('SPECIAL_SERVICES_OR_CARES.LIQUID_SOAP'),
  POWDER_SOAP: translate('SPECIAL_SERVICES_OR_CARES.POWDER_SOAP'),
  SHEETS_SOAP: translate('SPECIAL_SERVICES_OR_CARES.SHEETS_SOAP')
};
