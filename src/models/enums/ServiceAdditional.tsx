import { translate } from 'locales';

export type TypeServiceAdditional = 'IRONING' | 'REMOVE_STAIN';

export interface ServiceAdditional {
  name: TypeServiceAdditional;
  label: string;
  labelPrice: string;
  price: number;
}

export const ServicesAdditional: ServiceAdditional[] = [
  {
    name: 'IRONING',
    label: translate('SERVICE_ADDITIONAL.IRONING'),
    labelPrice: '15$',
    price: 15
  },
  {
    name: 'REMOVE_STAIN',
    label: translate('SERVICE_ADDITIONAL.REMOVE_STAIN'),
    labelPrice: '9$',
    price: 9
  }
];
