import { translate } from 'locales';

export type SPECIAL_SERVICE_CARES = 'IRONING' | 'REMOVE_STAIN';

export type TYPES_SPECIAL_SERVICE_CARES = {
  id: SPECIAL_SERVICE_CARES;
  name: string;
  price: number;
};

export const SPECIAL_SERVICE_CARES_NAMES: TYPES_SPECIAL_SERVICE_CARES[] = [
  // {
  //   id: 'IRONING',
  //   name: translate('SPECIAL_SERVICES_OR_CARES.IRONING'),
  //   price: 15
  // },
  {
    id: 'REMOVE_STAIN',
    name: translate('SPECIAL_SERVICES_OR_CARES.REMOVE_STAIN'),
    price: 9
  }
];

export type SpecialServiceCaresType = {
  IRONING: string;
  REMOVE_STAIN: string;
};

export const SPECIAL_SERVICE_CARES_I18: SpecialServiceCaresType = {
  IRONING: translate('SPECIAL_SERVICES_OR_CARES.IRONING'),
  REMOVE_STAIN: translate('SPECIAL_SERVICES_OR_CARES.REMOVE_STAIN')
};
