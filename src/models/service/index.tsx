// Libs
import Realm from 'realm';
// Models
import { typeClothes } from 'models/clothes';
import { SERVICE_CATEGORY } from 'models/enums/ServiceCategory';
import { SPECIAL_SERVICE_CARES } from 'models/enums/special-services-cares';
import { SOAPS } from 'models/enums/SoapTypes';
import User from 'models/user/User';
import { RequestTypes } from './Request';
import { bagTypes, typeBags } from 'models/bag';

export const SERVICE_NAME = 'Service';

export interface ServiceRequestModel {
  id: string;
  address: string;
  uniquePayment?: boolean;
  creditCardNumber?: string;
  cvv?: number;
  expirationDate?: string;
  services: ServiceModel[];
  couponCode?: string;
}

export type Frequency = 'single' | 'frequent';
export type Concurrence = 'SINGLE_TIME' | 'WEEKLY' | 'MONTHLY';

export type ServiceModel = {
  id?: string;
  type?: SERVICE_CATEGORY;
  serviceCategory: SERVICE_CATEGORY;
  clothes?: typeQuantity[];
  clothesInfo?: typeQuantity[];
  soapTypes?: SOAPS[];
  serviceAdditionals?: SPECIAL_SERVICE_CARES[];
  bags?: typeQuantity[];
  concurrence?: Concurrence;
  pickUpType?: string;
  pickUpSingleTime?: number;
  deliverySingleTime?: number;
  weeklyDays?: number[];
  monthDays?: number[];
  additionalNotes?: additionalNotes;
  frequency?: Frequency;
  remainingRepetitions?: number;
  repetitions: number;
};

export type ServiceResponseModel = {
  id: string;
  serviceCategory: SERVICE_CATEGORY;
  clothes: clothe[];
  bags: typeBags[];
  additionalNotes?: {
    specializedInstructions: string;
    stateOfClothe: string;
  };
  concurrence?: string;
  deliverySingleTime?: string;
  pickUpSingleTime?: string;
  pickUpType?: string;
  request?: string;
  serviceAdditionals?: {
    price: { value: number };
    value: string;
  }[];
  soapTypes?: string[];
  type?: string;
};
export interface clothe {
  type: typeClothes;
  specialServicesOrCares: SPECIAL_SERVICE_CARES[];
}

export interface ServiceRequestResponseServer {
  id: string;
  address: any;
  status: RequestTypes;
  createdAt: string;
  createdBy: User;
}

export interface typeQuantity {
  type: string;
  name?: string;
  image?: string;
  quantity?: number;
  price?: number;
}
export interface additionalNotes {
  specializedInstructions: string;
  stateOfClothe: string;
  photos: any[];
}
export interface weeklyDay {
  weekDay: number;
  deliveryTime: string;
  pickUpTime: string;
}
export interface monthDay {
  monthDay: number;
  deliveryTime: string;
  pickUpTime: string;
}

export const ClotheSchema: Realm.ObjectSchema = {
  name: 'Clothe',
  properties: {
    type: 'string',
    quantity: 'int'
  }
};

export const ServiceSchema: Realm.ObjectSchema = {
  name: SERVICE_NAME,
  primaryKey: 'id',
  properties: {
    id: 'string',
    serviceCategory: 'string',
    clothes: 'Clothe[]'
  }
};

class Service implements ServiceModel {
  public static schema: Realm.ObjectSchema = ServiceSchema;

  id: string;
  serviceCategory: SERVICE_CATEGORY;
  clothes?: typeQuantity[];

  constructor(data: ServiceModel) {
    if (data) {
      this.id = data.id ?? '';
      this.serviceCategory = data.serviceCategory;
      this.clothes = data.clothes;
    } else {
      this.id = '';
      this.serviceCategory = 'DRY_CLEANING';
      this.clothes = [];
    }
  }
}

export default Service;
