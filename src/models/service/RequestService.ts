// Libs
import Realm from 'realm';
// Models
import { SERVICE_CATEGORY } from 'models/enums/ServiceCategory';
import { SPECIAL_SERVICE_CARES } from 'models/enums/special-services-cares';
import { SOAPS } from 'models/enums/SoapTypes';
import User from 'models/user/User';
import { RequestTypes } from './Request';

export const SERVICE_NAME = 'Service';

export type ServiceRequestModel = {
  id: string;
  address: string;
  uniquePayment?: boolean;
  creditCardNumber?: string;
  cvv?: number;
  expirationDate?: string;
  services: ServiceModel[];
  couponCode?: string;
}

export type Frequency = 'single' | 'frequent';
export type Concurrence = 'SINGLE_TIME' | 'WEEKLY' | 'MONTHLY';

export type ServiceModel = {
  id?: string;
  type?: SERVICE_CATEGORY;
  serviceCategory: SERVICE_CATEGORY;
  clothes: typeQuantity[];
  clothesInfo?: typeQuantity[];
  soapTypes?: SOAPS[];
  serviceAdditionals?: SPECIAL_SERVICE_CARES[];
  bags?: typeQuantity[];
  concurrence?: Concurrence;
  pickUpType?: string;
  pickUpSingleTime?: number;
  deliverySingleTime?: number;
  weeklyDays?: weeklyDay[];
  monthDays?: monthDay[];
  additionalNotes?: additionalNotes;
  frequency?: Frequency;
};

export interface ServiceRequestResponseServer {
  id: string;
  address: any;
  status: RequestTypes;
  createdAt: string;
  createdBy: User;
}

export interface typeQuantity {
  type: string;
  name?: string;
  image?: string;
  quantity?: number;
  price?: number;
}
export interface additionalNotes {
  specializedInstructions: string;
  stateOfClothe: string;
  photos: any[];
}
export interface weeklyDay {
  weekDay: number;
  deliveryTime: string;
  pickUpTime: string;
}
export interface monthDay {
  monthDay: number;
  deliveryTime: string;
  pickUpTime: string;
}

export const ClotheSchema: Realm.ObjectSchema = {
  name: 'Clothe',
  properties: {
    type: 'string',
    quantity: 'int'
  }
};

export const ServiceSchema: Realm.ObjectSchema = {
  name: SERVICE_NAME,
  primaryKey: 'id',
  properties: {
    id: 'string',
    serviceCategory: 'string',
    clothes: 'Clothe[]'
  }
};

class RequestService implements ServiceRequestModel {
  public static schema: Realm.ObjectSchema = ServiceSchema;

  id: string;
  address: string;
  uniquePayment?: boolean;
  creditCardNumber?: string;
  cvv?: number;
  expirationDate?: string;
  services: ServiceModel[];
  couponCode?: string;

  constructor(data: ServiceRequestModel) {
    if (data) {
      this.id = data.id ?? '';
      this.address = data.address;
      this.uniquePayment = data.uniquePayment;
      this.creditCardNumber = data.creditCardNumber;
      this.cvv = data.cvv;
      this.expirationDate = data.expirationDate;
      this.services = data.services;
      this.couponCode = data.couponCode;
    } else {
      this.id = '';
      this.address = '';
      this.uniquePayment = false;
      this.creditCardNumber = '';
      this.cvv = 0;
      this.expirationDate = '';
      this.services = [];
      this.couponCode = '';
    }
  }
}

export default RequestService;
