import { translate } from "locales";
import { UserModel } from "models/user/User";
import { ServiceModel, ServiceResponseModel } from ".";

export type RequestTypes = 'WAITING_TO_BE_ASSIGNED' | 'SCHEDULED' | 'ON_DELIVERY' | 'DELIVERED' | 'CANCELLED';
export type RequestTypesEnum = {
  WAITING_TO_BE_ASSIGNED?: string;
  SCHEDULED?: string;
  ON_DELIVERY?: string;
  DELIVERED?: string;
  CANCELLED?: string;
}
export interface RequestType {
  type: RequestTypes;
  name: string;
}
export const RequestTypesNames: RequestTypesEnum = {
  WAITING_TO_BE_ASSIGNED: translate('REQUEST_TYPES.WAITING_TO_BE_ASSIGNED'),
  SCHEDULED: translate('REQUEST_TYPES.SCHEDULED'),
  ON_DELIVERY: translate('REQUEST_TYPES.ON_DELIVERY'),
  DELIVERED: translate('REQUEST_TYPES.DELIVERED'),
  CANCELLED: translate('REQUEST_TYPES.CANCELLED')
}

export const RequestTypesNamesArray: RequestType[] = [
  {
    type: 'WAITING_TO_BE_ASSIGNED',
    name: translate('REQUEST_TYPES.WAITING_TO_BE_ASSIGNED')
  },
  {
    type: 'SCHEDULED',
    name: translate('REQUEST_TYPES.SCHEDULED')
  },
  {
    type: 'ON_DELIVERY',
    name: translate('REQUEST_TYPES.ON_DELIVERY')
  },
  {
    type: 'DELIVERED',
    name: translate('REQUEST_TYPES.DELIVERED')
  },
  {
    type: 'CANCELLED',
    name: translate('REQUEST_TYPES.CANCELLED')
  }
];

export interface RequestModel {
  status: RequestTypes;
  createdBy: UserModel;
  address: RequestAddressModel;
  services?: ServiceResponseModel[];
  id: string;
}

export interface RequestAddressModel {
  additionalInstructions: string;
  address: string;
  city: string;
  coordinates: {
    coordinates: string[];
    type: string;
  };
  fullZipCode: string;
  id: string;
}
