export const ADDRESS_NAME = 'Address';


export interface AddressModel {
  id?: string | number[];
  fullZipCode?: string;
  address?: string;
  city?: string;
  additionalInstructions?: string;
  selected?: boolean;
  coordinates?: string;
}

export interface AddressResponseServer {
  id?: string;
  user?: {
    id: string;
    enabled: boolean;
  },
  address?: string;
  fullZipCode?: string;
  coordinates?: Coordinate,
  additionalInstructions?: string;
  city?: string;
  createdAt?: string;
}

export interface Coordinate {
  type: string;
  coordinates: string[]
}

export const ADDRESS_MODEL: AddressModel = {
  id: '',
  fullZipCode: '',
  address: '',
  city: '',
  additionalInstructions: '',
  selected: false,
  coordinates: ''
};

export const AddressSchema: Realm.ObjectSchema = {
  name: ADDRESS_NAME,
  primaryKey: 'id',
  properties: {
    id: 'string',
    fullZipCode: 'string',
    address: 'string',
    city: 'string',
    coordinates: 'string',
    additionalInstructions: { type: 'string', optional: true },
    selected: { type: 'bool', optional: true }
  }
};

class Address implements AddressModel {
  public static schema: Realm.ObjectSchema = AddressSchema;

  id?: string | number[];
  fullZipCode?: string;
  address?: string;
  city?: string;
  additionalInstructions?: string;
  selected?: boolean;
  coordinates?: string;

  constructor(data: AddressModel) {
    if (data) {
      this.id = data.id;
      this.fullZipCode = data.fullZipCode;
      this.address = data.address;
      this.city = data.city;
      this.additionalInstructions = data.additionalInstructions;
      this.selected = data.selected;
      this.coordinates = data.coordinates;
    } else {
      this.id = '';
      this.fullZipCode = '';
      this.address = '';
      this.city = '';
      this.additionalInstructions = '';
      this.selected = false;
      this.coordinates = '';
    }
  }

  static formatData(data: AddressResponseServer): AddressModel {
    const formattedData: AddressModel = {
      id: data.id,
      address: data.address,
      fullZipCode: data.fullZipCode,
      additionalInstructions: data.additionalInstructions,
      city: data.city,
      coordinates: `${data.coordinates?.coordinates[0]}, ${data.coordinates?.coordinates[1]}`
    }
    return formattedData;
  }

}

export default Address;