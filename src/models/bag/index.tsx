import { ASSETS } from 'configs/Constants';
import { translate } from 'locales';
import { SERVICE_CATEGORY } from 'models/enums/ServiceCategory';

export type bagTypes = 'SMALL' | 'MEDIUM' | 'BIG';

export type typeBags = {
  type: bagTypes | typeBag;
  name: string;
  quantity: number;
  price?: number;
  service?: SERVICE_CATEGORY;
};

export interface typeBag {
  price?: {
    value: number;
  };
  value: bagTypes;
}

export const BAGS: typeBags[] = [
  {
    name: translate('SELECT_BAG_QUANTITY.BAGS.SMALL'),
    type: 'SMALL',
    quantity: 0,
    price: 10,
    service: 'FOLDING'
  },
  {
    name: translate('SELECT_BAG_QUANTITY.BAGS.MEDIUM'),
    type: 'MEDIUM',
    quantity: 0,
    price: 20,
    service: 'FOLDING'
  },
  {
    name: translate('SELECT_BAG_QUANTITY.BAGS.LARGE'),
    type: 'BIG',
    quantity: 0,
    price: 30,
    service: 'FOLDING'
  },
  {
    name: translate('SELECT_BAG_QUANTITY.BAGS.SMALL'),
    type: 'SMALL',
    quantity: 0,
    price: 22.66,
    service: 'WASH_AND_FOLD'
  },
  {
    name: translate('SELECT_BAG_QUANTITY.BAGS.MEDIUM'),
    type: 'MEDIUM',
    quantity: 0,
    price: 40,
    service: 'WASH_AND_FOLD'
  },
  {
    name: translate('SELECT_BAG_QUANTITY.BAGS.LARGE'),
    type: 'BIG',
    quantity: 0,
    price: 59.5,
    service: 'WASH_AND_FOLD'
  }
];

export const BagsValues: { SMALL: string; MEDIUM: string; BIG: string } = {
  SMALL: '3 lbs',
  MEDIUM: '7 lbs',
  BIG: '15 lbs'
};
