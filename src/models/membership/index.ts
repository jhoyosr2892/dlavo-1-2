import { translate } from "locales";

export type typeMembership = 'WASH' | 'FOLD';

export const TYPES_MEMBERSHIPS = {
  WASH: translate('MEMBERSHIP_SCENE.TYPES.WASH'),
  FOLD: translate('MEMBERSHIP_SCENE.TYPES.FOLD')
};

export interface MembershipModel {
  id: string;
  price: number;
  type: typeMembership;
  tokens: number;
  monthDuration: number;
  createdAt: Date;
};

export interface ResponseMembershipModel {
  id: string;
  price: number;
  months: number;
  bags: number;
  createdAt: Date;
};

export class Membership {
  static formatData(membershipResponse: ResponseMembershipModel): MembershipModel {
    return {
      createdAt: membershipResponse.createdAt,
      id: membershipResponse.id,
      monthDuration: membershipResponse.months,
      price: membershipResponse.price,
      tokens: membershipResponse.bags,
      type: 'WASH'
    };
  };
}

export interface BuyProducts {
  creditCardNumber: string,
  productId: string,
  cvv: number,
  expirationDate: string
};
