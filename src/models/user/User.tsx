/* eslint-disable camelcase */
// Helpers
//import { CLog } from '@helpers/Message';

// Models

// Constants
//const URL_PATH = 'src/models/user/User.ts';

export interface UserModel {
  id?: number;
  firstName?: string | null;
  lastName?: string | null;
  fullName?: string | null;
  email?: string | null;
  username?: string | null;
  phone?: string | null;
  couponCode?: string | null;
}

export interface UserResponseServer {
  id: number;
  firstName?: string | null;
  lastName?: string | null;
  email?: string | null;
  username?: string | null;
  couponCode?: string | null;
  phone?: string | null;
}

export const USER_MODEL: UserModel = {
  id: 0,
  username: null,
  firstName: null,
  lastName: null,
  fullName: null,
  email: null
};

class User {
  data: UserModel;

  constructor(data: UserModel) {
    this.data = data;
  }

  /**
   * @method formatData
   * @description formatea la data que llega del servidor a la que se maneja en
   * la APP.
   * @param {UserResponseServer} data
   * @return {UserModel}
   */
  static formatData(data: UserResponseServer): UserModel {
    //CLog(URL_PATH, 'formatData()', 'data', data);
    const formattedData: UserModel = {
      id: data.id,
      firstName: data.firstName,
      lastName: data.lastName,
      fullName: `${data.firstName} ${data.lastName}`,
      email: data.email,
      username: data.username,
      couponCode: data.couponCode
    };
    return formattedData;
  }
}

export default User;
