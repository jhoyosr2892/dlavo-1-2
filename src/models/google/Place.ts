/* eslint-disable camelcase */
import { StatusCodeGoogle } from 'configs/Enum';
// import { AddressLocation } from 'configs/Interfaces';

// import Address from '@models/rental/Address';

export interface PlaceGoogleModel {
  location: {
    latitude: number;
    longitude: number;
  };
  formattedAddress: string;
  name: string;
  placeId: string;
  address_components?: addressComponent[];
  //icon: string;
}

export interface addressComponent {
  long_name: string;
  short_name: string;
  types: string[];
}

export const typesAddressComponent = {
  street_number: 'street_number',
  route: 'route',
  neighborhood: 'neighborhood',
  political: 'political',
  sublocality: 'sublocality',
  administrative_area_level_2: 'administrative_area_level_2',
  administrative_area_level_1: 'administrative_area_level_1',
  country: 'country',
  postal_code: 'postal_code'
};

export interface PlaceDetailResponseServer {
  result: {
    formatted_address: string;
    geometry: {
      location: {
        lat: number;
        lng: number;
      };
    };
    //icon: string;
    name: string;
    place_id: string;
  };
  status: StatusCodeGoogle;
}

export interface PlaceGeocodeResponseServer {
  formatted_address: string;
  geometry: {
    location: {
      lat: number;
      lng: number;
    };
  };
  place_id: string;
  address_components?: addressComponent[];
}

class PlaceGoogle {
  data: PlaceGoogleModel;

  constructor(data: PlaceGoogleModel) {
    this.data = data;
  }

  static formatedData(data: PlaceDetailResponseServer): PlaceGoogleModel {
    const dataFormated: PlaceGoogleModel = {
      location: {
        latitude: data.result.geometry.location.lat,
        longitude: data.result.geometry.location.lng
      },
      formattedAddress: data.result.formatted_address,
      name: data.result.name,
      placeId: data.result.place_id
      //icon: data.result.icon
    };
    return dataFormated;
  }

  static formatedDataGeocode(data: PlaceGeocodeResponseServer): PlaceGoogleModel {
    const dataFormated: PlaceGoogleModel = {
      location: {
        latitude: data.geometry.location.lat,
        longitude: data.geometry.location.lng
      },
      formattedAddress: data.formatted_address,
      placeId: data.place_id,
      name: data.formatted_address.split(',')[0],
      address_components: data.address_components
    }
    return dataFormated;
  }

  // convertAddressLocation() {
  //   const { name, formattedAddress, location, placeId } = this.data;

  //   const convert: AddressLocation = {
  //     name,
  //     description: formattedAddress,
  //     point: location,
  //     placeId
  //   };

  //   return convert;
  // }

  // convertAddress(): Address | undefined {
  //   try {
  //     return new Address(Address.convertAddressLocationToAddress(this.convertAddressLocation()));
  //   } catch (e) {
  //     return undefined;
  //   }
  // }
}

export default PlaceGoogle;
