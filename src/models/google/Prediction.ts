/* eslint-disable camelcase */
export interface PredictionModel {
  description: string;
  placeId?: string;
  /** nombre o direccion acortado **/
  mainText?: string;
}

export interface PredictionResponseServer {
  description: string;
  place_id?: string;
  structured_formatting?: {
    main_text: string;
  };
}

class Prediction {
  data: PredictionModel;

  constructor(data: PredictionModel) {
    this.data = data;
  }

  static formatedData(data: PredictionResponseServer): PredictionModel {
    const dataFormated: PredictionModel = {
      description: data.description,
      placeId: data.place_id,
      mainText: data.structured_formatting?.main_text
    };
    return dataFormated;
  }
}

export default Prediction;
