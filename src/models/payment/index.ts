export const PAYMENT_NAME = 'PaymentMethod';

export interface PaymentMethodModel {
  id?: string | number[];
  cardNumber?: string;
  cardCvs?: string;
  cardExpirationDate?: string;
  cardZipCode?: string;
  selected?: boolean;
  name?: string;
};
export interface ResponsePaymentMethodModel {
  creditCardNumber: string;
  cvv: number;
  expirationDate: string;
  name?: string;
};

export const PAYMENT_METHOD_MODEL: PaymentMethodModel = {
  id: '',
  cardNumber: '',
  cardCvs: '',
  cardExpirationDate: '',
  cardZipCode: '',
  selected: false,
  name: ''
};


export const PaymentMethodSchema: Realm.ObjectSchema = {
  name: PAYMENT_NAME,
  primaryKey: 'id',
  properties: {
    id: 'string',
    cardNumber: { type: 'string', optional: true },
    cardCvs: { type: 'string', optional: true },
    cardExpirationDate: { type: 'string', optional: true },
    cardZipCode: { type: 'string', optional: true },
    selected: { type: 'bool', optional: true }
  }
};

class PaymentMethod implements PaymentMethodModel {
  public static schema: Realm.ObjectSchema = PaymentMethodSchema;

  id?: string | number[];
  cardNumber?: string;
  cardCvs?: string;
  cardExpirationDate?: string;
  cardZipCode?: string;
  selected?: boolean;

  constructor(data: PaymentMethodModel) {
    if (data) {
      this.id = data.id;
      this.cardNumber = data.cardNumber;
      this.cardCvs = data.cardCvs;
      this.cardExpirationDate = data.cardExpirationDate;
      this.cardZipCode = data.cardZipCode;
      this.selected = data.selected;
    } else {
      this.id = '';
      this.cardNumber = '';
      this.cardCvs = '';
      this.cardExpirationDate = '';
      this.cardZipCode = '';
      this.selected = false;
    }
  }
}

export default PaymentMethod;