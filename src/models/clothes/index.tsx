import { ASSETS } from 'configs/Constants';
import { translate } from 'locales';
import { SERVICE_CATEGORY } from 'models/enums/ServiceCategory';

export type typeClothes = {
  name: string;
  image?: string;
  quantity: number;
  type: typesClothes;
  service: SERVICE_CATEGORY[];
  price?: number;
};
export type typesClothes =
  | 'ALL_SHIRTS'
  | 'POLO'
  | 'JACKETS'
  | 'PANTS'
  | 'CASUAL_DRESS'
  | 'BED_OR_BLANKENT'
  | 'PILLOW_CASE'
  | 'SCARF'
  | 'CURTAIN'
  | 'DUVET_COVER'
  | 'MATRESS_COVER'
  | 'BED_SHEETS'
  | 'NAPKIN'
  | 'BATH_MAT'
  | 'TOWELS'
  | 'OTHER';

export const ClothesValues = {
  ALL_SHIRTS: translate('CLOTHES.SHIRTS'),
  POLO: translate('CLOTHES.T_SHIRT'),
  JACKETS: translate('CLOTHES.JACKETS'),
  PANTS: translate('CLOTHES.PANTS'),
  CASUAL_DRESS: translate('CLOTHES.DRESS'),
  BED_OR_BLANKENT: translate('CLOTHES.BED_BLANKET'),
  PILLOW_CASE: translate('CLOTHES.PILLOW'),
  SCARF: translate('CLOTHES.SCARF'),
  CURTAIN: translate('CLOTHES.CURTAIN'),
  DUVET_COVER: translate('CLOTHES.DUVET'),
  MATRESS_COVER: translate('CLOTHES.COVERS'),
  BED_SHEETS: translate('CLOTHES.BED_SHEETS'),
  NAPKIN: translate('CLOTHES.NAPKIN'),
  BATH_MAT: translate('CLOTHES.BATH_MAT'),
  TOWELS: translate('CLOTHES.TOWELS'),
  OTHER: translate('CLOTHES.OTHER'),
};
export const ClothesPrices = {
  ALL_SHIRTS: 3.25,
  POLO: 3.25,
  JACKETS: 10.65,
  PANTS: 8.25,
  CASUAL_DRESS: 15.95,
  BED_OR_BLANKENT: 14.75,
  PILLOW_CASE: 2,
  SCARF: 5.25,
  CURTAIN: 1,
  DUVET_COVER: 29.85,
  MATRESS_COVER: 16,
  BED_SHEETS: 14.75,
  NAPKIN: 1,
  BATH_MAT: 6.5,
  TOWELS: 1,
  OTHER: 0,
};

export const CLOTHES: typeClothes[] = [
  {
    name: translate('CLOTHES.T_SHIRT'),
    image: ASSETS.tShit,
    quantity: 0,
    type: 'POLO',
    service: ['DRY_CLEANING'],
    price: 3.25
  },
  {
    name: translate('CLOTHES.SHIRTS'),
    image: ASSETS.shits,
    quantity: 0,
    type: 'ALL_SHIRTS',
    service: ['DRY_CLEANING'],
    price: 3.25
  },
  {
    name: translate('CLOTHES.JACKETS'),
    image: ASSETS.jackets,
    quantity: 1,
    type: 'JACKETS',
    service: ['DRY_CLEANING'],
    price: 10.65
  },
  {
    name: translate('CLOTHES.PANTS'),
    image: ASSETS.pants,
    quantity: 0,
    type: 'PANTS',
    service: ['DRY_CLEANING'],
    price: 8.25
  },
  {
    name: translate('CLOTHES.DRESS'),
    image: ASSETS.dress,
    quantity: 0,
    type: 'CASUAL_DRESS',
    service: ['DRY_CLEANING'],
    price: 15.95
  },
  {
    name: translate('CLOTHES.BED_BLANKET'),
    image: ASSETS.blanketBed,
    quantity: 0,
    type: 'BED_OR_BLANKENT',
    service: ['BULKY_ITEMS'],
    price: 14.75
  },
  {
    name: translate('CLOTHES.PILLOW'),
    image: ASSETS.pillow,
    quantity: 0,
    type: 'PILLOW_CASE',
    service: ['BULKY_ITEMS'],
    price: 2
  },
  {
    name: translate('CLOTHES.SCARF'),
    image: ASSETS.scarf,
    quantity: 0,
    type: 'SCARF',
    service: [],
    price: 5.25
  },
  // {
  //   name: translate('CLOTHES.CURTAIN'),
  //   image: ASSETS.curtain,
  //   quantity: 0,
  //   type: 'CURTAIN',
  //   service: ['BULKY_ITEMS'],
  //   price: 1
  // },
  {
    name: translate('CLOTHES.DUVET'),
    image: ASSETS.duvet,
    quantity: 0,
    type: 'DUVET_COVER',
    service: ['BULKY_ITEMS'],
    price: 29.85
  },
  {
    name: translate('CLOTHES.COVERS'),
    image: ASSETS.covers,
    quantity: 0,
    type: 'MATRESS_COVER',
    service: [],
    price: 16
  },
  // {
  //   name: translate('CLOTHES.BED_SHEETS'),
  //   image: ASSETS.bedSheets,
  //   quantity: 0,
  //   type: 'BED_SHEETS',
  //   service: ['BULKY_ITEMS'],
  //   price: 14.75
  // },
  {
    name: translate('CLOTHES.NAPKIN'),
    image: ASSETS.napkin,
    quantity: 0,
    type: 'NAPKIN',
    service: ['BULKY_ITEMS'],
    price: 1
  },
  {
    name: translate('CLOTHES.BATH_MAT'),
    image: ASSETS.bathMat,
    quantity: 0,
    type: 'BATH_MAT',
    service: ['BULKY_ITEMS'],
    price: 6.5
  }
  // {
  //   name: translate('CLOTHES.TOWELS'),
  //   image: ASSETS.towels,
  //   quantity: 0,
  //   type: 'TOWELS',
  //   service: ['BULKY_ITEMS'],
  //   price: 1
  // }
];
