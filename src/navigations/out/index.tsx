// Libs
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

// Scenes
import AuthScene from '@scenes/auth';
import RegisterEmailScene from '@scenes/auth/RegisterEmail';
import EnterCodeScene from '@scenes/auth/EnterCode';
import EnterNameScene from '@scenes/auth/EnterName';
import LoginScene from 'scenes/auth/Login';
import PrivacyScene from 'scenes/profile/privacy';

// Constants
import { NAME_ROUTE } from '@config/Constants';
import { optionsScreens } from 'utils/helpers/Navigation';
import { translate } from 'locales';

const Stack = createStackNavigator();

const StackOut = () => {
  return (
    <Stack.Navigator>
      {/* Auth */}
      <Stack.Screen options={{ headerShown: false }} name={NAME_ROUTE.auth} component={AuthScene} />
      {/* Login */}
      <Stack.Screen
        options={() => optionsScreens({ params: { title: translate('LOGIN_SCENE.HEADER') } })}
        name={NAME_ROUTE.login}
        component={LoginScene}
      />
      {/* Send email */}
      <Stack.Screen
        options={() => optionsScreens({ params: { title: translate('REGISTER_EMAIL_SCENE.HEADER') } })}
        name={NAME_ROUTE.registerEmail}
        component={RegisterEmailScene}
      />
      {/* Enter code */}
      <Stack.Screen
        options={() => optionsScreens({ params: { title: translate('ENTER_CODE_SCENE.HEADER') } })}
        name={NAME_ROUTE.enterCode}
        component={EnterCodeScene}
      />
      {/* Enter name */}
      <Stack.Screen
        options={() => optionsScreens({ params: { title: translate('ENTER_NAME_SCENE.HEADER') } })}
        name={NAME_ROUTE.enterName}
        component={EnterNameScene}
      />
      <Stack.Screen
        name={NAME_ROUTE.privacy}
        component={PrivacyScene}
        options={() => optionsScreens({ params: { title: translate('PRIVACY_SCENE.HEADER') } })}
      />
    </Stack.Navigator>
  );
};

export default StackOut;
