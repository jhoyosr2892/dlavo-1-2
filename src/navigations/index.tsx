// Libs
import React, { useContext, useEffect, useMemo, useReducer } from 'react';
import { BackHandler, Linking } from 'react-native';
import { NavigationContainer, LinkingOptions, PathConfigMap } from '@react-navigation/native';

// Context
import { AuthContext, RootContext } from '@config/ContextApp';

// Helpers
import { CLog } from '@helpers/Message';
import { navigationRef } from '@helpers/Navigation';

// Organisms
import SplashScreen from '@organisms/general/SplashScreen';

// StackNavigation
import StackIn from './in';
import StackOut from './out';

// Storages
import DeviceToken, { getTokenUserSession } from '@stores/Session';
import { getIdUserSession } from '@stores/UserState';

// Services
// import { requestUserPermission } from '@services/pushes';
import { onLogout, onLogin } from '@services/app/Auth';
// import { loadProfileSession } from '@services/app/User';

// Constants
import { EnumToast, URL_BASE } from '@constantsApp';
import { loadUserAddress } from 'services/app/Address';
import { getCurrentUser } from 'services/app/User';

const URL_PATH = 'src/navigations/index.tsx';

// Type
type StateSessionType = {
  isLoading: boolean;
  isSignout: boolean;
  userToken?: string | null;
};

enum ActionAuth {
  RESTORE_TOKEN = 'RESTORE_TOKEN',
  SIGN_IN = 'SIGN_IN',
  SIGN_OUT = 'SIGN_OUT'
}

type ActionType =
  | { type: ActionAuth.SIGN_IN; token?: string | null }
  | { type: ActionAuth.SIGN_OUT }
  | { type: ActionAuth.RESTORE_TOKEN; token?: string | null };

const STATE_SESSION: StateSessionType = {
  isLoading: true,
  isSignout: false,
  userToken: null
};

export const reducerState = (prevState: StateSessionType, action: ActionType): StateSessionType => {
  switch (action.type) {
    case ActionAuth.RESTORE_TOKEN:
      return {
        ...prevState,
        userToken: action.token,
        isLoading: false
      };
    case ActionAuth.SIGN_IN:
      return {
        ...prevState,
        isSignout: false,
        userToken: action.token
      };
    case ActionAuth.SIGN_OUT:
      return {
        ...prevState,
        isSignout: true,
        userToken: null
      };
    default:
      return prevState;
  }
};

const NavigationContainerApp = () => {
  const { showLoading, hideLoading, showToast, removeAlertSheet, isShowAlertSheet } = useContext(RootContext);
  const [stateSession, dispatch] = useReducer(reducerState, STATE_SESSION);

  useEffect(() => {
    const bootstrapAsync = async () => {
      let userToken: string | null | undefined;
      try {
        // solicita permisos para las notificaciones.
        // await requestUserPermission();
        // obtenemos el token de la session si esta activa.
        await getTokenUserSession();
        /** obtenemos el id del usuario de la session **/
        await getIdUserSession();
        if (DeviceToken.token) {
          // guardar en en los mobx los datos del usario de la session
          // y :cualquier otro relevante.
          const { message, status } = await getCurrentUser();
          if (!status) {
            showToast({ text: message, type: EnumToast.error });
          }
          await loadUserAddress();
          userToken = DeviceToken.token;
        } else {
          userToken = undefined;
        }
      } catch (e) {
        CLog(URL_PATH, 'bootstrapAsync()', 'e', e);
      }
      dispatch({ type: ActionAuth.RESTORE_TOKEN, token: userToken });
    };
    bootstrapAsync();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const authContext = useMemo(
    () => ({
      signIn: async () => {
        showLoading();
        // const { status, message } = await onLogin(email, password);
        // if (!status) {
        //   CLog(URL_PATH, 'onLogin()', 'message', message);
        //   showToast({ text: message || 'Error...', type: EnumToast.error });
        // }
        dispatch({ type: ActionAuth.SIGN_IN, token: DeviceToken.token });
        hideLoading();
        // CLog(URL_PATH, 'signIn()', 'data', data);
      },
      signOut: async () => {
        showLoading();
        CLog(URL_PATH, 'signOut()', 'data', '');
        const { message, status } = await onLogout();
        if (status) {
          dispatch({ type: ActionAuth.SIGN_OUT });
          showToast({ text: message, type: EnumToast.success });
        } else {
          showToast({ text: message, type: EnumToast.error });
        }
        hideLoading();
      }
    }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  if (stateSession.isLoading) {
    return <SplashScreen />;
  }

  return (
    <AuthContext.Provider
      value={{
        authState: stateSession,
        authFunction: authContext
      }}>
      <NavigationContainer ref={navigationRef}>{stateSession.userToken ? <StackIn /> : <StackOut />}</NavigationContainer>
    </AuthContext.Provider>
  );
};

export default NavigationContainerApp;
