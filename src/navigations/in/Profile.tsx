// Libs
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

/* Locales */
import { translate } from 'locales';
// Constants
import { NAME_ROUTE } from '@config/Constants';
/* Utils */
import { optionsScreens } from 'utils/helpers/Navigation';

// Scenes
import ProfileScene from 'scenes/profile';
import ProfileDetailScene from 'scenes/profile/ProfileDetail';

const Stack = createStackNavigator();

const StackProfile = () => {
  return (
    <Stack.Navigator screenOptions={({ route }) => optionsScreens({ route })}>
      {/* Profile */}
      <Stack.Screen name={NAME_ROUTE.profile} component={ProfileScene} options={() => optionsScreens({ params: { hideHeader: true } })} />
      {/* Profile detail */}
      <Stack.Screen
        name={NAME_ROUTE.profileDetail}
        component={ProfileDetailScene}
        options={() => optionsScreens({ params: { title: translate('PROFILE_SCENE.PROFILE') } })}
      />
    </Stack.Navigator>
  );
};

export default StackProfile;
