import * as React from 'react';
import { Route, Text, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

/* Components */
import SvgCss from '@atoms/svgs/SvgCss';

/* Config */
import { ASSETS, NAME_ROUTE, SIZE_ICONS, THEME_DEFAULT } from 'configs/Constants';
import { translate } from 'locales';
const Tab = createBottomTabNavigator();

/* Stacks */
import StackLaundry from './Laundry';
import StackMyService from './MyServices';
import StackProfile from './Profile';
import StackPrices from './Prices';

const getTabBarIcon = (route: Route, tintColor: string) => {
  let routeName = route.name;
  let icon = <></>;

  if (routeName === NAME_ROUTE.laundryTab) {
    icon = <SvgCss xml={ASSETS.laundry} height={SIZE_ICONS.general} width={SIZE_ICONS.generalSmallContainer} fill={tintColor} />;
    routeName = translate('TAPS.LAUNDRY');
  } else if (routeName === NAME_ROUTE.myServicesTab) {
    icon = <SvgCss xml={ASSETS.myServices} height={SIZE_ICONS.general} width={SIZE_ICONS.generalSmallContainer} fill={tintColor} />;
    routeName = translate('TAPS.MY_SERVICES');
  } else if (routeName === NAME_ROUTE.pricingTab) {
    icon = <SvgCss xml={ASSETS.pricing} height={SIZE_ICONS.general} width={SIZE_ICONS.generalSmallContainer} fill={tintColor} />;
    routeName = translate('TAPS.PRICING');
  } else if (routeName === NAME_ROUTE.profileTab) {
    icon = <SvgCss xml={ASSETS.profile} height={SIZE_ICONS.general} width={SIZE_ICONS.generalSmallContainer} fill={tintColor} />;
    routeName = translate('TAPS.PROFILE');
  }
  return (
    <View style={{ backgroundColor: 'transparent', width: 60, height: 55, alignItems: 'center', justifyContent: 'center' }}>
      {icon}
      <Text style={{ fontSize: 10, color: tintColor }}>{routeName}</Text>
    </View>
  );
};

export default function Tabs() {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color }) => getTabBarIcon(route, color),
        tabBarActiveTintColor: THEME_DEFAULT.secondary,
        tabBarInactiveTintColor: THEME_DEFAULT.white,
        tabBarShowLabel: false,
        tabBarStyle: { backgroundColor: THEME_DEFAULT.primary },
        tabBarHideOnKeyboard: true
      })}
      initialRouteName={NAME_ROUTE.laundryTab}>
      <Tab.Screen name={NAME_ROUTE.laundryTab} component={StackLaundry} options={{ headerShown: false }} />
      <Tab.Screen name={NAME_ROUTE.myServicesTab} component={StackMyService} options={{ headerShown: false }} />
      <Tab.Screen name={NAME_ROUTE.pricingTab} component={StackPrices} options={{ headerShown: false }} />
      <Tab.Screen name={NAME_ROUTE.profileTab} component={StackProfile} options={{ headerShown: false }} />
    </Tab.Navigator>
  );
}
