// Libs
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

// Scenes
import MyServicesScene from 'scenes/my-services';
import MyServiceDetailScene from 'scenes/my-services/MyServiceDetail';

// Constants
import { NAME_ROUTE, THEME_DEFAULT } from '@config/Constants';
import { optionsScreens } from 'utils/helpers/Navigation';
import { translate } from 'locales';
import { Text } from 'react-native-animatable';
import { RequestTypesNames } from 'models/service/Request';

const Stack = createStackNavigator();

const StackMyService = () => {
  return (
    <Stack.Navigator screenOptions={({ route }) => optionsScreens({ route })}>
      {/* My services */}
      <Stack.Screen
        name={NAME_ROUTE.myServices}
        component={MyServicesScene}
        options={() => optionsScreens({ params: { title: translate('TAPS.MY_SERVICES') } })}
      />
      {/* My service detail */}
      <Stack.Screen
        name={NAME_ROUTE.myServiceDetail}
        component={MyServiceDetailScene}
        options={({ route }) => {
          return {
            title: translate('MY_SERVICE_DETAIL_SCENE.HEADER'),
            headerRight: () => (
              <Text style={{ color: THEME_DEFAULT.white, marginRight: 19, fontSize: 11, textAlign: 'center' }}>
                {RequestTypesNames[(route.params as any).request.status]}
              </Text>
            )
          };
        }}
      />
    </Stack.Navigator>
  );
};

export default StackMyService;
