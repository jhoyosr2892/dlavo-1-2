// Libs
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

// Scenes
import PricingScene from 'scenes/pricing';

// Constants
import { NAME_ROUTE } from '@config/Constants';
import { optionsScreens } from 'utils/helpers/Navigation';
import { translate } from 'locales';

const Stack = createStackNavigator();

const StackPrices = () => {
  return (
    <Stack.Navigator screenOptions={({ route }) => optionsScreens({ route })}>
      {/* Memberships */}
      <Stack.Screen
        name={NAME_ROUTE.pricing}
        component={PricingScene}
        options={() => optionsScreens({ params: { title: translate('MEMBERSHIP_SCENE.HEADER') } })}
      />
    </Stack.Navigator>
  );
};

export default StackPrices;
