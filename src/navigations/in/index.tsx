// Libs
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';

// Tabs
import Tabs from '@navigations/in/Tabs';

// Helpers
import { optionsScreens } from '@helpers/Navigation';

/* Components */
import CloseHeaderAtom from 'components/atoms/headers/CloseHeader';

/* Locales */
import { translate } from 'locales';

// Constants
import { NAME_ROUTE } from '@config/Constants';

// Scenes
import HomeScene from '@scenes/home';
import AddressScene from 'scenes/address';
import EnterAddressScene from 'scenes/address/EnterAddress';
import DetailMembershipScene from 'scenes/membership/DetailMembership';
import PaymentMethodsScene from 'scenes/payment-methods';
import AddPaymentMethodScene from 'scenes/payment-methods/AddPaymentMethod';
import PricingScene from 'scenes/pricing';
import AboutUsScene from 'scenes/profile/about-us';
import CouponsScene from 'scenes/profile/coupons';
import FaqScene from 'scenes/profile/faq';
import HelpScene from 'scenes/profile/help';
import PrivacyScene from 'scenes/profile/privacy';
import ShareAndWinScene from 'scenes/profile/share-and-win';

const StackNavigator = createStackNavigator();

const StackIn = () => {
  return (
    <StackNavigator.Navigator initialRouteName={NAME_ROUTE.tabs} screenOptions={({ route }) => optionsScreens({ route })}>
      {/* Home */}
      <StackNavigator.Screen name={NAME_ROUTE.home} component={HomeScene} />

      {/* Tabs */}
      <StackNavigator.Screen name={NAME_ROUTE.tabs} component={Tabs} options={{ headerShown: false }} />

      {/* Address */}
      <StackNavigator.Group>
        <StackNavigator.Screen
          name={NAME_ROUTE.address}
          component={AddressScene}
          options={{ header: () => <CloseHeaderAtom title={translate('ADDRESS_SCENE.HEADER')} /> }}
        />
        <StackNavigator.Screen
          name={NAME_ROUTE.enterAddress}
          component={EnterAddressScene}
          options={() => optionsScreens({ params: { title: translate('ENTER_ADDRESS.HEADER') } })}
        />
      </StackNavigator.Group>

      {/* Payment */}
      <StackNavigator.Group>
        <StackNavigator.Screen
          name={NAME_ROUTE.payment}
          component={PaymentMethodsScene}
          options={() => optionsScreens({ params: { title: translate('PAYMENT_METHODS_SCENE.HEADER') } })}
        />
        <StackNavigator.Screen
          name={NAME_ROUTE.addPaymentMethod}
          component={AddPaymentMethodScene}
          options={() => optionsScreens({ params: { title: translate('ADD_PAYMENT_METHOD_SCENE.HEADER') } })}
        />
      </StackNavigator.Group>

      {/* Membership */}
      <StackNavigator.Group>
        <StackNavigator.Screen
          name={NAME_ROUTE.membership}
          component={PricingScene}
          options={() => optionsScreens({ params: { title: translate('MEMBERSHIP_SCENE.HEADER') } })}
        />
      </StackNavigator.Group>

      {/* Coupons */}
      <StackNavigator.Group>
        <StackNavigator.Screen
          name={NAME_ROUTE.coupons}
          component={CouponsScene}
          options={() => optionsScreens({ params: { title: translate('COUPONS_SCENE.HEADER') } })}
        />
      </StackNavigator.Group>

      {/* Share and win */}
      <StackNavigator.Group>
        <StackNavigator.Screen
          name={NAME_ROUTE.shareAndWin}
          component={ShareAndWinScene}
          options={() => optionsScreens({ params: { title: translate('SHARE_AND_WIN_SCENE.HEADER') } })}
        />
      </StackNavigator.Group>

      {/* Help */}
      <StackNavigator.Group>
        <StackNavigator.Screen
          name={NAME_ROUTE.help}
          component={HelpScene}
          options={() => optionsScreens({ params: { title: translate('HELP_SCENE.HEADER') } })}
        />
      </StackNavigator.Group>

      {/* About us */}
      <StackNavigator.Group>
        <StackNavigator.Screen
          name={NAME_ROUTE.about}
          component={AboutUsScene}
          options={() => optionsScreens({ params: { title: translate('ABOUT_US_SCENE.HEADER') } })}
        />
      </StackNavigator.Group>

      {/* Privacy */}
      <StackNavigator.Group>
        <StackNavigator.Screen
          name={NAME_ROUTE.privacy}
          component={PrivacyScene}
          options={() => optionsScreens({ params: { title: translate('PRIVACY_SCENE.HEADER') } })}
        />
      </StackNavigator.Group>

      {/* FAQ */}
      <StackNavigator.Group>
        <StackNavigator.Screen
          name={NAME_ROUTE.faq}
          component={FaqScene}
          options={() => optionsScreens({ params: { title: translate('FAQ_SCENE.HEADER') } })}
        />
      </StackNavigator.Group>

      {/* Membership detail */}
      <StackNavigator.Screen
        name={NAME_ROUTE.pricingDetail}
        component={DetailMembershipScene}
        options={() => optionsScreens({ params: { title: translate('MEMBERSHIP_SCENE.HEADER') } })}
      />
    </StackNavigator.Navigator>
  );
};

export default StackIn;
