// Libs
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

// Constants
import { NAME_ROUTE } from '@config/Constants';
import { SERVICE_CATEGORY, SERVICE_CATEGORY_NAMES } from 'models/enums/ServiceCategory';
import { translate } from 'locales';
import { optionsScreens } from 'utils/helpers/Navigation';

/* Components */
import CloseHeaderAtom from 'components/atoms/headers/CloseHeader';

// Scenes
import LaundryScene from '@scenes/laundry';
import SelectFrequencyScene from 'scenes/laundry/SelectFrequency';
import SelectClothesScene from 'scenes/laundry/SelectClothes';
import SelectServicesOrCaresScene from 'scenes/laundry/SelectServicesOrCares';
import BagScene from 'scenes/laundry/Bag';
import SelectBagQuantityScene from 'scenes/laundry/SelectBagQuantity';
import SelectScheduleScene from 'scenes/laundry/SelectSchedule';
import AdditionalNotesScene from 'scenes/laundry/AdditionalNotes';
import { ServiceModel } from 'models/service';

const Stack = createStackNavigator();

const StackLaundry = () => {
  return (
    <Stack.Navigator screenOptions={({ route }) => optionsScreens({ route })}>
      {/* Laundry */}
      <Stack.Screen name={NAME_ROUTE.laundry} component={LaundryScene} options={() => optionsScreens({ params: { hideHeader: true } })} />
      <Stack.Screen
        name={NAME_ROUTE.selectFrequency}
        component={SelectFrequencyScene}
        options={() => optionsScreens({ params: { title: translate('SELECT_FREQUENCY_SCENE.HEADER') } })}
      />
      <Stack.Screen
        name={NAME_ROUTE.selectGarments}
        component={SelectClothesScene}
        options={({ route, navigation }) => {
          return optionsScreens({
            params: {
              title: route.params
                ? SERVICE_CATEGORY_NAMES[(route.params as { data: ServiceModel })?.data.serviceCategory as SERVICE_CATEGORY]
                : ''
            }
          });
        }}
      />
      <Stack.Screen
        name={NAME_ROUTE.serviceOrCaresSelect}
        component={SelectServicesOrCaresScene}
        options={({ route, navigation }) =>
          optionsScreens({
            params: { title: translate('SELECT_SPECIAL_SERVICES_OR_CARES.HEADER') }
          })
        }
      />
      <Stack.Screen
        name={NAME_ROUTE.selectBagQuantity}
        component={SelectBagQuantityScene}
        options={({ route, navigation }) =>
          optionsScreens({
            params: {
              title: translate('SELECT_BAG_QUANTITY.HEADER')
            }
          })
        }
      />
      <Stack.Screen
        name={NAME_ROUTE.selectSchedule}
        component={SelectScheduleScene}
        options={({ route, navigation }) =>
          optionsScreens({
            params: {
              title: translate('SELECT_SCHEDULE.HEADER')
            }
          })
        }
      />
      <Stack.Screen
        name={NAME_ROUTE.additionalNotes}
        component={AdditionalNotesScene}
        options={({ route, navigation }) =>
          optionsScreens({
            params: {
              title: translate('ADDITIONAL_NOTES.HEADER')
            }
          })
        }
      />
      <Stack.Group screenOptions={{ presentation: 'modal' }}>
        <Stack.Screen
          name={NAME_ROUTE.bag}
          component={BagScene}
          options={{ header: () => <CloseHeaderAtom title={translate('BAG_SCENE.HEADER')} backRoute={NAME_ROUTE.laundry} /> }}
        />
      </Stack.Group>
    </Stack.Navigator>
  );
};

export default StackLaundry;
